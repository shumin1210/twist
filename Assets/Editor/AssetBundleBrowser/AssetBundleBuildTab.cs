using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEditor;
using UnityEngine.AssetBundles.AssetBundleDataSource;

namespace UnityEngine.AssetBundles {

    [System.Serializable]
    public class AssetBundleBuildTab {
        private const string k_BuildPrefPrefix = "ABBBuild:";

        // gui vars
        //[SerializeField]
        //private ValidBuildTarget m_BuildTarget = ValidBuildTarget.StandaloneWindows;
        //[SerializeField]
        //private CompressOptions m_Compression = CompressOptions.StandardCompression;
        //private string m_OutputPath = string.Empty;
        //[SerializeField]
        //private bool m_UseDefaultPath = true;
        private string m_streamingPath = "Assets/StreamingAssets";

        [SerializeField]
        private bool m_AdvancedSettings;

        private static string m_extensionName;
        private static string m_gameID;

        [SerializeField]
        private Vector2 m_ScrollPosition;

        private class ToggleData {

            public ToggleData(bool s,
                string title,
                string tooltip,
                List<string> onToggles,
                BuildAssetBundleOptions opt = BuildAssetBundleOptions.None) {
                if (onToggles.Contains(title)) {
                    state = true;
                } else {
                    state = s;
                }

                content = new GUIContent(title, tooltip);
                option = opt;
            }

            //public string prefsKey
            //{ get { return k_BuildPrefPrefix + content.text; } }
            public bool state;

            public GUIContent content;
            public BuildAssetBundleOptions option;
        }

        [SerializeField]
        private BuildTabData m_UserData;

        private List<ToggleData> m_ToggleData;
        private ToggleData m_ForceRebuild;
        private ToggleData m_CopyToStreaming;
        private GUIContent m_TargetContent;
        private GUIContent m_CompressionContent;

        public enum CompressOptions {
            Uncompressed = 0,
            StandardCompression,
            ChunkBasedCompression,
        }

        private GUIContent[] m_CompressionOptions =
        {
            new GUIContent("No Compression"),
            new GUIContent("Standard Compression (LZMA)"),
            new GUIContent("Chunk Based Compression (LZ4)")
        };

        private int[] m_CompressionValues = { 0, 1, 2 };

        public AssetBundleBuildTab() {
            m_AdvancedSettings = false;
            m_UserData = new BuildTabData();
            m_UserData.m_OnToggles = new List<string>();
            m_UserData.m_UseDefaultPath = true;
        }

        public void OnDisable() {
            string dataPath = System.IO.Path.GetFullPath(".");
            dataPath = dataPath.Replace("\\", "/");
            dataPath += "/Library/AssetBundleBrowserBuild.dat";

            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(dataPath);

            bf.Serialize(file, m_UserData);
            file.Close();
        }

        public void OnEnable(Rect pos, EditorWindow parent) {
            //LoadData...
            string dataPath = System.IO.Path.GetFullPath(".");
            dataPath = dataPath.Replace("\\", "/");
            dataPath += "/Library/AssetBundleBrowserBuild.dat";

            if (File.Exists(dataPath)) {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(dataPath, FileMode.Open);
                BuildTabData data = bf.Deserialize(file) as BuildTabData;
                if (data != null) {
                    m_UserData = data;
                }

                file.Close();
            }

            m_ToggleData = new List<ToggleData>();
            m_ToggleData.Add(new ToggleData(
                false,
                "Exclude Type Information",
                "Do not include type information within the asset bundle (don't write type tree).",
                m_UserData.m_OnToggles,
                BuildAssetBundleOptions.DisableWriteTypeTree));
            m_ToggleData.Add(new ToggleData(
                false,
                "Force Rebuild",
                "Force rebuild the asset bundles",
                m_UserData.m_OnToggles,
                BuildAssetBundleOptions.ForceRebuildAssetBundle));
            m_ToggleData.Add(new ToggleData(
                false,
                "Ignore Type Tree Changes",
                "Ignore the type tree changes when doing the incremental build check.",
                m_UserData.m_OnToggles,
                BuildAssetBundleOptions.IgnoreTypeTreeChanges));
            m_ToggleData.Add(new ToggleData(
                false,
                "Append Hash",
                "Append the hash to the assetBundle name.",
                m_UserData.m_OnToggles,
                BuildAssetBundleOptions.AppendHashToAssetBundleName));
            m_ToggleData.Add(new ToggleData(
                false,
                "Strict Mode",
                "Do not allow the build to succeed if any errors are reporting during it.",
                m_UserData.m_OnToggles,
                BuildAssetBundleOptions.StrictMode));
            m_ToggleData.Add(new ToggleData(
                false,
                "Dry Run Build",
                "Do a dry run build.",
                m_UserData.m_OnToggles,
                BuildAssetBundleOptions.DryRunBuild));

            m_ForceRebuild = new ToggleData(
                false,
                "Clear Folders",
                "Will wipe out all contents of build directory as well as StreamingAssets/AssetBundles if you are choosing to copy build there.",
                m_UserData.m_OnToggles);
            m_CopyToStreaming = new ToggleData(
                false,
                "Copy to StreamingAssets",
                "After build completes, will copy all build content to " + m_streamingPath + " for use in stand-alone player.",
                m_UserData.m_OnToggles);

            m_TargetContent = new GUIContent("Build Target", "Choose target platform to build for.");
            m_CompressionContent = new GUIContent("Compression", "Choose no compress, standard (LZMA), or chunk based (LZ4)");

            if (m_UserData.m_UseDefaultPath) {
                ResetPathToDefault();
            }
        }

        public void OnGUI(Rect pos) {
            m_ScrollPosition = EditorGUILayout.BeginScrollView(m_ScrollPosition);
            bool newState = false;
            GUIStyle centeredStyle = GUI.skin.GetStyle("Label");
            centeredStyle.alignment = TextAnchor.UpperCenter;
            GUILayout.Label(new GUIContent("Example build setup"), centeredStyle);
            //basic options
            EditorGUILayout.Space();
            GUILayout.BeginVertical();

            // build target
            using (new EditorGUI.DisabledScope(!AssetBundleModel.Model.DataSource.CanSpecifyBuildTarget)) {
                ValidBuildTarget tgt = (ValidBuildTarget)EditorGUILayout.EnumPopup(m_TargetContent, m_UserData.m_BuildTarget);
                if (tgt != m_UserData.m_BuildTarget) {
                    m_UserData.m_BuildTarget = tgt;
                    if (m_UserData.m_UseDefaultPath) {
                        m_UserData.m_OutputPath = "AssetBundles/";
                        m_UserData.m_OutputPath += m_UserData.m_BuildTarget.ToString();
                        //EditorUserBuildSettings.SetPlatformSettings(EditorUserBuildSettings.activeBuildTarget.ToString(), "AssetBundleOutputPath", m_OutputPath);
                    }
                }
            }

            ////output path
            using (new EditorGUI.DisabledScope(!AssetBundleModel.Model.DataSource.CanSpecifyBuildOutputDirectory)) {
                EditorGUILayout.Space();
                GUILayout.BeginHorizontal();
                string newExtension = EditorGUILayout.TextField("Extension", m_UserData.m_Extension);
                if (newExtension != m_UserData.m_Extension) {
                    m_UserData.m_Extension = newExtension;
                }
                m_extensionName = m_UserData.m_Extension;
                GUILayout.EndHorizontal();

                EditorGUILayout.Space();
                GUILayout.BeginHorizontal();
                m_UserData.m_GameID = m_gameID = EditorGUILayout.TextField("GameID", m_UserData.m_GameID);
                GUILayout.EndHorizontal();

                EditorGUILayout.Space();
                GUILayout.BeginHorizontal();
                string newPath = EditorGUILayout.TextField("Output Path", m_UserData.m_OutputPath);
                if ((newPath != m_UserData.m_OutputPath) &&
                     (newPath != string.Empty)) {
                    m_UserData.m_UseDefaultPath = false;
                    m_UserData.m_OutputPath = newPath;
                    //EditorUserBuildSettings.SetPlatformSettings(EditorUserBuildSettings.activeBuildTarget.ToString(), "AssetBundleOutputPath", m_OutputPath);
                }
                GUILayout.EndHorizontal();
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Browse", GUILayout.MaxWidth(75f))) {
                    BrowseForFolder();
                }

                if (GUILayout.Button("Reset", GUILayout.MaxWidth(75f))) {
                    ResetPathToDefault();
                }
                //if (string.IsNullOrEmpty(m_OutputPath))
                //    m_OutputPath = EditorUserBuildSettings.GetPlatformSettings(EditorUserBuildSettings.activeBuildTarget.ToString(), "AssetBundleOutputPath");
                GUILayout.EndHorizontal();
                EditorGUILayout.Space();

                newState = GUILayout.Toggle(
                    m_ForceRebuild.state,
                    m_ForceRebuild.content);
                if (newState != m_ForceRebuild.state) {
                    if (newState) {
                        m_UserData.m_OnToggles.Add(m_ForceRebuild.content.text);
                    } else {
                        m_UserData.m_OnToggles.Remove(m_ForceRebuild.content.text);
                    }

                    m_ForceRebuild.state = newState;
                }
                newState = GUILayout.Toggle(
                    m_CopyToStreaming.state,
                    m_CopyToStreaming.content);
                if (newState != m_CopyToStreaming.state) {
                    if (newState) {
                        m_UserData.m_OnToggles.Add(m_CopyToStreaming.content.text);
                    } else {
                        m_UserData.m_OnToggles.Remove(m_CopyToStreaming.content.text);
                    }

                    m_CopyToStreaming.state = newState;
                }
            }

            // advanced options
            using (new EditorGUI.DisabledScope(!AssetBundleModel.Model.DataSource.CanSpecifyBuildOptions)) {
                EditorGUILayout.Space();
                m_AdvancedSettings = EditorGUILayout.Foldout(m_AdvancedSettings, "Advanced Settings");
                if (m_AdvancedSettings) {
                    int indent = EditorGUI.indentLevel;
                    EditorGUI.indentLevel = 1;
                    CompressOptions cmp = (CompressOptions)EditorGUILayout.IntPopup(
                        m_CompressionContent,
                        (int)m_UserData.m_Compression,
                        m_CompressionOptions,
                        m_CompressionValues);

                    if (cmp != m_UserData.m_Compression) {
                        m_UserData.m_Compression = cmp;
                    }
                    foreach (ToggleData tog in m_ToggleData) {
                        newState = EditorGUILayout.ToggleLeft(
                            tog.content,
                            tog.state);
                        if (newState != tog.state) {
                            if (newState) {
                                m_UserData.m_OnToggles.Add(tog.content.text);
                            } else {
                                m_UserData.m_OnToggles.Remove(tog.content.text);
                            }

                            tog.state = newState;
                        }
                    }
                    EditorGUILayout.Space();
                    EditorGUI.indentLevel = indent;
                }
            }

            // build.
            EditorGUILayout.Space();
            if (GUILayout.Button("Build")) {
                EditorApplication.delayCall += ExecuteBuild;
            }
            GUILayout.EndVertical();
            EditorGUILayout.EndScrollView();
        }

        private void ExecuteBuild() {
            if (AssetBundleModel.Model.DataSource.CanSpecifyBuildOutputDirectory) {
                if (string.IsNullOrEmpty(m_UserData.m_OutputPath)) {
                    BrowseForFolder();
                }

                if (string.IsNullOrEmpty(m_UserData.m_OutputPath)) //in case they hit "cancel" on the open browser
                {
                    Debug.LogError("AssetBundle Build: No valid output path for build.");
                    return;
                }

                if (m_ForceRebuild.state) {
                    string message = "Do you want to delete all files in the directory " + m_UserData.m_OutputPath;
                    if (m_CopyToStreaming.state) {
                        message += " and " + m_streamingPath;
                    }

                    message += "?";
                    if (EditorUtility.DisplayDialog("File delete confirmation", message, "Yes", "No")) {
                        try {
                            if (Directory.Exists(m_UserData.m_OutputPath)) {
                                Directory.Delete(m_UserData.m_OutputPath, true);
                            }

                            if (m_CopyToStreaming.state) {
                                if (Directory.Exists(m_streamingPath)) {
                                    Directory.Delete(m_streamingPath, true);
                                }
                            }
                        } catch (System.Exception e) {
                            Debug.LogException(e);
                        }
                    }
                }
                if (!Directory.Exists(m_UserData.m_OutputPath)) {
                    Directory.CreateDirectory(m_UserData.m_OutputPath);
                }
            }

            BuildAssetBundleOptions opt = BuildAssetBundleOptions.None;

            if (AssetBundleModel.Model.DataSource.CanSpecifyBuildOptions) {
                if (m_UserData.m_Compression == CompressOptions.Uncompressed) {
                    opt |= BuildAssetBundleOptions.UncompressedAssetBundle;
                } else if (m_UserData.m_Compression == CompressOptions.ChunkBasedCompression) {
                    opt |= BuildAssetBundleOptions.ChunkBasedCompression;
                }

                foreach (ToggleData tog in m_ToggleData) {
                    if (tog.state) {
                        opt |= tog.option;
                    }
                }
            }

            ABBuildInfo buildInfo = new ABBuildInfo();

            buildInfo.outputDirectory = m_UserData.m_OutputPath;
            buildInfo.options = opt;
            buildInfo.buildTarget = (BuildTarget)m_UserData.m_BuildTarget;

            var manifest = AssetBundleModel.Model.DataSource.BuildAssetBundles(buildInfo);
            GenerateLoadConfigs(buildInfo.outputDirectory, manifest);

            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);

            if (m_CopyToStreaming.state) {
                DirectoryCopy(m_UserData.m_OutputPath, m_streamingPath);
            }
        }

        [Serializable]
        private class AssetBundleLoadConfigs {
            public string GameID;
            public string Platform;

            public AssetBundleLoadData[] AssetBundles;
        }

        [Serializable]
        private struct AssetBundleLoadData {
            public string Name;
            public string Hash;
            public uint CRC;

            public Hash128 Hash128 {
                get {
                    return Hash128.Parse(this.Hash);
                }

                set {
                    this.Hash = value.ToString();
                }
            }
        }

        private static string[] GetAssetBundleNamesInLoadOrder(AssetBundleManifest manifest) {
            var assetBundleRefs = new Dictionary<string, int>();
            foreach (var assetBundle in manifest.GetAllAssetBundles()) {
                assetBundleRefs.Add(assetBundle, 0);
            }

            foreach (var assetBundle in manifest.GetAllAssetBundles()) {
                foreach (var depAssetBundle in manifest.GetAllDependencies(assetBundle)) {
                    assetBundleRefs[depAssetBundle] += 1;
                }
            }

            return assetBundleRefs.OrderByDescending(kp => kp.Value).Select(v => v.Key).ToArray();
        }

        private static bool GetAssetBundleHashAndCRC(string outputPath, string assetBundleName, out Hash128 hash, out uint crc) {
            var assetBundlePath = Path.GetFullPath(outputPath) + Path.DirectorySeparatorChar + assetBundleName;
            if (!BuildPipeline.GetHashForAssetBundle(assetBundlePath, out hash)) {
                crc = 0;
                return false;
            }

            if (!BuildPipeline.GetCRCForAssetBundle(assetBundlePath, out crc)) {
                return false;
            }

            return true;
        }

        private AssetBundleLoadConfigs GenerateLoadConfigs(string manifestDir, AssetBundleManifest manifest, string configsName = "configs.json") {
            var assetBundleNames = GetAssetBundleNamesInLoadOrder(manifest);
            var assetBundleLoads = new List<AssetBundleLoadData>(assetBundleNames.Length);
            string path = Path.GetFullPath(manifestDir) + Path.DirectorySeparatorChar;
            foreach (var assetBundleName in assetBundleNames) {
                Hash128 hash;
                uint crc;
                if (!GetAssetBundleHashAndCRC(manifestDir, assetBundleName, out hash, out crc)) {
                    throw new Exception("Cannot find assetBundle - " + assetBundleName);
                }

                assetBundleLoads.Add(new AssetBundleLoadData() {
                    Name = assetBundleName,
                    Hash128 = hash,
                    CRC = crc,
                });
                if (File.Exists(path + assetBundleName)) {
                    if (File.Exists(path + assetBundleName + "." + m_extensionName)) {
                        File.Delete(path + assetBundleName + "." + m_extensionName);
                    }
                    File.Move(path + assetBundleName, path + assetBundleName + "." + m_extensionName);
                }
            }

            var configs = new AssetBundleLoadConfigs() {
                GameID = m_UserData.m_GameID,
                Platform = catagorizePlatform(m_UserData.m_BuildTarget),
                AssetBundles = assetBundleLoads.ToArray()
            };

            // ��X���J�]�w��
            var configsPath = Path.GetFullPath(manifestDir) + Path.DirectorySeparatorChar + configsName;
            File.WriteAllText(Path.GetFullPath(manifestDir) + Path.DirectorySeparatorChar + "version.json", JsonUtility.ToJson(new {
                version = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
            }));
            File.WriteAllText(configsPath, JsonUtility.ToJson(configs));

            return configs;
        }

        private static void DirectoryCopy(string sourceDirName, string destDirName) {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName)) {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files) {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            foreach (DirectoryInfo subdir in dirs) {
                string temppath = Path.Combine(destDirName, subdir.Name);
                DirectoryCopy(subdir.FullName, temppath);
            }
        }

        private void BrowseForFolder() {
            m_UserData.m_UseDefaultPath = false;
            string newPath = EditorUtility.OpenFolderPanel("Bundle Folder", m_UserData.m_OutputPath, string.Empty);
            if (!string.IsNullOrEmpty(newPath)) {
                string gamePath = System.IO.Path.GetFullPath(".");
                gamePath = gamePath.Replace("\\", "/");
                if (newPath.StartsWith(gamePath) && newPath.Length > gamePath.Length) {
                    newPath = newPath.Remove(0, gamePath.Length + 1);
                }

                m_UserData.m_OutputPath = newPath;
                //EditorUserBuildSettings.SetPlatformSettings(EditorUserBuildSettings.activeBuildTarget.ToString(), "AssetBundleOutputPath", m_OutputPath);
            }
        }

        private void ResetPathToDefault() {
            m_UserData.m_UseDefaultPath = true;
            m_UserData.m_OutputPath = "AssetBundles/";
            m_UserData.m_OutputPath += m_UserData.m_BuildTarget.ToString();
            //EditorUserBuildSettings.SetPlatformSettings(EditorUserBuildSettings.activeBuildTarget.ToString(), "AssetBundleOutputPath", m_OutputPath);
        }

        //Note: this is the provided BuildTarget enum with some entries removed as they are invalid in the dropdown
        public enum ValidBuildTarget {
            //NoTarget = -2,        --doesn't make sense
            //iPhone = -1,          --deprecated
            //BB10 = -1,            --deprecated
            //MetroPlayer = -1,     --deprecated

            //StandaloneOSXIntel = 4,
            //WebPlayer = 6,
            //WebPlayerStreamed = 7,
            //PS3 = 10,
            //XBOX360 = 11,
            //WSAPlayer = 21,
            //WP8Player = 26,
            //StandaloneOSXIntel64 = 27,
            //BlackBerry = 28,
            //Tizen = 29,
            //PSP2 = 30,
            //PS4 = 31,
            //PSM = 32,
            //XboxOne = 33,
            //SamsungTV = 34,
            //N3DS = 35,
            //WiiU = 36,
            //tvOS = 37,

            //Switch = 38

            WebGL = 20,

            StandaloneOSXUniversal = 2,
            StandaloneWindows = 5,
            StandaloneWindows64 = 19,
            StandaloneLinux = 17,
            StandaloneLinux64 = 24,
            StandaloneLinuxUniversal = 25,

            iOS = 9,
            Android = 13
        }

        private string catagorizePlatform(ValidBuildTarget buildTarget) {
            switch (buildTarget) {
                case ValidBuildTarget.WebGL:
                    return "webgl";

                case ValidBuildTarget.StandaloneOSXUniversal:
                case ValidBuildTarget.StandaloneWindows:
                case ValidBuildTarget.StandaloneWindows64:
                case ValidBuildTarget.StandaloneLinux:
                case ValidBuildTarget.StandaloneLinux64:
                case ValidBuildTarget.StandaloneLinuxUniversal:
                default:
                    return "windows";

                case ValidBuildTarget.iOS:
                    return "ios";

                case ValidBuildTarget.Android:
                    return "android";
            }
        }

        [System.Serializable]
        public class BuildTabData {
            public List<string> m_OnToggles;
            public ValidBuildTarget m_BuildTarget = ValidBuildTarget.StandaloneWindows;
            public CompressOptions m_Compression = CompressOptions.StandardCompression;
            public string m_OutputPath = string.Empty;
            public string m_Extension = "data";
            public string m_GameID = "lobby";
            public bool m_UseDefaultPath = true;
        }
    }
}