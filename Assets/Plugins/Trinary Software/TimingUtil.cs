﻿using System.Collections.Generic;
using UnityEngine;

namespace MEC {

    public static class TimingUtil {

        public static CoroutineHandle StartEmulateUpdate(System.Action func, MonoBehaviour scr) {
            return Timing.RunCoroutine(_EmulateUpdate(func, scr).CancelWith(scr.gameObject));
        }

        public static IEnumerator<float> _EmulateUpdate(System.Action func, MonoBehaviour scr) {
            yield return Timing.WaitForOneFrame;
            while (scr.gameObject != null) {
                if (scr.gameObject.activeInHierarchy && scr.enabled) {
                    func();
                }

                yield return Timing.WaitForOneFrame;
            }
        }    }
}