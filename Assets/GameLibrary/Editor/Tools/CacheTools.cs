﻿using UnityEditor;
using UnityEngine;

namespace GameLibrary.Utility {

    internal class CacheTools {

        [MenuItem("GameLibrary/Cache/Clear", isValidateFunction: true)]
        private static bool canClearCache() {
            return Caching.ready;
        }

        [MenuItem("GameLibrary/Cache/Clear")]
        private static void clearCache() {
            if (Caching.CleanCache()) {
                Debug.Log("Successfully cleaned the cache.");
            } else {
                Debug.Log("Cache is being used.");
            }
        }

        [MenuItem("GameLibrary/Cache/ClearAssetBundles")]
        private static void expireCache() {
            Caching.expirationDelay = 1;
            ;
        }
    }
}