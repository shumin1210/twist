﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace GameLibrary.Core {

    public class DefaultProjectSettings : MonoBehaviour {

        [MenuItem("GameLibrary/Setting/SetCameraProps")]
        private static void SetCameraDefault() {
            GameObject go = Selection.activeGameObject;
            if (go) {
                SetCameraProperties(go.GetComponent<Camera>());
            }
        }

        [MenuItem("GameLibrary/Setting/SetCanvasProps")]
        private static void SetDefaultCanvas() {
            GameObject go = Selection.activeGameObject;
            if (go) {
                SetCanvasProperties(go);
            }
        }

        [MenuItem("GameLibrary/Setting/SetCanvasProps", isValidateFunction: true)]
        private static bool IsDefaultCanvas() {
            return Selection.activeGameObject.GetComponent<Canvas>();
        }

        private static void SetCameraProperties(Camera camera) {
            if (camera) {
                camera.orthographic = true;
                camera.orthographicSize = 5;
                camera.depth = 2;
                camera.farClipPlane = 1000;
                camera.nearClipPlane = .3f;
                Debug.Log("Camera is modified.");
            } else {
                Debug.Log("Camera is null.");
            }
        }

        private static void SetCanvasProperties(GameObject go) {
            var canvas = go.GetComponent<Canvas>();
            var canvasScaler = go.GetComponent<CanvasScaler>();
            if (canvas && canvasScaler) {
                canvas.renderMode = RenderMode.ScreenSpaceCamera;
                canvas.pixelPerfect = true;
                canvas.planeDistance = 100;
                canvas.additionalShaderChannels =
                    AdditionalCanvasShaderChannels.TexCoord1
                    | AdditionalCanvasShaderChannels.Normal
                    | AdditionalCanvasShaderChannels.Tangent;

                canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
                canvasScaler.referenceResolution = new Vector2(1366, 768);
                canvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
                canvasScaler.matchWidthOrHeight = .5f;
                canvasScaler.referencePixelsPerUnit = 100;

                Debug.Log("Canvas is modified.");
            } else {
                Debug.Log("Canvas is null.");
            }
        }
    }
}