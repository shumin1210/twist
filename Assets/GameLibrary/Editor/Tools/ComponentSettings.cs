﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using GameLibrary.Utility;
using System.Linq;

namespace GameLibrary.UI {

    internal class ComponentSettings {

        [MenuItem("GameLibrary/Settins/DefaultFont", validate = true)]
        private static bool canSetDefaulktTextSetting() {
            return Selection.gameObjects.Count(o => !o.GetComponent<Text>()) == 0;
        }

        [MenuItem("GameLibrary/Settins/DefaultFont")]
        public static void SetDefaultTextSetting() {
            foreach (var item in Selection.gameObjects) {
                var txt = item.GetComponent<Text>();
                if (txt) {
                    setDefaultTextSetting(txt);
                }
            }
        }

        private static void setDefaultTextSetting(Text txt) {
            txt.font = DefaultResManager.DefaultFont;
            txt.alignment = TextAnchor.MiddleCenter;
            txt.resizeTextForBestFit = true;
        }
    }
}