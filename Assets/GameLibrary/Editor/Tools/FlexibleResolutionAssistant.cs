﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace GameLibrary.UI {

    internal class FlexibleResolutionAssistant {
        public bool isFlexible;

        #region Fit to self rect

        [MenuItem("GameLibrary/Flexible Resolution/Fit To Rect", true)]
        public static bool CanSetFlexible() {
            return Selection.activeGameObject != null;
        }

        [MenuItem("GameLibrary/Flexible Resolution/Fit To Rect")]
        public static void SetFlexible() {
            foreach (var item in Selection.gameObjects) {
                setFlexible(item);
            }
        }

        private static void setFlexible(GameObject gameObject) {
            RectTransform rect = gameObject.transform as RectTransform;
            RectTransform global = gameObject.transform.parent as RectTransform;
            /*pivot's position*/
            float w = global.rect.width;
            float h = global.rect.height;

            float left = rect.localPosition.x - rect.rect.width * rect.pivot.x + w * global.pivot.x;
            float minX = left / w;
            float right = rect.localPosition.x + rect.rect.width * (1 - rect.pivot.x) + w * global.pivot.x;
            float maxX = right / w;

            float top = rect.localPosition.y - rect.rect.height * rect.pivot.y + h * global.pivot.y;
            float minY = top / h;
            float bottom = rect.localPosition.y + rect.rect.height * (1 - rect.pivot.y) + h * global.pivot.y;
            float maxY = bottom / h;

            rect.anchorMin = new Vector2(minX, minY);
            rect.anchorMax = new Vector2(maxX, maxY);

            rect.offsetMin = new Vector2(0, 0);
            rect.offsetMax = new Vector2(0, 0);
        }

        #endregion Fit to self rect

        #region Fit to parent

        [MenuItem("GameLibrary/Flexible Resolution/Fit To Parent", true)]
        public static bool CanFitToBorder() {
            return Selection.activeGameObject != null;
        }

        [MenuItem("GameLibrary/Flexible Resolution/Fit To Parent")]
        public static void FitToBorder() {
            foreach (var item in Selection.gameObjects) {
                setFitToBorder(item);
            }
        }

        private static void setFitToBorder(GameObject gameObject) {
            RectTransform rect = gameObject.transform as RectTransform;
            RectTransform global = gameObject.transform.parent as RectTransform;

            rect.anchorMin = new Vector2(0, 0);
            rect.anchorMax = new Vector2(1, 1);

            rect.offsetMin = new Vector2(0, 0);
            rect.offsetMax = new Vector2(0, 0);
        }

        #endregion Fit to parent

        #region Anchor to own pivot

        [MenuItem("GameLibrary/Flexible Resolution/Anchor on pivot", true)]
        public static bool CanSetAnchorToPivot() {
            return Selection.activeGameObject != null;
        }

        [MenuItem("GameLibrary/Flexible Resolution/Anchor on pivot")]
        public static void SetAnchorToPivot() {
            foreach (var item in Selection.gameObjects) {
                setAnchorToPivot(item);
            }
        }

        private static void setAnchorToPivot(GameObject gameObject) {
            RectTransform rect = gameObject.transform as RectTransform;
            RectTransform global = gameObject.transform.parent as RectTransform;
            /*pivot's position*/
            float w = global.rect.width;
            float h = global.rect.height;

            float width = rect.rect.width;
            float height = rect.rect.height;

            float mid = rect.localPosition.x + w * global.pivot.x;
            float x = mid / w;

            float center = rect.localPosition.y + h * global.pivot.y;
            float y = center / h;

            rect.anchorMax = rect.anchorMin = new Vector2(x, y);

            rect.offsetMin = new Vector2(width * (rect.pivot.x - 1), height * (rect.pivot.y - 1));
            rect.offsetMax = new Vector2(width * rect.pivot.x, height * rect.pivot.y);
        }

        #endregion Anchor to own pivot
    }
}

#endif