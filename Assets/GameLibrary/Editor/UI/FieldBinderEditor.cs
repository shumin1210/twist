﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using GameLibrary.Utility;
using UnityEditor;
using UnityEngine;

namespace GameLibrary.UI {

    [CanEditMultipleObjects]
    [CustomEditor(typeof(FieldBinder))]
    public class FieldBinderEditor : Editor {
        private static bool initialized = false;
        private static Dictionary<string, Type> models = new Dictionary<string, Type>();
        private static List<string> modelKeys;
        private bool inverseActive = false;
        private string[] fields;
        private int modelIndex = -1;
        private int fieldIndex = 0;

        private void refshBtn() {
            if (!initialized) {
                initialized = true;
                models = new Type[] { typeof(object) }.Concat(AppDomain.CurrentDomain.GetAssemblies()
                         .SelectMany(s => s.GetTypes())
                         .Where(p => p.GetCustomAttributes(typeof(FieldBindableAttribute), true).Count() > 0)
                         ).ToDictionary(m => m.Name, m => m);
                modelKeys = models.Values.Select(t => t.FullName).ToList();
            }
        }

        public override void OnInspectorGUI() {
            //refshBtn();
            FieldBinder o = target as FieldBinder;

            EditorGUILayout.PropertyField(serializedObject.FindProperty("HideWhenNoValue"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Field"));

            valueFields();
            FieldBinder.FieldType flag = (FieldBinder.FieldType)serializedObject.FindProperty("Type").intValue;
            defaultValueField(flag);
            switch (flag) {
                default:
                    break;

                case FieldBinder.FieldType.Active:
                case FieldBinder.FieldType.Toggle:
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("InverseActive"));
                    if (flag == FieldBinder.FieldType.Toggle) {
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("onToggled"));
                    }
                    break;
            }
            EditorGUILayout.PropertyField(serializedObject.FindProperty("OnBinded"), true);

            serializedObject.ApplyModifiedProperties();
        }

        private void defaultValueField(FieldBinder.FieldType flag) {
            switch (flag) {
                case FieldBinder.FieldType.Image:
                    UnityEngine.Object obj = serializedObject.FindProperty("defaultValue").objectReferenceValue;
                    serializedObject.FindProperty("defaultValue").objectReferenceValue = EditorGUILayout.ObjectField("DefaultValue", obj as Sprite, typeof(Sprite), allowSceneObjects: false);
                    break;
            }
        }

        private void getFields(string Key, Type modelType) {
            if (modelType == null) {
                fields = null;
                fieldIndex = 0;
                return;
            }
            fields = modelType.GetFields().Cast<MemberInfo>().Concat(modelType.GetProperties()).Select(s => s.Name).ToArray();
            if (fields.Contains(Key)) {
                fieldIndex = fields.ToList().IndexOf(Key);
            } else {
                fieldIndex = 0;
            }
        }

        private void valueFields() {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Type"));
            FieldBinder.FieldType index = (FieldBinder.FieldType)serializedObject.FindProperty("Type").intValue;
            switch (index) {
                case FieldBinder.FieldType.Text:
                case FieldBinder.FieldType.GameObjectName:
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("Format"), includeChildren: true);
                    break;
            }
        }
    }
}