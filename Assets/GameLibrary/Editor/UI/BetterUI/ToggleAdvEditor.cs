﻿using System.Collections.Generic;
using System.Linq;
using GameLibrary.Utility;
using UnityEditor;
using UnityEditor.UI;
using UnityEngine;
using UnityEngine.UI;

namespace GameLibrary.UI {

    [CustomEditor(typeof(ToggleAdv))]
    [CanEditMultipleObjects]
    public class ToggleAdvEditor : SelectableEditor {
        private SerializedProperty m_OnValueChangedProperty;
        private SerializedProperty m_OnActivedProperty;
        private SerializedProperty m_OnDeactivedProperty;
        private SerializedProperty m_TransitionProperty;
        private SerializedProperty m_GraphicProperty;
        private SerializedProperty m_GroupProperty;
        private SerializedProperty m_IsOnProperty;
        private SerializedProperty m_LabelProperty;
        private SerializedProperty m_LabelTextProperty;

        protected override void OnEnable() {
            base.OnEnable();

            m_TransitionProperty = serializedObject.FindProperty("toggleTransition");
            m_GraphicProperty = serializedObject.FindProperty("graphic");
            m_GroupProperty = serializedObject.FindProperty("m_Group");
            m_IsOnProperty = serializedObject.FindProperty("m_IsOn");
            m_OnValueChangedProperty = serializedObject.FindProperty("onValueChanged");
            m_OnActivedProperty = serializedObject.FindProperty("onActived");
            m_OnDeactivedProperty = serializedObject.FindProperty("onDeactived");
            m_LabelProperty = serializedObject.FindProperty("m_Label");
            m_LabelTextProperty = serializedObject.FindProperty("m_LabelText");
        }

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            EditorGUILayout.Space();

            serializedObject.Update();
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(m_IsOnProperty);
            ToggleAdv o = target as ToggleAdv;
            o.isOn = m_IsOnProperty.boolValue;
            o.PlayEffect();

            EditorGUILayout.PropertyField(m_LabelProperty);
            if (m_LabelProperty.objectReferenceValue != null) {
                EditorGUILayout.PropertyField(m_LabelTextProperty);
                (m_LabelProperty.objectReferenceValue as Text).text = m_LabelTextProperty.stringValue;
            }

            EditorGUILayout.PropertyField(m_TransitionProperty);
            EditorGUILayout.PropertyField(m_GraphicProperty);
            EditorGUILayout.PropertyField(m_GroupProperty);

            EditorGUILayout.Space();

            // Draw the event notification options
            EditorGUILayout.PropertyField(m_OnValueChangedProperty);
            EditorGUILayout.PropertyField(m_OnActivedProperty);
            EditorGUILayout.PropertyField(m_OnDeactivedProperty);
            serializedObject.ApplyModifiedProperties();
        }

        private static string PrefabName = "ToggleAdv";

        private static GameObject prefab;

        [MenuItem("GameObject/UI/BetterUI/ToggleAdv", false, priority = 29)]
        private static void InstantiateToggleAdv() {
            if (!prefab) {
                IEnumerable<string> paths = AssetDatabase.FindAssets(PrefabName).Select(guid => AssetDatabase.GUIDToAssetPath(guid));
                for (int i = 0; i < paths.Count(); i++) {
                    if (paths.ElementAt(i).EndsWith(".prefab")) {
                        prefab = AssetDatabase.LoadAssetAtPath<GameObject>(paths.ElementAt(i));
                    }
                }
            }

            if (prefab == null) {
                Debug.LogErrorFormat("Prefab file \"{0}\" is Lost.", PrefabName);
            } else {
                GameObject go = Selection.activeGameObject;
                GameObject instance = GameObjectRelate.InstantiateGameObject(go, prefab);
                instance.name = PrefabName;
                instance.GetComponent<ToggleAdv>().group = go.GetComponentInParent<ToggleGroupAdv>();
                //instance.GetComponent<ToggleAdv>().Reset();
            }
        }
    }
}