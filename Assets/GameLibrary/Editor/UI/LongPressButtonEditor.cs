﻿using UnityEditor;
using UnityEditor.UI;

namespace GameLibrary.UI {

    [CustomEditor(typeof(LongPressButton))]
    public class LongPressButtonEditor : ButtonEditor {

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            LongPressButton targetBtn = (LongPressButton)target;
            targetBtn.holdTime = EditorGUILayout.FloatField("HoldTime", targetBtn.holdTime);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("onLongPress"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("onPointerExit"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("onPointerEnter"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("onPointerDown"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("onPointerUpInside"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("onPointerUpOutside"));

            serializedObject.ApplyModifiedProperties();
        }
    }
}