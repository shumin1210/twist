﻿using System.Linq;
using UnityEditor;
using UnityEngine;

namespace GameLibrary.Utility {

    [CustomEditor(typeof(VariantElement))]
    public class VariantElementEditor : Editor {
        private VariantAssetManager.AssetType assetType;

        public override void OnInspectorGUI() {
            //base.OnInspectorGUI();
            serializedObject.Update();

            VariantElement v = target as VariantElement;
            assetType = v.Type;

            string[] bundles = AssetDatabase.GetAllAssetBundleNames().Select(s => s.Split(".")[0]).Distinct().ToArray();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("AutoHide"));
            GUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            var bundleIndexes = bundles.FindIndexes(s => s == v.BundleName).ToArray();
            int bundleIndex = bundleIndexes.Length > 0 ? bundleIndexes[0] : 0;
            serializedObject.FindProperty("BundleName").stringValue = bundles[(int)EditorGUILayout.Popup("BundleName", bundleIndex, bundles)];
            GUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            v.Type = (VariantAssetManager.AssetType)EditorGUILayout.EnumPopup(label: "AssetType", selected: assetType);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Load resource name")) {
                (target as VariantElement).ApplySetting();
            }
            GUILayout.EndHorizontal();

            switch (assetType) {
                case VariantAssetManager.AssetType.Image:
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("ResName"));
                    GUILayout.EndHorizontal();
                    break;

                case VariantAssetManager.AssetType.SpriteUI:
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("ResNames"), true);
                    GUILayout.EndHorizontal();
                    break;
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}