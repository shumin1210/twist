﻿using System.Collections;
using UnityEditor;

namespace GameLibrary.Utility.SerializableDictionary {

    [CustomPropertyDrawer(typeof(DataMap), true)]
    public class AnySerializableDictionaryPropertyDrawer : SerializableDictionaryPropertyDrawer {
    }
}