﻿using UnityEditor;
using UnityEngine;

namespace GameLibrary.Utility {

    [CustomPropertyDrawer(typeof(DynamicValue))]
    public sealed class DynamicValuePropertyDrawer : PropertyDrawer {

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            return 18f;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            position.height = 18f;
            position = EditorGUI.IndentedRect(position);
            position.width /= 2f;
            int oldIndentLv = EditorGUI.indentLevel;
            EditorGUI.indentLevel = oldIndentLv + 1;

            DynamicValue.Type type = (DynamicValue.Type)property.FindPropertyRelative("type").enumValueIndex;
            property.FindPropertyRelative("type").intValue = (int)(DynamicValue.Type)EditorGUI.EnumPopup(position, type);

            position.x += position.width;
            switch (type) {
                case DynamicValue.Type.Boolean:
                    bool bval = false;
                    bool.TryParse(property.FindPropertyRelative("value").stringValue, out bval);
                    property.FindPropertyRelative("value").stringValue = EditorGUI.Toggle(position, bval).ToString();
                    break;

                case DynamicValue.Type.Integer:
                    int ival = 0;
                    int.TryParse(property.FindPropertyRelative("value").stringValue, out ival);
                    property.FindPropertyRelative("value").stringValue = EditorGUI.IntField(position, ival).ToString();
                    break;

                case DynamicValue.Type.BigInteger:
                    long lval = 0;
                    long.TryParse(property.FindPropertyRelative("value").stringValue, out lval);
                    property.FindPropertyRelative("value").stringValue = EditorGUI.LongField(position, lval).ToString();
                    break;

                case DynamicValue.Type.Float:
                    float fval = 0;
                    float.TryParse(property.FindPropertyRelative("value").stringValue, out fval);
                    property.FindPropertyRelative("value").stringValue = EditorGUI.FloatField(position, fval).ToString();
                    break;

                case DynamicValue.Type.String:
                    property.FindPropertyRelative("value").stringValue = EditorGUI.TextField(position, property.FindPropertyRelative("value").stringValue);
                    break;
            }

            EditorGUI.indentLevel = oldIndentLv;
        }
    }
}