﻿using System;
using GameLibrary.Core;
using GameLibrary.Utility;
using UnityEditor;
using UnityEngine;

namespace GameLibrary.UI {

    public class GameLibEditor : EditorWindow {

        [MenuItem("Window/GameLibrary")]
        private static void Init() {
            GameLibEditor window = (GameLibEditor)EditorWindow.GetWindow(typeof(GameLibEditor), false, "GameLibrary");
            window.Show();
        }

        private static string prefID = "";

        private void OnGUI() {
            EditorGUILayout.TextField(label: "PlayerID", text: Player.Instance ? Player.Instance.ID.ToString() : "");
            GUILayout.Label("ServerTime", EditorStyles.boldLabel);
            ServerTime.DebugDif = TimeSpan.FromSeconds((double)EditorGUILayout.FloatField(label: "Debug Delay(sec)", value: (float)ServerTime.DebugDif.TotalSeconds));
            LocalizationManager.Language = EditorGUILayout.TextField(label: "Language", text: LocalizationManager.Language);
            EditorGUILayout.BeginHorizontal();
            prefID = EditorGUILayout.TextField("PlayerPrefID", prefID);
            GUILayout.TextField(PlayerPrefUtil.Get<string>(prefID) ?? "");
            EditorGUILayout.EndHorizontal();
            //myString = EditorGUILayout.TextField("Text Field", myString);

            //groupEnabled = EditorGUILayout.BeginToggleGroup("Optional Settings", groupEnabled);
            //myBool = EditorGUILayout.Toggle("Toggle", myBool);
            //myFloat = EditorGUILayout.Slider("Slider", myFloat, -3, 3);
            //EditorGUILayout.EndToggleGroup();
        }
    }
}