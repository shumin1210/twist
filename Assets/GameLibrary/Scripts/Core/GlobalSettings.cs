﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.Events;

namespace GameLibrary.Core {

    public static class GlobalSettings {
        private static TDictionary<string, object> instance;

        private static TDictionary<string, object> Instance {
            get {
                if (instance == null) {
                    instance = new TDictionary<string, object>();
                }
                return instance;
            }
        }

        public static string Version {
            get {
                string result = "";
                return Instance.TryGet<string>("version", ref result) ? result : "";
            }
        }

        public static string FileName = "version.data";
        public static string RemotePath = "https://loginw.crazybet.win/";
        public static UnityEvent OnSettingsUpdated = new UnityEvent();

        public static bool Prepared {
            get;
            private set;
        }

        public static string FullFileName {
            get {
                return RemotePath + FileName + "?t=" + DateTime.Now.Ticks;
            }
        }

        public static void UpdateSettings() {
            CoroutineUtility.CallStartCoroutine(checkVersion(FullFileName));
        }

        private static IEnumerator checkVersion(string url) {
            Prepared = false;
            using (WWW req = new WWW(url)) {
                yield return req;

                if (req.isDone) {
                    string text = req.text;
                    Instance.Merge(text.Split("\r\n").SplitToDictionary<object>("="));
                    Debug.LogFormat("Global setting updates from {0}:\r\n{1}", FullFileName, text);
                    Prepared = true;
                    OnSettingsUpdated.Invoke();
                }
            }
        }

        public static string Get(string key) {
            return Instance.Get(key, default(string));
        }

        public static T Get<T>(string key) {
            return Instance.Get(key, default(T));
        }

        public static T Get<T>(string key, T defaultValue) {
            return Instance.Get(key, defaultValue);
        }

        public static void Set<T>(string key, T value) {
            Instance[key] = value;
        }

        public static void Merge(IDictionary<string, object> m) {
            Instance.Merge(m);
        }
    }
}