﻿using System;
using GameLibrary.Utility;

namespace GameLibrary.Core {

    [Serializable]
    public class Player {

        public struct ActType {
            public const string Update = "playerinfo";
        }

        #region Instance

        private static Player instance;

        public static Player Instance {
            get {
                return instance;
            }
            set {
                instance = value;
                DataShared.Instance.StaticData.Set(KEY, value);
                if (!value) {
                    DataShared.Instance.StaticData.Remove(KEY);
                }
            }
        }

        #endregion Instance

        public const string KEY = "Player";
        public int AccessLevel;
        public long EXP;
        public long EXPCap;
        public int Gender;
        public int GID;
        public string GuestAcount;
        public string GuestToken;
        public bool GuestUpgraded;
        public TDictionary<string, object> info = new TDictionary<string, object>();

        public bool IsGuest;

        public bool isOverMoneyLimit;

        public bool IsPhoneVerified;

        public int Level;

        public bool Reconnect;

        public long Refund;

        public string Token;

        public string VirtualID;

        public string photo;

        public int ID {
            get; set;
        }

        public string Name {
            get; set;
        }

        public string Photo {
            get {
                return GlobalSettings.Get<string>("photo_root") + photo;
            }
            set {
                photo = value;
            }
        }

        public long UD {
            get; set;
        }

        public object this[string key] {
            get {
                return info[key];
            }
            set {
                info[key] = value;
            }
        }

        public static implicit operator bool(Player p) {
            return p != null && !string.IsNullOrEmpty(p.Token);
        }
    }
}