﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using GameLibrary.Utility;
using UnityEngine;

namespace GameLibrary.Core {

    public class ExtractableField<T> : IValueBind<T> {
        private IAdditionalPlayerAttr src;

        private Func<Player, object> get;
        private Action<Player, object> set;
        private T _val;

        public T GetValue {
            get {
                return (T)getVal();
            }
        }

        object IValueBind.GetValue {
            get {
                return getVal();
            }
        }

        public ExtractableField(IAdditionalPlayerAttr src, Expression<Func<Player, object>> expression) {
            this.src = src;
            get = ExpressionUtils.CreateGetter(expression);
            set = ExpressionUtils.CreateSetter(expression);
        }

        internal object getVal() {
            if (src.parent) {
                return get(src.parent);
            } else {
                return _val;
            }
        }

        public void SetValue(T val) {
            if (src.parent) {
                set(src.parent, val);
            } else {
                _val = val;
            }
        }

        public void SetValue(object val) {
            SetValue((T)val);
        }
    }

    public abstract class IAdditionalPlayerAttr {

        public Player parent {
            get; internal set;
        }
    }

    /// <summary>提供規格化的玩家附加屬姓</summary>
    public abstract class IAdditionalPlayerAttr<T> : IAdditionalPlayerAttr where T : new() {

        /// <summary>玩家屬性實體的資料鍵值</summary>
        public static string INFO_KEY = "additional_player_attributes_" + typeof(T).Name;

        /// <summary>取得玩家屬性資料的擴充欄位值</summary>
        /// <param name="p">玩家屬性實體</param>
        /// <returns>擴充欄位實體</returns>
        /// <remarks>若擴充欄位實體不存在則建立新實體在玩家屬性實體中,並回傳新實體</remarks>
        public static T GetData(Player p) {
            Initialize(p);
            return (T)p[INFO_KEY];
        }

        /// <summary>初始化玩家屬性實體,使其附有IAdditionalPlayerAttr的實體</summary>
        /// <param name="p">玩家屬性實體</param>
        public static void Initialize(Player p) {
            if (!p.info.ContainsKey(INFO_KEY)) {
                Debug.Log("add info_key=" + INFO_KEY);
                p.info.Add(INFO_KEY, new T());
            }
        }
    }

    public static class AdditionPlayerAttrExtension {

        public static object GetAdditionAttrByType(this Player p, Type type) {
            FieldInfo f = type.BaseType.GetField("INFO_KEY", BindingFlags.Static | BindingFlags.Public);
            string key = f.GetValue(null).ToString();

            var m = typeof(AdditionPlayerAttrExtension);
            var tmp = m.GetMethod("GetAdditionAttr");

            //MethodInfo gm = m.GetGenericMethodDefinition();
            var mm = tmp.MakeGenericMethod(type);
            return mm.Invoke(null, new object[] { p });
        }

        public static T GetAdditionAttr<T>(this Player p) where T : IAdditionalPlayerAttr<T>, new() {
            T i = IAdditionalPlayerAttr<T>.GetData(p);
            i.parent = p;
            return i;
        }
    }
}