﻿using System;
using GameLibrary.Net;
using UnityEngine;

namespace GameLibrary.Core {

    /// <summary>提供觸發父層級事件的方法,實體化於遊戲大廳</summary>
    public abstract class ParentHandler : MsgHandler {

        /// <summary>父層級的訊息中控實體</summary>
        protected static ParentHandler instance;

        public static ParentHandler Instance {
            get {
                return instance;
            }
        }

        /// <summary>子層級的訊息中控實體</summary>
        protected static MsgHandler subHandler;

        /// <summary>子層級的訊息中控實體</summary>
        public static MsgHandler SubHandler {
            get {
                return subHandler;
            }
            internal set {
                if (value is ParentHandler) {
                    Debug.Log("skip core assignment.");
                } else {
                    subHandler = value;
                }
            }
        }

        public static MsgHandler HandlerInstance {
            get {
                return Instance ?? SubHandler;
            }
        }

        public static void InvokeLogout() {
            if (Instance) {
                Instance.Logout();
            }
        }

        public static bool HasParent {
            get {
                bool result = instance;
                if (!result) {
                    Debug.Log("{ParentHandler} hasn't been assigned.");
                }
                return result;
            }
        }

        protected static bool hasSubInstance {
            get {
                bool result = subHandler;
                if (!result) {
                    Debug.Log("{ParentHandler.SubHandler} hasn't been assigned.");
                }
                return result;
            }
        }

        protected override void Start() {
            instance = this;
            initHandlerModule();
            initConnect();
        }

        /// <summary>呼叫"離開遊戲"</summary>
        public static void InvokeLeaveFromGame() {
            if (SubHandler) {
                SubHandler.IsAutoReconnectEnabled = false;
                subHandler.TriggerEvent("logout");
                subHandler.Disconnect();
            }
            if (HasParent) {
                SubHandler = null;
                instance.OnLogoutFromGame();
            } else {
                SubHandler.IsAutoReconnectEnabled = true;
                SubHandler.Restart();
                Player.Instance = null;
            }
        }

        /// <summary>"離開遊戲"處理</summary>
        protected virtual void OnLogoutFromGame() {
        }

        /// <summary>呼叫父層級(ex:大廳)"切換UI"</summary>
        /// <param name="name">指定View的Name</param>
        /// <param name="on">開|關(無參數則為切換)</param>
        public static void InvokeTogglePanel(string name, bool? on = null) {
            if (HasParent) {
                instance.OnTogglePanel(name, on);
            }
        }

        /// <summary>依據panelType呼叫父層級(ex:大廳)指定類型的SharedView"切換UI",</summary>
        /// <param name="panelType">指定SharedView的型別</param>
        /// <param name="on">開|關(無參數則為切換)</param>
        public static void InvokeTogglePanel(Type panelType, bool? on = null) {
            if (HasParent) {
                instance.OnTogglePanel(panelType, on);
            }
        }

        protected virtual void OnTogglePanel(Type panelType, bool? on = null) {
        }

        protected virtual void OnTogglePanel(string name, bool? on = null) {
        }

        public virtual void Logout() {
        }
    }
}