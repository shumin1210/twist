﻿namespace GameLibrary.Core {

    [System.AttributeUsage(System.AttributeTargets.All, Inherited = false)]
    public sealed class SharedViewAttribute : System.Attribute {
        public string NAME;
    }

    public static partial class SharedViews {
    }
}