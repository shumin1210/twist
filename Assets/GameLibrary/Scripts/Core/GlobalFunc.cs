﻿using System;
using System.Collections;
using GameLibrary.UI;
using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameLibrary.Core {

    /// <summary>
    /// 提供全域控制功能(ex:音量控制,共用面板(Shared Panel)控制...)
    /// </summary>
    public class GlobalFunc : MonoBehaviour {

        #region Audio relates

        public void ToggleMasterMusic(bool on) {
            ChanneledAudio.ToggleMasterMute(!on);
        }

        public void ChangeAudio_Music(float val) {
            ChanneledAudio.SetVolume(AudioChannel.Music, val);
        }

        public void ChangeAudio_Sound(float val) {
            ChanneledAudio.SetVolume(AudioChannel.SE, val);
        }

        public void ChangeAudio_Voice(float val) {
            ChanneledAudio.SetVolume(AudioChannel.Voice, val);
        }

        #endregion Audio relates

        public const string KEY_Language = "language_setting";
        private static bool initialized = false;

        public void LoadScene(string sceneName) {
            StartCoroutine(loadScene(sceneName));

            //SceneManager.LoadScene(scenenName, LoadSceneMode.Single);
        }

        private IEnumerator loadScene(string sceneName) {
            AsyncOperation task = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
            while (task.progress < .9) {
                yield return null;
            }
            task.allowSceneActivation = true;
        }

        public string Argument {
            get; set;
        }

        public void Awake() {
            if (!initialized) {
                LocalizationManager.Language = PlayerPrefs.GetString(KEY_Language, "TW");

                initialized = true;
            }
        }

        public void ShowParentPanel(string key) {
            ParentHandler.InvokeTogglePanel(key, true);
        }

        public void HideParentPanel(string key) {
            ParentHandler.InvokeTogglePanel(key, false);
        }

        public void Logout() {
            ParentHandler.InvokeLogout();
        }

        public void LeaveFromGame() {
            ParentHandler.InvokeLeaveFromGame();
        }

        public void ExecuteWithArgumentField(string methodName) {
            try {
                if (ParentHandler.HandlerInstance) {
                    var m = ParentHandler.HandlerInstance.GetType().GetMethod(methodName);
                    m.Invoke(ParentHandler.HandlerInstance, new object[] { Argument });
                }
            } catch (System.Exception) {
            }
        }

        public void Execute(string cmd) {
            try {
                string method = cmd.Split(":")[0];
                string[] args = cmd.Split(":")[1].Split(",");
                if (ParentHandler.HandlerInstance) {
                    var m = ParentHandler.HandlerInstance.GetType().GetMethod(method);
                    if (m != null) {
                        if (args.Length > 0) {
                            m.Invoke(ParentHandler.HandlerInstance, args);
                        } else {
                            m.Invoke(ParentHandler.HandlerInstance, new object[] { null });
                        }
                    }
                }
            } catch (System.Exception) {
            }
        }

        public void SendMsg(string key) {
            if (ParentHandler.HandlerInstance) {
                ParentHandler.HandlerInstance.Send(key);
            }
        }

        public void SendMsgWithArgs(string param) {
            if (ParentHandler.HandlerInstance) {
                var args = param.Split(":");
                ParentHandler.HandlerInstance.SendMessage(args[0], args[1]);
            }
        }

        /// <summary>觸發父通訊實體的事件</summary>
        /// <param name="key">事件名稱</param>
        public void TriggerParentEvent(string key) {
            if (ParentHandler.Instance) {
                ParentHandler.Instance.TriggerEvent(key);
            }
        }

        /// <summary>觸發子通訊實體的事件</summary>
        /// <param name="key">事件名稱</param>
        public void TriggerSubEvent(string key) {
            if (ParentHandler.SubHandler) {
                ParentHandler.SubHandler.TriggerEvent(key);
            }
        }

        public void ChangeLanguage(string language) {
            //TODO send to backend for refresh when login.
            PlayerPrefs.SetString(KEY_Language, language);

            LocalizedText.Initialize();
        }

        public void ShowPanel(string name) {
            UIManager.Instance.ShowPanel(name);
        }

        public void Destroy(GameObject o) {
            Destroy(o);
        }
    }
}