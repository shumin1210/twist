﻿using System;
using GameLibrary.MVVM;
using GameLibrary.MVVM.Template;
using UnityEngine;

namespace GameLibrary.Core {

    public class TestCore : ParentHandler, IPageContetCapability {
        public bool OpenViewOnStart = false;

        public ViewData DefaultViewData;

        public void OpenDefaultView() {
            pageSettings.OpenLink(DefaultViewData);
        }

        //in dev
        public enum Method {
            MEMBER = 0,
            GUEST = 1,
            FACEBOOK = 2,
            MOBILE = 3,
            TOKEN = 10,
            NULL = 99
        }

        public struct ActType {
            public const string PlayerInfo = "playerinfo";

            public const string Login = "login";
            public const string DoubleLogin = "double.login";

            public const string GetSettings = "settings.get";
            public const string LVUpdate = "level.update";

            public const string EnterGame = "enter.game";
            public const string LeaveGame = "leave.game";
        }

        [SerializeField]
        public string SettingPath = GlobalSettings.RemotePath;

        [SerializeField]
        private PageContentCapability pageSettings;

        public PageContentCapability PageSettings {
            get {
                return pageSettings;
            }

            set {
                pageSettings = value;
            }
        }

        protected override void Start() {
            base.Start();
            pageSettings.Initialize(gameObject);
            SetGlobalSettingPath(SettingPath);
            GlobalSettings.UpdateSettings();
            if (OpenViewOnStart) {
                OpenDefaultView();
            }
        }

        public override void Logout() {
        }

        public void SetGlobalSettingPath(string path) {
            GlobalSettings.RemotePath = SettingPath = path;
        }

        public void LoginByToken(string token) {
            Send(ActType.Login, new {
                token,
                type = (int)Method.TOKEN
            });
        }

        protected override void OnLogoutFromGame() {
        }

        protected override void OnTogglePanel(Type panelType, bool? on = null) {
        }

        protected override void OnTogglePanel(string name, bool? on = null) {
        }

        public void onSubViewOpened(ViewData data, GameObject go) {
        }
    }
}