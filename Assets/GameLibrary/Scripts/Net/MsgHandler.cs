﻿using System;
using System.Collections.Generic;
using GameLibrary.Core;
using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.Events;

namespace GameLibrary.Net {

    public class MsgHandleEvent : UnityEvent<IDictionary<string, object>> {
    }

    [RequireComponent(typeof(Connection))]
    public class MsgHandler : MonoBehaviour {

        private void Reset() {
            connection = GetComponent<Connection>();
            connection.Handler = this;
        }

        private class CmdEvent : UnityEvent<object[]> {
        }

        public const string ErrorPattern = ".error";
        public bool DisconnectWhenBackground = true;
        public bool IsAutoReconnectEnabled = false;
        private bool FinishConnection = false;
        private bool initiated = false;
        public IMsgHandlerModule[] MsgHandlerModules;

        public DynamicEvents EventHandlers {
            get; protected set;
        }

        /// <summary>送出訊息處理式的集合</summary>
        public Dictionary<string, Func<object>> SendHandlers {
            get; protected set;
        }

        /// <summary>接收訊息處理式的集合</summary>
        public Dictionary<string, MsgHandleEvent> ReceiveHandlers {
            get; protected set;
        }

        /// <summary>錯誤訊息處理式的集合</summary>
        public Dictionary<string, MsgHandleEvent> ErrorHandlers {
            get; protected set;
        }

        protected Action<object> sender;

        protected bool isInBackground = false;
        protected string unlockType;

        [SerializeField]
        protected Connection connection;

        public MsgHandler() {
            SendHandlers = new Dictionary<string, Func<object>>();
            ReceiveHandlers = new Dictionary<string, MsgHandleEvent>();
            ErrorHandlers = new Dictionary<string, MsgHandleEvent>();
            EventHandlers = new DynamicEvents();
        }

        protected void initHandlerModule() {
            MsgTaskAttribute.RegistryToHandler(this, this);
            MsgHandlerModules.Foreach(s => s.Initiate(this));
        }

        protected virtual void Start() {
            ParentHandler.SubHandler = this;
            initHandlers();
            initHandlerModule();
            initConnect();
        }

        private void initHandlers() {
        }

        protected void initConnect() {
            initiated = true;
            FinishConnection = false;
            connection.StartConnect();
        }

        protected virtual void OnEnable() {
            if (initiated && ParentHandler.SubHandler != this || !ParentHandler.Instance) {
                Restart();
            }
        }

        #region connection

        public void SetSender(Action<object> send) {
            sender = send;
        }

        public virtual void Disconnect() {
            connection.Close();
        }

        public virtual void Connect() {
            if (!FinishConnection && connection.state == Connection.States.Closed) {
                connection.StartConnect();
            }
        }

        internal void onopen() {
            EndConnecting();
            onOpen();
        }

        public virtual void onOpen() {
        }

        public void StartConnecting(string unlockType = "") {
            this.unlockType = unlockType;
            OnStartConnecting();
        }

        public virtual void OnStartConnecting() {
            UI.UIManager.Instance.TogglePanel("Connecting", true);
        }

        public void EndConnecting() {
            OnEndConnecting();
            unlockType = string.Empty;
        }

        public virtual void OnEndConnecting() {
            UI.UIManager.Instance.TogglePanel("Connecting", false);
        }

        public void Restart() {
            if (!initiated) {
                return;
            }
            gameObject.SetActive(true);
            FinishConnection = false;
            connection.StartConnect();
        }

        public void Finish() {
            FinishConnection = true;
            connection.Close();
        }

        public virtual void onClose() {
            if (FinishConnection) {
                gameObject.SetActive(false);
            } else if (IsAutoReconnectEnabled) {
                connection.StartConnect();
            }
        }

        public virtual void onError(string errorMsg = "") {
        }

        public virtual void onMessage(string actType, IDictionary<string, object> content) {
            try {
                if (ReceiveHandlers.ContainsKey(actType)) {
                    ReceiveHandlers[actType].Invoke(content);
                } else if (actType.Contains(ErrorPattern)) {
                    string type = actType.Split(ErrorPattern)[0];
                    if (ErrorHandlers.ContainsKey(type)) {
                        ErrorHandlers[type].Invoke(content);
                    }
                }
            } catch (Exception e) {
                Debug.LogError(e);
            } finally {
                if (!string.IsNullOrEmpty(unlockType) && actType == unlockType || (actType.Split(ErrorPattern)[0] == unlockType)) {
                    EndConnecting();
                }
            }
        }

        public void SendAndWait(string actType) {
            StartConnecting(actType);
            Send(actType);
        }

        public void SendAndWait(string actType, object ct) {
            StartConnecting(actType);
            Send(actType, ct);
        }

        public void Send(string actType, object ct) {
            if (sender != null) {
                sender(new ActionObject(actType, ct));
            }
        }

        public void Send(string actType) {
            if (sender != null
                && SendHandlers.ContainsKey(actType)) {
                object ct = SendHandlers[actType]();
                if (ct is ActionObject || ct is ActionCommand) {
                    sender(ct);
                } else {
                    sender(new ActionObject(actType, ct));
                }
            }
        }

        public void RegistryHandler(
            string actType,
            Func<object> Send = null,
            UnityAction<IDictionary<string, object>> Receive = null,
            UnityAction<IDictionary<string, object>> Error = null) {
            if (Send != null) {
                if (!SendHandlers.ContainsKey(actType)) {
                    SendHandlers.Add(actType, Send);
                } else {
                    SendHandlers[actType] = Send;

                    //Debug.LogErrorFormat("[MsgHandler] Duplicated sendAction['{0}'].", actType);
                }
            }
            if (Receive != null) {
                if (!ReceiveHandlers.ContainsKey(actType)) {
                    ReceiveHandlers.Add(actType, new MsgHandleEvent());
                }
                ReceiveHandlers[actType].AddListener(Receive);
            }
            if (Error != null) {
                if (!ErrorHandlers.ContainsKey(actType)) {
                    ErrorHandlers.Add(actType, new MsgHandleEvent());
                }
                ErrorHandlers[actType].AddListener(Error);
            }
        }

        public void CancelReceiveHandler(string actType, UnityAction<IDictionary<string, object>> Receive) {
            if (Receive != null) {
                if (ReceiveHandlers.ContainsKey(actType)) {
                    ReceiveHandlers[actType].RemoveListener(Receive);
                    if (ReceiveHandlers[actType].GetPersistentEventCount() == 0) {
                        ReceiveHandlers.Remove(actType);
                    }
                }
            }
        }

        public void CancelErrorHandler(string actType, UnityAction<IDictionary<string, object>> Error) {
            if (Error != null) {
                if (ErrorHandlers.ContainsKey(actType)) {
                    ErrorHandlers[actType].RemoveListener(Error);
                    if (ErrorHandlers[actType].GetPersistentEventCount() == 0) {
                        ErrorHandlers.Remove(actType);
                    }
                }
            }
        }

        public void CancelHandler(
            string actType,
            Func<object> Send = null,
            UnityAction<IDictionary<string, object>> Receive = null,
            UnityAction<IDictionary<string, object>> Error = null) {
            if (Send != null) {
                if (SendHandlers.ContainsKey(actType)) {
                    SendHandlers.Remove(actType);
                }
            }

            CancelReceiveHandler(actType, Receive);
            CancelErrorHandler(actType, Error);
        }

        public void ClearHandler(string actType) {
            if (string.IsNullOrEmpty(actType)) {
                return;
            }
            SendHandlers.Remove(actType);
            ReceiveHandlers.Remove(actType);
            ErrorHandlers.Remove(actType);
        }

        #endregion connection

        #region View events

        private void OnApplicationFocus(bool focus) {
            if (focus && isInBackground) {
                GoToForeground();
            }
        }

        private void OnApplicationPause(bool pause) {
            if (pause && !isInBackground) {
                GoToBackground();
            }
        }

        public virtual void GoToForeground() {
            isInBackground = false;
            if (DisconnectWhenBackground) {
                Connect();
                IsAutoReconnectEnabled = true;
            }
        }

        public virtual void GoToBackground() {
            isInBackground = true;
            if (DisconnectWhenBackground) {
                IsAutoReconnectEnabled = false;
                Disconnect();
            }
        }

        #endregion View events

        public static implicit operator bool(MsgHandler handler) {
            return handler != null;
        }

        public void TriggerEvent(string key, params object[] args) {
            TriggerEvent_Module(key, args);
            TriggerEvent_Listener(key, args);
        }

        public void TriggetEvent(string key) {
            TriggerEvent(key);
        }

        public void TriggerEvent_Module(string key, params object[] args) {
            if (MsgHandlerModules.Length > 0) {
                MsgHandlerModules.TriggerEvent(key, args);
            }
        }

        public void TriggerEvent_Module(string key) {
            TriggerEvent_Module(key);
        }

        public void TriggerEvent_Listener(string key, params object[] args) {
            if (EventHandlers.Count > 0) {
                EventHandlers.Trigger(key, args);
            }
        }

        public void TriggerEvent_Listener(string key) {
            TriggerEvent_Listener(key);
        }
    }
}