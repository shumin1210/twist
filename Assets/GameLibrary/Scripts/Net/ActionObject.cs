﻿using System;
using System.Collections.Generic;
using GameLibrary.Utility;

namespace GameLibrary.Net {

    /// <summary>訊息包</summary>
    public class ActionObject {

        /// <summary>指令代碼</summary>
        public string Type;

        /// <summary>訊息時間</summary>
        public String Time;

        public long UnixTime;

        /// <summary>訊息內容</summary>
        public Dictionary<String, object> Content;

        /// <summary>建構式</summary>
        public ActionObject() {
            this.Time = DateTime.Now.toJSDate();
            this.UnixTime = DateTime.Now.unixTime();
            Content = new Dictionary<string, object>();
        }

        /// <summary>建構式</summary>
        /// <param name="type">指令代碼</param>
        /// <param name="content">訊息內容</param>
        public ActionObject(string type, params object[] parameters) : this() {
            this.Type = type;
            if (parameters.Length % 2 == 1) {
                throw new Exception("wrong key-value pair count.");
            }

            for (int i = 0; i < parameters.Length; i += 2) {
                Content.Add(parameters[i].ToString(), parameters[i + 1]);
            }
        }

        /// <summary>建構式</summary>
        /// <param name="type">指令代碼</param>
        /// <param name="content">訊息內容</param>
        public ActionObject(string type, object content = null) : this() {
            Type = type;
            if (content == null) {
                return;
            }
            if (content is string) {
                Content.Add("Msg", content);
                return;
            }
            Type info = content.GetType();
            foreach (System.Reflection.PropertyInfo item in info.GetProperties()) {
                Content.Add(item.Name, item.GetValue(content, null));
            };
            foreach (System.Reflection.FieldInfo item in info.GetFields()) {
                Content.Add(item.Name, item.GetValue(content));
            };
        }
    }
}