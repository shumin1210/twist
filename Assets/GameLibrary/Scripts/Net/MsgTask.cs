﻿using System;
using System.Collections.Generic;
using System.Reflection;
using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.Events;

namespace GameLibrary.Net {

    public enum HandleType {

        /// <summary>
        /// 接收方法
        /// </summary>
        /// <code>
        /// [MsgTask("acttype",Type=HandlerType.Receive)]
        /// void OnMessage(IDictionary<string,object>){}
        /// </code>
        Receive = 1,

        /// <summary>
        /// 預設發送方法
        /// </summary>
        /// <code>
        /// [MsgTask("acttype",Type=HandlerType.Send)]
        /// object OnMessage(){
        ///     return new {
        ///         Key,
        ///         Value...
        ///     };
        /// }
        /// </code>
        Send = 1 << 1,

        /// <summary>
        /// 接收錯誤方法
        /// </summary>
        /// <code>
        /// [MsgTask("acttype",Type=HandlerType.Error)]
        /// void OnMessage(IDictionary<string,object>){}
        /// </code>
        Error = 1 << 2,

        ReceiveAndError = Receive | Error,
    }

    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = true)]
    public sealed class MsgTaskAttribute : Attribute {
        public HandleType Type = HandleType.Receive;
        public string[] ActType;

        public MsgTaskAttribute(params string[] actType) {
            ActType = actType;
            //HandleType = hanleType;
        }

        public static void RegistryToHandler(object source, MsgHandler handler) {
            MethodInfo[] methods = source.GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
            //IEnumerable<MethodInfo> methods = ReflectionUtility.GetMembers(source.GetType()).SelectWhere(m => m.Method != null, m => m.Method);

            foreach (MethodInfo method in methods) {
                object[] types = method.GetCustomAttributes(typeof(MsgTaskAttribute), true);

                foreach (MsgTaskAttribute attr in types) {
                    string[] acttypes = attr.ActType;
                    Func<object> send;
                    UnityAction<IDictionary<string, object>> recv;
                    if (attr.Type.HasFlag(HandleType.Send)) {
                        if (method.GetParameters().Length > 0
                            || method.ReturnType != typeof(object)) {
                            Debug.LogWarningFormat("MsgTask[{0}] is not valid method.", method.Name);
                            continue;
                        }
                        send = () =>
                            method.Invoke(handler, null);
                        acttypes.Foreach(s => handler.RegistryHandler(s, Send: send));
                    }
                    if (attr.Type.HasFlag(HandleType.Receive)) {
                        if (method.GetParameters().Length != 1
                            || !typeof(IDictionary<string, object>).IsAssignableFrom(method.GetParameters()[0].ParameterType)) {
                            Debug.LogWarningFormat("MsgTask[{0}] is not valid method.", method.Name);
                            continue;
                        }
                        recv = (msg) =>
                            method.Invoke(handler, new object[] { msg });
                        acttypes.Foreach(s => handler.RegistryHandler(s, Receive: recv));
                    }
                    if (attr.Type.HasFlag(HandleType.Error)) {
                        if (method.GetParameters().Length != 1
                            || !typeof(IDictionary<string, object>).IsAssignableFrom(method.GetParameters()[0].ParameterType)) {
                            Debug.LogWarningFormat("MsgTask[{0}] is not valid method.", method.Name);
                            continue;
                        }
                        recv = (msg) =>
                            method.Invoke(handler, new object[] { msg });
                        acttypes.Foreach(s => handler.RegistryHandler(s, Error: recv));
                    }
                }
            }
        }
    }
}