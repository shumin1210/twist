﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.Events;

namespace GameLibrary.Net {

    /// <summary>
    /// 配合MsgHandler使用，套用的方法為Action<object[]>
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = true)]
    public sealed class MsgHandlerEventAttribute : Attribute {

        private class RefKey {
            public object source;
            public MsgHandler handler;

            public override bool Equals(object obj) {
                if (obj is RefKey) {
                    RefKey k = obj as RefKey;
                    return k.source == source && k.handler == handler;
                }
                return false;
            }

            public override int GetHashCode() {
                int hash = 17;
                hash = hash * 23 + source.GetHashCode();
                hash = hash * 23 + handler.GetHashCode();
                return hash;
            }
        }

        private class MethodPair {
            public MethodInfo method;
            public Action<object[]> delegation;
            public HashSet<string> evts = new HashSet<string>();
        }

        private static Dictionary<RefKey, HashSet<MethodPair>> cache = new Dictionary<RefKey, HashSet<MethodPair>>();
        public string EventName;

        /// <summary>
        /// 配合MsgHandler使用，套用的方法為Action<object[]>
        /// </summary>
        /// <param name="EventName">事件ID</param>
        public MsgHandlerEventAttribute(string EventName) {
            this.EventName = EventName;
        }

        public static void RegistryToHandler(object source, MsgHandler handler) {
            ReadOnlyCollection<ReflectionUtility.MemberData> methods = ReflectionUtility.GetMethods(source.GetType());
            RefKey key = new RefKey { source = source, handler = handler };
            if (cache.ContainsKey(key)) {
                return;
            }

            cache.Add(key, new HashSet<MethodPair>());
            HashSet<MethodPair> map = cache[key];
            foreach (ReflectionUtility.MemberData m in methods) {
                MethodInfo method = m.Method;
                object[] types = method.GetCustomAttributes(typeof(MsgHandlerEventAttribute), true);
                if (types.Length == 0
                    || method.GetParameters().Length != 1
                    || !typeof(object[]).IsAssignableFrom(method.GetParameters()[0].ParameterType)) {
                    Debug.LogWarningFormat("MsgHandlerEvent[{0}] is not valid method.", method.Name);
                    continue;
                }

                Action<object[]> listener;
                MethodPair methodPair = map.FirstOrDefault(mp => mp.method == method);
                if (methodPair == null) {
                    methodPair = new MethodPair() {
                        method = method,
                        delegation = (msg) =>
                            method.Invoke(source, new object[] { msg }),
                    };
                    map.Add(methodPair);
                }
                listener = methodPair.delegation;

                foreach (MsgHandlerEventAttribute attr in types) {
                    if (methodPair.evts.Contains(attr.EventName)) {
                        continue;
                    }
                    methodPair.evts.Add(attr.EventName);
                    handler.EventHandlers.Add(attr.EventName, listener);
                }
            }
        }

        public static void CancelToHandler(object source, MsgHandler handler) {
            RefKey key = new RefKey { source = source, handler = handler };
            if (!cache.ContainsKey(k=>k.Equals(key))) {
                return;
            }
            foreach (MethodPair item in cache[key]) {
                Action<object[]> method = item.delegation;
                foreach (string evt in item.evts) {
                    handler.EventHandlers.Remove(evt, method);
                }
            }
            cache[key].Clear();
            cache.Remove(key);
        }
    }
}