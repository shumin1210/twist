﻿using System;
using System.Collections.Generic;
using System.Reflection;
using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.Events;

namespace GameLibrary.Net {

    public interface IMsgHandleable {
    }

    public static class MsgHandleable {
        private static Dictionary<IMsgHandleable, Dictionary<string, Dictionary<HandleType, List<object>>>> registMap = new Dictionary<IMsgHandleable, Dictionary<string, Dictionary<HandleType, List<object>>>>();

        private static bool addRegMap(IMsgHandleable src, string acttype, HandleType type, object method) {
            if (!registMap.ContainsKey(src)) {
                registMap.Add(src, new Dictionary<string, Dictionary<HandleType, List<object>>>());
            }
            Dictionary<string, Dictionary<HandleType, List<object>>> regMap = registMap[src];
            if (!regMap.ContainsKey(acttype)) {
                regMap.Add(acttype, new Dictionary<HandleType, List<object>>());
            }
            Dictionary<HandleType, List<object>> map = regMap[acttype];
            if (!map.ContainsKey(type)) {
                map.Add(type, new List<object>());
            }
            List<object> list = map[type];
            if (list.Contains(method)) {
                return false;
            } else {
                list.Add(method);
                return true;
            }
        }

        public static void RegistryHandlers(this IMsgHandleable src, MsgHandler handler) {
            if (!handler) {
                Debug.LogError("Handler is not defined.");
            }
            registryMsgTask(src, handler);
            registryEvents(src, handler);
        }

        private static void registryEvents(IMsgHandleable src, MsgHandler handler) {
            MsgHandlerEventAttribute.RegistryToHandler(src, handler);
        }

        private static void registryMsgTask(IMsgHandleable src, MsgHandler handler) {
            IMsgHandleable source = src;
            MethodInfo[] methods = source.GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);

            foreach (MethodInfo method in methods) {
                object[] types = method.GetCustomAttributes(typeof(MsgTaskAttribute), true);

                foreach (MsgTaskAttribute attr in types) {
                    string[] acttypes = attr.ActType;
                    Func<object> send;
                    UnityAction<IDictionary<string, object>> recv;

                    if (attr.Type.HasFlag(HandleType.Send)) {
                        if (method.GetParameters().Length > 0 || method.ReturnType == typeof(void)) {
                            //Debug.LogWarningFormat("MsgTask[{0}] is not valid method.", method.Name);
                            continue;
                        }
                        send = () => method.Invoke(src, null);
                        acttypes.Foreach(s => {
                            if (addRegMap(src, s, attr.Type, send)) {
                                handler.RegistryHandler(s, Send: send);
                            }
                        });
                    }
                    if (attr.Type.HasFlag(HandleType.Receive)) {
                        if (method.GetParameters().Length != 1) {
                            //Debug.LogWarningFormat("MsgTask[{0}] is not valid method.", method.Name);
                            continue;
                        }
                        recv = (msg) => method.Invoke(src, new object[] { msg });
                        acttypes.Foreach(s => {
                            if (addRegMap(src, s, attr.Type, recv)) {
                                handler.RegistryHandler(s, Receive: recv);
                            }
                        });
                    }
                    if (attr.Type.HasFlag(HandleType.Error)) {
                        if (method.GetParameters().Length != 1) {
                            //Debug.LogWarningFormat("MsgTask[{0}] is not valid method.", method.Name);
                            continue;
                        }
                        recv = (msg) => method.Invoke(src, new object[] { msg });
                        acttypes.Foreach(s => {
                            if (addRegMap(src, s, attr.Type, recv)) {
                                handler.RegistryHandler(s, Error: recv);
                            }
                        });
                    }
                }
            }
        }

        public static void CancelHandlers(this IMsgHandleable src, MsgHandler handler) {
            if (!registMap.ContainsKey(src)) {
                return;
            }
            cancelMsgTask(src, handler);
            cancelEvents(src, handler);
        }

        private static void cancelEvents(IMsgHandleable src, MsgHandler handler) {
            MsgHandlerEventAttribute.CancelToHandler(src, handler);
        }

        private static void cancelMsgTask(IMsgHandleable src, MsgHandler handler) {
            Dictionary<string, Dictionary<HandleType, List<object>>> regMap = registMap[src];

            foreach (KeyValuePair<string, Dictionary<HandleType, List<object>>> map in regMap) {
                string actType = map.Key;
                foreach (KeyValuePair<HandleType, List<object>> handles in map.Value) {
                    HandleType type = handles.Key;
                    if (type.HasFlag(HandleType.Send)) {
                        if (handler.SendHandlers[actType].Equals(handles.Value[0])) {
                            handler.SendHandlers.Remove(actType);
                        }
                    }

                    if (type.HasFlag(HandleType.Receive)) {
                        handles.Value.ForEach(a => handler.ReceiveHandlers[actType].RemoveListener((UnityAction<IDictionary<string, object>>)a));
                    }

                    if (type.HasFlag(HandleType.Error)) {
                        handles.Value.ForEach(a => handler.ErrorHandlers[actType].RemoveListener((UnityAction<IDictionary<string, object>>)a));
                    }
                }
            }
            //regMap.Foreach(map => map.Value.Clear());
            regMap.Clear();
            registMap.Remove(src);
        }
    }
}