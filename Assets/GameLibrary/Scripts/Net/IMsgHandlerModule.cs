﻿using System;
using System.Collections.Generic;
using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.Events;

namespace GameLibrary.Net {

    public class MsgHandlerModuleManager {
        internal static Dictionary<string, IMsgHandlerModule> handlers = new Dictionary<string, IMsgHandlerModule>();

        public static void TryTriggerEvent(string name, string evt, params object[] args) {
            if (handlers.ContainsKey(name)) {
                handlers[name].TriggerEvent(evt, args);
            }
        }
    }

    public abstract class IMsgHandlerModule : MonoBehaviour {
        protected MsgHandler core;
        protected DynamicEvents EventHandlers = new DynamicEvents();
        private static IMsgHandlerModule _instance;

        public static void TryTriggerEvent(string evt, params object[] args) {
            if (_instance) {
                _instance.TriggerEvent(evt, args);
            }
        }

        private TDictionary<string, List<UnityAction<IDictionary<string, object>>>> registeredRecv = new TDictionary<string, List<UnityAction<IDictionary<string, object>>>>();
        private TDictionary<string, List<UnityAction<IDictionary<string, object>>>> registeredError = new TDictionary<string, List<UnityAction<IDictionary<string, object>>>>();
        private List<string> registeredActTypes = new List<string>();

        public void TriggerEvent(string evt, params object[] args) {
            try {
                EventHandlers.Trigger(evt, args);
            } catch (Exception) {
            }
        }

        public IMsgHandlerModule() {
            _instance = this;
        }

        public void Initiate(MsgHandler parent) {
            core = parent;
            MsgTaskAttribute.RegistryToHandler(this, parent);
            OnInitiate(core);
            MsgHandlerModuleManager.handlers.Add(GetType().Name, this);
        }

        protected void AddHandler(string actType,
            Func<object> Send = null,
            UnityAction<IDictionary<string, object>> Receive = null,
            UnityAction<IDictionary<string, object>> Error = null) {
            core.RegistryHandler(actType, Send, Receive, Error);
            if (Receive != null) {
                if (!registeredRecv.ContainsKey(actType)) {
                    registeredRecv.Add(actType, new List<UnityAction<IDictionary<string, object>>>());
                }
                registeredRecv[actType].Add(Receive);
            }

            if (Error != null) {
                if (!registeredError.ContainsKey(actType)) {
                    registeredError.Add(actType, new List<UnityAction<IDictionary<string, object>>>());
                }
                registeredError[actType].Add(Error);
            }
            registeredActTypes.Add(actType);
        }

        protected void CancelHandler(string actType) {
            if (core) {
                core.CancelHandler(actType);
                if (registeredRecv.ContainsKey(actType)) {
                    int length = registeredRecv[actType].Count;
                    for (int i = 0; i < length; i++) {
                        core.CancelReceiveHandler(actType, registeredRecv[actType][i]);
                    }
                    registeredRecv[actType].Clear();
                    registeredRecv.Remove(actType);
                }
                if (registeredError.ContainsKey(actType)) {
                    int length = registeredError[actType].Count;
                    for (int i = 0; i < length; i++) {
                        core.CancelErrorHandler(actType, registeredError[actType][i]);
                    }
                    registeredError[actType].Clear();
                    registeredError.Remove(actType);
                }

                registeredActTypes.Remove(actType);
            }
        }

        protected void AddEventListener(string evt, Action<object[]> callback) {
            EventHandlers.AddEventListener(evt, callback);
        }

        protected void AddEventListener(Action<object[]> callback = null, params string[] evts) {
            EventHandlers.AddEventListener(callback, evts);
        }

        protected virtual void OnInitiate(MsgHandler core) {
            Debug.Log("[{0}] initiate".format(GetType().Name));
        }

        protected virtual void OnDestroy() {
            if (core) {
                int length = registeredActTypes.Count;
                for (int i = 0; i < length; i++) {
                    CancelHandler(registeredActTypes[0]);
                }
            }
            MsgHandlerModuleManager.handlers.Remove(GetType().Name);
        }
    }

    /// <summary>通訊處理模組介面</summary>
    /// <typeparam name="T">繼承的實體</typeparam>
    public abstract class IMsgHandlerModule<T> : IMsgHandlerModule where T : class, new() {

        public static T Instance {
            get; protected set;
        }

        public IMsgHandlerModule() {
            Instance = this as T;
        }

        protected virtual void Start() {
        }
    }

    public static class MsgHandlerModuleExtension {

        public static void TriggerEvent(this IEnumerable<IMsgHandlerModule> handlers, string evt, params object[] args) {
            Debug.Log("trigger event: " + evt);
            handlers.Foreach(i => i.TriggerEvent(evt, args));
        }
    }
}