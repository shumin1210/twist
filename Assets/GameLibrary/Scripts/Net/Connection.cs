﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameLibrary.Utility;
using UnityEngine;

namespace GameLibrary.Net {

    [AddComponentMenu("Network/WebsocketConnection")]
    public class Connection : MonoBehaviour {

        public enum States : ushort {
            Connecting = 0,
            Open = 1,
            Closing = 2,
            Closed = 3
        }

        public enum ActionFormat {

            /// <summary>舊版傳輸格式</summary>
            ActionCommand,

            /// <summary>新版傳輸格式</summary>
            ActionObject
        }

        private WebSocket client;

        public States state = States.Closed;

        [SerializeField]
        public MsgHandler Handler;

        private bool isQuit = false;

        public bool EnableLog = true;
        public bool EnablePingPong = true;
        private static string ActType_PingPong = "system.ping";
        public float PingPongInterval = 3f;
        public int RetryPingPongCountLimit = 5;
        private int retryPingPongCount = 0;
        private DateTime lastPingPongSentTime;
        private DateTime lastPingPongRecvTime;
        public string URL;

        public float Timeout = 10f;
        private Coroutine task;

        public ActionFormat Format = ActionFormat.ActionObject;
        public Action<States> OnStateChange;
        public Action OnTimeout;
        private Coroutine timer;

        private void OnEnable() {
            if (Handler) {
                Handler.SetSender(this.Send);
            }
        }

        public void SetURL(string url) {
            if (URL != url) {
                URL = url;
            }
        }

        private void setState(States newState) {
            if (state != newState) {
                state = newState;
                if (EnableLog) {
                    Debug.Log("[Connection] State changed:" + newState.ToString());
                }

                if (OnStateChange != null) {
                    OnStateChange(newState);
                }
                switch (newState) {
                    case States.Connecting:
                        break;

                    case States.Open:
                        Handler.onopen();
                        break;

                    case States.Closing:
                        break;

                    case States.Closed:
                        Handler.onClose();
                        break;

                    default:
                        break;
                }
            }
        }

        private void OnDisable() {
            Close();
        }

        private void FixedUpdate() {
            if (client != null) {
                //client.StartPingThread = EnablePingPong;
            }
        }

        //private void OnApplicationPause(bool pause) {
        //    if (Application.runInBackground) {
        //    } else {
        //    }
        //}

        public void StartConnect() {
            if (isQuit) {
                Debug.LogErrorFormat("[{0}]:[Connection]app is quiting.", Handler.GetType().Name);
                return;
            }
            if (state != States.Closed) {
                Debug.LogErrorFormat("[{0}]:[Connection]abort connection new thread,because of websocket client still connecting.", Handler.GetType().Name);
                return;
            }
            if (Handler == null) {
                Debug.LogErrorFormat("[{0}]:[Connection]handler not asigned.", Handler.GetType().Name);
                return;
            }

            client = new WebSocket(new Uri(URL));
            client.Timeout = TimeSpan.FromSeconds(Timeout);
            //client.PingFrequency = (int)(PingPongInterval * 1000);
            //client.OnOpen += (ws) => setState(States.Open);
            //client.OnError += onError;
            //client.OnMessage += onMessage;
            //client.OnClosed += onClosed;

            //if (Application.internetReachability == NetworkReachability.NotReachable) {
            //    print("internetReachability = NotReachable.\r\ntry reconnect 3 secs later.");
            //    StartConnect();
            //    return;
            //} else {
            //    client.Open();
            //}
            task = StartCoroutine(connect());
        }

        private void onError(WebSocket webSocket, Exception ex) {
            setState(States.Closing);
            Handler.onError(ex.Message);
        }

        private void onClosed(WebSocket webSocket, ushort code, string message) {
            setState(States.Closed);

            Debug.Log("close from backend");
        }

        private void onMessage(WebSocket webSocket, string reply) {
            try {
                if (reply != null) {
                    if (EnableLog) {
                        Debug.LogFormat("[{0}]:[Connection] Receive message{1}", Handler ? Handler.GetType().Name : "", reply);
                    }
                    recvAct();
                    switch (Format) {
                        case ActionFormat.ActionCommand:
                            handleReceive_ActionCommand(reply);
                            break;

                        case ActionFormat.ActionObject:
                            handleReceive_ActionObject(reply);
                            break;
                    }
                }
            } catch (Exception e) {
                Debug.LogError(e);
            }
        }

        private IEnumerator connect() {
            Debug.LogFormat("[{0}]:[Connection] connect to {1}", Handler.GetType().Name, URL);

            if (Application.internetReachability == NetworkReachability.NotReachable) {
                print("internetReachability = NotReachable.\r\ntry reconnect 3 secs later.");
                yield return new WaitForSeconds(3f);
                StartCoroutine("connect");
                yield break;
            }

            client = new WebSocket(new Uri(URL));

            //client.Open();
            setState(States.Connecting);
            timer = StartCoroutine(countdown());
            //yield return new WaitUntil(() => client.IsOpen);
            yield return StartCoroutine(client.Connect());
            setState(States.Open);
            StopCoroutine(timer);

            Handler.onopen();
            lastPingPongRecvTime = lastPingPongSentTime = DateTime.Now;

            while (state == States.Open) {
                DateTime now = DateTime.Now;
                string reply = client.RecvString();
                try {
                    if (reply != null) {
                        string sign = "";
                        try {
                            recvAct();
                            switch (Format) {
                                case ActionFormat.ActionCommand:
                                    sign = handleReceive_ActionCommand(reply);
                                    break;

                                case ActionFormat.ActionObject:
                                    sign = handleReceive_ActionObject(reply);
                                    break;
                            }
                        } catch (Exception) {
                        }

                        if (sign != ActType_PingPong) {
                            if (EnableLog) {
                                Debug.LogFormat("[{0}]:[Connection] Receive message{1}", Handler ? Handler.GetType().Name : "", reply);
                            }
                        }
                    }
                    if (client.error != null) {
                        setState(States.Closing);
                        Handler.onError(client.error);
                        break;
                    }

                    if (EnablePingPong && (now - lastPingPongRecvTime).TotalSeconds > PingPongInterval) {
                        retryPingPongCount++;
                        Send(ActType_PingPong, new {
                        });
                        lastPingPongRecvTime = now;
                        if (retryPingPongCount++ > RetryPingPongCountLimit) {
                            OnTimeout.TryInvoke();
                            throw new Exception("Server has no response, please reconnect later.");
                        }
                    }
                } catch (Exception e) {
                    Debug.LogError(e);
                    break;
                }

                yield return 0;
            }
            Debug.Log("close from backend");
            client.Close();
            setState(States.Closed);
            Handler.onClose();

            //client = null;
            yield return 0;
        }

        private IEnumerator countdown() {
            if (Timeout <= 0) {
                yield break;
            }
            yield return new WaitForSeconds(Timeout);
            if (state != States.Open) {
                OnTimeout.TryInvoke();
                Close();
            }
        }

        private string handleReceive_ActionCommand(string reply, bool skip = false) {
            ActionCommand msg = JS.Deserialize<ActionCommand>(reply);
            if (msg != null) {
                if (msg.actioncontent is string) {
                    Handler.onMessage(msg.Type, new Dictionary<string, object>() { { "data", msg.actioncontent } });
                } else {
                    Handler.onMessage(msg.Type, msg.actioncontent as IDictionary<string, object>);
                }
                return msg.actionmaintype + "." + msg.actionsubtype;
            } else if (!skip) {
                return handleReceive_ActionObject(reply, skip: true);
            }
            return "";
        }

        private string handleReceive_ActionObject(string reply, bool skip = true) {
            ActionObject act = JS.Deserialize<ActionObject>(reply);
            if (act != null) {
                ServerTime.SetServerTime(new DateTime().FromUnix(act.UnixTime));
                if (act.Type == ActType_PingPong) {
                } else {
                    Handler.onMessage(act.Type, act.Content);
                }
                return act.Type;
            } else if (!skip) {
                return handleReceive_ActionCommand(reply, skip: true);
            }
            return "";
        }

        public void Close() {
            if (state == States.Open) {
                Debug.Log("close from client");
                StopCoroutine("connect");

                setState(States.Closing);
                client.Close();

                setState(States.Closed);
                Handler.onClose();
            }
        }

        public void Send(object act) {
            if (act is ActionObject) {
                Send(act as ActionObject);
            } else if (act is ActionCommand) {
                Send(act as ActionCommand);
            } else if (act is string) {
                Send(act as string);
            } else {
                Send(JS.Serialize(act));
            }
        }

        public void Send(string actype, object content) {
            switch (Format) {
                case ActionFormat.ActionCommand:
                    string[] types = actype.Split('.');
                    Send(JS.Serialize(new ActionCommand(types[0], types[1], content)), actype != ActType_PingPong);
                    break;

                case ActionFormat.ActionObject:
                    Send(JS.Serialize(new ActionObject(actype, content)), actype != ActType_PingPong);
                    break;
            }
        }

        public void Send(ActionObject act) {
            switch (Format) {
                case ActionFormat.ActionCommand:
                    Send(JS.Serialize(new ActionCommand(act.Type, act.Content["Msg"])), act.Type != ActType_PingPong);
                    break;

                case ActionFormat.ActionObject:
                    Send(JS.Serialize(act), act.Type != ActType_PingPong);
                    break;
            }
        }

        public void Send(ActionCommand act) {
            switch (Format) {
                case ActionFormat.ActionCommand:
                    Send(JS.Serialize(act), act.Type != ActType_PingPong);
                    break;

                case ActionFormat.ActionObject:
                    Send(JS.Serialize(new ActionObject(act.Type, act.actioncontent)), act.Type != ActType_PingPong);
                    break;
            }
        }

        public void Send(string msg, bool log = true) {
            if (EnableLog && log) {
                Debug.LogFormat("[{0}][Connection] send string:{1} ", Handler.GetType().Name, msg);
            }
            client.SendString(msg);
            sendAct();
        }

        private void sendAct() {
            lastPingPongSentTime = DateTime.Now;
        }

        private void recvAct() {
            retryPingPongCount = 0;
            lastPingPongRecvTime = DateTime.Now;
        }
    }
}