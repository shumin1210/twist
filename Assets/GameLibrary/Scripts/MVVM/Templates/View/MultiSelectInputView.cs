﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameLibrary.Utility;
using GameLibrary.Utility.SerializableDictionary;
using UnityEngine.UI;

namespace GameLibrary.MVVM.Template {

    public class MultiSelectInputView : InputView {

        [Serializable]
        public class OptionMap : DataMap<string, Toggle> {
        }

        public bool MultiSelectable = false;
        private bool lockVal = false;
        public OptionMap Options;

        protected override void OnInitialize() {
            base.OnInitialize();

            Options.Foreach(initToggle);
        }

        public override void OnBindingContextChanged(InputFieldViewModel oldValue, InputFieldViewModel newValue) {
            base.OnBindingContextChanged(oldValue, newValue);

            if (newValue && newValue.ValueType != null) {
                MultiSelectable =
                    (newValue.ValueType.IsEnum && Attribute.IsDefined(newValue.ValueType, typeof(FlagsAttribute)))
                    || newValue.Value is ICollection;
            }
        }

        private void initToggle(KeyValuePair<string, Toggle> obj) {
            obj.Value.onValueChanged.AddListener(val => setVal(obj.Key, val));
        }

        private void setVal(string key, bool val) {
            if (lockVal) {
                return;
            }
            lockVal = true;
            if (MultiSelectable) {
                setMultiVal(key, val);
            } else {
                setSingleVal(key, val);
            }
            lockVal = false;
        }

        private void setSingleVal(string key, bool val) {
            if (val) {
                Options.Foreach(o => o.Value.isOn = false);
            }
            if (BindingContext.ValueType.IsEnum) {
                BindingContext.SetValue(val ? Enum.Parse(BindingContext.ValueType, key) : 0);
            } else if (BindingContext.Value is ICollection) {
                BindingContext.SetValue(val ? new string[] { key }.Select(s => Convert.ChangeType(s, BindingContext.ValueType.GetElementType())) : null);
            } else {
                BindingContext.SetValue(val ? key : null);
            }
        }

        private void setMultiVal(string key, bool val) {
            if (BindingContext.ValueType.IsEnum) {
                IEnumerable<int> vals = Options.Where(o => o.Value.isOn).Select(a => (int)Enum.Parse(BindingContext.ValueType, a.Key));
                int v = 0;
                foreach (int item in vals) {
                    v |= item;
                }
                BindingContext.SetValue(v);
            } else if (BindingContext.Value is ICollection) {
                var eleType = BindingContext.ValueType.GetGenericElementType();
                IEnumerable<object> vals = Options.Where(o => o.Value.isOn).Select(a => Convert.ChangeType(a.Key, eleType));
                BindingContext.SetValue(vals);
            } else {
                BindingContext.SetValue(Convert.ChangeType(key, BindingContext.ValueType));
            }
        }

        protected override void onValueChanged(object oldValue, object newValue) {
            lockVal = true;
            Options.Foreach(t => t.Value.isOn = false);

            Type type = newValue.GetType();
            if (type.IsEnum) {
                foreach (KeyValuePair<string, Toggle> o in Options) {
                    Enum e = (Enum)Enum.Parse(type, o.Key);
                    if ((newValue as Enum).HasFlag(e)) {
                        o.Value.isOn = true;
                        if (!MultiSelectable) {
                            break;
                        }
                    }
                }
            } else if (newValue is ICollection) {
                foreach (object item in (ICollection)type) {
                    Toggle t;
                    if (Options.TryGetValue(item.ToString(), out t)) {
                        t.isOn = true;
                        if (!MultiSelectable) {
                            break;
                        }
                    }
                }
            } else {
                foreach (KeyValuePair<string, Toggle> o in Options) {
                    if (newValue.ToString() == o.Key) {
                        o.Value.isOn = true;
                        if (!MultiSelectable) {
                            break;
                        }
                    }
                }
            }
            lockVal = false;
        }
    }
}