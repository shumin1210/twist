﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameLibrary.Utility;
using GameLibrary.Utility.SerializableDictionary;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GameLibrary.MVVM.Template {

    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public sealed class FieldDataAttribute : Attribute {
    }

    public class SliderInputView : InputView {

        [Serializable]
        public class IntArrayData : RefData<int[]> {
        }

        [Serializable]
        public class ValueChangeEvent : UnityEvent<float> {
        }

        public Slider Slider;
        public Text Text;
        public IntArrayData Scales;
        public ValueChangeEvent OnValueChanged;

        protected override void OnInitialize() {
            base.OnInitialize();

            int[] arr = Scales;
            if (arr != null) {
                Slider.wholeNumbers = true;
                Slider.minValue = 0;
                Slider.maxValue = arr.Length;
            }
        }

        protected override void onValueChanged(object oldValue, object newValue) {
            int[] arr = Scales;
            if (arr != null) {
                //use scales
                int val = Convert.ToInt32(newValue);
                int index = arr.IndexOf(v => v == val);
                index = Math.Max(index, 0);
                Text.TrySetUtilValue(val.ToString());
                Slider.value = index;
                OnValueChanged.Invoke(val);
            } else {
                Text.TrySetUtilValue(newValue.ToString());
                var val = Convert.ToSingle(newValue);
                Slider.value = val;
                OnValueChanged.Invoke(val);
            }
        }
    }
}