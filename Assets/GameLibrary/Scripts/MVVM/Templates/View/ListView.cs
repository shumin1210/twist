﻿using System;
using System.Collections.Generic;
using System.Reflection;
using GameLibrary.UI;
using GameLibrary.Utility;
using SuperScrollView;
using UnityEngine;

namespace GameLibrary.MVVM.Template {

    public class ListView : UnityGuiView<ListViewModel> {
        public LoopListView2 Content;
        public GameObject Prefab_Item;

        public override void OnBindingContextChanged(ListViewModel oldValue, ListViewModel newValue) {
            base.OnBindingContextChanged(oldValue, newValue);
            if (oldValue) {
                oldValue.OnDataChanged -= onDataChanged;
            }
            if (newValue) {
                newValue.OnDataChanged += onDataChanged;
            }
        }

        protected override void OnInitialize() {
            base.OnInitialize();

            Content.InitListView(1, onGetItemByIndex);
        }

        private void onDataChanged(List<object> newValue) {
            Content.SetListItemCount(newValue.Count);
        }

        public void SetFilter(DataFilter filter) {
            BindingContext.Filter = filter;
        }

        private LoopListViewItem2 onGetItemByIndex(LoopListView2 view, int index) {
            if (!BindingContext || BindingContext.DataCount <= index || index < 0) {
                return null;
            }

            object data = BindingContext[index];
            if (data == null) {
                return null;
            }
            LoopListViewItem2 i = view.NewListViewItem(Prefab_Item.name);
            GameObject o = i.gameObject;
            FieldBinder.Bind(o, data);

            return i;
        }
    }
}