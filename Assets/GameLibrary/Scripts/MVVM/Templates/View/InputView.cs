﻿using UnityEngine.UI;

namespace GameLibrary.MVVM.Template {

    public class InputView : UnityGuiView<InputFieldViewModel> {
        public InputField Input;

        protected override void OnInitialize() {
            base.OnInitialize();

            if (Input) {
                Input.onValueChanged.AddListener(setValue);
            }
        }

        private void setValue(string arg0) {
            if (BindingContext) {
                BindingContext.SetValue(arg0);
            }
        }

        public override void OnBindingContextChanged(InputFieldViewModel oldValue, InputFieldViewModel newValue) {
            base.OnBindingContextChanged(oldValue, newValue);
            if (oldValue) {
                oldValue.OnValueChanged -= onValueChanged;
            }
            if (newValue) {
                newValue.OnValueChanged += onValueChanged;
            }
        }

        protected virtual void onValueChanged(object oldValue, object newValue) {
            Input.text = newValue != null ? newValue.ToString() : string.Empty;
        }
    }
}