﻿using System;
using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.UI;

namespace GameLibrary.MVVM.Template {

    public class PopView : UnityGuiView<MessageViewModel> {
        public Text Title;
        public Text Content;
        public Button BtnClose;
        public Button BtnConfirm;
        public GameObject Footer;

        protected override void OnInitialize() {
            base.OnInitialize();

            destroyOnHide = true;

            Binder.Add<string>("Title", onTitleChanged);
            Binder.Add<string>("Content", onContentChanged);
            Binder.Add<bool>("HasFooter", onFooterChanged);
            gameObject.SetActive(false);

            if (BtnClose) {
                BtnClose.onClick.AddListener(() => {
                    BindingContext.OnCancel.TryInvoke();
                    Hide();
                });
            }
            if (BtnConfirm) {
                BtnConfirm.onClick.AddListener(() => BindingContext.OnConfirm.TryInvoke());
            }
        }

        private void onFooterChanged(bool oldValue, bool newValue) {
            if (Footer) {
                Footer.SetActive(newValue);
            }
        }

        private void onContentChanged(string oldValue, string newValue) {
            trySetValue<Text>("Content", t => t.text = newValue);
        }

        private void onTitleChanged(string oldValue, string newValue) {
            trySetValue<Text>("Title", t => t.text = newValue);
        }
    }
}