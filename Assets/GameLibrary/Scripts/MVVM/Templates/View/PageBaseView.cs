﻿using System;
using System.Collections.Generic;
using GameLibrary.UI;
using UnityEngine;
using UnityEngine.UI;

namespace GameLibrary.MVVM.Template {

    public class PageBaseView : UnityGuiView<ViewModelBase> {
#if UNITY_EDITOR

        public void TestInitialize() {
            BindingContext = new ViewModelBase();
        }

#endif

        public Text Title;

        protected UnityGuiView currentPage;
        protected Dictionary<ViewData, UnityGuiView> Pages = new Dictionary<ViewData, UnityGuiView>();

        protected override void OnInitialize() {
            base.OnInitialize();

            PageSettings.Initialize(gameObject);
            Binder.Add<string>("Title", onTitleChanged);

            FieldBinder.Bind(gameObject);
        }

        private void onTitleChanged(string oldValue, string newValue) {
            trySetValue<Text>("Title", t => t.text = newValue);
        }

        public override void onSubViewOpened(ViewData data, GameObject go) {
            UnityGuiView page = go.GetComponent<UnityGuiView>();

            if (page) {
                if (currentPage && currentPage != page) {
                    currentPage.Hide(data.ShowImmediately);
                }
                currentPage = page;
            }
        }
    }
}