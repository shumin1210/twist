﻿using UnityEngine;

namespace GameLibrary.MVVM.Template {

    public class SlideData : ModelBase {
        public Sprite Image;
        public ViewData LinkData;
    }
}