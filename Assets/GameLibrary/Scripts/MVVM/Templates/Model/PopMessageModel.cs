﻿using UnityEngine;
using UnityEngine.Events;

namespace GameLibrary.MVVM.Template {

    [CreateAssetMenu(fileName = "Msg_", menuName = "MVVM/MessageData")]
    public class PopMessageModel : ModelBase {
        public string Title;

        public string Content;

        public bool HasFooter;

        public UnityEvent OnConfirm = new UnityEvent();
        public UnityEvent OnCancel = new UnityEvent();
        public UnityEvent OnClose = new UnityEvent();

        public static PopMessageModel Create(string title = "", string content = "", bool hasFooter = false
            , UnityAction onConfirm = null, UnityAction onCancel = null, UnityAction onClose = null) {
            PopMessageModel data = CreateInstance<PopMessageModel>();
            data.Title = title;
            data.Content = content;
            data.HasFooter = hasFooter;
            if (onConfirm != null) {
                data.OnConfirm.AddListener(onConfirm);
            }
            if (onCancel != null) {
                data.OnCancel.AddListener(onCancel);
            }
            if (onClose != null) {
                data.OnClose.AddListener(onClose);
            }
            return data;
        }
    }
}