﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameLibrary.Utility;
using UnityEngine;

namespace GameLibrary.MVVM.Template {

    [CreateAssetMenu(fileName = "DataSource", menuName = "MVVM/ListDataSource")]
    public class ListDataSource : ModelBase {
        public string DataKey;
        public string EventName;
        public bool GameHandlerFirst = true;

        public IEnumerable<object> Data {
            get {
                return (DataShared.Instance.Data[DataKey] as IList).OfType<object>();
            }
        }

        public override void Initialize(params object[] args) {
            base.Initialize(args);

            if (args.Length > 0) {
                DataKey = (string)(args[0] as DynamicValue).Value;
            }
            if (args.Length > 1) {
                EventName = (string)(args[1] as DynamicValue).Value;
            }
            if (args.Length > 2) {
                GameHandlerFirst = (bool)(args[2] as DynamicValue).Value;
            }
        }
    }
}