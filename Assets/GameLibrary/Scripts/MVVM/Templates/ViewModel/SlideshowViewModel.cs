﻿using System.Collections.Generic;
using System.Linq;
using GameLibrary.MVVM.Template;

namespace GameLibrary.MVVM.Template {

    public class SlideshowViewModel : ViewModelBase {
        public BindableProperty<SlideData[]> SlideDatas = new BindableProperty<SlideData[]>();

        public override void InitializeFromModel(object data) {
            base.InitializeFromModel(data);

            if (data is IEnumerable<SlideData>) {
                SlideDatas.Value = (data as IEnumerable<SlideData>).ToArray();
            }
        }
    }
}