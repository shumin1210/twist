﻿using GameLibrary.Core;

namespace GameLibrary.MVVM.Template {

    public class PlayerInfoViewModel : ViewModelBase {
        public BindableProperty<string> Name = new BindableProperty<string>();
        public BindableProperty<string> Photo = new BindableProperty<string>();
        public BindableProperty<int> Gender = new BindableProperty<int>();
        public BindableProperty<decimal> Money = new BindableProperty<decimal>();

        public override void InitializeFromModel(object data) {
            if (data is Player) {
                Player d = data as Player;
                Name.Value = d.Name;
                Photo.Value = d.Photo;
                Gender.Value = d.Gender;
                Money.Value = d.UD;
            }
        }
    }
}