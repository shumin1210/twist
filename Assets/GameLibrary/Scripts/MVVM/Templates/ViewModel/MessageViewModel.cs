﻿using System;
using System.Collections.Generic;
using GameLibrary.Utility;

namespace GameLibrary.MVVM.Template {

    /// <summary>
    /// 訊息視窗ViewModel
    /// </summary>
    /// <see cref="PopMessageModel"/>
    public class MessageViewModel : ViewModelBase {
        public BindableProperty<string> Content = new BindableProperty<string>();
        public BindableProperty<bool> HasFooter = new BindableProperty<bool>();

        public Action OnConfirm;

        public Action OnCancel;

        public Action OnClose;

        public override void InitializeFromData(IDictionary<string, string> data) {
            Title.Value = data.TryGet("Title");
            Content.Value = data.TryGet("Content").Replace("<br>", "\r\n");
            HasFooter.Value = TConvert.ToBoolean(data.TryGet("HasFooter"));
        }

        public override void InitializeFromModel(object data) {
            PopMessageModel o = data as PopMessageModel;
            if (o != null) {
                Title.Value = o.Title;
                Content.Value = o.Content.Replace("<br>", "\r\n");
                HasFooter.Value = o.HasFooter;
                OnConfirm += o.OnConfirm.Invoke;
                OnCancel += o.OnCancel.Invoke;
                OnClose += o.OnClose.Invoke;
            }
        }

        public override void Initialize(params object[] args) {
            Title.Value = args[0].ToString();
            Content.Value = args[1].ToString().Replace("<br>", "\r\n");
            if (args.Length > 2) {
                HasFooter.Value = TConvert.ToBoolean(args[2]);
            } else {
                HasFooter.Value = true;
            }
        }

        public override void OnDestory() {
            base.OnDestory();

            OnClose.TryInvoke();
        }
    }
}