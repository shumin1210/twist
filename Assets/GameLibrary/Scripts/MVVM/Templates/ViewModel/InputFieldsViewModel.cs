﻿using System;
using System.Collections.Generic;
using GameLibrary.Utility;

namespace GameLibrary.MVVM.Template {

    public class InputFieldViewModel : ViewModelBase {
        private string key;
        private string field;
        private object value;
        private Type srcType;
        private object refSrc;
        public Type ValueType;

        public object Value {
            get {
                return value;
            }
        }

        public delegate void ValueChangeHandler(object oldVal, object newVal);

        public ValueChangeHandler OnValueChanged;

        public override void Initialize(params object[] args) {
            base.Initialize(args);
            key = args[0].ToString();
            field = args[1].ToString();
            getValue();
        }

        public override void InitializeFromData(IDictionary<string, string> data) {
            base.InitializeFromData(data);
            key = data["Key"];
            field = data["Field"];
            getValue();
        }

        private void getValue() {
            object src = null, val = null, old = value;
            if (DataShared.Instance.Data.TryGet(key, ref src)
                && ReflectionUtility.TryGetValue(src, field, out val)) {
                srcType = src.GetType();
                refSrc = src;
                value = val;
                ValueType = val.GetType();
            } else {
                value = default(object);
            }
            tryTriggerOnValueChanged(old);
        }

        private void tryTriggerOnValueChanged(object old) {
            if (OnValueChanged != null) {
                OnValueChanged.Invoke(old, value);
            }
        }

        public void SetValue(object newVal) {
            object old = value;
            try {
                object o = TConvert.ToGeneric(ValueType, newVal);
                if (o != null) {
                    value = o;
                    ReflectionUtility.GetMember(srcType, field).SetValue(refSrc, value);
                }
                tryTriggerOnValueChanged(old);
            } catch (Exception) {
            }
        }
    }
}