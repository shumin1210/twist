﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameLibrary.Core;
using GameLibrary.MVVM;
using UnityEngine;

namespace GameLibrary.MVVM.Template {

    /// <summary>
    /// in dev
    /// 清單VM
    /// </summary>
    public class ListViewModel : ViewModelBase {
        private ListDataSource dataSource;
        private bool eventInitialized = false;

        public delegate void OnDataChangedHandler(List<object> newValue);

        public OnDataChangedHandler OnDataChanged;
        private List<object> tempData;
        private DataFilter currentFilter;

        public DataFilter Filter {
            get {
                return currentFilter;
            }
            set {
                if (value != currentFilter) {
                    currentFilter = value;
                    onListUpdate(null);
                }
            }
        }

        public int DataCount {
            get {
                return tempData == null ? 0 : tempData.Count;
            }
        }

        public object this[int index] {
            get {
                return index < DataCount ? tempData[index] : null;
            }
        }

        public override void InitializeFromModel(object data) {
            base.InitializeFromModel(data);
            if (data is ListDataSource) {
                dataSource = data as ListDataSource;
                eventPather();
            }
        }

        private void eventPather(bool add = true) {
            if (dataSource) {
                Net.MsgHandler handler = dataSource.GameHandlerFirst ? ParentHandler.HandlerInstance : (ParentHandler.SubHandler ?? ParentHandler.Instance);
                if (handler) {
                    if (add) {
                        handler.EventHandlers.Add(dataSource.EventName, onListUpdate);
                    } else {
                        handler.EventHandlers.Remove(dataSource.EventName, onListUpdate);
                    }
                }
            }
        }

        ~ListViewModel() {
            eventPather(false);
        }

        private void onListUpdate(object[] obj) {
            if (Filter) {
                tempData = dataSource.Data.Where(Filter).ToList();
            } else {
                tempData = dataSource.Data.ToList();
            }
            OnDataChanged.Invoke(tempData);
        }
    }
}