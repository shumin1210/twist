﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GameLibrary.MVVM.Template {
    public class DataAdaptorViewModel : ViewModelBase {
        public BindableProperty<ModelBase> Data = new BindableProperty<ModelBase>();

        public override void InitializeFromModel(object data) {
            base.InitializeFromModel(data);

            if (data is ModelBase) {
                Data.Value = data as ModelBase;
            }
        }
    }
}