﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameLibrary.Utility;
using GameLibrary.Utility.SerializableDictionary;
using UnityEngine;
namespace GameLibrary.MVVM.Template {
    [Serializable]
    public class FilterMap : DataMap<string, FilterMethod> {
    }

    [Serializable]
    [CreateAssetMenu(fileName = "Filter_", menuName = "MVVM/DataFilter")]
    public class DataFilter : ScriptableObject, IWhereCondition {
        public string Name;
        public bool IgnoreNull = true;
        public FilterMap Filters;

        bool IWhereCondition.Match(object a) {
            if (a == null) {
                if (IgnoreNull) {
                    return false;
                }
            }

            foreach (KeyValuePair<string, FilterMethod> item in Filters) {
                object val;
                if (!tryGetValue(a, item.Key, out val)) {
                    return false;
                }
                if (!item.Value.Match(val)) {
                    return false;
                }
            }
            return true;
        }

        private bool tryGetValue(object src, string key, out object val) {
            val = null;
            if (src == null) {
                return false;
            }
            Type type = src.GetType();
            var prop = type.GetProperty(key);
            var field = type.GetField(key);
            if (prop != null) {
                val = prop.GetValue(src, new object[0]);
                return true;
            } else if (field != null) {
                val = field.GetValue(src);
                return true;
            }
            return false;
        }
    }

    [Serializable]
    public class FilterMethod {
        public enum MethodType {
            Exceed,
            Smaller,
            Between
        }

        public enum FieldType {
            Number,
            Enum,
            Text,
            Bool,
            DateTime
        }

        public bool Equal = true;
        public MethodType Method;
        public FieldType Field;
        public string Value1;
        public string Value2;

        public bool Match(object val) {
            try {
                switch (Field) {
                    case FieldType.Number:
                        return checkNumber(val);

                    case FieldType.Enum:
                        return checkEnum(val);

                    default:
                    case FieldType.Text:
                        return checkString(val);

                    case FieldType.Bool:
                        return checkBool(val);

                    case FieldType.DateTime:
                        return checkDateTime(val);
                }
            } catch (Exception) {
                return false;
            }
        }

        private bool checkDateTime(object val) {
            DateTime s1, s2, t = Convert.ToDateTime(val);
            DateTime.TryParse(Value1, out s1);
            DateTime.TryParse(Value2, out s2);

            if (Equal && t == s1) {
                return true;
            }
            switch (Method) {
                case MethodType.Exceed:
                    return t > s1;

                case MethodType.Smaller:
                    return t < s1;

                case MethodType.Between:
                    if (Equal) {
                        return t >= s1 && t <= s2;
                    } else {
                        return t > s1 && t < s2;
                    }
                default:
                    return false;
            }
        }

        private bool checkBool(object val) {
            return Equal == (bool)val;
        }

        private bool checkString(object val) {
            string t = val as string;
            return t == Value1;
        }

        private bool checkEnum(object val) {
            Type g = val.GetType();
            int s1, s2, t = (int)Convert.ChangeType(val, g);
            if (!int.TryParse(Value1, out s1)) {
                s1 = (int)Enum.Parse(g, Value1);
            }
            if (!int.TryParse(Value2, out s2)) {
                s2 = (int)Enum.Parse(g, Value2);
            }
            if (Equal) {
                return t == s1;
            }
            switch (Method) {
                case MethodType.Exceed:
                    return t > s1;

                case MethodType.Smaller:
                    return t < s1;

                case MethodType.Between:
                    if (Equal) {
                        return t >= s1 && t <= s2;
                    } else {
                        return t > s1 && t < s2;
                    }
                default:
                    return false;
            }
        }

        private bool checkNumber(object val) {
            decimal s1 = TConvert.ToGeneric<decimal>(Value1);
            decimal s2 = TConvert.ToGeneric<decimal>(Value2);

            decimal t = Convert.ToDecimal(val);
            if (Equal && s1 == t) {
                return true;
            }
            switch (Method) {
                default:
                case MethodType.Exceed:
                    return t > s1;

                case MethodType.Smaller:
                    return t < s1;

                case MethodType.Between:
                    if (Equal) {
                        return t >= s1 && t <= s2;
                    } else {
                        return t > s1 && t < s2;
                    }
            }
        }
    }
}