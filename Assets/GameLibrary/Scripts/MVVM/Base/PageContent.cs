﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameLibrary.MVVM.Template;
using GameLibrary.Utility;
using GameLibrary.Utility.SerializableDictionary;
using UnityEngine;
using UnityEngine.UI;

namespace GameLibrary.MVVM {

    public class PageContent : MonoBehaviour {

        public void KillView(string name) {
            PageContentCapability.KillView(name);
        }

        public void KillGlobalView(string name) {
            PageContentCapability.KillGlobalView(name);
        }
    }

    [Serializable]
    public class LinkBtnMap : DataMap<Button, ViewData> {
    }

    [Serializable]
    public class ViewMap : DataMap<string, ViewData> {
    }

    public interface IPageContetCapability {

        [SerializeField]
        PageContentCapability PageSettings {
            get; set;
        }

        void onSubViewOpened(ViewData data, GameObject go);
    }

    [Serializable]
    public class PageContentCapability {
        private static HashList<UnityGuiView> TopViews = new HashList<UnityGuiView>();

        public ViewMap Views;
        public LinkBtnMap LinkBtns;
        public GameObjectMap Containers;

        public bool initialized {
            get; private set;
        }

        private Dictionary<ViewData, GameObject> createdViews = new Dictionary<ViewData, GameObject>();

        [NonSerialized]
        public ViewData data;

        protected GameObject root;
        private static Dictionary<string, List<GameObject>> globalViews = new Dictionary<string, List<GameObject>>();
        private Dictionary<string, List<GameObject>> subViews = new Dictionary<string, List<GameObject>>();
        private Dictionary<string, List<ViewData>> groups = new Dictionary<string, List<ViewData>>();

        public void Initialize(GameObject root) {
            if (initialized) {
                return;
            }
            initialized = true;
            this.root = root;
            //bind button events
            LinkBtns.Foreach(b => b.Key.onClick.AddListener(() => OpenLink(b.Value)));

            if (data == null) {
                return;
            }

            //create sub views
            foreach (KeyValuePair<string, ViewData> sub in data.SubViews) {
                string key = sub.Key;
                ViewData layoutData = sub.Value;

                GameObject go = createSubView(layoutData);
                bindData(layoutData, go);
            }
        }

        private GameObject createSubView(ViewData layoutData) {
            GameObject container = Containers.ContainsKey(layoutData.RectName) ? Containers[layoutData.RectName] : root;
            GameObject go = GameObjectRelate.InstantiateGameObject(container, layoutData.Prefab);
            IPageContetCapability pageContent = go.GetComponent<IPageContetCapability>();
            if (pageContent != null) {
                pageContent.PageSettings.data = layoutData;
            }
            return go;
        }

        private static void bindData(ViewData data, GameObject go, params object[] args) {
            UnityGuiView view = go.GetComponent<UnityGuiView>();
            if (view) {
                ViewModelBase viewModel = DataShared.Instance.GetData<ViewModelBase>(data.ViewModelKey, ignoreCatagory: true);
                if (viewModel) {
                    view.BindingContext = viewModel;
                } else if (data.ViewModelType.Type != null) {
                    var o = Activator.CreateInstance(data.ViewModelType.Type);
                    view.BindingContext = viewModel = o as ViewModelBase;
                } else {
                    viewModel = view.BindingContext = new ViewModelBase();
                }
                if (viewModel && !string.IsNullOrEmpty(data.ViewModelKey)) {
                    viewModel.ViewModelDataKey = data.ViewModelKey;
                    DataShared.Instance.Data.Set(data.ViewModelKey, viewModel);
                }

                object model = getModelInstance(data);
                tryInitilizeViewModel(viewModel, args, model, data);

                if (string.IsNullOrEmpty(viewModel.Title.Value)) {
                    viewModel.Title.Value = data.Title;
                }
            }
        }

        private static void tryInitilizeViewModel(ViewModelBase viewmodel, object[] args, object model, ViewData vd) {
            if (args.Length > 0) {
                viewmodel.Initialize(args);
            } else if (vd.ViewModelArgs.Count > 0) {
                viewmodel.InitializeFromData(vd.ViewModelArgs);
            } else if (model != null) {
                viewmodel.InitializeFromModel(model);
            } else {
                viewmodel.Initialize();
            }
            if (vd.Data.Count > 0) {
                viewmodel.LoadData(vd.Data);
            }
        }

        private static object getModelInstance(ViewData data) {
            object model = null;
            if (data.modelData) {
                model = data.modelData;
            } else if (!string.IsNullOrEmpty(data.ModelKey)) {
                model = DataShared.Instance.GetData(data.ModelKey, ignoreCatagory: true);
            } else if (data.ModelType.Type != null) {
                model = Activator.CreateInstance(data.ModelType.Type);
                if (model is ModelBase) {
                    if (data.ModelArgs.Length == 0) {
                        (model as ModelBase).Initialize();
                    } else {
                        (model as ModelBase).Initialize(data.ModelArgs);
                    }
                }
            }
            if (model != null && model is ModelBase) {
                (model as ModelBase).OnInitialized();
            }
            return model;
        }

        private GameObject getInstance(ViewData data) {
            switch (data.ActOnDuplicate) {
                default:
                case ActionOnDuplicate.NoAction:
                    return createInstance(data);

                case ActionOnDuplicate.UseNewOne:
                    if (isViewExist(data)) {
                        List<GameObject> views = (data.Global ? globalViews : subViews)[data.Key];
                        views.Foreach(UnityEngine.Object.DestroyImmediate);
                    }
                    return createInstance(data);

                case ActionOnDuplicate.UsePreviousOne:
                    if (isViewExist(data)) {
                        List<GameObject> views = (data.Global ? globalViews : subViews)[data.Key];
                        if (views.Count > 0 && views[0]) {
                            return views[0];
                        } else {
                            views.RemoveAt(0);
                        }
                    }
                    return createInstance(data);
            }
        }

        private bool isViewExist(ViewData data) {
            var list = data.Global ? globalViews : subViews;
            var str = list.Find(v => v.Key == data.Key).Key;
            if (!string.IsNullOrEmpty(str)) {
                if (list[str].Count > 0) {
                    return true;
                } else {
                    list.Remove(str);
                    return false;
                }
            }
            return false;
        }

        private GameObject createInstance(ViewData data) {
            GameObject go = null;
            Dictionary<string, List<GameObject>> views = data.Global ? globalViews : subViews;

            if (!views.ContainsKey(data.Key)) {
                views.Add(data.Key, new List<GameObject>());
            }
            Transform container = null;
            if (data.OpenNewPage) {
                container = root.transform.FindParentWithTag("GameRoot");
            } else {
                container = Containers.ContainsKey(data.RectName) ? Containers[data.RectName].transform : root.transform;
            }

            go = GameObjectRelate.InstantiateGameObject(container.gameObject, data.Prefab);
            go.name = data.Prefab.name;
            //go = UnityEngine.Object.Instantiate(data.Prefab, data.Prefab.transform.position, data.Prefab.transform.rotation, container);
            views[data.Key].Add(go);

            createdViews[data] = go;
            return go;
        }

        public GameObject OpenLink(ViewData data, params object[] args) {
            return OpenLink(data, onComplete: null, onCreated: null, args: args);
        }

        public GameObject OpenLink(ViewData data, Action<GameObject> onComplete = null, Action<GameObject> onCreated = null, GameObject targetContainer = null, ModelBase model = null, params object[] args) {
            GameObject go = getInstance(data);
            GameObject _root = root;
            GameObjectMap containers = Containers;
            if (model != null) {
                data.modelData = model;
            }

            if (onCreated != null) {
                onCreated(go);
            }
            bindData(data, go, args);
            IPageContetCapability view = _root.GetComponent<IPageContetCapability>();

            if (!string.IsNullOrEmpty(data.ParentName)) {
                _root = root.transform.FindParentByName(data.ParentName).gameObject;
                view = _root.GetComponent<IPageContetCapability>();
                if (view != null) {
                    containers = view.PageSettings.Containers;
                    Transform container = (containers.ContainsKey(data.RectName)) ? containers[data.RectName].transform : _root.transform;
                    GameObjectRelate.SetParentAndResetRect(container.gameObject, go);
                }
            }

            if (targetContainer) {
                GameObjectRelate.SetParentAndResetRect(targetContainer, go);
            } else if (data.IgnoreParentNode) {
                GameObjectRelate.SetParentAndResetRect(_root.transform.FindParentWithTag("GameRoot").gameObject, go);
            }

            UnityGuiView rootPage = _root.GetComponent<UnityGuiView>();
            UnityGuiView page = go.GetComponent<UnityGuiView>();

            if (rootPage) {
                rootPage.Children.Add(go);
                if (page) {
                    page.OnRelease += () => rootPage.Children.Remove(go);
                }
            }

            if (page) {
                if (data.OpenNewPage) {
                    hideTopView();
                    TopViews.Add(page);
                    page.OnRelease += () => removeTopView(page);
                }

                page.OnRelease += () => {
                    Dictionary<string, List<GameObject>> list = data.Global ? globalViews : subViews;
                    if (list.ContainsKey(data.Key)) {
                        if (list[data.Key].Contains(go)) {
                            list[data.Key].Remove(go);
                        }
                    }
                };
                page.Reveal(data.ShowImmediately);
            }
            if (view != null) {
                view.onSubViewOpened(data, go);
            }
            if (onComplete != null) {
                onComplete(go);
            }
            return go;
        }

        private void hideTopView() {
            while (TopViews.Count > 0 && !TopViews.Last()) {
                TopViews.RemoveAt(TopViews.Count - 1);
            }
            if (TopViews.Count > 0 && TopViews.Last()) {
                //if (!TopViews.Peek().destroyOnHide) {
                TopViews.Last().Hide(immediate: true);
                //}
            }
        }

        private void removeTopView(UnityGuiView go) {
            try {
                if (TopViews.Last() == go) {
                    do {
                        TopViews.RemoveAt(TopViews.Count - 1);
                    } while (TopViews.Count > 0 && (!TopViews.Last() || TopViews.Last() == go));
                    if (TopViews.Count > 0) {
                        TopViews.Last().GetComponent<UnityGuiView>().Reveal(immediate: true);
                    }
                }
            } catch (Exception) {
            }
        }

        public GameObject OpenView(string key, Action<GameObject> onComplete = null, Action<GameObject> onCreated = null, ModelBase model = null, params object[] args) {
            if (!Views.ContainsKey(key)) {
                return null;
            }
            return OpenLink(data: Views[key], onComplete: onComplete, onCreated: onCreated, model: model, args: args);
        }

        public GameObject OpenView(string key, params object[] args) {
            if (!Views.ContainsKey(key)) {
                return null;
            }
            return OpenLink(Views[key], args: args);
        }

        public GameObject OpenView(ViewData d, params object[] args) {
            return OpenLink(d, args: args);
        }

        public GameObject OpenView(string key, ModelBase data) {
            if (!Views.ContainsKey(key)) {
                return null;
            }
            Views[key].modelData = data;
            return OpenLink(Views[key]);
        }

        public static void KillView(string name) {
            var g = GameObject.Find(name);
            var p = g.GetComponent<UnityGuiView>();
            if (p) {
                p.Close();
            } else {
                UnityEngine.Object.Destroy(g);
            }
        }

        public static void KillGlobalView(string name) {
            var g = globalViews.Find(a => a.Key == name);
            if (g.Value == null) {
                return;
            }
            g.Value.ForEach(a => {
                if (!a) {
                    return;
                }
                var p = a.GetComponent<UnityGuiView>();
                if (p) {
                    p.Close();
                } else {
                    UnityEngine.Object.Destroy(a);
                }
            });
        }

        public void Hide(params ViewData[] args) {
            args.Foreach(a => {
                if (!createdViews.ContainsKey(a)) {
                    return;
                }
                GameObject o = createdViews[a];
                if (o) {
                    UnityGuiView u = o.GetComponent<UnityGuiView>();
                    if (u) {
                        u.Hide(true);
                    }
                }
            });
        }

        public void Close(params ViewData[] args) {
            args.Foreach(a => {
                if (!createdViews.ContainsKey(a)) {
                    return;
                }
                GameObject o = createdViews[a];
                createdViews.Remove(a);
                if (o) {
                    var u = o.GetComponent<UnityGuiView>();
                    if (u) {
                        u.Close();
                    } else {
                        UnityEngine.Object.Destroy(o);
                    }
                }
            });
        }

        public void Close(string view) {
            if (!Views.ContainsKey(view)) {
                return;
            }
            Close(Views[view]);
        }

        public void CloseAll(bool includeGlobal = false) {
            if (includeGlobal) {
                globalViews.Foreach(a => a.Value.Foreach(v => UnityEngine.Object.Destroy(v)));
                globalViews.Clear();
            }
            subViews.Foreach(a => a.Value.Foreach(v => UnityEngine.Object.Destroy(v)));
            subViews.Clear();
            TopViews.Clear();
        }
    }
}