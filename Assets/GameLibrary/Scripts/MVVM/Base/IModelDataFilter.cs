﻿using System.Collections.Generic;
using System.Linq;
using GameLibrary.MVVM;

namespace GameLibrary.MVVM.Template {

    public static class IWhereConditionExtends {

        public static IEnumerable<T> Where<T>(this IEnumerable<T> src, IWhereCondition con) {
            return src.Where(o => con.Match(o));
        }
    }

    public interface IWhereCondition {

        bool Match(object a);
    }

    public interface IModelDataFilter<T> : IWhereCondition {

        bool Match(T gs);

        IEnumerable<T> Group(IEnumerable<T> src);
    }
}