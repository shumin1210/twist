﻿using System;
using System.Collections.Generic;
using GameLibrary.Utility.SerializableDictionary;

namespace GameLibrary.MVVM {

    [Serializable]
    public class ViewModelBase {
        private bool _isInitialized;
        public string ViewModelDataKey;
        public BindableProperty<string> Title = new BindableProperty<string>();

        public ViewModelBase ParentViewModel {
            get; set;
        }

        public bool IsRevealed {
            get; private set;
        }

        public bool IsRevealInProgress {
            get; private set;
        }

        public bool IsHideInProgress {
            get; private set;
        }

        protected virtual void OnInitialize() {
        }

        public virtual void OnStartReveal() {
            IsRevealInProgress = true;
            if (!_isInitialized) {
                OnInitialize();
                _isInitialized = true;
            }
        }

        public virtual void OnFinishReveal() {
            IsRevealInProgress = false;
            IsRevealed = true;
        }

        public virtual void OnStartHide() {
            IsHideInProgress = true;
        }

        public virtual void OnFinishHide() {
            IsHideInProgress = false;
            IsRevealed = false;
        }

        public virtual void OnDestory() {
        }

        public virtual void LoadData(UObjectMap data) {
        }

        public virtual void Initialize() {
        }

        public virtual void Initialize(params object[] args) {
        }

        public virtual void InitializeFromData(IDictionary<string, string> data) {
        }

        public virtual void InitializeFromModel(object data) {
        }

        public static implicit operator bool(ViewModelBase b) {
            return b != null;
        }
    }

    public static class ViewModelBaseExtensions {

        public static IEnumerable<T> Ancestors<T>(this ViewModelBase origin) where T : ViewModelBase {
            if (origin == null) {
                yield break;
            }
            var parentViewModel = origin.ParentViewModel;
            while (parentViewModel != null) {
                var castedViewModel = parentViewModel as T;
                if (castedViewModel != null) {
                    yield return castedViewModel;
                }
                parentViewModel = parentViewModel.ParentViewModel;
            }
        }
    }
}