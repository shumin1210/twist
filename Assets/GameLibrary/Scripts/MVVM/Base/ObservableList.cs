﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace GameLibrary.MVVM {

    public class ObservableDictionary<T, U> : IDictionary<T, U> {

        public delegate void AddHandler(T key, U instance);

        public delegate void RemoveHandler(T key);

        public delegate void UpdateHandler(T key, U oldVal, U newVAl);

        public delegate void ValueChangedHandler(Dictionary<T, U> oldValue, Dictionary<T, U> newValue);

        public AddHandler OnAdd;
        public RemoveHandler OnRemove;
        public UpdateHandler OnUpdate;
        public ValueChangedHandler OnValueChanged;

        //预先初始化，防止空异常
        private Dictionary<T, U> _value = new Dictionary<T, U>();

        public int Count {
            get {
                return _value.Count;
            }
        }

        public ICollection<T> Keys {
            get {
                return _value.Keys;
            }
        }

        public Dictionary<T, U> Value {
            get {
                return _value;
            }
            set {
                if (!Equals(_value, value)) {
                    Dictionary<T, U> old = _value;
                    _value = value;
                    onValueChanged(old, _value);
                }
            }
        }

        public ICollection<U> Values {
            get {
                return _value.Values;
            }
        }

        public bool IsReadOnly {
            get {
                return ((IDictionary<T, U>)_value).IsReadOnly;
            }
        }

        public U this[T key] {
            get {
                return _value[key];
            }

            set {
                if (_value.ContainsKey(key)) {
                    U oldVal = _value[key];
                    _value[key] = value;
                    onUpdate(key, oldVal, value);
                } else {
                    _value[key] = value;
                    onAdd(key, value);
                }
            }
        }

        public void Add(KeyValuePair<T, U> item) {
            _value.Add(item.Key, item.Value);
            onAdd(item.Key, item.Value);
        }

        public void Add(T key, U value) {
            _value.Add(key, value);
            onAdd(key, value);
        }

        public void Clear() {
            _value.Clear();
        }

        public bool Contains(KeyValuePair<T, U> item) {
            return _value.ContainsKey(item.Key) && _value[item.Key].Equals(item.Value);
        }

        public bool ContainsKey(T key) {
            return _value.ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<T, U>[] array, int arrayIndex) {
            ((IDictionary<T, U>)_value).CopyTo(array, arrayIndex);
        }

        public IEnumerator<KeyValuePair<T, U>> GetEnumerator() {
            return _value.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return _value.GetEnumerator();
        }

        public bool Remove(KeyValuePair<T, U> item) {
            if (((IDictionary<T, U>)_value).Remove(item)) {
                onRemove(item.Key);
                return true;
            } else {
                return false;
            }
        }

        public bool Remove(T key) {
            if (_value.Remove(key)) {
                onRemove(key);
                return true;
            } else {
                return false;
            }
        }

        public void SilentSetValue(Dictionary<T, U> val) {
            _value = val;
        }

        public bool TryGetValue(T key, out U value) {
            return _value.TryGetValue(key, out value);
        }

        private void onAdd(T key, U val) {
            if (OnAdd != null) {
                OnAdd(key, val);
            }
        }

        private void onRemove(T key) {
            if (OnRemove != null) {
                OnRemove(key);
            }
        }

        private void onUpdate(T key, U oldVal, U newVal) {
            if (OnUpdate != null) {
                OnUpdate(key, oldVal, newVal);
            }
        }

        private void onValueChanged(Dictionary<T, U> oldValue, Dictionary<T, U> newValue) {
            if (OnValueChanged != null) {
                OnValueChanged(oldValue, newValue);
            }
        }
    }

    public class ObservableHashSet<T> : ICollection<T>, IEnumerable<T> {

        public delegate void AddHandler(T instance);

        public delegate void RemoveHandler(T instance);

        public delegate void ValueChangedHandler(HashSet<T> oldValue, HashSet<T> newValue);

        public AddHandler OnAdd;
        public RemoveHandler OnRemove;
        public ValueChangedHandler OnValueChanged;

        //预先初始化，防止空异常
        private HashSet<T> _value = new HashSet<T>();

        public int Count {
            get {
                return _value.Count;
            }
        }

        public bool IsReadOnly {
            get; private set;
        }

        public HashSet<T> Value {
            get {
                return _value;
            }
            set {
                if (!Equals(_value, value)) {
                    HashSet<T> old = _value;
                    _value = value;
                    ValueChanged(old, _value);
                }
            }
        }

        public void Add(T item) {
            _value.Add(item);
            if (OnAdd != null) {
                OnAdd(item);
            }
        }

        public void Clear() {
            _value.Clear();
        }

        public bool Contains(T item) {
            return _value.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex) {
            _value.CopyTo(array, arrayIndex);
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return _value.GetEnumerator();
        }

        public IEnumerator<T> GetEnumerator() {
            return _value.GetEnumerator();
        }

        public bool Remove(T item) {
            if (_value.Remove(item)) {
                if (OnRemove != null) {
                    OnRemove(item);
                }
                return true;
            }
            return false;
        }

        public void SilentSetValue(HashSet<T> val) {
            _value = val;
        }

        private void ValueChanged(HashSet<T> oldValue, HashSet<T> newValue) {
            if (OnValueChanged != null) {
                OnValueChanged(oldValue, newValue);
            }
        }
    }

    public class ObservableList<T> : IList<T> {

        public delegate void AddHandler(T instance);

        public delegate void InsertHandler(int index, T instance);

        public delegate void RemoveHandler(T instance);

        public delegate void UpdateHandler(int index, T instance);

        public delegate void ValueChangedHandler(List<T> oldValue, List<T> newValue);

        public AddHandler OnAdd;
        public InsertHandler OnInsert;
        public RemoveHandler OnRemove;
        public UpdateHandler OnUpdate;
        public ValueChangedHandler OnValueChanged;

        //预先初始化，防止空异常
        private List<T> _value = new List<T>();

        public int Count {
            get {
                return _value.Count;
            }
        }

        public bool IsReadOnly {
            get; private set;
        }

        public List<T> Value {
            get {
                return _value;
            }
            set {
                if (!Equals(_value, value)) {
                    var old = _value;
                    _value = value;
                    ValueChanged(old, _value);
                }
            }
        }

        public T this[int index] {
            get {
                return _value[index];
            }
            set {
                _value[index] = value;
            }
        }

        public void Add(T item) {
            _value.Add(item);
            if (OnAdd != null) {
                OnAdd(item);
            }
        }

        public void Clear() {
            var old = _value.ToList();
            _value.Clear();
            if (OnValueChanged != null) {
                OnValueChanged(old, _value);
            }
        }

        public bool Contains(T item) {
            return _value.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex) {
            _value.CopyTo(array, arrayIndex);
        }

        public IEnumerator<T> GetEnumerator() {
            return _value.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }

        public int IndexOf(T item) {
            return _value.IndexOf(item);
        }

        public void Insert(int index, T item) {
            _value.Insert(index, item);
            if (OnInsert != null) {
                OnInsert(index, item);
            }
        }

        public bool Remove(T item) {
            if (_value.Remove(item)) {
                if (OnRemove != null) {
                    OnRemove(item);
                }
                return true;
            }
            return false;
        }

        public void RemoveAt(int index) {
            _value.RemoveAt(index);
        }

        public void SilentSetValue(List<T> val) {
            _value = val;
        }

        public void Update(int index, T item) {
            _value[index] = item;
            if (OnUpdate != null) {
                OnUpdate(index, item);
            }
        }

        private void ValueChanged(List<T> oldValue, List<T> newValue) {
            if (OnValueChanged != null) {
                OnValueChanged(oldValue, newValue);
            }
        }

        public void TriggerValueChangeEvent() {
            if (OnValueChanged != null) {
                ValueChanged(_value, _value);
            }
        }
    }
}