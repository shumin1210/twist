﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace GameLibrary.MVVM {

    public class PropertyBinder<T> where T : ViewModelBase {

        private delegate void BindHandler(T viewmodel);

        private delegate void UnBindHandler(T viewmodel);

        private readonly List<BindHandler> _binders = new List<BindHandler>();

        //internal void Add<T1>(string v, Action<T1, T1> p) {
        //    throw new NotImplementedException();
        //}

        private readonly List<UnBindHandler> _unbinders = new List<UnBindHandler>();

        public void AddDictionary<TKey, TElement>(string name,
            ObservableDictionary<TKey, TElement>.ValueChangedHandler valueChangedHandler = null,
            ObservableDictionary<TKey, TElement>.AddHandler valueAddHandler = null,
            ObservableDictionary<TKey, TElement>.RemoveHandler valueRemovedHandler = null,
            ObservableDictionary<TKey, TElement>.UpdateHandler valueUpdateHandler = null) {
            var fieldInfo = typeof(T).GetField(name, BindingFlags.Instance | BindingFlags.Public);
            if (fieldInfo == null) {
                throw new Exception(string.Format("Unable to find bindableproperty field '{0}.{1}'", typeof(T).Name, name));
            }
            if (valueChangedHandler != null) {
                _binders.Add(viewmodel => {
                    GetObservableDictionaryValue<TKey, TElement>(name, viewmodel, fieldInfo).OnValueChanged += valueChangedHandler;
                });

                _unbinders.Add(viewModel => {
                    GetObservableDictionaryValue<TKey, TElement>(name, viewModel, fieldInfo).OnValueChanged -= valueChangedHandler;
                });
            }

            if (valueAddHandler != null) {
                _binders.Add(viewmodel => {
                    GetObservableDictionaryValue<TKey, TElement>(name, viewmodel, fieldInfo).OnAdd += valueAddHandler;
                });

                _unbinders.Add(viewModel => {
                    GetObservableDictionaryValue<TKey, TElement>(name, viewModel, fieldInfo).OnAdd -= valueAddHandler;
                });
            }

            if (valueRemovedHandler != null) {
                _binders.Add(viewmodel => {
                    GetObservableDictionaryValue<TKey, TElement>(name, viewmodel, fieldInfo).OnRemove += valueRemovedHandler;
                });

                _unbinders.Add(viewModel => {
                    GetObservableDictionaryValue<TKey, TElement>(name, viewModel, fieldInfo).OnRemove -= valueRemovedHandler;
                });
            }
            if (valueUpdateHandler != null) {
                _binders.Add(viewmodel => {
                    GetObservableDictionaryValue<TKey, TElement>(name, viewmodel, fieldInfo).OnUpdate += valueUpdateHandler;
                });

                _unbinders.Add(viewModel => {
                    GetObservableDictionaryValue<TKey, TElement>(name, viewModel, fieldInfo).OnUpdate -= valueUpdateHandler;
                });
            }
        }

        public void AddList<TElement>(string name, Action onUpdate) {
            AddList<TElement>(
                name,
                valueChangedHandler: (o, n) => onUpdate(),
                valueAddHandler: o => onUpdate(),
                valueRemovedHandler: o => onUpdate(),
                valueUpdateHandler: (o, n) => onUpdate(),
                valueInsertHandler: (o, i) => onUpdate()
            );
        }

        public void AddList<TElement>(string name,
            ObservableList<TElement>.ValueChangedHandler valueChangedHandler = null,
            ObservableList<TElement>.AddHandler valueAddHandler = null,
            ObservableList<TElement>.InsertHandler valueInsertHandler = null,
            ObservableList<TElement>.RemoveHandler valueRemovedHandler = null,
            ObservableList<TElement>.UpdateHandler valueUpdateHandler = null) {
            var fieldInfo = typeof(T).GetField(name, BindingFlags.Instance | BindingFlags.Public);
            if (fieldInfo == null) {
                throw new Exception(string.Format("Unable to find bindableproperty field '{0}.{1}'", typeof(T).Name, name));
            }
            if (valueChangedHandler != null) {
                _binders.Add(viewmodel => {
                    GetObservableListValue<TElement>(name, viewmodel, fieldInfo).OnValueChanged += valueChangedHandler;
                });

                _unbinders.Add(viewModel => {
                    GetObservableListValue<TElement>(name, viewModel, fieldInfo).OnValueChanged -= valueChangedHandler;
                });
            }

            if (valueAddHandler != null) {
                _binders.Add(viewmodel => {
                    GetObservableListValue<TElement>(name, viewmodel, fieldInfo).OnAdd += valueAddHandler;
                });

                _unbinders.Add(viewModel => {
                    GetObservableListValue<TElement>(name, viewModel, fieldInfo).OnAdd -= valueAddHandler;
                });
            }
            if (valueInsertHandler != null) {
                _binders.Add(viewmodel => {
                    GetObservableListValue<TElement>(name, viewmodel, fieldInfo).OnInsert += valueInsertHandler;
                });

                _unbinders.Add(viewModel => {
                    GetObservableListValue<TElement>(name, viewModel, fieldInfo).OnInsert -= valueInsertHandler;
                });
            }
            if (valueRemovedHandler != null) {
                _binders.Add(viewmodel => {
                    GetObservableListValue<TElement>(name, viewmodel, fieldInfo).OnRemove += valueRemovedHandler;
                });

                _unbinders.Add(viewModel => {
                    GetObservableListValue<TElement>(name, viewModel, fieldInfo).OnRemove -= valueRemovedHandler;
                });
            }
            if (valueUpdateHandler != null) {
                _binders.Add(viewmodel => {
                    GetObservableListValue<TElement>(name, viewmodel, fieldInfo).OnUpdate += valueUpdateHandler;
                });

                _unbinders.Add(viewModel => {
                    GetObservableListValue<TElement>(name, viewModel, fieldInfo).OnUpdate -= valueUpdateHandler;
                });
            }
        }

        public void Add<TProperty>(string name, BindableProperty<TProperty>.ValueChangedHandler valueChangedHandler) {
            var fieldInfo = typeof(T).GetField(name, BindingFlags.Instance | BindingFlags.Public);
            if (fieldInfo == null) {
                throw new Exception(string.Format("Unable to find bindableproperty field '{0}.{1}'", typeof(T).Name, name));
            }

            _binders.Add(viewmodel => {
                GetPropertyValue<TProperty>(name, viewmodel, fieldInfo).OnValueChanged += valueChangedHandler;
            });

            _unbinders.Add(viewModel => {
                GetPropertyValue<TProperty>(name, viewModel, fieldInfo).OnValueChanged -= valueChangedHandler;
            });
        }

        private ObservableDictionary<TKey, TELement> GetObservableDictionaryValue<TKey, TELement>(string name, T viewModel, FieldInfo fieldInfo) {
            var value = fieldInfo.GetValue(viewModel);
            ObservableDictionary<TKey, TELement> result = value as ObservableDictionary<TKey, TELement>;
            if (result == null) {
                throw new Exception(string.Format("Illegal bindableproperty field '{0}.{1}' ", typeof(T).Name, name));
            }

            return result;
        }

        private ObservableList<TELement> GetObservableListValue<TELement>(string name, T viewModel, FieldInfo fieldInfo) {
            var value = fieldInfo.GetValue(viewModel);
            ObservableList<TELement> observableList = value as ObservableList<TELement>;
            if (observableList == null) {
                throw new Exception(string.Format("Illegal bindableproperty field '{0}.{1}' ", typeof(T).Name, name));
            }

            return observableList;
        }

        private BindableProperty<TProperty> GetPropertyValue<TProperty>(string name, T viewModel, FieldInfo fieldInfo) {
            var value = fieldInfo.GetValue(viewModel);
            BindableProperty<TProperty> bindableProperty = value as BindableProperty<TProperty>;
            if (bindableProperty == null) {
                throw new Exception(string.Format("Illegal bindableproperty field '{0}.{1}' ", typeof(T).Name, name));
            }

            return bindableProperty;
        }

        public void Bind(T viewmodel) {
            if (viewmodel != null) {
                for (int i = 0; i < _binders.Count; i++) {
                    _binders[i](viewmodel);
                }
            }
        }

        public void Unbind(T viewmodel) {
            if (viewmodel != null) {
                for (int i = 0; i < _unbinders.Count; i++) {
                    _unbinders[i](viewmodel);
                }
            }
        }
    }
}