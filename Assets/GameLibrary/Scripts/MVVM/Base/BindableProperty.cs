﻿namespace GameLibrary.MVVM {

    public class BindableProperty<T> {

        public delegate void ValueChangedHandler(T oldValue, T newValue);

        public ValueChangedHandler OnValueChanged;

        private T value;

        public T Value {
            get {
                return value;
            }
            set {
                if (!Equals(this.value, value)) {
                    ForceSetValue(value);
                }
            }
        }

        public void ForceSetValue(T val) {
            T old = value;
            value = val;
            ValueChanged(old, value);
        }

        public void SilentSetValue(T val) {
            value = val;
        }

        public void TriggerValueChangeEvent() {
            ValueChanged(value, value);
        }

        private void ValueChanged(T oldValue, T newValue) {
            if (OnValueChanged != null) {
                OnValueChanged(oldValue, newValue);
            }
        }

        public override string ToString() {
            return (Value != null ? Value.ToString() : "null");
        }
    }
}