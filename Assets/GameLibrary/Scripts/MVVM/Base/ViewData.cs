﻿using System;
using GameLibrary.Utility;
using GameLibrary.Utility.SerializableDictionary;
using UnityEngine;

namespace GameLibrary.MVVM.Template {

    [Serializable]
    public class LayoutData {
        public string RectName;
        public string DataKey;
        public GameObject prefab;
    }

    [Serializable]
    public class LayoutMap : DataMap<string, ViewData> {
    }

    [CreateAssetMenu(fileName = "View_", menuName = "MVVM/ViewData")]
    public class ViewData : ScriptableObject {
        public bool ShowImmediately = true;
        public bool OpenNewPage;
        public bool IgnoreParentNode = false;
        public string ParentName;
        public bool Global = false;
        public ActionOnDuplicate ActOnDuplicate = ActionOnDuplicate.NoAction;

        [SerializeField]
        private string key;

        public string Key {
            get {
                return string.IsNullOrEmpty(key) ? (name) : key;
            }
            set {
                key = value;
            }
        }

        public string Title;

        public string RectName;
        public string DataKey;

        public string ViewModelKey;
        public StringMap ViewModelArgs;

        [ClassExtends(typeof(ViewModelBase))]
        public ClassTypeReference ViewModelType;

        public string ModelKey;

        [ClassExtends(typeof(ModelBase))]
        public ClassTypeReference ModelType;

        public DynamicValue[] ModelArgs;

        public GameObject Prefab;

        public LayoutMap SubViews;
        public UObjectMap Data;

        /// <summary>
        /// 程式呼叫時可自訂資料來源
        /// </summary>
        [NonSerialized]
        public ModelBase modelData;
    }
}