﻿using UnityEngine;

namespace GameLibrary.MVVM {

    public abstract class ModelBase : ScriptableObject {

        public virtual void OnInitialized() {
        }

        public virtual void Initialize() {
        }

        public virtual void Initialize(params object[] args) {
        }

        public virtual void Initialize(object src) {
        }

        public static implicit operator bool(ModelBase b) {
            return b != null;
        }
    }
}