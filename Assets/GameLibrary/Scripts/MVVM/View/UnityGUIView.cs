﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using GameLibrary.MVVM.Template;
using GameLibrary.Utility;
using UnityEngine;

namespace GameLibrary.MVVM {

    public enum ActionOnDuplicate {
        NoAction,
        UsePreviousOne,
        UseNewOne
    }

    public interface ISearchableView {
    }

    public abstract class UnityGuiView : MonoBehaviour, ISearchableView, IPageContetCapability {
        public HashList<GameObject> Children = new HashList<GameObject>();
        public bool destroyOnHide;

        public onRelease OnRelease;

        protected virtual void Start() {
            PageSettings.Initialize(gameObject);
        }

        [SerializeField]
        protected PageContentCapability PageSettings;

        public void OpenLink(ViewData d) {
            PageSettings.OpenLink(d);
        }

        public void OpenLink(ViewData d, Action<GameObject> onComplete = null, Action<GameObject> onCreated = null, GameObject targetContainer = null, params object[] args) {
            PageSettings.OpenLink(d, onComplete: onComplete, onCreated: onCreated, targetContainer: targetContainer, args: args);
        }

        public void CloseView(ViewData d) {
            PageSettings.Close(d);
        }

        public void CloseView(params ViewData[] d) {
            PageSettings.Close(d);
        }

        public void CloseView(string viewName) {
            PageSettings.Close(viewName);
        }

        public void HideView(ViewData d) {
            PageSettings.Hide(d);
        }

        public GameObject OpenView(string key, ModelBase data) {
            return PageSettings.OpenView(key, data);
        }

        public GameObject OpenView(string key, params object[] args) {
            return PageSettings.OpenView(key, args);
        }

        public GameObject OpenView(ViewData data, params object[] args) {
            return PageSettings.OpenView(data, args);
        }

        public GameObject OpenView(string key, Action<GameObject> onComplete = null, Action<GameObject> onCreated = null, params object[] args) {
            return PageSettings.OpenView(key: key, onComplete: onComplete, onCreated: onCreated, args: args);
        }

        public ViewModelBase BindingContext {
            get {
                return getViewModelBase();
            }
            set {
                setViewModelBase(value);
            }
        }

        PageContentCapability IPageContetCapability.PageSettings {
            get {
                return PageSettings;
            }

            set {
                PageSettings = value;
            }
        }

        public delegate void onRelease();

        [ContextMenu("Close")]
        public void Close() {
            Destroy(gameObject);
        }

        public void Hide() {
            Hide(false, null);
        }

        public abstract void Hide(bool immediate = false, Action action = null);

        public void Reveal() {
            Reveal(false, null);
        }

        public abstract void Reveal(bool immediate = false, Action action = null);

        protected abstract ViewModelBase getViewModelBase();

        protected abstract void setViewModelBase(ViewModelBase value);

        protected void tryAct<T>(T o, Action<T> act) where T : MonoBehaviour {
            if (o) {
                act(o);
            }
        }

        protected void trySetValue<T>(GameObject o, Action<T> act) where T : Component {
            trySetValue(o, "", act);
        }

        protected void trySetValue<T>(string key, Action<T> act) where T : Component {
            trySetValue(gameObject, key, act);
        }

        protected void trySetValue<T>(GameObject o, string key = "", Action<T> act = null) where T : Component {
            T c;
            if (string.IsNullOrEmpty(key)) {
                c = o.GetComponent<T>();
            } else {
                c = o.GetComponentOfChild<T>(key);
            }
            if (c) {
                act(c);
            }
        }

        public virtual void onSubViewOpened(ViewData data, GameObject go) {
        }
    }

    [RequireComponent(typeof(CanvasGroup))]
    public abstract class UnityGuiView<T> : UnityGuiView, IView<T> where T : ViewModelBase {
        public readonly BindableProperty<T> ViewModelProperty = new BindableProperty<T>();
        protected readonly PropertyBinder<T> Binder = new PropertyBinder<T>();
        private bool _isInitialized;

        public new T BindingContext {
            get {
                return ViewModelProperty.Value;
            }
            set {
                if (!_isInitialized) {
                    OnInitialize();
                    _isInitialized = true;
                }
                //触发OnValueChanged事件
                ViewModelProperty.Value = value;
            }
        }

        /// <summary>
        /// 隐藏之后的回掉函数
        /// </summary>
        public Action HiddenAction {
            get; set;
        }

        /// <summary>
        /// 显示之后的回掉函数
        /// </summary>
        public Action RevealedAction {
            get; set;
        }

        public GameObject CreateSubView<TViewModel>(GameObject prefab, TViewModel viewmodel, object model = null) where TViewModel : ViewModelBase {
            var go = GameObjectRelate.InstantiateGameObject(gameObject, prefab);
            var view = go.GetComponent<UnityGuiView<TViewModel>>();

            view.BindingContext = viewmodel;
            if (model != null) {
                viewmodel.InitializeFromModel(model);
            }
            view.Reveal(true);
            return go;
        }

        public override void Hide(bool immediate = false, Action action = null) {
            if (action != null) {
                HiddenAction += action;
            }
            OnHide(immediate);
            OnHidden();
            OnDisappear();
        }

        /// <summary>
        /// 激活gameObject,Disable->Enable
        /// </summary>
        public virtual void OnAppear() {
            if (gameObject) {
                gameObject.SetActive(true);
            }
            if (BindingContext != null) {
                BindingContext.OnStartReveal();
            }
        }

        /// <summary>
        /// 绑定的上下文发生改变时的响应方法
        /// 利用反射+=/-=OnValuePropertyChanged
        /// </summary>
        public virtual void OnBindingContextChanged(T oldValue, T newValue) {
            if (oldValue != newValue) {
                Binder.Unbind(oldValue);
                Binder.Bind(newValue);
            }
        }

        /// <summary>
        /// 消失 Enable->Disable
        /// </summary>
        public virtual void OnDisappear() {
            gameObject.SetActive(false);
            if (BindingContext != null) {
                BindingContext.OnFinishHide();
            }

            if (destroyOnHide) {
                //销毁
                Destroy(this.gameObject);
            }
        }

        /// <summary>
        /// alpha 1->0时
        /// </summary>
        public virtual void OnHidden() {
            //回掉函数
            if (HiddenAction != null) {
                HiddenAction();
            }
        }

        /// <summary>
        /// alpha 0->1 之后执行
        /// </summary>
        public virtual void OnRevealed() {
            if (BindingContext != null) {
                BindingContext.OnFinishReveal();
            }
            //回掉函数
            if (RevealedAction != null) {
                RevealedAction();
            }
        }

        public override void Reveal(bool immediate = false, Action action = null) {
            if (action != null) {
                RevealedAction += action;
            }
            OnAppear();
            OnReveal(immediate);
            OnRevealed();
        }

        protected override ViewModelBase getViewModelBase() {
            return BindingContext;
        }

        /// <summary>
        /// 当gameObject将被销毁时，这个方法被调用
        /// </summary>
        protected virtual void OnDestroy() {
            Children.Foreach(DestroyImmediate);
            if (OnRelease != null) {
                OnRelease();
            }
            if (BindingContext != null) {
                if (BindingContext.IsRevealed) {
                    Hide(true);
                }
                BindingContext.OnDestory();
                BindingContext = null;
            } else {
                Hide(true);
            }

            ViewModelProperty.OnValueChanged = null;
        }

        /// <summary>
        /// 初始化View，当BindingContext改变时执行
        /// </summary>
        protected virtual void OnInitialize() {
            //无所ViewModel的Value怎样变化，只对OnValueChanged事件监听(绑定)一次
            ViewModelProperty.OnValueChanged += OnBindingContextChanged;
        }

        protected override void setViewModelBase(ViewModelBase value) {
            BindingContext = value as T;
        }

        public void Freeze() {
            var canvasGroup = GetComponent<CanvasGroup>();
            canvasGroup.interactable = false;
        }

        public void Defreeze() {
            var canvasGroup = GetComponent<CanvasGroup>();
            canvasGroup.interactable = true;
        }

        /// <summary>
        /// alpha:0,scale:0
        /// </summary>
        protected virtual void StartAnimatedHide() {
            var canvasGroup = GetComponent<CanvasGroup>();
            canvasGroup.interactable = false;
            canvasGroup.DOFade(0, 0.2f).SetDelay(0.2f).OnComplete(() => {
                transform.localScale = Vector3.zero;
                canvasGroup.interactable = true;
            });
        }

        /// <summary>
        /// scale:1,alpha:1
        /// </summary>
        protected virtual void StartAnimatedReveal() {
            var canvasGroup = GetComponent<CanvasGroup>();
            canvasGroup.interactable = false;
            transform.localScale = Vector3.one;

            canvasGroup.DOFade(1, 0.2f).SetDelay(0.2f).OnComplete(() => {
                canvasGroup.interactable = true;
            });
        }

        private void OnHide(bool immediate) {
            if (BindingContext != null) {
                BindingContext.OnStartHide();
            }

            if (immediate) {
                //立即隐藏
                transform.localScale = Vector3.zero;
                GetComponent<CanvasGroup>().alpha = 0;
            } else {
                StartAnimatedHide();
            }
        }

        /// <summary>
        /// 开始显示
        /// </summary>
        /// <param name="immediate"></param>
        private void OnReveal(bool immediate) {
            if (immediate) {
                //立即显示
                transform.localScale = Vector3.one;
                GetComponent<CanvasGroup>().alpha = 1;
            } else {
                StartAnimatedReveal();
            }
        }
    }
}