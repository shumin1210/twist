﻿using System.Collections.Generic;
using GameDevWare.Serialization;

namespace GameLibrary.Utility {

    public static class JS {

        public static string Serialize(object o) {
            return Json.SerializeToString(o, SerializationOptions.SuppressTypeInformation);
        }

        public static IDictionary<string, object> Deserialize(string str) {
            return Json.Deserialize<IDictionary<string, object>>(str, SerializationOptions.SuppressTypeInformation);
        }

        public static bool TryDeserialize<T>(string data, out T obj) {
            if (data.Length > 1 && data[0] == '{' && data[data.Length - 1] == '}') {
                obj = Deserialize<T>(data);
                return true;
            } else {
                obj = default(T);
                return false;
            }
        }

        public static T Deserialize<T>(string str) {
            return Json.Deserialize<T>(str, SerializationOptions.SuppressTypeInformation);
        }
    }
}