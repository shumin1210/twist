﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

namespace GameLibrary.Utility {

    public class CSVReader {
        private bool hasHeader = false;
        private TextAsset assetSrc;

        public TDataTable datas {
            get; protected set;
        }

        public CSVReader(TextAsset src, bool hasHeader = false) {
            datas = new TDataTable();

            assetSrc = src;
            this.hasHeader = hasHeader;
            load();
        }

        public string GetData(int rowIndex, int colIndex) {
            return datas.GetData(rowIndex, colIndex).ToString();
        }

        public string GetData(int rowIndex, string colName) {
            return datas.GetData(rowIndex, colName).ToString();
        }

        public T GetData<T>(int rowIndex, int colIndex) where T : struct {
            return datas.GetData<T>(rowIndex, colIndex);
        }

        public T GetData<T>(int rowIndex, string colName) where T : struct {
            return datas.GetData<T>(rowIndex, colName);
        }

        public void Reload() {
            load();
        }

        private const string CSV_PATTERN = "(?<=^|,)(\"(?:[^\"]|\"\")*\"|[^,]*)";

        public static TDataTable LoadCSV(string data, bool hasHeader = false) {
            bool inQuote = false;

            bool fieldStart = true;
            string[] header = null;
            List<object> cols = new List<object>();
            List<object[]> row = new List<object[]>();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < data.Length; i++) {
                char tmp = data[i];
                bool isEnd = i == data.Length - 1;

                if (inQuote) {
                    //double quote
                    if (!isEnd && data[i + 1] == '"') {
                        sb.Append(tmp);
                        i++;
                    } else {
                        //exit quote
                        inQuote = false;
                    }
                } else {
                    switch (tmp) {
                        case '\r':
                            //next row (or last col has no value)
                            if (sb.Length > 0 || fieldStart) {
                                cols.Add(sb.ToString());
                                sb.Remove(0, sb.Length);
                            }
                            if (hasHeader && header == null) {
                                header = cols.Cast(Convert.ToString);
                            } else {
                                row.Add(cols.ToArray());
                            }
                            cols.Clear();
                            fieldStart = true;
                            //check \r\n
                            if (!isEnd && data[i + 1] == '\n') {
                                i++;
                            }
                            break;

                        case ',':
                            //new field
                            cols.Add(sb.ToString());
                            sb.Remove(0, sb.Length);

                            break;

                        case '"':
                            if (fieldStart) {
                                inQuote = true;
                            } else {
                                sb.Append(tmp);
                            }
                            break;

                        default:
                            fieldStart = false;
                            sb.Append(tmp);
                            break;
                    }
                }
            }
            if (sb.Length > 0) {
                if (sb.Length > 0 || fieldStart) {
                    cols.Add(sb.ToString());
                    sb.Remove(0, sb.Length);
                }
                if (hasHeader && header == null) {
                    header = cols.Cast(Convert.ToString);
                } else {
                    row.Add(cols.ToArray());
                }
                cols.Clear();
                fieldStart = true;
            }
            var datas = new TDataTable();
            if (hasHeader && header != null) {
                datas.Load(header, row);
            } else {
                datas.Load(row);
            }
            return datas;
        }

        private void load() {
            string[] columns = null;
            string[][] data = null;
            string src = assetSrc.text;
            string[] splited = src.Split("\r\n");
            IEnumerable<MatchCollection> matches = splited.Select(s => Regex.Matches(s, CSV_PATTERN));
            string[][] results = matches.Select(m => m.Select(n => Regex.Replace(n.Groups[1].Value, "^(\")|(\")$", "").Replace("\"\"", "")).ToArray()).ToArray();
            if (hasHeader) {
                columns = results[0];
                data = results.Skip(1).ToArray();
            } else {
                data = results;
            }
            datas.Load(columns, data);
        }
    }

    internal static class MatchCollectionExtension {

        public static IEnumerable<T> Select<T>(this MatchCollection matches, Func<Match, T> selector) {
            List<T> results = new List<T>();
            foreach (Match item in matches) {
                results.Add(selector(item));
            }
            return results;
        }
    }
}