﻿using System;

namespace GameLibrary.Utility {

    /// <summary>陣列擴充方法</summary>
    public static class TArray {

        /// <summary>產生長度為length，元素為預設值的陣列</summary>
        /// <typeparam name="T">元素型別</typeparam>
        /// <param name="length">陣列長度</param>
        /// <param name="defaultValue">預設值</param>
        /// <returns>產生結果陣列</returns>
        public static T[] CreateInstance<T>(int length, T defaultValue) where T : struct {
            T[] a = new T[length];
            for (int i = 0; i < length; i++) {
                a[i] = defaultValue;
            }

            return a;
        }

        public static int IndexOf<T>(this T[] src, Func<T, bool> checker) {
            if (checker == null) {
                throw new ArgumentNullException("checker");
            }
            for (int i = 0; i < src.Length; i++) {
                if (checker(src[i])) {
                    return i;
                }
            }
            return -1;
        }

        public static int IndexOf<T>(this T[] src, T checker) where T : class {
            if (checker == null) {
                throw new ArgumentNullException("checker");
            }
            for (int i = 0; i < src.Length; i++) {
                if (checker == src[i]) {
                    return i;
                }
            }
            return -1;
        }
    }
}