﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameLibrary.Utility {

    public class Timer : MonoBehaviour {

        [Serializable]
        public class ProgressEvent : UnityEvent<float> {
        }

        public bool IgnoreAppPause = true;
        public bool IsPaused;
        public int Times = 1;
        public int TimeIndex = 0;
        public float Interval;
        public float Passed;
        public float Progress;

        private DateTime passedTime;
        public Action<float> OnProgress;

        public ProgressEvent onProgress = new ProgressEvent();
        public UnityEvent OnTimeUp = new UnityEvent();

        private MEC.CoroutineHandle coroutine;

        private void OnApplicationPause(bool pause) {
            if (pause && IgnoreAppPause) {
                passedTime = DateTime.UtcNow;
            }
        }

        private void OnApplicationFocus(bool focus) {
            if (focus && IgnoreAppPause) {
                Passed += (float)(passedTime - DateTime.UtcNow).TotalSeconds;
            }
        }

        private void OnEnable() {
            coroutine = MEC.TimingUtil.StartEmulateUpdate(_Update, this);
        }

        private void OnDisable() {
            if (coroutine != null) {
                MEC.Timing.KillCoroutines(coroutine);
            }
        }

        private void _Update() {
            if (IsPaused) {
                return;
            }
            Passed += Time.deltaTime;
            Progress = Passed / Interval;
            if (OnProgress != null) {
                OnProgress(Progress);
            }
            onProgress.Invoke(Progress);

            int index = (int)Math.Floor(Progress);
            if (TimeIndex < index) {
                TimeIndex = index;
                TriggerTimeUp();
            }
        }

        public void TriggerTimeUp() {
            OnTimeUp.Invoke();
            if (TimeIndex == Times) {
                Pause();
            }
        }

        public void Restart() {
            IsPaused = false;
            TimeIndex = 0;
            Passed = 0;
            Progress = 0;
        }

        public void Resume() {
            IsPaused = false;
        }

        public void Pause() {
            IsPaused = true;
        }
    }
}