﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameLibrary.Utility {

    /// <summary>字典類別擴充</summary>
    public static class DictionaryExtensions {

        /// <summary>將集合輸出成TDictionary類別</summary>
        /// <typeparam name="TSource">元素型別</typeparam>
        /// <typeparam name="TKey">鍵值型別</typeparam>
        /// <param name="source">來源集合物件</param>
        /// <param name="keySelector">決定鍵值的方法</param>
        /// <returns>轉換後物件</returns>
        public static TDictionary<TKey, TSource> ToTDictionary<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector) {
            TDictionary<TKey, TSource> d = new TDictionary<TKey, TSource>();
            foreach (TSource item in source) {
                d[keySelector(item)] = item;
            }

            return d;
        }

        /// <summary>將集合輸出成TDictionary類別</summary>
        /// <typeparam name="TSource">元素型別</typeparam>
        /// <typeparam name="TKey">鍵值型別</typeparam>
        /// <typeparam name="TElement">值型別</typeparam>
        /// <param name="source">來源集合物件</param>
        /// <param name="keySelector">決定鍵值的方法</param>
        /// <param name="elementSelector">決定值的方法</param>
        /// <returns>轉換後的物件</returns>
        public static TDictionary<TKey, TElement> ToTDictionary<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector) {
            TDictionary<TKey, TElement> d = new TDictionary<TKey, TElement>();
            foreach (TSource item in source) {
                d[keySelector(item)] = elementSelector(item);
            }

            return d;
        }

        /// <summary>增加KeyValuePair,若key已存在,則依據overwrite決定是否更新value</summary>
        /// <typeparam name="T">key型別</typeparam>
        /// <typeparam name="U">value型別</typeparam>
        /// <param name="src">目標Dictionary物件</param>
        /// <param name="key">key值</param>
        /// <param name="value">value值</param>
        /// <param name="overwrite">true則在key存在的情況下覆寫value</param>
        public static void TryAdd<T, U>(this IDictionary<T, U> src, T key, U value, bool overwrite = true) {
            if (src.ContainsKey(key) && overwrite) {
                src[key] = value;
            } else {
                src.Add(key, value);
            }
        }
    }

    /// <summary>擴充後的Dictionary類別</summary>
    public struct TDictionary {

        /// <summary>字典自動實作增減Key值的模式</summary>
        public enum MODE {

            /// <summary>自動增減字典的key</summary>
            AUTO,

            /// <summary>與Dictionary同樣的操作</summary>
            NORMAL,

            /// <summary>只自動增加key，不因value=null而刪減key(default)</summary>
            AUTO_ADD,

            /// <summary>只自動刪減key，</summary>
            AUTO_DEL
        }
    }

    /// <summary>擴充後的Dictionary</summary>
    /// <typeparam name="TKey">鍵值型別</typeparam>
    /// <typeparam name="TValue">值型別</typeparam>
    public class TDictionary<TKey, TValue> : Dictionary<TKey, TValue> {

        public TKey[] keys {
            get {
                return Keys.ToArray();
            }
        }

        public TValue[] values {
            get {
                return Values.ToArray();
            }
        }

        /// <summary>空建構式</summary>
        public TDictionary()
            : base() { }

        /// <summary>
        /// 指定初始容量的建構式
        /// </summary>
        /// <param name="count">初始容量</param>
        public TDictionary(int count)
            : base(count) {
        }

        /// <summary>建構式，指定模式</summary>
        /// <param name="flag">增減模式</param>
        public TDictionary(TDictionary.MODE flag)
            : base() {
            this.KeyFlag = flag;
        }

        /// <summary>建構式，指定複製來源</summary>
        /// <param name="src">要複製的來源</param>
        public TDictionary(TDictionary<TKey, TValue> src)
            : base(src) {
            this.KeyFlag = src.KeyFlag;
        }

        /// <summary>鍵值增減的模式</summary>
        public TDictionary.MODE KeyFlag = TDictionary.MODE.AUTO;

        /// <summary>取得或設定對應鍵值的值</summary>
        /// <param name="key">指定鍵值</param>
        /// <returns>值</returns>
        public new TValue this[TKey key] {
            get {
                return (KeyFlag == TDictionary.MODE.NORMAL) ? base[key] : this.Get(key);
            }
            set {
                switch (KeyFlag) {
                    case TDictionary.MODE.AUTO:
                        this.Add(key, value, autoAddKey: true, autoRemoveKey: true);
                        break;

                    case TDictionary.MODE.NORMAL:
                        this.Add(key, value, autoAddKey: false, autoRemoveKey: false);
                        break;

                    case TDictionary.MODE.AUTO_ADD:
                        this.Add(key, value, autoAddKey: true, autoRemoveKey: false);
                        break;

                    case TDictionary.MODE.AUTO_DEL:
                        this.Add(key, value, autoAddKey: false, autoRemoveKey: true);
                        break;
                }
            }
        }

        /// <summary>取得鍵值對應的值</summary>
        /// <param name="key">鍵值</param>
        /// <returns>值</returns>
        public TValue Get(TKey key) {
            if (ContainsKey(key)) {
                return base[key];
            } else {
                return default(TValue);
            }
        }

        /// <summary>新增鍵值與值的組</summary>
        /// <param name="key">鍵值</param>
        /// <param name="val">值</param>
        /// <param name="autoAddKey">是否為不存在的鍵值自動新增</param>
        /// <param name="autoRemoveKey">是否刪除值為NULL的鍵</param>
        public void Add(TKey key, TValue val, bool autoAddKey = true, bool autoRemoveKey = false) {
            //自動增加key
            if (autoAddKey && !this.ContainsKey(key)) {
                base.Add(key, default(TValue));
            }

            //自動將賦值為default(TValue)的KeyValuePair刪除
            if (autoRemoveKey && TType.isDefaultObject(val)) {
                base.Remove(key);
                return;
            }

            base[key] = val;
        }

        /// <summary>合併不同的IDictionary物件</summary>
        /// <param name="src">合併的來源</param>
        public void Merge(IDictionary<TKey, TValue> src) {
            foreach (KeyValuePair<TKey, TValue> item in src) {
                this[item.Key] = item.Value;
            }
        }

        /// <summary>以鍵值取得指定型別U的值</summary>
        /// <typeparam name="U">指定型別</typeparam>
        /// <param name="key">只定鍵值</param>
        /// <returns>型別為U的值</returns>
        public U Get<U>(TKey key) {
            TValue a = this[key];
            if (!TType.isDefaultObject(a)) {
                return TConvert.ToGeneric<U>(a);
            }

            return default(U);
        }

        /// <summary>以鍵值取得指定型別U的值，若無符合的鍵值，則返回預設值</summary>
        /// <typeparam name="U">指定型別</typeparam>
        /// <param name="key">只定鍵值</param>
        /// <param name="defaultValue">預設值，若無符合的鍵值則返回此值</param>
        /// <returns>型別為U的值</returns>
        public U Get<U>(TKey key, U defaultValue) {
            if (ContainsKey(key)) {
                return Get<U>(key);
            } else {
                return defaultValue;
            }
        }
    }
}