﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameLibrary.Utility {

    public class TCurrency {

        public enum TYPE {
            ALL = 0,
            UD = 1,
            Cash = 2,

            NTD = 10,
            RMB,
            USD,
        }
    }
}