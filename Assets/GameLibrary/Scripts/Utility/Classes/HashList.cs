﻿using System.Collections.Generic;

namespace GameLibrary.Utility {

    public class NonDuplicateList<T> : List<T> {

        public new void Add(T item) {
            if (Contains(item)) {
                RemoveAt(IndexOf(item));
                base.Add(item);
            } else {
                base.Add(item);
            }
        }
    }
}