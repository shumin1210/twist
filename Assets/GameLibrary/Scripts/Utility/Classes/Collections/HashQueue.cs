﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace GameLibrary.Utility {

    public class HashList<T> : IList<T> {

        public enum OrderType {
            OldToNew,
            NewToOld
        }

        private List<T> list = new List<T>();
        private HashSet<T> hashSet = new HashSet<T>();
        public OrderType Order = OrderType.OldToNew;

        public int Count {
            get {
                return ((IList<T>)list).Count;
            }
        }

        public bool IsReadOnly {
            get {
                return ((IList<T>)list).IsReadOnly;
            }
        }

        public T this[int index] {
            get {
                return ((IList<T>)list)[index];
            }

            set {
                ((IList<T>)list)[index] = value;
            }
        }

        public void Rearrange() {
            hashSet.Clear();
            switch (Order) {
                case OrderType.NewToOld:
                    for (int i = Count - 1; i >= 0; i--) {
                        Add(this[i]);
                    }
                    break;

                case OrderType.OldToNew:
                    for (int i = 0; i < Count; i++) {
                        Add(this[i]);
                    }
                    break;
            }
        }

        public void Add(T obj) {
            if (hashSet.Contains(obj)) {
                switch (Order) {
                    case OrderType.OldToNew:
                        RemoveAt(IndexOf(obj));
                        list.Add(obj);
                        break;

                    case OrderType.NewToOld:

                        break;
                }
            } else {
                hashSet.Add(obj);
                list.Add(obj);
            }
        }

        public int IndexOf(T item) {
            return ((IList<T>)list).IndexOf(item);
        }

        public void Insert(int index, T obj) {
            if (hashSet.Contains(obj)) {
                int oldIndex = IndexOf(obj);
                switch (Order) {
                    case OrderType.OldToNew:
                        if (index > oldIndex) {
                            list.Insert(index, obj);
                            RemoveAt(oldIndex);
                        }
                        break;

                    case OrderType.NewToOld:
                        if (index < oldIndex) {
                            RemoveAt(oldIndex);
                            list.Insert(index, obj);
                        }
                        break;
                }
            } else {
                hashSet.Add(obj);
                list.Insert(index, obj);
            }
        }

        public void RemoveAt(int index) {
            if (index >= list.Count) {
                return;
            }
            T obj = list[index];
            if (hashSet.Contains(obj)) {
                hashSet.Remove(obj);
            }
            list.RemoveAt(index);
        }

        public void Clear() {
            ((IList<T>)list).Clear();
            hashSet.Clear();
        }

        public bool Contains(T item) {
            return hashSet.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex) {
            ((IList<T>)list).CopyTo(array, arrayIndex);
        }

        public bool Remove(T obj) {
            if (hashSet.Contains(obj)) {
                hashSet.Remove(obj);
            }
            return list.Remove(obj);
        }

        public IEnumerator<T> GetEnumerator() {
            return ((IList<T>)list).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return ((IList<T>)list).GetEnumerator();
        }
    }
}