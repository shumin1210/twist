﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace GameLibrary.Utility {

    public struct CryptoUtility {

        /// <summary>
        /// 使用MD5加密資料
        /// </summary>
        /// <param name="data">要加密的字串資料</param>
        /// <returns>加密後的字串</returns>
        public static string MD5Encode(string data) {
            MD5 md5 = MD5.Create();
            byte[] value = Encoding.UTF8.GetBytes(data);
            byte[] hash = md5.ComputeHash(value);
            StringBuilder sb = new StringBuilder();
            hash.Foreach(s => sb.Append(s.ToString("x2")));
            return sb.ToString();
        }
    }

    public struct TMath {

        public static bool InRange(int source, int min, int max) {
            if (min > max) {
                int tmp = max;
                max = min;
                min = tmp;
            }
            return source >= min && source <= max;
        }

        /// <summary>回傳不低於min,且不超過max的值</summary>
        /// <param name="source">來源值</param>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <returns>區間內的值</returns>
        public static int Range(int source, int min, int max) {
            if (min > max) {
                int tmp = max;
                max = min;
                min = tmp;
            }

            return Mathf.Min(Mathf.Max(min, source), max);
        }

        /// <summary>回傳不低於min,且不超過max的值</summary>
        /// <param name="source">來源值</param>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <returns>區間內的值</returns>
        public static float Range(float source, float min, float max) {
            if (min > max) {
                float tmp = max;
                max = min;
                min = tmp;
            }

            return Mathf.Min(Mathf.Max(min, source), max);
        }

        public static long Factorial(long end) {
            long result = 1;
            for (long i = 1; i <= end; i++) {
                result *= i;
            }

            return result;
        }

        public static long Factorial(long start, long end) {
            long result = 1;
            for (long i = start; i <= end; i++) {
                result *= i;
            }

            return result;
        }

        public static long CRN(long n, long r) {
            /* n!/((n-m)!*m!)=> (n*n1*n2...m)/(n-m)! */
            return Factorial(r, n) / Factorial(n - r);
        }

        /// <summary>
        /// Cn取m
        /// </summary>
        /// <param name="n"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static long C(int n, int m) {
            //Cn取m = n! / (n-m)! m!
            long result = 1;
            //n!/(n-m)!
            for (int i = n; i > (n - m); i--) {
                result *= i;
            }
            result /= (int)Factorial(m);

            return result;
        }

        //將數字轉成K/M
        public static string ConvertNumberUnit(int target) {
            if ((target < 10000 && target >= 0)
                || (target < 0 && target > -10000)) {
                return target.ToString();
            }

            string result = "";
            decimal temp = target / 1000m;

            if (temp >= 1000) {
                result = (temp / 1000m).ToString("#.##") + "M";
            } else {
                result = System.Math.Round(temp, 2).ToString("#.##") + "K";
            }

            return result;
        }

        public static string ConvertNumberUnit(int target, string format) {
            if (target < 10000) {
                return target.ToString();
            }

            string result = "";
            decimal temp = target / 1000m;

            if (temp >= 1000) {
                result = (temp / 1000m).ToString(format) + "M";
            } else {
                result = temp.ToString(format) + "K";
            }

            return result;
        }
    }
}