﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameLibrary.Utility {

    /// <summary>Lniq擴充方法</summary>
    public static class LinQExtensions {

        public static void Swap<T>(this IList<T> src, int indexA, int indexB) {
            T tmpA = src.ElementAt(indexA);
            src[indexA] = src[indexB];
            src[indexB] = tmpA;
        }

        /// <summary>判斷集合中的元素是否全部不重複</summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="src">要判斷的集合</param>
        /// <returns></returns>
        public static bool IsDistinct<T>(this IEnumerable<T> src) {
            HashSet<T> keys = new HashSet<T>();
            foreach (T item in src) {
                if (keys.Contains(item)) {
                    return false;
                } else {
                    keys.Add(item);
                }
            }

            return true;
        }

        /// <summary>返回來源集合的不重複的元素</summary>
        /// <typeparam name="TElement">元素型別</typeparam>
        /// <typeparam name="TKey">元素判斷值的型，將依據此值比對元素間是否相等</typeparam>
        /// <param name="source">來源集合</param>
        /// <param name="keySelector">取得元素判斷值的方法</param>
        /// <returns></returns>
        public static IEnumerable<TElement> Distinct<TElement, TKey>(this IEnumerable<TElement> source, Func<TElement, TKey> keySelector) {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TElement element in source) {
                TKey elementValue = keySelector(element);
                if (seenKeys.Add(elementValue)) {
                    yield return element;
                }
            }
        }

        public static bool Exist<TKey, TValue>(this ICollection<KeyValuePair<TKey, TValue>> src, Func<KeyValuePair<TKey, TValue>, bool> check) {
            foreach (KeyValuePair<TKey, TValue> item in src) {
                if (check(item)) {
                    return true;
                }
            }
            return false;
        }

        public static bool Exist<TSource>(this IEnumerable<TSource> src, Func<TSource, bool> check) {
            foreach (TSource item in src) {
                if (check(item)) {
                    return true;
                }
            }
            return false;
        }

        /// <summary>降低維度</summary>
        /// <param name="src">來源陣列物件</param>
        public static object[] DownGrade(this object[] src) {
            object[] arr = new object[0];
            for (int i = 0; i < src.Count(); i++) {
                object obj = src.ElementAt(i);
                if (obj != null && src.ElementAt(i).GetType().IsArray) {
                    arr = arr.Concat(obj as object[]).ToArray();
                } else {
                    arr = arr.Concat(new object[] { obj }).ToArray();
                }
            }
            return arr;
        }

        /// <summary>降低維度</summary>
        /// <typeparam name="T">元素型別</typeparam>
        /// <param name="src">來源陣列物件</param>
        public static T[] DownGrade<T>(this IEnumerable<T[]> src) {
            T[] arr = new T[0];
            if (src.Count() > 0 && src.ElementAt(0) is T[]) {
                for (int i = 0; i < src.Count(); i++) {
                    arr = arr.Concat(src.ElementAt(i)).ToArray();
                }
            }

            return arr;
        }

        /// <summary>轉換型別</summary>
        /// <typeparam name="Element">元素型別</typeparam>
        /// <param name="src">來源陣列物件</param>
        public static Output[] Cast<Element, Output>(this IEnumerable<Element> src, Func<Element, Output> covertor) {
            return src.Select(covertor).ToArray();
        }

        /// <summary>For迴圈Linq版</summary>
        /// <typeparam name="Element">集合元素</typeparam>
        /// <param name="src">集合來源</param>
        /// <param name="action">回圈內對每個元素的行為</param>
        public static void Foreach<Element>(this IEnumerable<Element> src, Action<Element> action) {
            for (int i = 0; i < src.Count(); i++) {
                action(src.ElementAt(i));
            }
        }

        /// <summary>For迴圈Linq版</summary>
        /// <typeparam name="Element">集合元素</typeparam>
        /// <param name="src">集合來源</param>
        /// <param name="action">回圈內對每個元素的行為</param>
        public static void Foreach<Element>(this IEnumerable<Element> src, Action<Element, int> action) {
            if (src == null) {
                return;
            }
            for (int i = 0; i < src.Count(); i++) {
                action(src.ElementAt(i), i);
            }
        }

        public static IEnumerable<Output> SelectWhere<Element, Output>(this IEnumerable<Element> src, Func<Element, Output> selector, Func<Output, bool> restrict) {
            List<Output> result = new List<Output>();
            for (int i = 0; i < src.Count(); i++) {
                Element e = src.ElementAt(i);
                Output o = selector(e);
                if (restrict(o)) {
                    result.Add(o);
                }
            }
            return result;
        }

        public static IEnumerable<Output> SelectWhere<Element, Output>(this IEnumerable<Element> src, Func<Element, bool> restrict, Func<Element, Output> selector) {
            List<Output> result = new List<Output>();
            for (int i = 0; i < src.Count(); i++) {
                Element e = src.ElementAt(i);
                if (restrict(e)) {
                    result.Add(selector(e));
                }
            }
            return result;
        }

        public static void ForeachWhere<T>(this IEnumerable<T> src, Func<T, bool> restrict, Action<T> act) {
            foreach (T item in src.ToArray()) {
                if (restrict(item)) {
                    act(item);
                }
            }
        }

        public static void ForeachWhere<T>(this IEnumerable<T> src, Func<T, int, bool> restrict, Action<T, int> act) {
            int length = src.Count();
            for (int i = 0; i < length; i++) {
                T item = src.ElementAt(i);
                if (restrict(item, i)) {
                    act(item, i);
                }
            }
        }

        /// <summary>尋找符合條件的元素的索引值</summary>
        /// <typeparam name="T">集合元素</typeparam>
        /// <param name="src">集合來源</param>
        /// <param name="condition">條件判斷式</param>
        /// <returns>符合條件的索引值集合</returns>
        public static IEnumerable<int> FindIndexes<T>(this IEnumerable<T> src, Func<T, bool> condition) {
            List<int> indexes = new List<int>();
            for (int i = 0; i < src.Count(); i++) {
                if (condition(src.ElementAt(i))) {
                    indexes.Add(i);
                }
            }
            return indexes;
        }

        /// <summary>從指定索引值開始向前/後遍歷,遇到末端(開頭或結尾)會從反向的末端繼續遍歷</summary>
        /// <typeparam name="T">子元素</typeparam>
        /// <param name="src">來源集合</param>
        /// <param name="act">遍歷動作</param>
        /// <param name="startIndex">開始索引值</param>
        /// <param name="increasing">是否為向後遍立</param>
        public static void LoopEach<T>(this IEnumerable<T> src, Action<T> act, int startIndex = 0, bool increasing = true) {
            LoopEach(src, (d, i) => act(d), startIndex, increasing);
        }

        /// <summary>從指定索引值開始向前/後遍歷,遇到末端(開頭或結尾)會從反向的末端繼續遍歷</summary>
        /// <typeparam name="T">子元素</typeparam>
        /// <param name="src">來源集合</param>
        /// <param name="act">遍歷動作</param>
        /// <param name="startIndex">開始索引值</param>
        /// <param name="increasing">是否為向後遍立</param>
        public static void LoopEach<T>(this IEnumerable<T> src, Action<T, int> act, int startIndex = 0, bool increasing = true) {
            int len = src.Count();
            if (increasing) {
                for (int i = startIndex; i < len; i++) {
                    act(src.ElementAt(i), i);
                }
                for (int i = 0; i < startIndex; i++) {
                    act(src.ElementAt(i), i);
                }
            } else {
                for (int i = startIndex; i >= 0; i--) {
                    act(src.ElementAt(i), i);
                }
                for (int i = len - 1; i > startIndex; i--) {
                    act(src.ElementAt(i), i);
                }
            }
        }

        /// <summary>從指定索引值開始向前/後尋找符合條件的元素,遇到末端(開頭或結尾)會從反向的末端繼續遍歷</summary>
        /// <typeparam name="T">子元素</typeparam>
        /// <param name="src">來源集合</param>
        /// <param name="act">遍歷動作</param>
        /// <param name="startIndex">開始索引值</param>
        /// <param name="increasing">是否為向後遍立</param>
        public static T LoopFind<T>(this IEnumerable<T> src, Func<T, bool> act, int startIndex = 0, bool increasing = true) {
            return LoopFind(src, (d, i) => act(d), startIndex, increasing);
        }

        /// <summary>從指定索引值開始向前/後尋找符合條件的元素,遇到末端(開頭或結尾)會從反向的末端繼續遍歷</summary>
        /// <typeparam name="T">子元素</typeparam>
        /// <param name="src">來源集合</param>
        /// <param name="act">遍歷動作</param>
        /// <param name="startIndex">開始索引值</param>
        /// <param name="increasing">是否為向後遍立</param>
        public static T LoopFind<T>(this IEnumerable<T> src, Func<T, int, bool> act, int startIndex = 0, bool increasing = true) {
            int len = src.Count();
            if (increasing) {
                for (int i = startIndex; i < len; i++) {
                    if (act(src.ElementAt(i), i)) {
                        return src.ElementAt(i);
                    }
                }
                for (int i = 0; i < startIndex; i++) {
                    if (act(src.ElementAt(i), i)) {
                        return src.ElementAt(i);
                    }
                }
            } else {
                for (int i = startIndex; i >= 0; i--) {
                    if (act(src.ElementAt(i), i)) {
                        return src.ElementAt(i);
                    }
                }
                for (int i = len - 1; i > startIndex; i--) {
                    if (act(src.ElementAt(i), i)) {
                        return src.ElementAt(i);
                    }
                }
            }
            return default(T);
        }

        public static bool HasValue<T>(this IEnumerable<T> src) {
            foreach (var item in src) {
                if (item != null && !item.Equals(default(T))) {
                    return true;
                }
            }
            return false;
        }

        public static KeyValuePair<TKey, TValue> Find<TKey, TValue>(this IDictionary<TKey, TValue> src, Func<KeyValuePair<TKey, TValue>, bool> checker) {
            foreach (KeyValuePair<TKey, TValue> item in src) {
                if (checker(item)) {
                    return item;
                }
            }
            return new KeyValuePair<TKey, TValue>();
        }

        public static bool ContainsKey<TKey, TValue>(this IDictionary<TKey, TValue> src, Func<TKey, bool> check) {
            var list = src.Keys;
            foreach (var key in list) {
                if (check(key)) {
                    return true;
                }
            }
            return false;
        }

        public static int IndexOfKey<TKey, TValue>(this IDictionary<TKey, TValue> src, TKey key) {
            return src.Keys.ToList().IndexOf(key);
        }

        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> src) {
            return src.OrderBy(a => TRandom.Next());

            var length = src.Count();
            var indexes = src.Select((o, i) => i).ToList();
            List<T> result = new List<T>(length);
            for (int i = 0; i < length; i++) {
                int index = indexes.RandomElement(true);
                result[i] = src.ElementAt(index);
                indexes.RemoveAt(index);
            }
            return result;
        }

        public static T RandomElement<T>(this IEnumerable<T> l) {
            if (l.Count() > 0) {
                int index = TRandom.Next(0, l.Count());
                T result = l.ElementAt(index);
                return result;
            }
            return default(T);
        }

        public static int IndexOf<T>(this IEnumerable<T> src, Func<T, bool> condition) {
            for (int i = 0; i < src.Count(); i++) {
                if (condition(src.ElementAt(i))) {
                    return i;
                }
            }
            return -1;
        }
    }

    public static class LinQUtil {

        public static void For(this int end, Action<int> act) {
            For(0, end, act);
        }

        public static void For(int start = 0, int end = int.MaxValue, Action<int> act = null) {
            if (act == null) {
                return;
            }
            for (int i = start; i < end; i++) {
                act(i);
            }
        }

        public static void While(Func<bool> flag = null, Action act = null) {
            if (flag == null || act == null) {
                return;
            }
            while (flag()) {
                act();
            }
        }

        public static void DoWhile(Action act = null, Func<bool> flag = null) {
            if (flag == null || act == null) {
                return;
            }
            do {
                act();
            } while (flag());
        }
    }
}