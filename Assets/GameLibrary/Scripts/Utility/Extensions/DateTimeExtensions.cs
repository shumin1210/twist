﻿using System;
using System.Text.RegularExpressions;

namespace GameLibrary.Utility {

    /// <summary>時間類別擴充方法</summary>
    public static class DateTimeExtensions {

        /// <summary>輸出為JS時間字串</summary>
        /// <param name="target">時間</param>
        /// <param name="toUTC">是否轉為UTC格式</param>
        /// <returns>JS時間字串</returns>
        public static string toJSDate(this DateTime target, bool toUTC = true) {
            if (toUTC) {
                return target.ToUniversalTime().ToString("yyyy/MM/dd HH:mm:ss GMT");
            } else {
                return target.ToString("yyyy/MM/dd HH:mm:ss");
            }
        }

        /// <summary>輸出為MySQL時間字串</summary>
        /// <param name="target">時間</param>
        /// <returns>MySQL時間字串</returns>
        public static string toMySQLDateTime(this DateTime target) {
            return target.ToString("yyyy-MM-dd HH:mm:ss");
        }

        /// <summary>取得現在時間的TimeStamp</summary>
        /// <returns></returns>
        public static long unixTime() {
            return unixTime(DateTime.Now);
        }

        /// <summary>JSTimeStamp基準點</summary>
        private static DateTime unixStd = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);

        /// <summary>從UnixTimeStamp轉換成DateTime(本地時間)</summary>
        /// <param name="time"></param>
        /// <param name="unixTimeStamp"></param>
        /// <returns></returns>
        public static DateTime FromUnix(this DateTime time, long unixTimeStamp) {
            return FromUnix(unixTimeStamp);
        }

        /// <summary>將時間轉成UnixTimeStamp</summary>
        /// <param name="time">要轉換的時間</param>
        /// <returns>UnixTime格式時間</returns>
        public static long unixTime(this DateTime time) {
            return (long)(time.ToUniversalTime().Subtract(unixStd)).TotalMilliseconds;
        }

        public static DateTime FromUnix(long unixTimeStamp) {
            try {
                return unixStd.AddMilliseconds(unixTimeStamp).ToLocalTime();
            } catch (Exception) {
                return DateTime.MinValue;
            }
        }

        private static string JSDateFormat = @"/Date\((\d+)\)/";

        public static DateTime FromUnix(this DateTime time, string jsDateString) {
            return FromUnix(jsDateString);
        }

        public static DateTime FromUnix(string jsDateString) {
            DateTime result = default(DateTime);
            TryParse(jsDateString, out result);
            return result;
        }

        public static bool TryParse(string str, out DateTime time) {
            time = default(DateTime);
            Match m = Regex.Match(str, JSDateFormat);
            if (m.Success) {
                long ticks = Convert.ToInt64(m.Groups[1].Value);
                //try {
                //    time = new DateTime(ticks);
                //} catch (Exception) {
                time = FromUnix(ticks);
                //}
                return true;
            } else {
                m = Regex.Match(str, @"\d");
                long timestamp;
                if (m.Success && DateTime.TryParse(str, out time)) {
                    return !time.Equals(default(DateTime));
                } else if (Int64.TryParse(str, out timestamp)) {
                    time = FromUnix(timestamp);
                    return true;
                } else {
                    return false;
                }
            }
        }

        public static bool TryParse(long s, out DateTime time) {
            time = FromUnix(s);
            return true;
        }
    }
}