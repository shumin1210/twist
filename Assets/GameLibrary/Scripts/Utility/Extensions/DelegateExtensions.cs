﻿using System;
using UnityEngine;

namespace GameLibrary.Utility {

    public static class DelegateExtensions {

        public static TResult TryInvoke<TResult>(this Func<TResult> func) {
            if (func != null) {
                try {
                    return (TResult)func.DynamicInvoke();
                } catch (Exception e) {
                    Debug.Log(e);
                }
            }
            return default(TResult);
        }

        public static void TryInvoke(this Delegate d, params object[] args) {
            if (d != null) {
                try {
                    d.DynamicInvoke(args);
                } catch (Exception e) {
                    Debug.Log(e);
                }
            }
        }
    }
}