﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace GameLibrary.Utility {

    public static class ExpressionUtils {

        public static PropertyInfo GetProperty<TEntity, TProperty>(Expression<Func<TEntity, TProperty>> expression) {
            var member = GetMemberExpression(expression).Member;
            var property = member as PropertyInfo;
            if (property == null) {
                throw new InvalidOperationException(string.Format("Member with Name '{0}' is not a property.", member.Name));
            }
            return property;
        }

        public static FieldInfo GetField<TEntity, TField>(Expression<Func<TEntity, TField>> expression) {
            var member = GetMemberExpression(expression).Member;
            var field = member as FieldInfo;
            if (field == null) {
                throw new InvalidOperationException(string.Format("Member with Name '{0}' is not a property.", member.Name));
            }
            return field;
        }

        public static MemberInfo GetMember<TEntity, TField>(Expression<Func<TEntity, TField>> expression) {
            var member = GetMemberExpression(expression).Member;
            var field = member as MemberInfo;
            if (field == null) {
                throw new InvalidOperationException(string.Format("Member with Name '{0}' is not a property.", member.Name));
            }
            return field;
        }

        private static MemberExpression GetMemberExpression<TEntity, TProperty>(Expression<Func<TEntity, TProperty>> expression) {
            MemberExpression memberExpression = null;
            if (expression.Body.NodeType == ExpressionType.Convert) {
                var body = (UnaryExpression)expression.Body;
                memberExpression = body.Operand as MemberExpression;
            } else if (expression.Body.NodeType == ExpressionType.MemberAccess) {
                memberExpression = expression.Body as MemberExpression;
            }

            if (memberExpression == null) {
                throw new ArgumentException("Not a member access", "expression");
            }

            return memberExpression;
        }

        public static Action<TEntity, TProperty> CreateSetter<TEntity, TProperty>(Expression<Func<TEntity, TProperty>> property) {
            PropertyInfo propertyInfo = ExpressionUtils.GetProperty(property);

            ParameterExpression instance = System.Linq.Expressions.Expression.Parameter(typeof(TEntity), "instance");
            ParameterExpression parameter = System.Linq.Expressions.Expression.Parameter(typeof(TProperty), "param");

            var body = System.Linq.Expressions.Expression.Call(instance, propertyInfo.GetSetMethod(), parameter);
            var parameters = new ParameterExpression[] { instance, parameter };

            return System.Linq.Expressions.Expression.Lambda<Action<TEntity, TProperty>>(body, parameters).Compile();
        }

        public static Func<TEntity, TProperty> CreateGetter<TEntity, TProperty>(Expression<Func<TEntity, TProperty>> property) {
            PropertyInfo propertyInfo = ExpressionUtils.GetProperty(property);

            ParameterExpression instance = System.Linq.Expressions.Expression.Parameter(typeof(TEntity), "instance");

            var body = System.Linq.Expressions.Expression.Call(instance, propertyInfo.GetGetMethod());
            var parameters = new ParameterExpression[] { instance };

            return System.Linq.Expressions.Expression.Lambda<Func<TEntity, TProperty>>(body, parameters).Compile();
        }

        public static Func<TEntity> CreateDefaultConstructor<TEntity>() {
            var body = System.Linq.Expressions.Expression.New(typeof(TEntity));
            var lambda = System.Linq.Expressions.Expression.Lambda<Func<TEntity>>(body);

            return lambda.Compile();
        }

        public static Type GetGenericElementType(this Type type) {
            if (type.IsGenericType) {
                return type.GetGenericArguments()[0];
            } else {
                return type.GetElementType();
            }
        }

        public static bool IsClassOrStruct(this Type t) {
            return (t.IsClass || (t.IsValueType && !t.IsPrimitive));
        }
    }

    public interface IValueBind {

        object GetValue {
            get;
        }

        void SetValue(object val);
    }

    public interface IValueBind<T> : IValueBind {

        new T GetValue {
            get;
        }

        void SetValue(T val);
    }

    public static class ReflectionUtility {
        private static Dictionary<Type, Dictionary<string, Type>> cache_subtypes = new Dictionary<Type, Dictionary<string, Type>>();

        private static Dictionary<Type, Dictionary<string, MemberData>> cache_members = new Dictionary<Type, Dictionary<string, MemberData>>();

        public class MemberData {

            public MemberInfo MemberInfo {
                get {
                    return Field ?? Property ?? (MemberInfo)Method;
                }
            }

            public FieldInfo Field {
                get; private set;
            }

            public PropertyInfo Property {
                get; private set;
            }

            public MethodInfo Method {
                get; private set;
            }

            public string FieldName {
                get {
                    if (Field != null) {
                        return Field.Name;
                    } else if (Property != null) {
                        return Property.Name;
                    } else if (Method != null) {
                        return Method.Name;
                    } else {
                        return string.Empty;
                    }
                }
            }

            public MemberData(MemberInfo m) {
                if (m is FieldInfo) {
                    Field = (m as FieldInfo);
                } else if (m is PropertyInfo) {
                    Property = (m as PropertyInfo);
                } else if (m is MethodInfo) {
                    Method = (m as MethodInfo);
                }
            }

            public MemberData(FieldInfo f) : this(f as MemberInfo) {
            }

            public MemberData(PropertyInfo p) : this(p as MemberInfo) {
            }

            public MemberData(MethodInfo p) : this(p as MemberInfo) {
            }

            public Type MemberType {
                get {
                    if (Field != null) {
                        return Field.FieldType;
                    } else if (Property != null) {
                        return Property.PropertyType;
                    } else {
                        return null;
                    }
                }
            }

            public object GetValue(object src) {
                if (Field != null) {
                    return Field.GetValue(src);
                } else if (Property != null) {
                    return Property.GetValue(src, new object[0]);
                } else {
                    return null;
                }
            }

            public void SetValue(object src, object val) {
                if (Field != null) {
                    Field.SetValue(src, val);
                } else if (Property != null) {
                    Property.SetValue(src, val, null);
                }
            }

            public object TryInvoke(object source, params object[] args) {
                if (Method != null) {
                    return Method.Invoke(source, args);
                } else {
                    return default(object);
                }
            }
        }

        private static bool checkIsGeneric(Type generic, Type toCheck) {
            while (toCheck != null && toCheck != typeof(object)) {
                var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
                if (generic == cur) {
                    return true;
                }
                toCheck = toCheck.BaseType;
            }
            return false;
        }

        public static Type GetSubTypes(Type rootType, string name) {
            if (!cache_subtypes.ContainsKey(rootType)) {
                var tmp = AppDomain.CurrentDomain.GetAssemblies()
                         .SelectMany(s => s.GetTypes()).ToList();
                var tmp2 = tmp.Where(t => checkIsGeneric(rootType, t)).ToDictionary(t => t.Name, t => t);
                cache_subtypes.Add(rootType, tmp2);
            }

            if (cache_subtypes[rootType].ContainsKey(name)) {
                return cache_subtypes[rootType][name];
            } else {
                return null;
            }
        }

        private static void cacheType(Type type) {
            if (!cache_members.ContainsKey(type)) {
                MemberInfo[] Fields = type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                cache_members[type] = Fields.ToDictionary(s => s.Name, s => new MemberData(s));
                MemberInfo[] Properties = type.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                Properties.Foreach(p => cache_members[type][p.Name] = new MemberData(p));
                MemberInfo[] Methods = type.GetMethods(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                Methods.Foreach(m => cache_members[type][m.Name] = new MemberData(m));
            }
        }

        public static ReadOnlyCollection<string> GetMemberNames(Type type) {
            cacheType(type);

            if (cache_members.ContainsKey(type)) {
                return cache_members[type].Keys.ToList().AsReadOnly();
            } else {
                return null;
            }
        }

        public static ReadOnlyCollection<MemberData> GetFieldsAndProperties(Type type, bool isPublic = false) {
            cacheType(type);

            if (cache_members.ContainsKey(type)) {
                return cache_members[type].Values.Where(m => m.Field != null || m.Property != null).ToList().AsReadOnly();
            } else {
                return null;
            }
        }

        public static ReadOnlyCollection<MemberData> GetMembers(Type type) {
            cacheType(type);

            if (cache_members.ContainsKey(type)) {
                return cache_members[type].Values.ToList().AsReadOnly();
            } else {
                return null;
            }
        }

        public static ReadOnlyCollection<MemberData> GetMethods(Type type) {
            cacheType(type);

            if (cache_members.ContainsKey(type)) {
                return cache_members[type].Values.Where(m => m.Method != null).ToList().AsReadOnly();
            } else {
                return null;
            }
        }

        public static MemberData GetMember(Type type, string memberName) {
            cacheType(type);

            if (cache_members.ContainsKey(type) && cache_members[type].ContainsKey(memberName)) {
                return cache_members[type][memberName];
            } else {
                return null;
            }
        }

        public static T[] GetCustomAttributes<T>(this MemberInfo minfo, bool inherit) {
            return minfo.GetCustomAttributes(typeof(T), inherit).Select(a => (T)a).ToArray();
        }

        public static bool TryGetValue(object data, string fieldName, out object val) {
            val = null;
            if (data == null) {
                return false;
            }

            Type type = data.GetType();
            MemberData m = GetMember(type, fieldName);
            if (m != null) {
                val = m.GetValue(data);
                return true;
            } else {
                return false;
            }
        }
    }
}