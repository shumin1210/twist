﻿using System.IO;
using System.Linq;
using SysPAth = System.IO.Path;

namespace GameLibrary.Utility {

    public static class Path {

        public static string Combine(params object[] args) {
            if (args.Length == 0) {
                return string.Empty;
            } else if (args.Length == 1) {
                return args[0] != null ? args[0].ToString() : string.Empty;
            } else if (args.Length == 2) {
                return SysPAth.Combine(args[0].ToString(), args[1].ToString());
            }

            string origin = args[0].ToString();
            for (int i = 1; i < args.Length; i++) {
                if (args[i] != null) {
                    char endChar = origin.Last();
                    origin = SysPAth.Combine(origin, args[i].ToString());
                }
            }
            return origin.Replace("\\", "/");
        }
    }

    public static class IOExtensions {
    }
}