﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameLibrary.Utility {

    public static class GameObjectExtensions {

        public static void MoveInHierarchy(this GameObject g, int delta) {
            int index = g.transform.GetSiblingIndex();
            g.transform.SetSiblingIndex(index + delta);
        }

        public static void ToggleActive(this GameObject o) {
            o.SetActive(!o.activeSelf);
        }

        /// <summary>set gameobject active on/off</summary>
        /// <param name="o">target gameobject</param>
        /// <param name="on">on/off,null for toggle</param>
        public static void SetActive(this GameObject o, bool? on = null) {
            if (on == null) {
                o.SetActive(!o.activeSelf);
            } else {
                o.SetActive((bool)on);
            }
        }

        /// <summary>set gameobject active on/off</summary>
        /// <param name="c">compoent object</param>
        /// <param name="on">on/off,null for toggle</param>
        public static void SetActive(this Component c, bool? on = null) {
            if (c && c.gameObject) {
                c.gameObject.SetActive(on);
            }
        }

        public static T GetOrAddComponent<T>(this GameObject src) where T : Component {
            var comp = src.GetComponent<T>();
            if (!comp) {
                comp = src.AddComponent<T>();
            }
            return comp;
        }

        public static T GetComponentOfChild<T>(this GameObject src, string key) {
            if (src) {
                var child = src.transform.FindDeepChild(key);
                if (child) {
                    return child.GetComponent<T>();
                }
            }
            return default(T);
        }

        public static List<T> GetComponentsOfChildren<T>(this GameObject src, string key) where T : Component {
            if (src) {
                var list = src.transform.FindDeepChildren(key);
                return list.SelectWhere(g => g.GetComponent<T>(), c => c).ToList();
            } else {
                return new List<T>();
            }
        }

        public static void DestroyChild(this GameObject src, string name) {
            var child = src.transform.FindDeepChild(name);
            if (child) {
                GameObject.Destroy(child.gameObject);
            }
        }
    }
}