﻿using System;

namespace GameLibrary.Utility {

    public static class EnumExtension {

        public static bool HasFlag(this Enum self, Enum flag) {
            if (self.GetType() != flag.GetType()) {
                throw new ArgumentException("wrong enum type.");
            }

            ulong selfValue = Convert.ToUInt64(self);
            ulong flagValue = Convert.ToUInt64(flag);

            return (selfValue & flagValue) == flagValue;
        }

        public static T Next<T>(this T src) where T : struct, IConvertible {
            T[] Arr = (T[])Enum.GetValues(src.GetType());
            int j = Array.IndexOf(Arr, src) + 1;
            return (Arr.Length == j) ? Arr[0] : Arr[j];
        }

        public static T Previous<T>(this T src) where T : struct, IConvertible {
            T[] Arr = (T[])Enum.GetValues(src.GetType());
            int j = Array.IndexOf(Arr, src) - 1;
            return (j < 0) ? Arr[Arr.Length - 1] : Arr[j];
        }
    }
}