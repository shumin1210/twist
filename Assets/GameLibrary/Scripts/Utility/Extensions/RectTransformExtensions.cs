﻿using UnityEngine;

namespace GameLibrary.Utility {

    public static class RectTransformExtensions {

        public static void AddSizeDelta(this RectTransform r, float x = 0, float y = 0) {
            Vector2 origin = r.sizeDelta;
            r.sizeDelta = new Vector2(origin.x + x, origin.y + y);
        }

        public static void SetSizeDelta(this RectTransform r, float x = 0, float y = 0) {
            Vector2 origin = r.sizeDelta;
            r.sizeDelta = new Vector2(x == 0 ? origin.x : x, y == 0 ? origin.y : y);
        }
    }
}