﻿/**********************************************************

// Author   : K.(k79k06k02k)
// FileName : SingletonObject.cs
// Reference: http://wiki.unity3d.com/index.php/Singleton
**********************************************************/

using UnityEngine;

namespace GameLibrary.Utility {

    public class SingletonObject<T> : MonoBehaviour where T : MonoBehaviour {
        public const string InstanceName = "[Singleton[{0}]]";

        private static T _instance;
        private static bool applicationIsQuitting = false;
        private static object _lock = new object();

        [Button]
        public void test() {
            Debug.Log("😁");
        }

        public static T Instance {
            get {
                //判斷物件是否已刪除
                if (applicationIsQuitting) {
                    Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                        "' already destroyed on application quit." +
                        " Won't create again - returning null.");
                    return null;
                }

                //多執行緒
                lock (_lock) {
                    if (_instance == null) {
                        var list = FindObjectsOfType(typeof(T));

                        //是否已經建立出物件
                        if (list.Length > 1) {
                            _instance = (T)list[0];
                            DontDestroyOnLoad(_instance);
                            Debug.LogWarning("[Singleton] Something went really wrong - there should never be more than 1 singleton! Reopenning the scene might fix it.");
                            return _instance;
                        } else if (list.Length == 1) {
                            _instance = (T)list[0];
                            DontDestroyOnLoad(_instance);
                            return _instance;
                        }

                        if (_instance == null) {
                            GameObject singleton = new GameObject {
                                name = InstanceName.format(typeof(T).Name)
                            };

                            _instance = singleton.AddComponent<T>();

                            //始終不刪除(EX:換場景時不刪除)
                            DontDestroyOnLoad(singleton);
                            //Debug.Log("[Singleton] An instance of " + typeof(T) + " is needed in the scene, so '" + singleton + "' was created with DontDestroyOnLoad.");
                        } else {
                            //Debug.Log("[Singleton] Using instance already created: " + _instance.gameObject.name);
                        }
                    }

                    return _instance;
                }
            }
        }

        /// <summary>
        /// When Unity quits, it destroys objects in a random order.
        /// In principle, a Singleton is only destroyed when application quits.
        /// If any script calls Instance after it have been destroyed,
        ///   it will create a buggy ghost object that will stay on the Editor scene
        ///   even after stopping playing the Application. Really bad!
        /// So, this was made to be sure we're not creating that buggy ghost object.
        /// </summary>
        public void OnDestroy() {
            applicationIsQuitting = true;
        }
    }
}