﻿using System.Reflection;

namespace GameLibrary.Utility.Proxy {

    public interface IInvocationHandler {

        void PreProcess();

        object Invoke(object proxy, MethodInfo method, object[] args);

        void PostProcess();
    }
}