﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameLibrary.Utility {

    internal class ObjectPool<T> where T : new() {
        private readonly Stack<T> m_Stack = new Stack<T>();
        private readonly UnityAction<T> m_ActionOnGet;
        private readonly UnityAction<T> m_ActionOnRelease;
        private readonly Func<T> m_ActionInstanciate;

        public int countAll {
            get; private set;
        }

        public int countActive {
            get {
                return countAll - countInactive;
            }
        }

        public int countInactive {
            get {
                return m_Stack.Count;
            }
        }

        public ObjectPool(UnityAction<T> actionOnGet = null, UnityAction<T> actionOnRelease = null, Func<T> actionInstanciate = null) {
            m_ActionOnGet = actionOnGet;
            m_ActionOnRelease = actionOnRelease;
            m_ActionInstanciate = actionInstanciate;
        }

        public T Get() {
            T element;
            if (m_Stack.Count == 0) {
                if (m_ActionInstanciate != null) {
                    element = m_ActionInstanciate();
                } else {
                    element = new T();
                }
                countAll++;
            } else {
                element = m_Stack.Pop();
            }
            if (m_ActionOnGet != null)
                m_ActionOnGet(element);
            return element;
        }

        public void Release(T element) {
            if (m_Stack.Count > 0 && ReferenceEquals(m_Stack.Peek(), element)) {
                Debug.LogError("Internal error. Trying to destroy object that is already released to pool.");
            }

            if (m_ActionOnRelease != null) {
                m_ActionOnRelease(element);
            }

            m_Stack.Push(element);
        }
    }
}