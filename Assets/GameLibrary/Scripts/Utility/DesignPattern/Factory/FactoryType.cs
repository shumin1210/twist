﻿namespace GameLibrary.Utility.Factory {

    public enum FactoryType {
        Singleton,
        Transient,
        Pool
    }
}