﻿using System;

namespace GameLibrary.Utility.Factory {

    public class TransientObjectFactory : IObjectFactory {

        public object Acquire(string className) {
            return Acquire(TypeFinder.ResolveType(className));
        }

        public object Acquire(Type type) {
            var obj = Activator.CreateInstance(type, false);
            return obj;
        }

        public object Acquire<TInstance>() where TInstance : class, new() {
            var instance = new TInstance();
            return instance;
        }

        public void Release(object obj) {
        }
    }
}