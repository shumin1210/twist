﻿using System;

namespace GameLibrary.Utility.Factory {

    public interface IObjectFactory {

        object Acquire(string className);

        object Acquire(Type type);

        object Acquire<IInstance>() where IInstance : class, new();

        void Release(object obj);
    }
}