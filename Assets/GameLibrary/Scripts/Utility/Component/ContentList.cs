﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameLibrary.UI;
using GameLibrary.Utility.SerializableDictionary;

using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace GameLibrary.Utility {

    /// <summary>可自訂物件集合的元件</summary>
    /// <remarks>
    /// 使用方法類似於transform.Find的概念,實際以Key值作為索引,將GameObject整理成集合,建立類似Dictionary的物件,方便取用.
    /// 提供indexer取得指定的單個GameObject
    /// 及GetList取得指定GameObject集合
    /// </remarks>
    public class ContentList : MonoBehaviour, ISerializationCallbackReceiver {
        public LayoutGroup layout;

        [Serializable]
        [Obsolete("不再被使用", false)]
        public class GameObjectPair {
            public string Key;
            public GameObject Value;
        }

        [SerializeField]
        [Obsolete("已被SerializableDictionary替代", false)]
        private GameObjectPair[] Items;

        [SerializeField]
        private GameObjectMap ContentMap;

        public GameObject this[string key] {
            get {
                return ContentMap.ContainsKey(key) ? ContentMap[key] : null;
                //return Items.FirstOrDefault(s => s.Key == key).Value;
            }
        }

        public void RefreshLayout() {
            refreshLayout();
        }

        private void refreshLayout() {
            if (layout) {
                LayoutRebuilder.ForceRebuildLayoutImmediate(layout.transform as RectTransform);
            }
        }

        public GameObject this[int index] {
            get {
                if (index < ContentMap.Count) {
                    return ContentMap.Values.ElementAt(index);
                }
                return null;
                //if (index < Items.Length) {
                //    return Items[index].Value;
                //} else {
                //    return default(GameObject);
                //}
            }
        }

        public TComponent GetComponetOfGameObject<TComponent>(string key) {
            GameObject o = this[key];
            if (o) {
                return o.GetComponent<TComponent>();
            } else {
                return default(TComponent);
            }
        }

        public void ShowItem(string key) {
            this[key].SetActive(true);
            refreshLayout();
        }

        public void HideItem(string key) {
            this[key].SetActive(false);
            refreshLayout();
        }

        /// <summary>在Items裡取得指定Key值得GameObject,並取得指定Component</summary>
        /// <typeparam name="T">指定Component型別</typeparam>
        /// <param name="key">Item的Key值</param>
        /// <returns>Component實體</returns>
        public T GetComponent<T>(string key) where T : Component {
            return this[key].GetComponent<T>();
        }

        public T GetComponent<T>(int index) where T : Component {
            return this[index].GetComponent<T>();
        }

        [SerializeField]
        internal List<UISet> collections;

        public List<UISet> Collections {
            get {
                return collections;
            }
        }

        /// <summary>取得指定Key值的GameObject集合</summary>
        /// <param name="key">Key值</param>
        /// <returns>GameObject集合</returns>
        public List<GameObject> GetList(string key) {
            return Collections.Get(key).Values;
        }

        public List<GameObject> GetList(int index) {
            return Collections.Get(index).Values;
        }

        public void ToggleAll(bool on) {
            Collections.ToggleAll(on);
            refreshLayout();
        }

        public void ToggleGroup(string key, bool on) {
            Collections.Toggle(key, on);
            refreshLayout();
        }

        public void ToggleGroup(string key) {
            Collections.Toggle(key);
            refreshLayout();
        }

        public void ToggleGroupIndividual(string key) {
            Collections.ToggleIndividual(key);
            refreshLayout();
        }

        public void ShowGroup(string key) {
            Collections.Toggle(key, true);
            refreshLayout();
        }

        public void HideGroup(string key) {
            Collections.Toggle(key, false);
            refreshLayout();
        }

        public void ShowGroupIndividual(string key) {
            Collections.ToggleIndividual(key, on: true);
            refreshLayout();
        }

        public void HideGroupIndividual(string key) {
            Collections.ToggleIndividual(key, on: false);
            refreshLayout();
        }

        public void OnBeforeSerialize() {
            transferData();
        }

        public void OnAfterDeserialize() {
            transferData();
        }

        private void transferData() {
            if (Items != null && Items.Length > 0) {
                Items.Foreach(p => {
                    ContentMap[p.Key] = p.Value;
                });
                Items = new GameObjectPair[0];
            }
        }

#if UNITY_EDITOR

        [CustomEditor(typeof(ContentList))]
        public class Editor : UnityEditor.Editor {
            private ContentList source;

            private SerializedProperty items;
            private SerializedProperty items_map;
            private SerializedProperty collections;

            public void OnEnable() {
                source = target as ContentList;
                items = serializedObject.FindProperty("Items");
                items_map = serializedObject.FindProperty("ContentMap");
                collections = serializedObject.FindProperty("collections");
            }

            public override void OnInspectorGUI() {
                if (source.Items != null && source.Items.Length > 0) {
                    EditorGUILayout.PropertyField(items, true);
                } else {
                    EditorGUILayout.PropertyField(items_map, true);
                }
                EditorGUILayout.PropertyField(collections, true);

                serializedObject.ApplyModifiedProperties();
            }
        }

#endif
    }
}