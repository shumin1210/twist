﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DynamicInfo : MonoBehaviour, IDictionary<string, object> {

    [Serializable]
    public struct Info {
        public string Key;
        public string Value;
    }

    [SerializeField]
    private List<Info> Infos = new List<Info>();

    public ICollection<string> Keys {
        get {
            return Infos.Select(i => i.Key).ToList();
        }
    }

    public ICollection<object> Values {
        get {
            return Infos.Select(i => i.Value as object).ToList();
        }
    }

    public int Count {
        get {
            return Infos.Count;
        }
    }

    public bool IsReadOnly {
        get {
            return false;
        }
    }

    public object this[string key] {
        get {
            return Infos.FirstOrDefault(i => i.Key == key).Value;
        }

        set {
            if (string.IsNullOrEmpty(key)) {
                return;
            }
            if (Infos.Exists(i => i.Key == key)) {
                Infos.RemoveAt(Infos.FindIndex(i => i.Key == key));
            }

            Infos.Add(new Info {
                Key = key,
                Value = value.ToString()
            });
        }
    }

    public string Get(string key) {
        return this[key].ToString();
    }

    public T Get<T>(string key) {
        return (T)Convert.ChangeType(this[key], typeof(T));
    }

    public void Add(string key, object value) {
        this[key] = value.ToString();
    }

    public bool ContainsKey(string key) {
        return Infos.Exists(i => i.Key == key);
    }

    public bool Remove(string key) {
        if (!ContainsKey(key)) {
            return true;
        }
        Infos.RemoveAt(Infos.FindIndex(i => i.Key == key));
        return true;
    }

    public bool TryGetValue(string key, out object value) {
        if (!ContainsKey(key)) {
            value = "";
            return false;
        }
        value = this[key];
        return true;
    }

    public void Add(KeyValuePair<string, object> item) {
        Infos.Add(new Info {
            Key = item.Key,
            Value = item.Value.ToString()
        });
    }

    public void Clear() {
        Infos.Clear();
    }

    public bool Contains(KeyValuePair<string, object> item) {
        return ContainsKey(item.Key) && this[item.Key] == item.Value;
    }

    public void CopyTo(KeyValuePair<string, object>[] array, int arrayIndex) {
    }

    public bool Remove(KeyValuePair<string, object> item) {
        if (!Contains(item)) {
            return true;
        }

        Remove(item.Key);
        return true;
    }

    public IEnumerator<KeyValuePair<string, object>> GetEnumerator() {
        return Infos.Select(i => new KeyValuePair<string, object>(i.Key, i.Value)).GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator() {
        return this.GetEnumerator();
    }
}