﻿using UnityEngine;
using UnityEngine.Events;
using System;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace GameLibrary.Utility {

    public class UnityEventHandler : MonoBehaviour {

        [Serializable]
        public class IntEvent : UnityEvent<int> {
        }

        public UnityEvent onEnable;
        public UnityEvent onStart;
        public UnityEvent onDisable;
        public UnityEvent onUpdate;
        public UnityEvent onAwake;
        public UnityEvent onDestroy;
        public IntEvent onLevelWasLoaded;

        private void Awake() {
            onAwake.Invoke();
        }

        private void Start() {
            onStart.Invoke();
        }

        private void Update() {
            onUpdate.Invoke();
        }

        private void OnEnable() {
            onEnable.Invoke();
        }

        private void OnDisable() {
            onDisable.Invoke();
        }

        private void OnDestroy() {
            onDestroy.Invoke();
        }

        private void OnLevelWasLoaded(int level) {
            onLevelWasLoaded.Invoke(level);
        }

        public void DestroyThis() {
            Destroy(gameObject);
        }
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(UnityEventHandler))]
    public class EventEditor : Editor {
        private int tabIndex = 0;

        public override void OnInspectorGUI() {
            tabIndex = GUILayout.Toolbar(tabIndex, new string[] { "Actives", "Updates", "LifeTime" });
            EditorGUILayout.Space();
            switch (tabIndex) {
                default:
                case 0:
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("onEnable"), true);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("onStart"), true);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("onDisable"), true);
                    break;

                case 1:
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("onUpdate"), true);
                    break;

                case 2:
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("onLevelWasLoaded"), true);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("onAwake"), true);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("onDestroy"), true);
                    break;
            }

            serializedObject.ApplyModifiedProperties();
        }
    }

#endif
}