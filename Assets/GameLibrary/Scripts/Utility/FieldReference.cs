﻿using System;
using UnityEngine;
using System.Linq;
using System.Reflection;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace GameLibrary.Utility {

    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public abstract class FieldReferencableAttribute : Attribute {
    }

    [Serializable]
    public class FieldReference {

        [SerializeField]
        [ClassAttribute(typeof(FieldReferencableAttribute))]
        public ClassTypeReference Class;

        public string FieldName {
            get {
                return _fieldName;
            }
        }

        [SerializeField]
        private string _fieldName;
    }

#if UNITY_EDITOR

    [CanEditMultipleObjects]
    [CustomPropertyDrawer(typeof(FieldReference), true)]
    public class FieldDrawer : PropertyDrawer {
        private const string KEY_Class = "Class";
        private const string KEY_FieldName = "_fieldName";
        private string[] fields;
        private bool init = false;
        private int fieldIndex;
        private float lineHeight = EditorGUIUtility.singleLineHeight;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            return property.isExpanded ? lineHeight * 3 : lineHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            if (!init) {
                init = true;
                getFields(property.FindPropertyRelative(KEY_FieldName).stringValue, property.FindPropertyRelative(KEY_Class).FindPropertyRelative("_classRef").stringValue);
            }
            label = EditorGUI.BeginProperty(position, label, property);

            Rect tab = new Rect(position.x, position.y, position.width, lineHeight);

            //position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            property.isExpanded = EditorGUI.Foldout(tab, property.isExpanded, label, toggleOnLabelClick: true);
            if (property.isExpanded) {
                int indent = EditorGUI.indentLevel;
                EditorGUI.indentLevel = indent > 1 ? indent : 1;
                Rect classRect = new Rect(position.x, position.y + lineHeight, position.width, lineHeight);
                Rect fieldRect = new Rect(position.x, position.y + lineHeight * 2, position.width, lineHeight);
                EditorGUI.BeginChangeCheck();
                EditorGUI.PropertyField(classRect, property.FindPropertyRelative(KEY_Class));
                if (EditorGUI.EndChangeCheck()) {
                    getFields(property.FindPropertyRelative(KEY_FieldName).stringValue, property.FindPropertyRelative(KEY_Class).FindPropertyRelative("_classRef").stringValue);
                }

                DrawFields(fieldRect, property);
                EditorGUI.indentLevel = indent;
            }
            EditorGUI.EndProperty();
        }

        private void DrawFields(Rect position, SerializedProperty property) {
            if (fields != null && fields.Length > 0) {
                fieldIndex = EditorGUI.Popup(position, "Field", fieldIndex, fields);
                string field = fields[fieldIndex];
                property.FindPropertyRelative(KEY_FieldName).stringValue = field;
            } else {
                EditorGUI.PropertyField(position, property.FindPropertyRelative(KEY_FieldName));
            }
        }

        private void getFields(string fieldName, string modelType) {
            var type = Type.GetType(modelType, false, true);
            getFields(fieldName, type);
        }

        private void getFields(string fieldName, Type modelType) {
            if (modelType == null) {
                fields = null;
                fieldIndex = 0;
                return;
            }
            fields = modelType.GetFields().Cast<MemberInfo>().Concat(modelType.GetProperties()).Select(s => s.Name).OrderBy(n => n[0]).ToArray();
            //ReflectionUtility.GetFieldsAndProperties(modelType).Select(s => s.FieldName).ToArray();
            if (fields.Contains(fieldName)) {
                fieldIndex = fields.ToList().IndexOf(fieldName);
            } else {
                fieldIndex = 0;
            }
        }
    }

#endif
}