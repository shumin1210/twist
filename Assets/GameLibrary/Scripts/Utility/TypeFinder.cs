﻿using System;
using System.Linq;
using System.Reflection;

namespace GameLibrary.Utility {

    public static class TypeFinder {

        /// <summary>
        /// 根据class name反射获取Type
        /// </summary>
        /// <param name="className"></param>
        /// <returns></returns>
        public static Type ResolveType(string className) {
            Assembly assembly = Assembly.GetExecutingAssembly();
            Type type = null;
            if (string.IsNullOrEmpty(className)) {
                return type;
            }
            type = Type.GetType(className) ?? assembly.GetTypes().FirstOrDefault(t => t.FullName.Contains(className));
            if (type == null) {
                //throw new Exception(string.Format("Cant't find Class by class name:'{0}'", className));
            }
            return type;
        }
    }
}