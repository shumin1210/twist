﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using UnityEngine;
using UnityEngine.UI;

namespace GameLibrary.Utility {
    /*
     * 2018-07-31 Updates: callbackMap 以物件為key值，記錄最新回調方法，防止同一物件被重複回調
     */

    public class WWWLoader : SingletonComponent<WWWLoader> {

        private struct Res {
            public int size;
            public object Resource;
        }

        private interface ILoadCompleteAction {

            void OnComplete(object val);
        }

        private class LoadCompleteAction<T> : ILoadCompleteAction {
            private Action<T> onComplete;

            public LoadCompleteAction(Action<T> onComplete) {
                this.onComplete = onComplete;
            }

            public void OnComplete(object val) {
                if (val is T) {
                    if (onComplete != null) {
                        onComplete.Invoke((T)val);
                    }
                } else {
                    Debug.LogWarningFormat("val is not typeof {0}", typeof(T).ToString());
                }
            }
        }

        private static Dictionary<string, Dictionary<object, ILoadCompleteAction>> callbackMap = new Dictionary<string, Dictionary<object, ILoadCompleteAction>>();
        internal static Dictionary<string, object> cached = new Dictionary<string, object>();

        internal static bool tryGetCahcedRes<T>(string key, out T val) {
            if (cached.ContainsKey(key) && cached[key] is T) {
                val = (T)cached[key];
                return true;
            } else {
                val = default(T);
                return false;
            }
        }

        public static void ClearCache() {
            cached.Clear();
        }

        private static void onImageLoaded(Image target, Sprite sprite, Action<Sprite> onComplete = null) {
            if (sprite && target) {
                target.sprite = sprite;
            }

            if (onComplete != null) {
                onComplete(sprite);
            }
        }

        //TODO cache size control
        //private int maxxCachedCount = 10<<20;

        //private void autoReleaseCache() {
        //    if (cached.Count > maxxCachedCount) {
        //    }
        //}

        public static void LoadSprite(string url, Action<Sprite> onComplete = null) {
            bool needStart = !callbackMap.ContainsKey(url);
            if (needStart) {
                callbackMap.Add(url, new Dictionary<object, ILoadCompleteAction>());
            }
            callbackMap[url][new object()] = new LoadCompleteAction<Sprite>(onComplete);

            if (needStart) {
                Instance.StartCoroutine(loadSprite(url));
            }
        }

        public static void LoadSpriteFromWWW(Image target, string url, Action<Sprite> onComplete = null) {
            bool needStart = !callbackMap.ContainsKey(url);

            if (needStart) {
                callbackMap.Add(url, new Dictionary<object, ILoadCompleteAction>());
            }
            callbackMap[url][target] = new LoadCompleteAction<Sprite>(s => onImageLoaded(target, s, onComplete));
            if (needStart) {
                Instance.StartCoroutine(loadSprite(url));
            }
        }

        internal static IEnumerator loadSprite(string url) {
            Sprite sprite = null;
            if (!cached.ContainsKey(url) || !(cached[url] is Sprite)) {
                cached[url] = null;
                using (WWW www = new WWW(Uri.EscapeUriString(url))) {
                    yield return www;

                    if (www.error != null) {
                        Debug.LogFormat("WWWLoader.LoadSprite Error : url={0}\r\nerror={1}", url, www.error);
                    } else {
                        sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
                        sprite.name = url;
                        cached.TryAdd(url, sprite, true);
                        Destroy(www.texture);
                    }
                }
            }
            sprite = (Sprite)cached[url];
            if (callbackMap.ContainsKey(url) && callbackMap[url].Count > 0) {
                var d = callbackMap;
                callbackMap[url].Foreach(ac => ac.Value.OnComplete(sprite));
                callbackMap[url].Clear();
                callbackMap.Remove(url);
            }
        }

        public static void LoadText(string url, Action<string> onComplete) {
            bool needStart = !callbackMap.ContainsKey(url);
            if (needStart) {
                callbackMap.Add(url, new Dictionary<object, ILoadCompleteAction>());
            }

            callbackMap[url][new object()] = new LoadCompleteAction<string>(onComplete);

            if (needStart) {
                Instance.StartCoroutine(loadText(url));
            }
        }

        internal static IEnumerator loadText(string url) {
            using (WWW www = new WWW(url)) {
                yield return www;

                if (callbackMap.ContainsKey(url) && callbackMap[url].Count > 0) {
                    callbackMap[url].Foreach(ac => ac.Value.OnComplete(www.text));
                    callbackMap[url].Clear();
                    callbackMap.Remove(url);
                }
            }
        }
    }

    public static class WWWLoaderExtension {

        /// <summary>
        /// 用WWW下載Sprite
        /// </summary>
        /// <param name="target"> 要套用此Sprite的Image物件 </param>
        /// <param name="url"> Sprite網址 </param>
        public static void LoadSpriteFromWWW(this Image target, string url, Action<Sprite> onComplete = null) {
            if (string.IsNullOrEmpty(url)) {
                return;
            }
            WWWLoader.LoadSpriteFromWWW(target, url, onComplete);
        }
    }
}