﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using UnityEngine;

namespace GameLibrary.Utility.SerializableDictionary {

    [Serializable]
    public class DataMap {
        protected Type valType;
        protected ISerializable _object;
    }

    public class DataMap<TKey, TValue> : DataMap, IDictionary<TKey, TValue>, ISerializationCallbackReceiver, ISerializable, IDeserializationCallback {

        [SerializeField]
        protected TKey[] m_keys;

        [SerializeField]
        protected TValue[] m_values;

        private Dictionary<TKey, TValue> _dictionary {
            get {
                return base._object as Dictionary<TKey, TValue>;
            }
        }

        public ICollection<TKey> Keys {
            get {
                return _dictionary.Keys;
            }
        }

        public ICollection<TValue> Values {
            get {
                return _dictionary.Values;
            }
        }

        public int Count {
            get {
                return _dictionary.Count;
            }
        }

        public bool IsReadOnly {
            get {
                return false;
            }
        }

        public TValue this[TKey key] {
            get {
                return _dictionary[key];
            }

            set {
                _dictionary[key] = value;
            }
        }

        public DataMap() {
            base._object = new SerializableDictionary<TKey, TValue>();
        }

        public void CopyFrom(IDictionary<TKey, TValue> dict) {
            _dictionary.Clear();
            foreach (var kvp in dict) {
                _dictionary[kvp.Key] = kvp.Value;
            }
        }

        public void OnAfterDeserialize() {
            if (m_keys != null && m_values != null && m_keys.Length == m_values.Length) {
                _dictionary.Clear();
                int n = m_keys.Length;
                for (int i = 0; i < n; ++i) {
                    _dictionary[m_keys[i]] = m_values[i];
                }

                m_keys = null;
                m_values = null;
            }
        }

        public virtual void OnBeforeSerialize() {
            int n = _dictionary.Count;
            m_keys = new TKey[n];
            m_values = new TValue[n];

            int i = 0;
            foreach (var kvp in _dictionary) {
                m_keys[i] = kvp.Key;
                m_values[i] = kvp.Value;
                ++i;
            }
        }

        public void Add(TKey key, TValue value) {
            _dictionary.Add(key, value);
        }

        public bool ContainsKey(TKey key) {
            return _dictionary.ContainsKey(key);
        }

        public bool Remove(TKey key) {
            return _dictionary.Remove(key);
        }

        public bool TryGetValue(TKey key, out TValue value) {
            return _dictionary.TryGetValue(key, out value);
        }

        public void Add(KeyValuePair<TKey, TValue> item) {
            _dictionary.Add(item.Key, item.Value);
        }

        public void Clear() {
            _dictionary.Clear();
        }

        public bool Contains(KeyValuePair<TKey, TValue> item) {
            return _dictionary.Contains(item);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) {
            foreach (var item in array) {
                if (_dictionary.ContainsKey(item.Key)) {
                    this[item.Key] = item.Value;
                } else {
                    Add(item);
                }
            }
        }

        public bool Remove(KeyValuePair<TKey, TValue> item) {
            return _dictionary.Remove(item.Key);
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() {
            return _dictionary.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return _dictionary.GetEnumerator();
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context) {
            _dictionary.GetObjectData(info, context);
        }

        public void OnDeserialization(object sender) {
            _dictionary.OnDeserialization(sender);
        }
    }

    [Serializable]
    public class StringMap : DataMap<string, string> {
    }

    [Serializable]
    public class SpriteMap : DataMap<string, Sprite> {
    }

    [Serializable]
    public class UObjectMap : DataMap<string, UnityEngine.Object> {
    }

    [Serializable]
    public class GameObjectMap : DataMap<string, GameObject> {
    }

    [Serializable]
    public class DynamicValueMap : DataMap<string, DynamicValue> {
    }
}