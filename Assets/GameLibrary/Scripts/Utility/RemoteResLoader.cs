﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameLibrary.Core;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GameLibrary.Utility {

    public class RemoteResLoader : MonoBehaviour {

        [Serializable]
        public class OnLoadEvent : UnityEvent<object> {
        }

        [Serializable]
        public class OnSpriteLoaded : UnityEvent<Sprite> {
        }

        public UnityEvent<object> OnLoaded = new OnLoadEvent();
        private Coroutine task;
        private readonly static Type[] Type_Text = new Type[] { typeof(TextAsset), typeof(String) };
        private readonly static Type[] Type_Image = new Type[] { typeof(Sprite) };

        #region For Real Gameobject

        public string ConfigKey;

        [SerializeField]
        public OnSpriteLoaded OnCompleted = new OnSpriteLoaded();

        private void OnEnable() {
            var url = GlobalSettings.Get<string>(ConfigKey);
            if (!String.IsNullOrEmpty(url)) {
                WWWLoader.LoadSprite(url, onComplete: s => OnCompleted.Invoke(s));
            }
        }

        #endregion For Real Gameobject

        public void LoadRes<T>(string url) {
            //if (!gameObject.activeInHierarchy) {
            //    return;
            //}
            Type type = typeof(T);
            if (Type_Text.Contains(type)) {
                task = CoroutineUtility.CallStartCoroutine(loadText(url));
            } else if (Type_Image.Contains(type)) {
                task = CoroutineUtility.CallStartCoroutine(loadSprite(url));
            } else {
                throw new ArgumentException(type.Name + " is not supported.");
            }
        }

        public void Stop() {
            if (task != null) {
                CoroutineUtility.CallStopCoroutine(task);
            }
        }

        private void OnDestroy() {
            Stop();
        }

        private IEnumerator loadText(string url) {
            string result = "";
            if (!WWWLoader.tryGetCahcedRes(url, out result)) {
                yield return WWWLoader.loadText(url);
                result = WWWLoader.cached[url].ToString();
            }
            OnLoaded.Invoke(result);
            Destroy(this);
        }

        private IEnumerator loadSprite(string url) {
            Sprite result = null;
            if (!WWWLoader.tryGetCahcedRes(url, out result)) {
                yield return WWWLoader.loadSprite(url);
                result = WWWLoader.cached[url] as Sprite;
            }
            OnLoaded.Invoke(result);
            Destroy(this);
        }

        public static void Load<T>(Component target, string url, Action<T> onLoaded) {
            Load(target.gameObject, url, onLoaded);
        }

        public static void Load<T>(GameObject target, string url, Action<T> onLoaded) {
            RemoteResLoader loader = target.GetOrAddComponent<RemoteResLoader>();
            loader.Stop();
            loader.OnLoaded.AddListener(o => onLoaded((T)o));
            loader.LoadRes<T>(url);
        }
    }
}