﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MicRecorder : MonoBehaviour {

    [Serializable]
    private class CompleteEvent : UnityEvent<AudioClip> {
    }

    public bool IsRecording;
    public AudioSource output;
    public int Length = 10;
    public float Remains = 0;
    public int Frequency = 44100;
    private static string default_device = "Built-in Microphone";
    private bool hasPermission = false;
    private Coroutine task;

    [SerializeField]
    private CompleteEvent OnComplete = new CompleteEvent();

    private UnityEvent OnError = new UnityEvent();

    public void Record() {
        if (IsRecording) {
            StopCoroutine(task);
            IsRecording = false;
        }

        task = StartCoroutine(StartRecord());
    }

    private IEnumerator StartRecord() {
        yield return obtainPermission();

        if (!hasPermission) {
            yield break;
        }

        IsRecording = true;
        try {
            output.clip = Microphone.Start(default_device, false, Length, Frequency);
        } catch (Exception) {
            OnError.Invoke();
            IsRecording = false;
            yield break;
        }
        Remains = Length;
        while (Remains > 0) {
            Remains -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        Remains = 0;
        OnComplete.Invoke(output.clip);
        IsRecording = false;
    }

    private IEnumerator obtainPermission() {
#if UNITY_EDITOR
        hasPermission = true;
        yield break;
#else
        yield return Application.RequestUserAuthorization(UserAuthorization.Microphone);
        hasPermission = Application.HasUserAuthorization(UserAuthorization.Microphone);
#endif
    }
}