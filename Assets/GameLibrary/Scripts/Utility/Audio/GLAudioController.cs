﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameLibrary.Utility {

    public struct GLAudioPlayConfig {
        public bool Loop;
        public AudioChannel Channel;
        public string Group;
        public bool SingleSource;
    }

    /// <summary>音樂控制的核心</summary>
    public class GLAudioController : SingletonComponent<GLAudioController> {
        private const string trackName = "AudioTrack_";
        private const int TRACK_NULL = -1;
        private const int musicTrackID = 1;

        private static int tracks = musicTrackID + 1;

        private static AudioSource trackBGM {
            get {
                return checkChannelObject(AudioChannel.Music) ?? channels[AudioChannel.Music].GetComponent<AudioSource>();
            }
        }

        private static Dictionary<AudioChannel, GameObject> channels = new Dictionary<AudioChannel, GameObject>();

        private static Dictionary<string, int> groups = new Dictionary<string, int>();
        private static HashSet<AudioSource> pauseds = new HashSet<AudioSource>();
        private static Queue<AudioSource> idleSources = new Queue<AudioSource>();
        private static Dictionary<int, AudioSource> playingSources = new Dictionary<int, AudioSource>();

        private static Queue<AudioQueue> idleQueueSources = new Queue<AudioQueue>();
        private static Dictionary<int, AudioQueue> queueSources = new Dictionary<int, AudioQueue>();

        [SerializeField]
        private bool isPlaying = false;

        protected override void Start() {
            base.Start();
            ChanneledAudio.Initialize();
        }

        public static int PlayQueue(AudioChannel channel, string[] queue, Action callback = null) {
            return PlayQueue(channel, queue.Select(s => DataShared.Instance.Audios[s] ?? DataShared.Instance.StaticAudios[s]).ToArray(), callback);
        }

        public static int PlayQueue(AudioChannel channel, AudioClip[] queue, Action callback = null) {
            AudioQueue aq = createQueue(channel);
            aq.Play(queue, callback);
            queueSources.Add(tracks, aq);
            return tracks++;
        }

        private static AudioQueue createQueue(AudioChannel channel) {
            //create queue instance
            AudioSource source = checkChannelObject(channel) ?? getPooledAudioSource(channel);

            // ref audioSource to queue
            AudioQueue queue = getPooledAudioQueue(channel);
            queue.source = source;

            return queue;
        }

        private static AudioQueue getPooledAudioQueue(AudioChannel channel) {
            AudioQueue queue;

            if (idleQueueSources.Count == 0) {
                queue = channels[channel].AddComponent<AudioQueue>();
            } else {
                queue = idleQueueSources.Dequeue();
            }
            return queue;
        }

        private static AudioClip getAudioClipFromDataShared(string key) {
            return DataShared.Instance.Audios[key] ?? DataShared.Instance.StaticAudios[key];
        }

        public static int PlayMusic(string key) {
            return PlayMusic(getAudioClipFromDataShared(key));
        }

        public static int PlayMusic(AudioClip clip) {
            return Play(clip, new GLAudioPlayConfig {
                Channel = AudioChannel.Music,
                Loop = true
            });
        }

        public static int PlaySE(string key, bool loop = false) {
            return PlaySE(getAudioClipFromDataShared(key), loop);
        }

        public static int PlaySE(AudioClip clip, bool loop = false) {
            return Play(clip, new GLAudioPlayConfig {
                Channel = AudioChannel.SE,
                Loop = loop
            });
        }

        public static int PlayVoice(string key, bool loop = false) {
            return PlayVoice(getAudioClipFromDataShared(key), loop);
        }

        public static int PlayVoice(AudioClip clip, bool loop = false) {
            return Play(clip, new GLAudioPlayConfig {
                Channel = AudioChannel.Voice,
                Loop = loop
            });
        }

        private static AudioSource checkChannelObject(AudioChannel channel) {
            AudioSource source = null;
            if (!channels.ContainsKey(channel)) {
                var go = Instance.gameObject.InstantiateGameObject(channel.ToString());
                source = go.AddComponent<AudioSource>();
                ChanneledAudio.AddSourceToChannel(source, channel);
                channels.Add(channel, go);
            }
            return source;
        }

        public static int Play(AudioChannel channel, AudioClip clip, bool loop) {
            return Play(clip, new GLAudioPlayConfig {
                Channel = channel,
                Loop = loop
            });
        }

        public static int Play(AudioClip clip, GLAudioPlayConfig config) {
            if (!clip) {
                return -1;
            }
            lock (Instance) {
                AudioChannel channel = config.Channel;
                AudioSource source = checkChannelObject(channel);
                switch (channel) {
                    case AudioChannel.Music:
                        source = source ?? channels[channel].GetComponent<AudioSource>();
                        if (clip && (source.clip != clip || !source.isPlaying)) {
                            source.PlayClip(clip, true);
                        }
                        return musicTrackID;

                    default:
                        if (config.SingleSource && !canPlaySingleSource(clip)) {
                            return TRACK_NULL;
                        }
                        if (!string.IsNullOrEmpty(config.Group)) {
                            if (groups.ContainsKey(config.Group)) {
                                int t = groups[config.Group];
                                if (!playingSources.ContainsKey(t) || !playingSources[t].isPlaying) {
                                    releaseAudioSource(t);
                                } else {
                                    return TRACK_NULL;
                                }
                            }

                            groups[config.Group] = tracks;
                        }

                        source = source ?? getPooledAudioSource(channel);
                        source.loop = config.Loop;
                        playingSources.Add(tracks, source);

                        ChanneledAudio.ChannelSettings[channel].ApplyTo(source);
                        source.PlayClip(clip, config.Loop);
                        Instance.tracksMonitor();
                        return tracks++;
                }
            }
        }

        private static bool canPlaySingleSource(AudioClip clip) {
            if (!clip) {
                return false;
            }
            return !playingSources.Exist(a => a.Value.clip == clip);
        }

        private static AudioSource getPooledAudioSource(AudioChannel channel) {
            AudioSource source;
            if (idleSources.Count == 0) {
                source = channels[channel].AddComponent<AudioSource>();
                ChanneledAudio.AddSourceToChannel(source, channel);
                source.playOnAwake = false;
            } else {
                source = idleSources.Dequeue();
            }

            return source;
        }

        private static void trackAction(AudioChannel channel, Action<AudioSource> action, int id = 0) {
            switch (channel) {
                case AudioChannel.Music:
                    action(trackBGM);
                    break;

                default:
                    AudioSource source;
                    if (playingSources.TryGetValue(id, out source)) {
                        try {
                            action(source);
                        } catch (Exception) {
                            Debug.LogFormat("AudioSource[{0}] is null", id);
                        }
                    }
                    break;
            }
        }

        private static void resume(AudioSource source) {
            if (pauseds.Contains(source)) {
                pauseds.Remove(source);
            }

            source.UnPause();
        }

        public static void Resume(AudioChannel channel, int id = 0) {
            trackAction(channel, resume, id);
        }

        private static void pause(AudioSource source) {
            if (!pauseds.Contains(source)) {
                pauseds.Add(source);
            }

            source.Pause();
        }

        public static void Pause(AudioChannel channel, int id = 0) {
            trackAction(channel, pause, id);
        }

        public static void Stop(AudioChannel channel, int id = 0) {
            trackAction(channel, a => a.Stop(), id);
        }

        public static bool CheckPlaying(AudioChannel channel, int id) {
            switch (channel) {
                case AudioChannel.Music:
                    return trackBGM.isPlaying;

                default:
                    AudioSource source;
                    if (playingSources.TryGetValue(id, out source)) {
                        return source ? source.isPlaying : false;
                    }
                    return false;
            }
        }

        private void tracksMonitor() {
            if (isPlaying) {
                return;
            } else {
                StartCoroutine(monitoring());
            }
        }

        private IEnumerator monitoring() {
            isPlaying = true;
            while (checkPlaying()) {
                yield return new WaitForSeconds(.1f);
            }
            isPlaying = false;
        }

        private static void releaseAudioSource(int track) {
            AudioSource source = playingSources[track];
            playingSources.Remove(track);
            pauseds.Remove(source);
            source.clip = null;
            idleSources.Enqueue(source);
        }

        private bool checkPlaying() {
            lock (Instance) {
                if (playingSources.Count == 0) {
                    return false;
                }

                var keys = playingSources.Keys.ToArray();
                foreach (int item in keys) {
                    if (!playingSources[item].isPlaying && !pauseds.Contains(playingSources[item])) {
                        releaseAudioSource(item);
                        string group = groups.Find(v => v.Value == item).Key;
                        if (!string.IsNullOrEmpty(group)) {
                            groups.Remove(group);
                        }
                    }
                }

                keys = queueSources.Keys.ToArray();
                foreach (int item in keys) {
                    AudioQueue queue = queueSources[item];
                    if (!queue.isPlaying) {
                        queueSources.Remove(item);
                        idleSources.Enqueue(queue.source);
                        queue.source = null;
                        queue.clips = null;

                        idleQueueSources.Enqueue(queue);
                    }
                }
                return playingSources.Count != 0;
            }
        }
    }
}