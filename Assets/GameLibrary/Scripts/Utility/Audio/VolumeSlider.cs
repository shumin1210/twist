﻿using UnityEngine;
using UnityEngine.UI;

namespace GameLibrary.Utility {

    [RequireComponent(typeof(Slider))]
    public class VolumeSlider : MonoBehaviour {
        public AudioChannel channel;

        private void Start() {
            GetComponent<Slider>().onValueChanged.AddListener(changeVolume);
        }

        private void changeVolume(float val) {
            ChanneledAudio.SetVolume(channel, val);
        }

        private void OnEnable() {
            GetComponent<Slider>().value = ChanneledAudio.GetVolume(channel);
        }
    }
}