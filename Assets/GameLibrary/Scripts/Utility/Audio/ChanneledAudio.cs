﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameLibrary.Utility {

    [Serializable]
    public enum AudioChannel {
        Music,
        SE,
        Voice
    }

    [RequireComponent(typeof(AudioSource))]
    public class ChanneledAudio : MonoBehaviour {

        public class ChannelSetting {
            public float RealVol;

            public float Volume {
                get {
                    return Mute ? 0 : RealVol;
                }
                set {
                    RealVol = value;
                }
            }

            public bool Mute;

            public void ApplyTo(AudioSource src) {
                src.mute = Mute;
                src.volume = Volume;
            }
        }

        public const string PREF_MUSIC_ON = "settings.audio.on";

        public static Dictionary<AudioChannel, ChannelSetting> ChannelSettings {
            get; protected set;
        }

        private static Dictionary<AudioChannel, HashSet<AudioSource>> channels;

        public static bool MusicOn {
            get {
                return PlayerPrefUtil.Get(PREF_MUSIC_ON, true);
            }
            set {
                PlayerPrefUtil.Set(PREF_MUSIC_ON, value);
            }
        }

        public static void AddSourceToChannel(AudioSource source, AudioChannel Channel) {
            channels[Channel].Add(source);
            source.volume = ChannelSettings[Channel].Volume;
        }

        public static void RemoveSourceFromChannel(AudioSource source, AudioChannel Channel) {
            channels[Channel].Remove(source);
        }

        public static bool IsChannelMute(AudioChannel channel) {
            return ChannelSettings[channel].Mute;
        }

        public static float GetVolume(AudioChannel channel) {
            return ChannelSettings[channel].Volume;
        }

        private static void eachChannel(Action<AudioSource> audioAction = null, Action<AudioChannel> channelAction = null) {
            foreach (AudioChannel channel in Enum.GetValues(typeof(AudioChannel))) {
                if (channelAction != null) {
                    channelAction(channel);
                }
                eachAudioInChannel(channel, audioAction);
            }
        }

        private static void eachAudioInChannel(AudioChannel channel, Action<AudioSource> audioAction = null) {
            if (audioAction != null) {
                foreach (AudioSource item in channels[channel]) {
                    if (item) {
                        audioAction(item);
                    }
                }
            }
        }

        public static void Initialize() {
            ToggleMasterMute(!MusicOn);
        }

        public static void ToggleMasterMute(bool mute) {
            eachChannel(
                audioAction: a => a.mute = mute,
                channelAction: c => ChannelSettings[c].Mute = mute);
            MusicOn = !mute;
        }

        public static void ToggleMute(AudioChannel channel, bool mute) {
            ChannelSettings[channel].Mute = mute;
            eachAudioInChannel(channel, a => a.mute = mute);
        }

        public static void SetMasterVolume(float val) {
            eachChannel(
                a => a.volume = val,
                c => ChannelSettings[c].Volume = TMath.Range(val, 0, 1));
        }

        public static void SetVolume(AudioChannel channel, float val) {
            ChannelSettings[channel].Volume = TMath.Range(val, 0, 1);
            eachAudioInChannel(channel, a => a.volume = val);
        }

        static ChanneledAudio() {
            ChannelSettings = new Dictionary<AudioChannel, ChannelSetting>();
            channels = new Dictionary<AudioChannel, HashSet<AudioSource>>();
            foreach (AudioChannel s in Enum.GetValues(typeof(AudioChannel))) {
                channels.Add(s, new HashSet<AudioSource>());
                ChannelSettings.Add(s, new ChannelSetting {
                    RealVol = 1,
                    Volume = 1,
                    Mute = false
                });
                //volumes.Add(s, 1f);
            }
        }

        public AudioSource source;
        public AudioChannel Channel;

        private void Start() {
            source = GetComponent<AudioSource>();
            channels[Channel].Add(source);
            source.volume = ChannelSettings[Channel].Volume;
        }

        private void OnDestroy() {
            channels[Channel].Remove(source);
        }
    }

#if UNITY_EDITOR

    [UnityEditor.CanEditMultipleObjects]
    [UnityEditor.CustomEditor(typeof(ChanneledAudio))]
    public class ChanneledAudioEditor : UnityEditor.Editor {

        public override void OnInspectorGUI() {
            ChanneledAudio o = target as ChanneledAudio;
            UnityEditor.EditorGUILayout.PropertyField(serializedObject.FindProperty("Channel"));
            serializedObject.ApplyModifiedProperties();
            UnityEditor.EditorGUILayout.LabelField("Volume", ChanneledAudio.GetVolume(o.Channel).ToString());
        }
    }

#endif
}