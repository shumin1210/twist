﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameLibrary.Utility {

    public static class AudioSourceExtension {

        public static void PlayClip(this AudioSource src, string res) {
            src.PlayClip(DataShared.Instance.Audios[res]);
        }

        public static void PlayClip(this AudioSource src, string res, bool loop) {
            src.PlayClip(DataShared.Instance.Audios[res], loop);
        }

        public static void PlayClip(this AudioSource src, AudioClip clip) {
            PlayClip(src, clip, false);
        }

        public static void PlayClip(this AudioSource src, AudioClip clip, bool loop) {
            src.clip = clip;
            src.loop = loop;
            src.Play();
        }
    }
}