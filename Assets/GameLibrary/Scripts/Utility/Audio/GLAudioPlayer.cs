﻿using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace GameLibrary.Utility {

    public class GLAudioPlayer : MonoBehaviour {
        public bool useAudioClip = false;
        public AudioChannel Channel;
        public bool PlayWhenActive = false;
        public bool IsButtonSE = false;
        public bool Loop = false;
        public string key;
        public AudioClip clip;
        public int trackID;
        public string Group;

        [Tooltip("True則同一音源只會允許一個音軌播放")]
        public bool SingleSource;

        public bool TriggerOnClickBeforePlayEnd = false;

        private void Start() {
            if (IsButtonSE) {
                Button btn = GetComponent<Button>();
                if (btn) {
                    btn.onClick.AddListener(Play);
                }
            }
        }

        private void OnEnable() {
            if (PlayWhenActive) {
                Play();
            }
        }

        private void OnDisable() {
            if (PlayWhenActive) {
                Stop();
            }
        }

        public void Play() {
            clip = useAudioClip ? clip : (DataShared.Instance.Audios[key] ?? DataShared.Instance.StaticAudios[key]);
            trackID = GLAudioController.Play(clip, new GLAudioPlayConfig {
                Channel = Channel,
                Loop = Loop,
                Group = Group,
                SingleSource = SingleSource
            });
        }

        public void Pause() {
            GLAudioController.Pause(Channel, trackID);
        }

        public void Stop() {
            GLAudioController.Stop(Channel, trackID);
        }
    }

#if UNITY_EDITOR

    [CanEditMultipleObjects]
    [CustomEditor(typeof(GLAudioPlayer))]
    public class GLAudioPlayerEditor : Editor {
        private SerializedProperty Channel;
        private SerializedProperty PlayWhenActive;
        private SerializedProperty Loop;
        private SerializedProperty IsButtonSE;
        private SerializedProperty UseAudioClip;
        private SerializedProperty SrcKey;
        private SerializedProperty SrcClip;
        private SerializedProperty Group;
        private SerializedProperty SingleSource;

        private void OnEnable() {
            Channel = serializedObject.FindProperty("Channel");
            PlayWhenActive = serializedObject.FindProperty("PlayWhenActive");
            Loop = serializedObject.FindProperty("Loop");
            IsButtonSE = serializedObject.FindProperty("IsButtonSE");
            UseAudioClip = serializedObject.FindProperty("useAudioClip");
            SrcKey = serializedObject.FindProperty("key");
            SrcClip = serializedObject.FindProperty("clip");
            Group = serializedObject.FindProperty("Group");
            SingleSource = serializedObject.FindProperty("SingleSource");
        }

        public override void OnInspectorGUI() {
            GLAudioPlayer player = target as GLAudioPlayer;
            EditorGUILayout.PropertyField(Channel);
            EditorGUILayout.PropertyField(SingleSource);
            if (!SingleSource.boolValue) {
                EditorGUILayout.PropertyField(Group);
            }
            EditorGUILayout.PropertyField(PlayWhenActive);
            if ((AudioChannel)Channel.enumValueIndex == AudioChannel.Music) {
                //serializedObject.FindProperty("Loop").boolValue = true;
            } else {
                EditorGUILayout.PropertyField(Loop);
            }

            EditorGUILayout.PropertyField(IsButtonSE);

            player.useAudioClip = UseAudioClip.boolValue = EditorGUILayout.Toggle("Use AudioClip", player.useAudioClip);
            if (player.useAudioClip) {
                EditorGUILayout.PropertyField(SrcClip);
            } else {
                EditorGUILayout.PropertyField(SrcKey);
            }

            serializedObject.ApplyModifiedProperties();
        }
    }

#endif
}