﻿using System;
using System.Collections;
using UnityEngine;

namespace GameLibrary.Utility {

    [RequireComponent(typeof(AudioSource))]
    public class AudioQueue : MonoBehaviour {
        public AudioClip[] clips;

        public AudioSource source;

        public float delay_sec = .3f;

        public bool isPlaying {
            get; private set;
        }

        private bool stop;
        private Coroutine thread;
        private Action callback;

        private void Start() {
            source = source ?? gameObject.GetComponent<AudioSource>();
        }

        public void Play(AudioClip[] clips, Action onComplete = null) {
            if (!source || !source.isActiveAndEnabled) {
                return;
            }
            source.loop = false;
            source.Stop();
            stop = false;
            this.clips = clips;
            callback = onComplete;
            thread = StartCoroutine(playQueue());
        }

        public void Play(params AudioClip[] clips) {
            Play(clips, null);
        }

        public void Stop() {
            stop = true;
            source.Stop();
        }

        private IEnumerator playQueue() {
            if (clips == null) {
                yield break;
            }
            isPlaying = true;
            int length = clips.Length;
            for (int i = 0; i < length; i++) {
                if (!stop && clips != null) {
                    AudioClip clip = clips[i];
                    source.clip = clip;
                    source.Play();
                    yield return new WaitForSeconds(clip.length + delay_sec);
                }
            }

            if (callback != null) {
                callback();
            }
            isPlaying = false;
        }
    }
}