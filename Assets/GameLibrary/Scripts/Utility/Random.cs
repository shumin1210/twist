﻿using System;
using System.Linq;
using System.Security.Cryptography;

namespace GameLibrary.Utility {

    /// <summary>密碼學式的亂數產生擴充方法</summary>
    public static class RNGCryptoServiceProviderExtensions {
        private static byte[] rb = new byte[4];

        /// <summary>產生一個非負數的亂數</summary>
        /// <param name="rngp">亂數器</param>
        public static int Next(this RNGCryptoServiceProvider rngp) {
            rngp.GetBytes(rb);
            int value = BitConverter.ToInt32(rb, 0);
            return (value < 0) ? -value : value;
        }

        /// <summary>產生一個非負數且不超過最大值 max 的亂數</summary>
        /// <param name="rngp">亂數器</param>
        /// <param name="max">最大值</param>
        public static int Next(this RNGCryptoServiceProvider rngp, int max) {
            return Next(rngp) % (max);
        }

        /// <summary>產生一個非負數且最小值在 min 以上且不超過最大值 max的亂數</summary>
        /// <param name="rngp">亂數器</param>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        public static int Next(this RNGCryptoServiceProvider rngp, int min, int max) {
            return Next(rngp, max - min) + min;
        }
    }

    /// <summary>亂數擴充方法</summary>
    public static class TRandom {

        /// <summary>亂數器實體</summary>
        public static RNGCryptoServiceProvider rr = new RNGCryptoServiceProvider();

        /// <summary>產生亂數</summary>
        /// <returns>亂數值</returns>
        public static int Next() {
            return rr.Next();
        }

        /// <summary>產生一個小於最大值的非負數亂數</summary>
        /// <param name="max">最大值</param>
        public static int Next(int max) {
            return rr.Next(max);
        }

        /// <summary>產生一個非負數且最小值在 min 以上且不超過最大值 max的亂數</summary>
        /// <param name="min">下限值</param>
        /// <param name="max">上限值</param>
        /// <returns></returns>
        public static int Next(int min, int max) {
            return rr.Next(min, max);
        }

        /// <summary>提供產生亂碼字串使用的字元集合</summary>
        public const string ran = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFFGHIJKLMNOPQRSTUVWXYZ";

        /// <summary>產生亂碼字串</summary>
        /// <param name="length">字串長度</param>
        /// <param name="exclude">排除字元</param>
        public static string Generator(int length, string exclude) {
            return Generator(string.Join("", ran.Where(p => !exclude.Contains(p)).Select(s => s.ToString()).ToArray()), length);
        }

        /// <summary>以src為字元來源產生亂碼字串</summary>
        /// <param name="src">字元來源</param>
        /// <param name="length">字串長度</param>
        public static string Generator(string src, int length) {
            string result = "";
            for (int i = 0; i < length; i++) {
                result += src[rr.Next(src.Length)];
            }

            return result;
        }

        /// <summary>產生亂碼字串</summary>
        /// <param name="length">字串長度</param>
        public static string Generator(int length) {
            return Generator(ran, length);
        }
    }
}