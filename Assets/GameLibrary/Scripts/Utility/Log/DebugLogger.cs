﻿using System.Collections.Generic;
using GameLibrary.Core;
using UnityEngine;
using UnityEngine.UI;

namespace GameLibrary.Utility {

    public class DebugLogger : MonoBehaviour {
        public GameObject logger;

        public struct ActType {
            public const string Report = "report.log";
        }

        public bool enableListening = false;
        private static Dictionary<LogType, string> typeColor = new Dictionary<LogType, string>();

        static DebugLogger() {
            typeColor.Add(LogType.Assert, "blue");
            typeColor.Add(LogType.Log, "yellow");
            typeColor.Add(LogType.Error, "red");
            typeColor.Add(LogType.Warning, "orange");
            typeColor.Add(LogType.Exception, "pink");
        }

        public bool EnableStacks;

        public bool enableStack {
            get {
                return EnableStacks;
            }
            set {
                EnableStacks = value;
            }
        }

        private Queue<string> queue = new Queue<string>();

        private Queue<GameObject> texts = new Queue<GameObject>();

        public int MaxLog = 20;
        public string pattern = "[{2}]{0} \r\n\t{1}";
        private const string colorPattern = "<color={1}>{0}</color>";

        public void Report() {
            if (ParentHandler.HasParent) {
                Application.logMessageReceived -= log;
                ParentHandler.Instance.Send(ActType.Report, new {
                    platform = Application.platform.ToString(),
                    version = Application.version.ToString(),
                    log = queue
                });
                Application.logMessageReceived += log;
            }
        }

        public void Clear() {
            queue.Clear();
            texts.Clear();
            logger.ClearChildren();
        }

        private void Start() {
            Application.logMessageReceived += log;
        }

        private void log(string condition, string stackTrace, LogType type) {
            queue.Enqueue(pattern.format(condition, EnableStacks ? stackTrace : "", type.ToString()));
            if (queue.Count > MaxLog) {
                queue.Dequeue();
            }

            if (enableListening && logger) {
                if (texts.Count > MaxLog) {
                    var r = texts.Dequeue();
                    GameObjectRelate.DestroyGameObject(r.transform);
                }
                var row = GameObjectRelate.InstantiateGameObject(logger, "Log");
                var text = row.AddComponent<Text>();
                text.font = DefaultResManager.DefaultFont;
                var consize = row.AddComponent<ContentSizeFitter>();
                consize.verticalFit = ContentSizeFitter.FitMode.PreferredSize;

                texts.Enqueue(row);
                text.text = pattern.format(condition, EnableStacks ? stackTrace : "", colorPattern.format(type.ToString(), typeColor[type]));
            }
        }

        private void OnApplicationQuit() {
            Report();
        }
    }
}