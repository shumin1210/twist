﻿namespace GameLibrary.Utility.Log {

    public interface IContentWriter {

        void Write(string message);
    }
}