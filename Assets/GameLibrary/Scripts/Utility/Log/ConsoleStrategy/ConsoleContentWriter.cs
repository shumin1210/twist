﻿using UnityEngine;

namespace GameLibrary.Utility.Log {

    public class ConsoleContentWriter : IContentWriter {

        public void Write(string message) {
            Debug.Log("Console Log!:" + message);
        }
    }
}