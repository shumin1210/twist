﻿using UnityEngine;

namespace GameLibrary.Utility.Log {

    public class DatabaseContentWriter : IContentWriter {

        public void Write(string message) {
            Debug.Log("Database Log!:" + message);
        }
    }
}