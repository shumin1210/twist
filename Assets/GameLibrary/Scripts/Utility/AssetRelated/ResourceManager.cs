﻿using System;
using System.Collections;
using System.Linq;
using GameLibrary.UI;
using GameLibrary.Utility.SerializableDictionary;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.Video;

namespace GameLibrary.Utility {

    [Serializable]
    public struct SpriteAtlasKeyValyePair {
        public string Key;
        public Sprite[] Value;
    }

    public class ResourceManager : MonoBehaviour {

        [Serializable]
        private class TextMap : DataMap<string, TextAsset> {
        }

        private bool initialized = false;
        public bool Individual = false;

        [SerializeField]
        private GameObject[] prefabs;

        [SerializeField]
        private AssetMap assets;

        [SerializeField]
        private SpriteAtlasKeyValyePair[] sprites;

        [SerializeField]
        private AudioClip[] audios;

        [SerializeField]
        private TextAsset[] texts;

        public string LocalizationLanguage;

        [SerializeField]
        private TextMap LocalizationText;

        private ResManager<GameObject> resPrefabs;

        public ResManager<GameObject> Prefab {
            get {
                return Individual ? resPrefabs : DataShared.Instance.Prefabs;
            }
        }

        private SpriteResManager resSprites;

        public SpriteResManager Sprite {
            get {
                return Individual ? resSprites : DataShared.Instance.Sprites;
            }
        }

        private ResManager<AudioClip> resAudios;

        public ResManager<AudioClip> Audio {
            get {
                return Individual ? resAudios : DataShared.Instance.Audios;
            }
        }

        private ResManager<TextAsset> resTexts;

        public ResManager<TextAsset> Text {
            get {
                return Individual ? resTexts : DataShared.Instance.TextAssets;
            }
        }

        private AssetMap resAssets;

        public AssetMap Assets {
            get {
                return Individual ? resAssets : DataShared.Instance.Assets;
            }
        }

        public DataShared.ScriptableRes Datas;

        private void Start() {
            Initialize();
        }

        public void Initialize() {
            if (initialized) {
                return;
            }

            if (Individual) {
                resPrefabs = new ResManager<GameObject>();
                Prefab.Load(prefabs);

                resSprites = new SpriteResManager();
                sprites.Foreach(s => resSprites.SetAtlas(s.Key, s.Value));

                resAudios = new ResManager<AudioClip>();
                Audio.Load(audios.ToArray());

                resTexts = new ResManager<TextAsset>();
                Text.Load(texts);

                resAssets = new AssetMap();
                resAssets.Load(assets);
            } else {
                DataShared.Instance.Prefabs.Load(prefabs);
                sprites.Foreach(s => DataShared.Instance.Sprites.SetAtlas(s.Key, s.Value));
                DataShared.Instance.Audios.Load(audios.ToArray());
                DataShared.Instance.TextAssets.Load(texts);
                DataShared.Instance.Assets.Load(assets);
                InitializableObject<LocalizedText>.Initialize();
            }
            if (!string.IsNullOrEmpty(LocalizationLanguage)) {
                LocalizationManager.Language = LocalizationLanguage;
            }
            DataShared.Instance.ScriptableObjects.LoadFrom(Datas);
            LocalizationText.Foreach(t => LocalizationManager.SetLanguageAsset(t.Key, t.Value));
            initialized = true;
        }

        private void OnDestroy() {
            if (Individual) {
                resPrefabs.Clear();
                resAudios.Clear();
                resSprites.Clear();
                resTexts.Clear();
            }
            prefabs = null;
            sprites = null;
            audios = null;

            LocalizationText.Foreach(t => LocalizationManager.ReleaseLanguageAsset(t.Key));
            LocalizationText.Clear();
            LocalizationText = null;
        }
    }

    [Serializable]
    public class ResourceManagerField {
        private bool initialized = false;
        public bool Individual = false;

        [SerializeField]
        private GameObject[] prefabs;

        [SerializeField]
        private SpriteAtlasKeyValyePair[] sprites;

        [SerializeField]
        private AudioClip[] audios;

        [SerializeField]
        private TextAsset[] texts;

        private ResManager<GameObject> resPrefabs;

        public ResManager<GameObject> Prefab {
            get {
                return Individual ? resPrefabs : DataShared.Instance.Prefabs;
            }
        }

        private SpriteResManager resSprites;

        public SpriteResManager Sprite {
            get {
                return Individual ? resSprites : DataShared.Instance.Sprites;
            }
        }

        private ResManager<AudioClip> resAudios;

        public ResManager<AudioClip> Audio {
            get {
                return Individual ? resAudios : DataShared.Instance.Audios;
            }
        }

        private ResManager<TextAsset> resTexts;

        public ResManager<TextAsset> Text {
            get {
                return Individual ? resTexts : DataShared.Instance.TextAssets;
            }
        }

        public void Initialize() {
            if (initialized) {
                return;
            }

            if (Individual) {
                resPrefabs = new ResManager<GameObject>();
                Prefab.Load(prefabs);

                resSprites = new SpriteResManager();
                sprites.Foreach(s => resSprites.SetAtlas(s.Key, s.Value));

                resAudios = new ResManager<AudioClip>();
                Audio.Load(audios.ToArray());

                resTexts = new ResManager<TextAsset>();
                Text.Load(texts);
            } else {
                DataShared.Instance.Prefabs.Load(prefabs);
                sprites.Foreach(s => DataShared.Instance.Sprites.SetAtlas(s.Key, s.Value));
                DataShared.Instance.Audios.Load(audios.ToArray());
                DataShared.Instance.TextAssets.Load(texts);

                InitializableObject<LocalizedText>.Initialize();
            }
            initialized = true;
        }

        ~ResourceManagerField() {
            if (Individual) {
                resPrefabs.Clear();
                resAudios.Clear();
                resSprites.Clear();
                resTexts.Clear();
            }
            prefabs = null;
            sprites = null;
            audios = null;
        }
    }
}