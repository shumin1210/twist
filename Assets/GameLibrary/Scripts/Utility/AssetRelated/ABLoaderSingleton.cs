﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

namespace GameLibrary.Utility {

    public static class ABLoader {
        /*
         * class of utilities to load and manage asset bundles(ab).
         * concepts:
         *      download ab:download and can be instanciate immediately.
         *      load ab:load (if not in cache then download).
         *      unload ab:unload specific ab.
         *      change variant:unload current,replace with new one.
         * future feature:
         *      manage ab in storage system.
         *      dynamical detect and auto download from remote.
         */

        private enum AssetState {
            None,
            Downloaded,
            Loaded
        }

        #region Classes

        public class Task_LoadAssetBundle : IEnumerator<AssetBundleConfig>, IEnumerable<AssetBundleConfig> {
            public List<AssetBundleConfig> AssetBundleConfigs;

            public AssetBundleConfig AssetBundleConfig {
                get {
                    return AssetBundleConfigs[CurrentAssetBundleIndex];
                }
            }

            private bool initialized = false;
            private int CurrentAssetBundleIndex = 0;

            public bool LoadResourceImmediately;
            public long CurrentLoadedSize;
            public long TotalSize;
            private long loadedSize;

            public long LoadedSize {
                get {
                    return CurrentLoadedSize + loadedSize;
                }
            }

            public float Progress {
                get {
                    return loadedSize * 1f / TotalSize;
                }
            }

            public string Key {
                get;
                internal set;
            }

            public Action<AssetBundleConfig> OnStart = onStart;
            public Action<float, AssetBundleConfig> OnProgress = onProgress;
            public Action<AssetBundle> OnComplete = onComplete;
            public Action<Dictionary<AssetBundleConfig, AssetBundle>> OnCompleteAll = onCompleteAll;
            public Action<AssetBundleConfig> OnError = onError;
            public Action<string> OnMessage = onMessage;

            public void TriggerOnProgress() {
                OnProgress(Progress, AssetBundleConfig);
            }

            public Task_LoadAssetBundle() {
                initialized = true;
            }

            public Task_LoadAssetBundle(IEnumerable<AssetBundleConfig> assetBundleConfigs) : this() {
                AssetBundleConfigs = assetBundleConfigs.ToList();
            }

            public Task_LoadAssetBundle(params AssetBundleConfig[] assetBundleConfigs) : this() {
                AssetBundleConfigs = assetBundleConfigs.ToList();
            }

            public readonly static Task_LoadAssetBundle Default = new Task_LoadAssetBundle();

            public static implicit operator bool(Task_LoadAssetBundle events) {
                return events.initialized;
            }

            #region default event log

            private static void onStart(AssetBundleConfig abc) {
                Debug.LogFormat("start load asset bundle[{0}]", abc.Name);
            }

            private static void onProgress(float p, AssetBundleConfig abc) {
            }

            private static void onComplete(AssetBundle ab) {
                Debug.LogFormat("asset bundle[{0}] load complete.", ab.name);
            }

            private static void onCompleteAll(Dictionary<AssetBundleConfig, AssetBundle> abcs) {
                Debug.LogFormat("load asset bundle set complete.\r\n[{1}]", abcs.Select(a => "\t" + a.Key).Join("\r\n"));
            }

            private static void onError(AssetBundleConfig abc) {
                Debug.LogFormat("failed to load asset bundle[{0}] from path{1}", abc.Name, abc.fileUrl);
            }

            private static void onMessage(string obj) {
                Debug.Log(obj);
            }

            #endregion default event log

            #region Implements

            public bool MoveNext() {
                if (CurrentAssetBundleIndex < AssetBundleConfigs.Count - 1) {
                    CurrentAssetBundleIndex++;
                    return true;
                } else {
                    return false;
                }
            }

            public void Reset() {
                CurrentAssetBundleIndex = 0;
            }

            public void Dispose() {
                CurrentAssetBundleIndex = 0;
            }

            AssetBundleConfig IEnumerator<AssetBundleConfig>.Current {
                get {
                    return AssetBundleConfig;
                }
            }

            public object Current {
                get {
                    return AssetBundleConfig;
                }
            }

            IEnumerator<AssetBundleConfig> IEnumerable<AssetBundleConfig>.GetEnumerator() {
                Reset();
                while (MoveNext()) {
                    loadedSize += CurrentLoadedSize;
                    CurrentLoadedSize = 0;
                    yield return AssetBundleConfig;
                }
            }

            IEnumerator IEnumerable.GetEnumerator() {
                Reset();
                while (MoveNext()) {
                    yield return Current;
                }
            }

            #endregion Implements
        }

        #endregion Classes

        #region Static Properties

        public static Dictionary<AssetBundleConfig, AssetBundle> ABInUse {
            get; private set;
        }

        private static Dictionary<AssetBundleConfig, HashSet<string>> ABMapping {
            get; set;
        }

        public static Dictionary<string, List<AssetBundleConfig>> ABSets {
            get; private set;
        }

        #endregion Static Properties

        static ABLoader() {
            ABInUse = new Dictionary<AssetBundleConfig, AssetBundle>();
            ABSets = new Dictionary<string, List<AssetBundleConfig>>();

            ABMapping = new Dictionary<AssetBundleConfig, HashSet<string>>();
        }

        #region Start methods

        public static Coroutine LoadAB(AssetBundleConfig info, Action<AssetBundle> onComplete) {
            return CoroutineUtility.CallStartCoroutine(work_loadAssetBundle(new Task_LoadAssetBundle(info) {
                OnComplete = onComplete
            }));
        }

        public static Coroutine LoadAB(Task_LoadAssetBundle task) {
            return CoroutineUtility.CallStartCoroutine(work_loadAssetBundle(task));
        }

        public static Coroutine LoadABs(Task_LoadAssetBundle task) {
            return CoroutineUtility.CallStartCoroutine(work_loadAssetBundles(task));
        }

        public static Coroutine LoadABSet(string key, Task_LoadAssetBundle events, params AssetBundleConfig[] infos) {
            events = events ?? new Task_LoadAssetBundle(infos);
            events.Key = key;
            return CoroutineUtility.CallStartCoroutine(work_loadAssetBundles(events));
        }

        public static Coroutine LoadABSet(string key, IEnumerable<AssetBundleConfig> infos, Action<AssetBundle> onComplete, Action<Dictionary<AssetBundleConfig, AssetBundle>> onCompleteAll) {
            return CoroutineUtility.CallStartCoroutine(work_loadAssetBundles(new Task_LoadAssetBundle(infos) {
                Key = key,
                OnComplete = onComplete,
                OnCompleteAll = onCompleteAll
            }));
        }

        #endregion Start methods

        #region functions

        private static AssetState checkAssetBundleState(AssetBundleConfig abc) {
            if (ABInUse.ContainsKey(abc)) {
                return AssetState.Loaded;
            } else if (Caching.IsVersionCached(abc.fileUrl, abc.HashCode)) {
                return AssetState.Downloaded;
            } else {
                return AssetState.None;
            }
        }

        public static void Unload(AssetBundleConfig abc, bool unloadAllLoadedObjects, bool forceUnload = false) {
            if (ABInUse.ContainsKey(abc)) {
                HashSet<string> map = ABMapping[abc];
                if (map.Count == 0 || forceUnload) {
                    ABInUse[abc].Unload(unloadAllLoadedObjects);
                } else {
                    Debug.LogWarningFormat("Cannot unload asset bundle[{0}] because of {1} is using this one.", abc.Name, map.Join(","));
                }
            }
        }

        public static void UnloadSet(string key, bool unloadAllLoadedObjects) {
            if (ABSets.ContainsKey(key)) {
                foreach (AssetBundleConfig abc in ABSets[key]) {
                    HashSet<string> map = ABMapping[abc];
                    map.Remove(key);
                    if (map.Count == 0) {
                        Unload(abc, unloadAllLoadedObjects);
                    } else {
                        Debug.LogWarningFormat("Cannot unload asset bundle[{0}] because of {1} is using this one.", abc.Name, map.Join(","));
                    }
                }
                ABSets.Remove(key);
            }
        }

        private static void unloadAndCancelReg(AssetBundleConfig abc, bool unloadAllLoadedObjects) {
            Unload(abc, unloadAllLoadedObjects);
            cancelReggistAssetBundle(abc);
        }

        public static void ReplaceVariant(AssetBundleConfig pre, AssetBundleConfig apply) {
            //check current exist
            //check is same name
            //check variant exist (remote or cache)
            //start download variant
            //unload current and cancel from manager
            //load variant and registry to manager
            //done
            if (ABInUse.ContainsKey(pre)) {
                unloadAndCancelReg(pre, false);
            }
            registAssetBundle(apply);
            LoadAB(new Task_LoadAssetBundle(apply) {
                Key = apply.GameID
            });
        }

        private static bool registAssetBundle(AssetBundleConfig abc, string key = "") {
            //if key exist then
            if (!ABMapping.ContainsKey(abc)) {
                ABMapping.Add(abc, new HashSet<string>());
            }

            if (!string.IsNullOrEmpty(key)) {
                // ref to mapping
                ABMapping[abc].Add(key);

                // ref to abset
                if (!ABSets.ContainsKey(key)) {
                    ABSets.Add(key, new List<AssetBundleConfig>());
                }
                if (!ABSets[key].Contains(abc)) {
                    ABSets[key].Add(abc);
                }
            }

            //registry to manager
            if (!ABInUse.ContainsKey(abc)) {
                // if not exist then add null
                ABInUse.Add(abc, null);
                return true;
            } else if (ABInUse[abc] == null) {
                return true;
            } else {
                // if exist and ab is loaded then return
                Debug.LogFormat("Asset Bundle [{0}] is already loaded.", abc.Name);
                return false;
            }
        }

        private static void cancelReggistAssetBundle(AssetBundleConfig abc) {
            //cancel ref to ab set
            if (ABMapping.ContainsKey(abc)) {
                ABMapping[abc].Foreach(key => ABSets[key] = null);
            }
            //cancel reg from manager
            if (ABMapping.ContainsKey(abc)) {
                ABMapping.Remove(abc);
            }

            //remove from mapping
            if (ABInUse.ContainsKey(abc)) {
                ABInUse.Remove(abc);
            }
        }

        private static void onAssetBundleLoaded(AssetBundleConfig abc, AssetBundle ab, string key = "") {
            if (!string.IsNullOrEmpty(key)) {
                if (!ABSets.ContainsKey(key)) {
                    ABSets.Add(key, new List<AssetBundleConfig>());
                    ABSets[key].Add(abc);
                } else {
                    //ABSets[key][abc] = ab;
                }
            }
            if (ABInUse.ContainsKey(abc)) {
                if (ABInUse[abc] != null) {
                    ABInUse[abc].Unload(false);
                }
                ABInUse[abc] = ab;
            }
        }

        #endregion functions

        private static IEnumerator work_loadAssetBundle(Task_LoadAssetBundle events) {
            //check cached or in use
            //if cached or not in use then do normal download
            //if is loaded then skip

            AssetBundleConfig abc = events.AssetBundleConfig;

            if (abc.Size == 0) {
                events.OnError(abc);
                yield break;
            }

            AssetState state = checkAssetBundleState(abc);
            switch (state) {
                case AssetState.None:
                    registAssetBundle(abc, events.Key);
                    break;

                case AssetState.Downloaded:
                    break;

                case AssetState.Loaded:
                    unloadAndCancelReg(abc, false);
                    registAssetBundle(abc, events.Key);
                    break;
            }

            using (UnityWebRequest req = UnityWebRequest.GetAssetBundle(abc.fileUrl, abc.HashCode, abc.CRC)) {
                req.disposeDownloadHandlerOnDispose = true;
                AsyncOperation request = req.Send();

                events.OnStart(abc);

                while (!request.isDone) {
                    if (state == AssetState.None) {
                        //load from remote res.
                        events.CurrentLoadedSize = (long)(request.progress * abc.Size);
                        events.OnMessage("Loading resource[{3}]: {0}/{1}KB ({2:0.#}%)".format(events.CurrentLoadedSize >> 10, abc.Size >> 10, request.progress * 100, abc.Name));
                        events.OnProgress(events.Progress, abc);
                    } else {
                        //load from cache.
                        events.OnMessage("Decompressing resource[{0}].".format(abc.Name));
                        events.CurrentLoadedSize = abc.Size;
                        events.TriggerOnProgress();
                    }
                    yield return new WaitForEndOfFrame();
                }

                events.OnMessage("Loading resource[{0}] complete.".format(abc.Name));
                events.TriggerOnProgress();
                yield return new WaitForEndOfFrame();

                if (events.LoadResourceImmediately) {
                    AssetBundle ab = DownloadHandlerAssetBundle.GetContent(req);
                    onAssetBundleLoaded(abc, ab);
                    events.OnComplete(ab);
                }
            }
        }

        private static IEnumerator work_loadAssetBundles(Task_LoadAssetBundle events) {
            if (ABSets.ContainsKey(events.Key)) {
            }

            foreach (AssetBundleConfig item in events) {
                yield return getFileSize(events);
            }

            foreach (AssetBundleConfig item in events) {
                yield return work_loadAssetBundle(events);
            }
            events.OnCompleteAll(ABSets[events.Key].ToDictionary(abc => abc, abc => ABInUse[abc]));
        }

        private static IEnumerator getFileSize(Task_LoadAssetBundle events) {
            long fileSize = 0;
            using (UnityWebRequest headRequest = UnityWebRequest.Head(events.AssetBundleConfig.fileUrl)) {
                yield return headRequest.Send();

                if (headRequest.responseCode != 200) {
                    // TODO: Error response
                } else {
                    string contentLength = headRequest.GetResponseHeader("CONTENT-LENGTH");
                    long.TryParse(contentLength, out fileSize);
                    events.AssetBundleConfig.Size = fileSize;
                }
            }
            events.TotalSize += fileSize;
        }
    }
}