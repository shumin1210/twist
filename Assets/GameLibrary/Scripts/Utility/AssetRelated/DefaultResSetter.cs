﻿using System.Linq;
using GameLibrary.UI;
using UnityEngine;

namespace GameLibrary.Utility {

    public class DefaultResSetter : MonoBehaviour {
        public Font DefaultFont;
        public Font OriginFont;
        public GameObject RootCanvas;
        public ResourceIndex[] Resource;

        private void Start() {
#if UNITY_WEBGL
            DefaultResManager.DefaultFont = DefaultFont ?? OriginFont ;
#else
            DefaultResManager.DefaultFont = OriginFont ?? DefaultFont;
#endif

            UIManager.Instance.CanvasRoot = RootCanvas;

            Resource.Foreach(r => r.sprites.Foreach(s => DataShared.Instance.StaticSprites.SetAtlas(s.Key, s.Value)));
            DataShared.Instance.StaticAudios.Load(Resource.Select(a => a.audios.ToArray()).DownGrade());
            DataShared.Instance.TextAssets.Load(Resource.Select(a => a.texts.ToArray()).DownGrade());
        }
    }
}