﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace GameLibrary.Utility {

    public abstract class LoadOperation : IEnumerator {

        public object Current {
            get {
                return null;
            }
        }

        public bool MoveNext() {
            return !IsDone();
        }

        public void Reset() {
        }

        abstract public bool Update();

        abstract public bool IsDone();

        abstract public void Finished();
    }

    public class WWWDownloadOperation : LoadOperation {
        private WWW www;
        private Action<WWW> callback;

        public WWWDownloadOperation(string path, Action<WWW> callback) {
            www = new WWW(path);
            this.callback = callback;
        }

        public override void Finished() {
            callback(www);
            www.assetBundle.Unload(false);
        }

        public override bool IsDone() {
            return www.isDone;
        }

        public override bool Update() {
            if (!string.IsNullOrEmpty(www.error)) {
                Debug.LogError(www.error);
                return false;
            } else {
                if (www.isDone) {
                    Finished();
                }
                return !www.isDone;
            }
        }
    }

    public class ABManager : MonoBehaviour {
        private AssetBundleManifest manifest;
        public string RemoteURL;
        public string GameID;
        public bool AutoUpdate = true;
        public List<string> Variants = new List<string>();
        private Dictionary<string, AssetBundle> loadedABs = new Dictionary<string, AssetBundle>();
        private const string FOLDER = "AssetBundles";
        private const string EXTEND = ".data";
        private List<LoadOperation> operations = new List<LoadOperation>();

        private string gameIDFolder {
            get {
                return string.IsNullOrEmpty(GameID) ? string.Empty : GameID + "/";
            }
        }

        private string combinePath(params string[] args) {
            if (args.Length > 1) {
                var pre = args[0];
                for (int i = 1; i < args.Length; i++) {
                    Path.Combine(pre, args[i]);
                }
                return pre;
            }
            return args.Length == 1 ? args[0] : string.Empty;
        }

        private string RemotePath {
            get {
                return combinePath(RemoteURL, gameIDFolder, getPlatformName());
            }
        }

        private string ManifestPath {
            get {
                return Path.Combine(RemotePath, getPlatformName() + EXTEND);
            }
        }

        private string localPath {
            get {
                return Path.Combine(Application.persistentDataPath, FOLDER);
            }
        }

        public void LoadResource() {
        }

        public void Initialize() {
            if (AutoUpdate) {
                //compare local and remote manifest
                //download new assets and write to disk
            }
        }

        public void LoadManifest() {
            if (!manifest) {
                operations.Add(new WWWDownloadOperation(ManifestPath, www => manifest = (AssetBundleManifest)www.assetBundle.LoadAsset("AssetBundleManifest", typeof(AssetBundleManifest))));
            }
        }

        private string getPlatformName() {
            RuntimePlatform platform = Application.platform;
            switch (platform) {
                case RuntimePlatform.OSXPlayer:
                case RuntimePlatform.IPhonePlayer:
                    return "iOs";

                default:
                case RuntimePlatform.WindowsPlayer:
                    return "StandaloneWindows";

                case RuntimePlatform.OSXWebPlayer:
                case RuntimePlatform.WindowsWebPlayer:
                case RuntimePlatform.WebGLPlayer:
                    return "WebGL";

                case RuntimePlatform.Android:
                    return "Android";
            }
        }

        public void Update() {
            for (int i = 0; i < operations.Count;) {
                LoadOperation op = operations[i];
                if (op.Update()) {
                    i++;
                } else {
                    operations.RemoveAt(i);
                }
            }
        }

        public void LoadAB() {
            //load all
            foreach (var item in manifest.GetAllAssetBundles()) {
                LoadAB(item);
            }
        }

        public AssetBundleManifest getManifest(AssetBundle ab) {
            return (AssetBundleManifest)ab.LoadAsset("AssetBundleManifest", typeof(AssetBundleManifest));
        }

        public void LoadAB(string abName) {
            string varianted = RemapVariantName(abName);

            if (loadedABs.ContainsKey(abName)) {
                return;
            }
            //check dep and load forst
            foreach (string item in manifest.GetAllDependencies(abName)) {
                LoadAB(item);
            }

            //var l = manifest.GetAllAssetBundles();
            //var d = manifest.GetAllDependencies("poker13_prefab");

            //load from local
            AssetBundle ab = AssetBundle.LoadFromFile(Path.Combine(localPath, abName));

            ab.LoadAllAssets();
            loadedABs.Add(abName, ab);
        }

        public void ClearAB() {
            List<AssetBundle> list = loadedABs.Values.ToList();
            loadedABs.Clear();
            foreach (AssetBundle item in list) {
                item.Unload(false);
            }
            Resources.UnloadUnusedAssets();
        }

        public void ClearAB(string abName) {
            if (!loadedABs.ContainsKey(abName)) {
                return;
            }

            loadedABs[abName].Unload(false);
            loadedABs.Remove(abName);
            Resources.UnloadUnusedAssets();
        }

        protected string RemapVariantName(string assetBundleName) {
            string[] bundlesWithVariant = manifest.GetAllAssetBundlesWithVariant();

            // Get base bundle name
            string baseName = assetBundleName.Split('.')[0];

            int bestFit = int.MaxValue;
            int bestFitIndex = -1;
            // Loop all the assetBundles with variant to find the best fit variant assetBundle.
            for (int i = 0; i < bundlesWithVariant.Length; i++) {
                string[] curSplit = bundlesWithVariant[i].Split('.');
                string curBaseName = curSplit[0];
                string curVariant = curSplit[1];

                if (curBaseName != baseName)
                    continue;

                int found = Variants.IndexOf(curVariant);

                // If there is no active variant found. We still want to use the first
                if (found == -1)
                    found = int.MaxValue - 1;

                if (found < bestFit) {
                    bestFit = found;
                    bestFitIndex = i;
                }
            }

            if (bestFit == int.MaxValue - 1) {
                Debug.LogWarning("Ambigious asset bundle variant chosen because there was no matching active variant: " + bundlesWithVariant[bestFitIndex]);
            }

            if (bestFitIndex != -1) {
                return bundlesWithVariant[bestFitIndex];
            } else {
                return assetBundleName;
            }
        }
    }
}