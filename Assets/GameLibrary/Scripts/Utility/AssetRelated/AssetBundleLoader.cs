﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameLibrary.UI;
using UnityEngine;
using UnityEngine.Networking;

namespace GameLibrary.Utility {

    /// <summary>資源包設定</summary>
    public class AssetBundleConfig {

        public string Key {
            get {
                return GameID + ":" + Name;
            }
        }

        public string GameID;

        /// <summary>Bundle Name</summary>
        public string Name;

        public uint CRC;

        public int Order;
        public string Hash;

        public int Level;

        public Hash128 HashCode {
            get {
                return Hash128.Parse(Hash);
            }
        }

        public string TrimedBundleName {
            get {
                return Name.Split(".")[0];
            }
        }

        /// <summary>Bundle file name</summary>
        public string FileName {
            get {
                return Name + AssetBundleLoader.Extension;
            }
        }

        public long Size;

        public bool IsVariant {
            get {
                return Name.Contains(".");
            }
        }

        public string Variant {
            get {
                return IsVariant ? Name.Split(".")[1] : "";
            }
        }

        public string fileUrl {
            get {
                return AssetBundleLoader.urlPattern.format(AssetBundleLoader.RemotePath, AssetBundleLoader.platform, GameID) + FileName;
            }
        }

        public override bool Equals(object obj) {
            if (obj is AssetBundleConfig) {
                var abc = (AssetBundleConfig)obj;
                return abc.GameID == this.GameID
                    && abc.Name == this.Name;
                return abc.Hash == this.Hash
                    && abc.CRC == this.CRC
                    && abc.Name == this.Name;
            }
            return false;
        }

        public override int GetHashCode() {
            return (GameID + Name).GetHashCode();
            return base.GetHashCode();
        }

        public static implicit operator bool(AssetBundleConfig abl) {
            return !string.IsNullOrEmpty(abl.Name);
        }

        public static string TrimAssetBundleVariantName(AssetBundle ab) {
            if (ab.name.Contains(".")) {
                return ab.name.Substring(0, ab.name.LastIndexOf("."));
            } else {
                return ab.name;
            }
        }
    }

    /// <summary>資源包下載工具</summary>
    /// <remarks>(TODO:由快取轉存本地)</remarks>
    public class AssetBundleLoader : MonoBehaviour {

        public class AssetBundleDictionary : Dictionary<AssetBundleConfig, AssetBundle> {

            new public AssetBundle this[AssetBundleConfig c] {
                get {
                    return this.First(a => a.Key.Key == c.Key).Value;
                }
            }

            public AssetBundle this[string gameid, string name] {
                get {
                    return this.First(a => a.Key.Key == gameid + ":" + name).Value;
                }
            }

            public bool Contains(string gameid, string name) {
                foreach (var key in Keys) {
                    if (key.Key == gameid + ":" + name) {
                        return true;
                    }
                }
                return false;
            }

            public void Remove(string gameid, string name) {
                foreach (var key in Keys) {
                    if (key.Key == gameid + ":" + name) {
                        Remove(key);
                        break;
                    }
                }
            }
        }

        #region Const

        internal const string Extension = ".data";

        internal const string configName = "configs.json";

        internal const string urlPattern = "{0}/{1}/{2}/";

        #endregion Const

        #region 設定

        /// <summary>載入資源的狀態</summary>
        public string StateText = "";

        public static string RemotePath = "https://loginw.crazybet.win/AssetBundles";

        /// <summary>資源網址</summary>
        public string ResPath = "https://loginw.crazybet.win/AssetBundles";

        /// <summary>資源分區代號</summary>
        public string Variant = "";

        /// <summary>平台(唯讀)</summary>
#if UNITY_EDITOR
        public static string platform = "windows";
#elif UNITY_WEBGL
        public static string platform = "webgl";
#elif UNITY_IOS
        public static string platform = "ios";
#elif UNITY_ANDROID
        public static string platform = "android";
#else
        public static string platform = "windows";
#endif

        /// <summary>本地路徑(未啟用)</summary>
        public string pathLocal;

        /// <summary>重載遊戲資源包時不重載的資源</summary>
        public List<string> keep = new List<string>();

        /// <summary>載入資源時,忽略的資源(多為共用,通用資源)</summary>
        public List<string> ignores = new List<string>() { "shared" };

        public bool ImmediatelyLoadAsset = true;
        public bool UseTotalProgress = false;
        public long TotalSize = 0;
        public long LoadedSize = 0;
        public long NowLoadSize = 0;
        public float Progress = 0f;

        #endregion 設定

        #region 事件

        /// <summary>事件:讀取進度更新</summary>
        protected Action<float, AssetBundleConfig> OnProgress;

        private void _onProgress(float f, AssetBundleConfig c) {
        }

        /// <summary>事件:完成載入一個資源包</summary>
        protected Action<AssetBundle> OnCompleteOne;

        private void _onCompleteOne(AssetBundle c) {
        }

        /// <summary>事件:完成載入全部資源包</summary>
        protected Action<Dictionary<string, AssetBundle>> OnCompleteAll;

        private void _onCompleteAll(Dictionary<string, AssetBundle> c) {
        }

        /// <summary>事件:仔入資源包發生錯誤(無法連線,遠端資源不存在等)</summary>
        protected Action<AssetBundleConfig> OnError;

        private void _onError(AssetBundleConfig c) {
        }

        #endregion 事件

        private static AssetBundleDictionary assetBundleDictionary = new AssetBundleDictionary();

        /// <summary>依據遊戲ID分類的資源包集合</summary>
        public static TDictionary<string, TDictionary<string, AssetBundle>> GameAssets = new TDictionary<string, TDictionary<string, AssetBundle>>();

        private static HashSet<AssetBundleConfig> assetBundleLoaded = new HashSet<AssetBundleConfig>();

        private void unloadAssetBundle(AssetBundle assetBundle, bool unloadAllLoadedObjects) {
            assetBundle.Unload(unloadAllLoadedObjects);
        }

        private UnityWebRequest req;
        private Dictionary<string, AssetBundle> loadedAB = new Dictionary<string, AssetBundle>();
        private int requestCount = 0;

        /// <summary>是否在執行中</summary>
        public bool IsWorking {
            get {
                return loadedAB.Count < requestCount;
            }
        }

        #region Unity方法

        private void Awake() {
        }

        private void Start() {
        }

        public void ClearCahce() {
            Caching.expirationDelay = 1;

            Caching.CleanCache();
        }

        private void OnDisable() {
            if (req != null) {
                req.Dispose();
            }
        }

        #endregion Unity方法

        /// <summary>取得刪減分區代碼後的資源包名稱字串</summary>
        /// <param name="ab">資源包物件</param>
        /// <returns>資源包名稱</returns>
        public static string TrimAssetBundleVariantName(AssetBundle ab) {
            if (ab.name.Contains(".")) {
                return ab.name.Substring(0, ab.name.LastIndexOf("."));
            } else {
                return ab.name;
            }
        }

        /// <summary>指定事件</summary>
        /// <param name="onProgress">指派事件:進度更新</param>
        /// <param name="onCompleteOne">指派事件:完成載入單個資源</param>
        /// <param name="onCompleteAll">指派事件:完成載入全部資源</param>
        /// <param name="onError">指派事件:載入失敗</param>
        public void SetEvents(Action<float, AssetBundleConfig> onProgress = null
            , Action<AssetBundle> onCompleteOne = null
            , Action<Dictionary<string, AssetBundle>> onCompleteAll = null
            , Action<AssetBundleConfig> onError = null) {
            OnProgress = onProgress ?? _onProgress;
            OnCompleteOne = onCompleteOne ?? _onCompleteOne;
            OnCompleteAll = onCompleteAll ?? _onCompleteAll;
            OnError = onError ?? _onError;
        }

        public void StartLoadAssetBundle(string gameid
            , AssetBundleConfig bundleConfig
            , string path = null
            , Action<float, AssetBundleConfig> onProgress = null
            , Action<AssetBundle> onCompleteOne = null
            , Action<Dictionary<string, AssetBundle>> onCompleteAll = null
            , Action<AssetBundleConfig> onError = null) {
            if (IsWorking) {
                Debug.LogError("Cannot start load asset bundle while loader is working.");
                return;
            }
            ResPath = path ?? ResPath;
            SetEvents(onProgress, onCompleteOne, onCompleteAll, onError);

            string url = urlPattern.format(ResPath, platform, gameid);
            StartCoroutine(loadAssetBundle(bundleConfig, ResPath, gameid));
        }

        /// <summary>開始載入Variant資源</summary>
        /// <param name="gameid">遊戲ID</param>
        /// <param name="bundleConfig">資源包設定資訊</param>
        /// <param name="path">遠端路徑</param>
        /// <param name="onProgress"></param>
        /// <param name="onCompleteOne"></param>
        /// <param name="onCompleteAll"></param>
        /// <param name="onError"></param>
        public void StartLoadVariantAsset(string gameid
            , AssetBundleConfig bundleConfig
            , string path = null
            , Action<float, AssetBundleConfig> onProgress = null
            , Action<AssetBundle> onCompleteOne = null
            , Action<Dictionary<string, AssetBundle>> onCompleteAll = null
            , Action<AssetBundleConfig> onError = null) {
            if (IsWorking) {
                Debug.LogError("Cannot start load asset bundle while loader is working.");
                return;
            }
            if (!bundleConfig.IsVariant) {
                Debug.LogErrorFormat("AssetBundle:{0} is not variant.", bundleConfig.Name);
                return;
            }
            RemotePath = path ?? ResPath;
            SetEvents(onProgress, onCompleteOne, onCompleteAll, onError);
            StartCoroutine(loadAssetBundle(bundleConfig));
        }

        /// <summary>開始載入指定遊戲的所有資源包</summary>
        /// <param name="gameid">指定遊戲ID</param>
        /// <param name="path">遠端路徑</param>
        /// <param name="variant">分區代碼</param>
        /// <param name="onProgress">指派事件:進度更新</param>
        /// <param name="onCompleteOne">指派事件:完成載入單個資源</param>
        /// <param name="onCompleteAll">指派事件:完成載入全部資源</param>
        /// <param name="onError">指派事件:載入失敗</param>
        public void StartLoadGameAssets(string gameid, string path = null, string variant = null
            , Action<float, AssetBundleConfig> onProgress = null
            , Action<AssetBundle> onCompleteOne = null
            , Action<Dictionary<string, AssetBundle>> onCompleteAll = null
            , Action<AssetBundleConfig> onError = null) {
            if (IsWorking) {
                Debug.LogError("Cannot start load asset bundle while loader is working.");
                return;
            }
            SetEvents(onProgress, onCompleteOne, onCompleteAll, onError);
            RemotePath = path ?? ResPath;
            Variant = variant ?? LocalizationManager.Language;
            StartCoroutine(loadGameAssets(gameid));
        }

        /// <summary>載入指定遊戲的所有資源</summary>
        /// <param name="gameID">指定遊戲ID</param>
        /// <returns>coroutine</returns>
        private IEnumerator loadGameAssets(string gameID) {
            refreshGameAssets(gameID);

            List<AssetBundleConfig> map = new List<AssetBundleConfig>();
            string url = urlPattern.format(ResPath, platform, gameID);
            pathLocal = Application.persistentDataPath + "/AssetBundles/" + gameID + "/";

            #region get config

            using (UnityWebRequest rqConfig = UnityWebRequest.Get(url + configName)) {
                yield return rqConfig.Send();

                map = getMap(gameID, rqConfig);
            }

            #endregion get config

            foreach (AssetBundleConfig item in map) {
                yield return loadAssetBundle(item);
            }
            if (OnCompleteAll != null) {
                OnCompleteAll(GameAssets[gameID]);
            }
        }

        public void StartLoadAssetBundles(AssetBundleConfig[] bundleConfigs, string path = null
            , Action<float, AssetBundleConfig> onProgress = null
            , Action<AssetBundle> onCompleteOne = null
            , Action<Dictionary<string, AssetBundle>> onCompleteAll = null
            , Action<AssetBundleConfig> onError = null) {
            if (IsWorking) {
                Debug.LogError("Cannot start load asset bundle while loader is working.");
                return;
            }

            SetEvents(onProgress
                , a => {
                    string key = TrimAssetBundleVariantName(a);
                    if (loadedAB.ContainsKey(key)) {
                        try {
                            loadedAB[key].Unload(true);
                            if (loadedAB == null) {
                                Debug.Log("loadedAB lost.");
                                loadedAB = new Dictionary<string, AssetBundle>();
                            }
                        } catch (Exception) {
                        } finally {
                            loadedAB.Remove(key);
                        }
                    }

                    loadedAB.Add(key, a);

                    if (onCompleteOne != null) {
                        onCompleteOne(a);
                    }
                }, onCompleteAll, onError);

            StartCoroutine(loadAssetBundles(bundleConfigs));
        }

        private IEnumerator loadAssetBundles(AssetBundleConfig[] list) {
            requestCount = list.Length;

            if (UseTotalProgress) {
                LoadedSize = NowLoadSize = TotalSize = 0;
                var nonCached = list.GetNotCached().ToArray();
                //calc total size
                foreach (AssetBundleConfig item in nonCached) {
                    yield return getFileSize(item.fileUrl, s => TotalSize += s);
                }
                TotalSize = TotalSize >> 10;
            }

            foreach (AssetBundleConfig item in list) {
                yield return loadAssetBundle(item);
            }

            if (OnCompleteAll != null) {
                OnCompleteAll(loadedAB);
            }
            requestCount = 0;
            loadedAB.Clear();
        }

        private IEnumerator loadAssetBundle(AssetBundleConfig abConfig) {
            yield return loadAssetBundle(abConfig, abConfig.fileUrl, abConfig.GameID);
        }

        /// <summary>載入單個資源包</summary>
        /// <param name="name">資源包名稱</param>
        /// <param name="hash">資源包的hash值</param>
        /// <param name="CRC">資源包的CRC值</param>
        /// <param name="url">遠端路徑</param>
        /// <param name="gameID">資源包所屬的遊戲ID</param>
        /// <returns>coroutine</returns>
        private IEnumerator loadAssetBundle(string name, string hash, uint CRC, string url, string gameID) {
            yield return loadAssetBundle(new AssetBundleConfig {
                GameID = gameID,
                Name = name,
                Hash = hash,
                CRC = CRC
            }, url + name + Extension, gameID);
        }

        /// <summary>載入單個資源包</summary>
        /// <param name="abConfig">資源包設定檔</param>
        /// <param name="url">遠端路徑</param>
        /// <param name="gameID">資源包所屬的遊戲ID</param>
        /// <returns>coroutine</returns>
        private IEnumerator loadAssetBundle(AssetBundleConfig abConfig, string url, string gameID) {

            #region check exist

            if (assetBundleDictionary.ContainsKey(abConfig)) {
                StateText = "Decompressing resource[{0}].".format(abConfig.Name);
                OnProgress(1f, abConfig);
                OnCompleteOne(assetBundleDictionary[abConfig]);
                addToGameAssets(gameID, assetBundleDictionary[abConfig]);
                yield break;
            }

            #endregion check exist

            #region GetFileSize

            NowLoadSize = 0;
            long fileSize = 0;
            yield return getFileSize(abConfig.fileUrl, s => fileSize = s);
            fileSize = fileSize >> 10;
            ;

            if (fileSize == 0) {
                OnError(abConfig);
            }

            #endregion GetFileSize

            using (UnityWebRequest req = UnityWebRequest.GetAssetBundle(url, abConfig.HashCode, abConfig.CRC)) {
                this.req = req;
                req.disposeDownloadHandlerOnDispose = true;
                AsyncOperation request = req.Send();

                bool cached = Caching.IsVersionCached(req.url, abConfig.HashCode);

                Debug.Log("start download " + abConfig.Name);

                while (!request.isDone) {
                    if (cached) {
                        StateText = "Decompressing resource[{0}].".format(abConfig.Name);
                        OnProgress(UseTotalProgress ? Progress : 1f, abConfig);

                        yield return new WaitForEndOfFrame();
                    } else {
                        NowLoadSize = (long)(request.progress * fileSize);
                        if (UseTotalProgress) {
                            Progress = (NowLoadSize + LoadedSize) * 1f / TotalSize;
                        } else {
                            Progress = request.progress;
                        }
                        StateText = "Loading resource[{3}]: {0}/{1}KB ({2:0.#}%)".format(NowLoadSize, fileSize, request.progress * 100, abConfig.Name);
                        OnProgress(Progress / .9f, abConfig);

                        yield return null;
                    }
                }
                if (!cached) {
                    LoadedSize += fileSize;
                    if (UseTotalProgress) {
                        Progress = LoadedSize * 1f / TotalSize;
                    }
                }
                StateText = "Loading resource[{0}] complete.".format(abConfig.Name);
                OnProgress(UseTotalProgress ? Progress : 1f, abConfig);

                yield return null;

                checkVariant(gameID, abConfig);
                if (ImmediatelyLoadAsset) {
                    print("cache space _free before bundle[{0}]=".format(abConfig.Name) + Caching.spaceFree);
                    AssetBundle ab = DownloadHandlerAssetBundle.GetContent(req);
                    print("cache space _used after bundle[{0}]=".format(abConfig.Name) + Caching.spaceOccupied);

                    assetBundleDictionary.Add(abConfig, ab);
                    addToGameAssets(gameID, ab);
                    OnCompleteOne(ab);
                }
            }
            //req.Dispose();
            this.req = null;
        }

        /// <summary>檢測是否存在同資源包的Variant</summary>
        /// <param name="gameid">遊戲ID</param>
        /// <param name="abConfig">資源包設定資料</param>
        private void checkVariant(string gameid, AssetBundleConfig abConfig) {
            if (abConfig.IsVariant) {
                string abName = abConfig.TrimedBundleName;
                if (GameAssets.ContainsKey(gameid)
                    && GameAssets[gameid].ContainsKey(abName)) {
                    GameAssets[gameid][abName].Unload(false);
                    assetBundleDictionary.Remove(abConfig);
                }
            } else if (GameAssets.ContainsKey(gameid)
                    && GameAssets[gameid].ContainsKey(abConfig.TrimedBundleName)) {
                GameAssets[gameid][abConfig.TrimedBundleName].Unload(false);
                assetBundleDictionary.Remove(abConfig);
            }
        }

        /// <summary>將新載入的資源記錄到遊戲資源的控制集合</summary>
        /// <param name="gameID">遊戲ID</param>
        /// <param name="ab">新載入的資源包</param>
        private void addToGameAssets(string gameID, AssetBundle ab) {
            string abName = TrimAssetBundleVariantName(ab);

            if (!GameAssets.ContainsKey(gameID)) {
                GameAssets.Add(gameID, new TDictionary<string, AssetBundle>());
            }

            GameAssets[gameID][abName] = ab;

            VariantAssetManager.Instance.Refresh(abName, ab);
        }

        public void UnloadGameAssets(string gameID) {
            if (GameAssets.ContainsKey(gameID)) {
                GameAssets[gameID].Foreach(s => {
                    assetBundleDictionary.Remove(gameID, s.Value.name);
                    s.Value.Unload(true);
                });
                GameAssets[gameID].Clear();
                GameAssets.Remove(gameID);
                Resources.UnloadUnusedAssets();
            }
        }

        private void refreshGameAssets(string gameID) {
            if (!GameAssets.ContainsKey(gameID)) {
                GameAssets.Add(gameID, new TDictionary<string, AssetBundle>());
            } else {
                GameAssets[gameID].Foreach(s => {
                    assetBundleDictionary.Remove(gameID, s.Value.name);
                    s.Value.Unload(true);
                });
                GameAssets[gameID].Clear();
                Resources.UnloadUnusedAssets();
            }
        }

        private IEnumerator getFileSize(string url, Action<long> result) {
            long fileSize = 0;
            using (UnityWebRequest headRequest = UnityWebRequest.Head(url)) {
                yield return headRequest.Send();
                if (headRequest.responseCode != 200) {
                    // TODO: Error response
                } else {
                    string contentLength = headRequest.GetResponseHeader("CONTENT-LENGTH");
                    long.TryParse(contentLength, out fileSize);
                }
            }
            result(fileSize);
        }

        private List<AssetBundleConfig> getMap(string gameid, UnityWebRequest rqConfig) {
            if (rqConfig.responseCode != 200) {
                // TODO: Error response
                return null;
            } else {
                return DynamicParser.ToDynamicList(JS.Deserialize(rqConfig.downloadHandler.text)["AssetBundles"])
                          .SelectWhere(
                            s => GenAssetBundleConfigs(gameid, s),
                            s => {
                                if (ignores.Contains(s.Name)) {
                                    return false;
                                } else if (s.IsVariant) {
                                    return s.Variant == Variant.ToLower();
                                } else {
                                    return true;
                                }
                            }).ToList();
            }
        }

        public static AssetBundleConfig GenAssetBundleConfigs(string gameid, string text) {
            return GenAssetBundleConfigs(gameid, JS.Deserialize(text));
        }

        public static AssetBundleConfig GenAssetBundleConfigs(string gameid, IDictionary<string, object> s) {
            return new AssetBundleConfig {
                GameID = gameid,
                Name = s.Get("Name"),
                Hash = s.Get("Hash"),
                CRC = s.Get<uint>("CRC")
            };
        }
    }

    public static class AssetBundleCollectionExtensions {

        public static bool IsAllCached(this IEnumerable<AssetBundleConfig> cfgs) {
            foreach (var cfg in cfgs) {
                if (cfg && !Caching.IsVersionCached(cfg.fileUrl, cfg.HashCode)) {
                    return false;
                }
            }
            return true;
        }

        public static IEnumerable<AssetBundleConfig> GetNotCached(this IEnumerable<AssetBundleConfig> cfgs) {
            return cfgs.Where(c => !Caching.IsVersionCached(c.fileUrl, c.HashCode));
        }
    }
}