﻿using System.Linq;
using GameLibrary.UI;
using UnityEngine;
using UnityEngine.UI;

namespace GameLibrary.Utility {

    public class VariantElement : MonoBehaviour {
        public string ResName;

        [SerializeField]
        private string[] ResNames;

        public VariantAssetManager.AssetType Type;

        public string BundleName;

        public bool AutoHide = true;

        public void Reset() {
            Type = GetComponent<SpriteUI>() ? VariantAssetManager.AssetType.SpriteUI
                : GetComponent<Image>() ? VariantAssetManager.AssetType.Image
                : VariantAssetManager.AssetType.Image;
            ApplySetting();
        }

        public void ApplySetting() {
            switch (Type) {
                case VariantAssetManager.AssetType.Image:
                    Image img = GetComponent<Image>();
                    if (img) {
                        ResName = img.sprite ? img.sprite.name : ResName;
                    }
                    break;

                case VariantAssetManager.AssetType.SpriteUI:
                    SpriteUI spriteUI = GetComponent<SpriteUI>();
                    if (spriteUI) {
                        ResNames = spriteUI.SpriteUsed ? spriteUI.Sprites.Select(s => s.name).ToArray() : new string[0];
                        ResName = ResNames.Length > 0 ? ResNames[0] : string.Empty;
                    }
                    break;

                default:
                    break;
            }
        }

        private void Awake() {
            VariantAssetManager.Instance.Registry(this);
        }

        private void OnDestroy() {
            VariantAssetManager.Instance.Deregistry(this);
        }

        internal void RefreshAsset(AssetBundle ab) {
            switch (Type) {
                case VariantAssetManager.AssetType.Image:
                    Image image = GetComponent<Image>();
                    if (image) {
                        image.sprite = ab.LoadAsset<Sprite>(ResName) ?? image.sprite;
                        if (AutoHide) {
                            image.gameObject.SetActive(image.sprite);
                        }
                    }
                    break;

                case VariantAssetManager.AssetType.SpriteUI:
                    SpriteUI spriteUI = GetComponent<SpriteUI>();
                    if (spriteUI) {
                        spriteUI.Sprites = new Sprite[ResNames.Length];
                        for (int i = 0; i < ResNames.Length; i++) {
                            spriteUI.Sprites[i] = ab.LoadAsset<Sprite>(ResNames[i]);
                        }
                    }
                    break;

                default:
                    break;
            }
        }
    }
}