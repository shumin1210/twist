﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameLibrary.Utility.SerializableDictionary;
using UnityEngine;
using GObject = UnityEngine.Object;

namespace GameLibrary.Utility {

    [Serializable]
    public class ResManager<T> : DataMap<string, T> where T : GObject, new() {
        //protected TDictionary<string, T> base = new TDictionary<string, T>(TDictionary.MODE.AUTO_DEL);

        public virtual T this[object key] {
            get {
                if (ContainsKey(key.ToString())) {
                    return base[key.ToString()];
                } else {
                    //throw new KeyNotFoundException(key.ToString());
                    return null;
                }
            }
            set {
                base[key.ToString()] = value;
            }
        }

        public string[] Keys {
            get {
                return base.Keys.ToArray();
            }
        }

        public T[] Values {
            get {
                return base.Values.ToArray();
            }
        }

        public virtual T Set(string key, T res) {
            return base[key] = res;
        }

        public virtual T Get(string key) {
            return base[key];
        }

        public virtual void Clear() {
            this.Foreach(o => {
                try {
                    Resources.UnloadAsset(o.Value);
                } catch (System.Exception) {
                };
            });
            base.Clear();
        }

        public virtual void LoadFrom(ResManager<T> src) {
            src.Foreach(s => this[s.Key] = s.Value);
        }

        public virtual T Load(T source) {
            return this[source.name] = source;
        }

        public virtual T Load(string source) {
            if (!this[source]) {
                this[source] = Resources.Load<T>(source);
            }
            return this[source];
        }

        public virtual T[] Load(params T[] source) {
            return source.SelectWhere(o => Load(o), o => o != null).ToArray();
        }

        public virtual T[] Load(params string[] source) {
            return source.SelectWhere(o => Load(o), o => o != null).ToArray();
        }

        public virtual void Unload(params string[] names) {
            foreach (var name in names) {
                if (base.ContainsKey(name)) {
                    var o = base[name];
                    base.Remove(name);
                    if (o) {
                        try {
                            GObject.Destroy(o);
                        } catch (System.Exception e) {
                            Debug.LogWarningFormat("fail to destory object[{0}:{1}]\r\nerror message:{2}\r\nstack trace:{3}", name, o.GetType().ToString(), e.Message, e.StackTrace);
                        }
                    }
                }
            }
        }
    }

    [Serializable]
    public class ResList {
        public List<GObject> Values = new List<GObject>();
    }

    [Serializable]
    public class AssetMap : DataMap<string, ResList> {
        private const string default_key = "__default__";

        public GObject this[object key] {
            get {
                string[] keys = key.ToString().Split(".");
                if (keys.Length == 1) {
                    keys = new string[2] { default_key, keys[0] };
                }
                if (keys.Length == 2
                    && ContainsKey(keys[0])) {
                    return base[keys[0]].Values.FirstOrDefault(v => v.name == keys[1]);
                } else {
                    return default(GObject);
                }
            }
        }

        public void SetList(string key, ResList resList) {
            ResList atlas;
            if (ContainsKey(key)) {
                atlas = base[key];
            } else {
                base[key] = atlas = new ResList();
            }
            resList.Values.ForEach(atlas.Values.TryAdd);
        }

        public void Load(AssetMap map) {
            map.Foreach(v => SetList(v.Key, v.Value));
        }
    }

    [Serializable]
    public class SpriteRes : ResManager<Sprite> {
    }

    [Serializable]
    public class SpriteResManager : DataMap<string, SpriteRes> {
        //protected TDictionary<string, ResManager<Sprite>> atlases = new TDictionary<string, ResManager<Sprite>>();

        public Sprite this[object key] {
            get {
                var result = this.FirstOrDefault(a => a.Value.Keys.Contains(key));
                if (result.Value != null) {
                    return result.Value[key];
                } else {
                    return default(Sprite);
                }
            }
            //set {
            //    base[key] = value;
            //}
        }

        public SpriteRes SetAtlas(string root, params Sprite[] sprites) {
            SpriteRes atlas;
            if (ContainsKey(root)) {
                atlas = base[root];
            } else {
                base[root] = atlas = new SpriteRes();
            }
            sprites.Foreach(o => atlas[o.name] = o);
            return atlas;
        }

        public SpriteRes GetAtlas(string root) {
            if (!ContainsKey(root)) {
                return LoadAtlas(root);
            } else {
                return base[root];
            }
        }

        public SpriteRes LoadAtlas(string root) {
            SpriteRes atlas;
            if (ContainsKey(root)) {
                atlas = base[root];
            } else {
                atlas = base[root] = new SpriteRes();
            }
            Resources.LoadAll<Sprite>(root).Foreach(s => atlas[s.name] = s);
            return atlas;
        }

        new public void Clear() {
            this.Foreach(a => a.Value.Clear());
            base.Clear();
        }
    }
}