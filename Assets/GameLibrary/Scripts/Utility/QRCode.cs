﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using ZXing;
using ZXing.QrCode;

public class QRCode : MonoBehaviour {

    [Serializable]
    public class StrEvent : UnityEvent<string> {
    }

    public StrEvent OnScanned;

#if UNITY_EDITOR
    public Image output;

    public void generate(string code) {
        output.sprite = GenerateQRSprite(code);
    }

#endif

    private WebCamTexture cam;

    private bool flag = true;

    //接收攝影機返回的圖片數據
    private BarcodeReader reader = new BarcodeReader();

    //ZXing的解碼
    private Result res;

    public static Color32[] Encode(string code, int width, int height) {
        var writer = new BarcodeWriter {
            Format = BarcodeFormat.QR_CODE,
            Options = new QrCodeEncodingOptions {
                Height = height,
                Width = width
            }
        };
        return writer.Write(code);
    }

    public static Texture2D GenerateQR(string code) {
        Texture2D encoded = new Texture2D(256, 256);
        Color32[] color32 = Encode(code, encoded.width, encoded.height);
        encoded.SetPixels32(color32);
        encoded.Apply();
        return encoded;
    }

    public static Sprite GenerateQRSprite(string code) {
        Texture2D tx = GenerateQR(code);
        return Sprite.Create(tx, new Rect(0, 0, tx.width, tx.height), new Vector2(.5f, .5f));
    }

    //儲存掃描後回傳的資訊
    //判斷掃描是否執行完畢

    public void Scan() {
        StartCoroutine(open_Camera());//開啟攝影機鏡頭
    }

    public void ScanScreen() {
        StartCoroutine(scan());
    }

    public void StopScan() {
        cam.Stop();
    }

    private void OnDisable() {
        //當程式關閉時會自動呼叫此方法關閉攝影機
        StopScan();
    }

    private void OnGUI() {
        if (cam != null
            && cam.isPlaying == true) {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), cam);
            if (flag == true) {
                StartCoroutine(scan());
            }
        }
    }

    private IEnumerator open_Camera() {
        yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
        if (Application.HasUserAuthorization(UserAuthorization.WebCam)) {
            //設置攝影機要攝影的區域
            cam = new WebCamTexture(WebCamTexture.devices[0].name, Screen.width, Screen.height, 60);
            cam.Play();
        }
    }

    private IEnumerator scan() {
        flag = false;//掃描開始執行

        Texture2D t2D = new Texture2D(Screen.width, Screen.height);//掃描後的影像儲存大小，越大會造成效能消耗越大，若影像嚴重延遲，請降低儲存大小。
        yield return new WaitForEndOfFrame();//等待攝影機的影像繪製完畢

        t2D.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);//掃描的範圍，設定為整個攝影機拍到的影像，若影像嚴重延遲，請降低掃描大小。
        t2D.Apply();//開始掃描

        res = reader.Decode(t2D.GetPixels32(), t2D.width, t2D.height);//對剛剛掃描到的影像進行解碼，並將解碼後的資料回傳

        //若是掃描不到訊息，則res為null
        if (res != null) {
            OnScanned.Invoke(res.Text);
            Debug.Log(res.Text);//將解碼後的資料列印出來
        }
        flag = true;//掃描完畢
    }
}