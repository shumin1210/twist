﻿using System.Collections.Generic;
using UnityEngine;

namespace GameLibrary.Utility {

    [CreateAssetMenu(fileName = "items", menuName = "Inventory")]
    public class ResourceIndex : ScriptableObject {
        public List<Item> items = new List<Item>();
        public List<AudioClip> audios;
        public List<SpriteAtlasKeyValyePair> sprites;
        public List<TextAsset> texts;

        [System.Serializable]
        public struct Item {
            public int ID;
            public string image;
            public string desc;
        }
    }
}