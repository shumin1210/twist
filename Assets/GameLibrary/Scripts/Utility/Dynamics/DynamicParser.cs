﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace GameLibrary.Utility {

    /// <summary>將物件轉換成指定類型,以方便取用物件內容,可配合JSON反序列化後的物件使用</summary>
    public static class DynamicParser {

        private struct members {
            public FieldInfo[] Fields;
            public PropertyInfo[] Properties;
        }

        public static Dictionary<string, T> Cast<T>(this IDictionary<string, object> src) where T : struct {
            return ToDictionary<T>(src);
        }

        public static Dictionary<string, String> Cast(this IDictionary<string, object> src) {
            return ToDictionary(src);
        }

        public static void CopyField<T>(T target, string key, object value) {
            FieldInfo field = target.GetType().GetField(key);
            PropertyInfo prop = target.GetType().GetProperty(key);
            object obj = target;
            if (field != null) {
                field.SetValue(obj, Convert.ChangeType(value, field.FieldType, null));
            } else if (prop != null) {
                prop.SetValue(obj, Convert.ChangeType(value, prop.PropertyType), null);
            }
        }

        public static T CopyFields<T>(this IDictionary<string, object> src, string key, T target) {
            try {
                return src.ToDynamic(key).CopyFields(target);
            } catch (Exception) {
                return default(T);
            }
        }

        public static T CopyFields<T>(this IDictionary<string, object> src, T target, bool stopWhenTargetIsNull = false) {
            if (target == null) {
                if (stopWhenTargetIsNull) {
                    return target;
                } else {
                    target = (T)typeof(T).GetConstructor(new Type[0]).Invoke(new object[0]);
                }
            }

            Type type = target.GetType();
            object obj = (object)target;
            if (type == typeof(IDictionary<string, object>)) {
                //Dictionary .keyValuePairs
                IDictionary<string, object> dic = obj as IDictionary<string, object>;
                foreach (string fieldName in src.Keys) {
                    //if (dic.ContainsKey(fieldName)) {
                    dic[fieldName] = src[fieldName];
                    //} else {
                    //    dic.Add(fieldName, src[fieldName]);
                    //}
                }
            } else if (src != null) {
                //Object .properties

                IEnumerable<string> fieldNames = src.Keys.Intersect(ReflectionUtility.GetMemberNames(type));
                foreach (string fieldName in fieldNames) {
                    ReflectionUtility.MemberData m = ReflectionUtility.GetMember(type, fieldName);
                    object value = src[fieldName];
                    if (m != null) {
                        try {
                            if (m.MemberType == typeof(DateTime)) {
                                if (value is string) {
                                    value = Convert.ChangeType(ServerTime.ConvertFromServerDateString((string)src[fieldName]), m.MemberType, null);
                                } else {
                                    long val;
                                    long.TryParse(value.ToString(), out val);
                                    value = ServerTime.ConvertFromUnix(val);
                                }
                            } else if (m.MemberType == typeof(IDictionary<string, object>)) {
                                IDictionary<string, object> a = (m.GetValue(obj) ?? new Dictionary<string, object>()) as IDictionary<string, object>;
                                value = CopyFields(ToDynamic(src[fieldName]), a);
                            } else if (m.MemberType.IsEnum) {
                                value = parseEnum(m.MemberType, value);
                            } else if (m.MemberType.IsArray) {
                                Type eleType = m.MemberType.GetGenericElementType();
                                if (eleType.IsClassOrStruct()) {
                                    //object array
                                    value = ParseObjectArray(value as IList<object>, m.MemberType);
                                } else {
                                    value = ToArray(eleType, src[fieldName]);
                                }
                            } else if ((m.MemberType.IsGenericType && m.MemberType.GetInterface(typeof(IList).FullName) != null)) {
                                Type eleType = m.MemberType.GetGenericElementType();
                                value = ParseObjectArray(value as IList<object>, m.MemberType);
                            } else {
                                value = Convert.ChangeType(value, m.MemberType, null);
                            }
                            if (value != null) {
                                m.SetValue(obj, value);
                            }
                        } catch (Exception e) {
                            Debug.LogWarning(e);
                        }
                    }
                }
            }

            return (T)obj;
        }

        private static object parseEnum(Type type, object value) {
            int intVal = 0;
            if (int.TryParse(value.ToString(), out intVal)) {
                value = intVal;
            } else {
                value = Enum.Parse(type, value.ToString());
            }

            return value;
        }

        public static object ParseObjectArray(this IList<object> src, Type type) {
            Type eleType = type.GetGenericElementType();
            int length = src.Count;
            if (type.IsGenericType && typeof(IList).IsAssignableFrom(type)) {
                //generic
                IList container = Activator.CreateInstance(type, new object[] { length }) as IList;
                for (int i = 0; i < src.Count; i++) {
                    object item;
                    if (eleType.IsEnum) {
                        item = parseEnum(eleType, src[i]);
                    } else if (eleType.IsArray) {
                        item = ParseObjectArray(src[i] as List<object>, eleType);
                    } else if (eleType.IsClassOrStruct()) {
                        item = CopyFields(ToDynamic(src[i]), Activator.CreateInstance(eleType));
                    } else {
                        item = Convert.ChangeType(src[i], eleType);
                    }
                    container.Add(item);
                }
                return container;
            } else if (typeof(Array).IsAssignableFrom(type)) {
                //array
                Array container = Array.CreateInstance(eleType, src.Count);
                for (int i = 0; i < src.Count; i++) {
                    object item;
                    if (eleType.IsEnum) {
                        item = parseEnum(eleType, src[i]);
                    } else if (eleType.IsArray) {
                        item = ParseObjectArray(src[i] as List<object>, eleType);
                    } else if (eleType.IsClassOrStruct()) {
                        item = CopyFields(ToDynamic(src[i]), Activator.CreateInstance(eleType));
                    } else {
                        item = Convert.ChangeType(src[i], eleType);
                    }
                    container.SetValue(item, i);
                }
                return container;
            } else {
                return null;
            }
        }

        public static string Get(this IDictionary<string, object> src, string key) {
            return tryGet(src, key, "").ToString();
        }

        public static T Get<T>(this IDictionary<string, object> src, string key) {
            return TConvert.ToGeneric<T>(tryGet(src, key, default(T)));
        }

        public static T Get<T>(this IDictionary<string, object> src, string key, T defaultValue) {
            return TConvert.ToGeneric<T>(tryGet(src, key, defaultValue));
        }

        public static T Parse<T>(this IDictionary<string, object> src, string key) where T : new() {
            return src.ToDynamic(key).CopyFields(new T());
        }

        public static T Parse<T>(this IDictionary<string, object> src) where T : new() {
            return src.CopyFields(new T());
        }

        public static IEnumerable<T> ParseArray<T>(this IDictionary<string, object> src, string key) where T : new() {
            IList<object> tmp = (src[key] as IList<object>);
            return ParseObjectArray(tmp, typeof(List<T>)) as List<T>;
            //List<T> result = new List<T>();
            //IList<object> tmp = (src[key] as IList<object>);
            //for (int i = 0; i < tmp.Count; i++) {
            //    result.Add(Parse<T>(tmp[i] as IDictionary<string, object>));
            //}
            //return result;
        }

        /// <summary>轉換成指定元素型別的List物件</summary>
        /// <param name="src">要轉換的來源物件</param>
        /// <returns>List物件</returns>
        public static String[] ToArray(object src) {
            IList<object> tmp = (src as IList<object>);
            if (tmp == null) {
                return null;
            }
            return tmp.Select(d => d.ToString()).ToArray();
        }

        /// <summary>轉換成指定元素型別的Array物件</summary>
        /// <typeparam name="T">元素的型別</typeparam>
        /// <param name="src">要轉換的來源物件</param>
        /// <returns>List物件</returns>
        public static Array ToArray(Type t, object src) {
            IList<object> tmp = (src as IList<object>);
            if (tmp == null) {
                return null;
            }
            if (t.IsClassOrStruct()) {
                return tmp.Select(d => d).ToArray();
            } else {
                object[] arr = tmp.Select(d => Convert.ChangeType(d, t)).ToArray();
                Array result = Array.CreateInstance(t, arr.Length);
                Array.Copy(arr, result, result.Length);
                return result;
            }
        }

        /// <summary>轉換成指定元素型別的Array物件</summary>
        /// <typeparam name="T">元素的型別</typeparam>
        /// <param name="src">要轉換的來源物件</param>
        /// <returns>List物件</returns>
        public static T[] ToArray<T>(object src) {
            IList<object> tmp = (src as IList<object>);
            if (tmp == null) {
                return null;
            }
            Type t = typeof(T);
            if (t.IsValueType) {
                return tmp.Select(d => (T)Convert.ChangeType(d, typeof(T))).ToArray();
            } else {
                return tmp.Select(d => (T)d).ToArray();
            }
        }

        /// <summary>轉換成字串Dictionary</summary>
        /// <param name="src">要轉換的來源物件</param>
        /// <returns>value型別為string的Dictionary</returns>
        public static Dictionary<string, string> ToDictionary(object src) {
            IDictionary<string, object> tmp = (src as IDictionary<string, object>);
            if (tmp == null) {
                return null;
            }
            return tmp.ToDictionary(d => d.Key, d => d.Value.ToString());
        }

        /// <summary>轉換成指定value型別Dictionary物件</summary>
        /// <typeparam name="T">指定value型別,僅接受實質型別</typeparam>
        /// <param name="src">要轉換的來源物件</param>
        /// <returns>Dictionary物件</returns>
        public static Dictionary<string, T> ToDictionary<T>(object src) where T : struct {
            IDictionary<string, object> tmp = (src as IDictionary<string, object>);
            if (tmp == null) {
                return null;
            }
            return tmp.ToDictionary(d => d.Key, d => (T)Convert.ChangeType(d.Value, typeof(T)));
        }

        /// <summary>轉換成Dictionary類型的動態物件</summary>
        /// <param name="src">要轉換的來源物件</param>
        /// <returns>Dictionary物件</returns>
        public static IDictionary<string, object> ToDynamic(object src) {
            return src as IDictionary<string, object>;
        }

        public static IDictionary<string, object> ToDynamic(this IDictionary<string, object> src, string key) {
            if (src != null && src.ContainsKey(key)) {
                return src[key] as IDictionary<string, object>;
            } else {
                return null;
            }
        }

        public static T ToDynamic<T>(string[] keys, object[] values) where T : new() {
            if (keys.Length != values.Length) {
                return default(T);
            }
            T result = new T();
            object obj = result;
            keys.Foreach((s, i) => CopyField(obj, keys[i], values[i]));
            return (T)obj;
        }

        public static T ToDynamic<T>(this IDictionary<string, object> root, string key) where T : new() {
            IDictionary<string, object> src = root[key] as IDictionary<string, object>;
            if (src == null) {
                return default(T);
            }

            T target = new T();
            Type type = typeof(T);
            object obj = (object)target;
            if (type == typeof(IDictionary<string, object>)) {
                //Dictionary .keyValuePairs
                IDictionary<string, object> dic = obj as IDictionary<string, object>;
                foreach (string fieldName in src.Keys) {
                    if (dic.ContainsKey(fieldName)) {
                        dic[fieldName] = src[fieldName];
                    } else {
                        dic.Add(fieldName, src[fieldName]);
                    }
                }
            } else {
                //Object .properties
                foreach (string fieldName in src.Keys) {
                    FieldInfo field = type.GetField(fieldName);
                    if (field != null && src[fieldName] != null) {
                        try {
                            if (field.FieldType == typeof(DateTime) && src[fieldName] is string) {
                                field.SetValue(obj, Convert.ChangeType(ServerTime.ConvertFromServerDateString((string)src[fieldName]), field.FieldType, null));
                            } else if (field.FieldType == typeof(IDictionary<string, object>)) {
                                IDictionary<string, object> a = (field.GetValue(obj) ?? new Dictionary<string, object>()) as IDictionary<string, object>;
                                CopyFields(ToDynamic(src[fieldName]), a);
                                field.SetValue(obj, a);
                            } else if (field.FieldType.IsEnum) {
                                int val = Convert.ToInt32(src[fieldName]);
                                field.SetValue(obj, val);
                            } else {
                                field.SetValue(obj, Convert.ChangeType(src[fieldName], field.FieldType, null));
                            }
                        } catch (Exception) { }
                    }
                }
            }
            return (T)obj;
        }

        /// <summary>轉換成動態元素的List物件</summary>
        /// <param name="src">要轉換的來源物件</param>
        /// <returns>List物件</returns>
        public static List<IDictionary<string, object>> ToDynamicList(object src) {
            List<object> tmp = (src as List<object>);
            if (tmp == null) {
                return null;
            }
            return tmp.Select(d => d as IDictionary<string, object>).ToList();
        }

        public static List<IDictionary<string, object>> ToDynamicList(this IDictionary<string, object> src, string key) {
            return ToDynamicList(src[key]);
        }

        /// <summary>轉換成元素為字串的List</summary>
        public static List<string> ToList(object src) {
            IList<object> tmp = (src as IList<object>);
            if (tmp == null) {
                return null;
            }
            return tmp.Select(d => d.ToString()).ToList();
        }

        /// <summary>轉換成指定元素型別的List物件</summary>
        /// <typeparam name="T">元素的型別</typeparam>
        /// <param name="src">要轉換的來源物件</param>
        /// <returns>List物件</returns>
        public static List<T> ToList<T>(object src) {
            IList<object> tmp = (src as IList<object>);
            if (tmp == null) {
                return null;
            }
            if (typeof(T) == typeof(object)) {
                return tmp as List<T>;
            }
            return tmp.Select(d => (T)Convert.ChangeType(d, typeof(T))).ToList();
        }

        public static bool TryGet(this IDictionary<string, object> src, string key, ref string result) {
            object value;
            bool find = src.TryGetValue(key, out value);
            if (find) {
                result = value.ToString();
            }
            return find;
        }

        public static bool TryGet<T>(this IDictionary<string, object> src, string key, ref T result) {
            object value;
            bool find = src.TryGetValue(key, out value);
            if (find) {
                result = TConvert.ToGeneric<T>(value);
            }
            return find;
        }

        private static object tryGet(IDictionary<string, object> src, string key, object defaultVal) {
            object value;
            if (src.TryGetValue(key, out value)) {
                return value;
            } else {
                Debug.LogWarningFormat("[DynamicParser] key:{0} is not in target.", key);
                return defaultVal;
            }
        }

        public static TOutput TryGet<TKey, TValue, TOutput>(this IDictionary<TKey, TValue> src, TKey key, TOutput defaultValue = default(TOutput)) {
            TValue tmp;
            if (src.TryGetValue(key, out tmp)) {
                defaultValue = TConvert.ToGeneric<TOutput>(tmp);
            }
            return defaultValue;
        }

        public static TValue TryGet<TKey, TValue>(this IDictionary<TKey, TValue> src, TKey key, TValue defaultValue = default(TValue)) {
            src.TryGetValue(key, out defaultValue);
            return defaultValue;
        }

        #region Extensions

        public static Dictionary<string, T> SplitToDictionary<T>(this IEnumerable<string> src, string seperator) {
            return src.Select(a => a.Split(seperator)).ToDictionary(s => s[0], s => TConvert.ToGeneric<T>(s[1]));
        }

        public static T ToDynamic<T>(this TDataTable src, int row) where T : new() {
            if (row >= src.Rows.Count) {
                return default(T);
            }
            TDataTable.DataRow dr = src.Rows[row];
            return src.Columns.ToDictionary(c => c.Name, c => dr[c]).CopyFields(new T());
        }

        #endregion Extensions
    }
}