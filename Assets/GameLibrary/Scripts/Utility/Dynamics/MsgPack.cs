﻿using System.Collections.Generic;

namespace GameLibrary.Utility {

    public class MsgPack : Dictionary<string, object> {

        public MsgPack(IDictionary<string, object> src) {
            src.Foreach(o => this.Add(o.Key, o.Value));
        }

        public T Get<T>(string key) {
            return DynamicParser.Get<T>(this, key);
        }

        public string Get(string key) {
            return DynamicParser.Get(this, key);
        }

        public IDictionary<string, object> GetDynamic(string key) {
            return DynamicParser.ToDynamic(this[key]);
        }

        public T Extract<T>(string key) where T : new() {
            return DynamicParser.Parse<T>(GetDynamic(key));
        }
    }
}