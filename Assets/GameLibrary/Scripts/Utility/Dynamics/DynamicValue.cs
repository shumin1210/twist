﻿using System;
using UnityEngine;

namespace GameLibrary.Utility {

    [Serializable]
    public class DynamicValue {

        public enum Type {
            Boolean, Integer, BigInteger, Float, String
        }

        public Type type;

        [SerializeField]
        private string value;

        private System.Type getType {
            get {
                switch (type) {
                    case Type.Boolean:
                        return typeof(bool);

                    case Type.Integer:
                        return typeof(int);

                    case Type.BigInteger:
                        return typeof(long);

                    case Type.Float:
                        return typeof(float);

                    default:
                    case Type.String:
                        return typeof(string);
                }
            }
        }

        public object Value {
            get {
                return Convert.ChangeType(value, getType);
            }
        }
    }
}