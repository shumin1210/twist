﻿using System;
using System.Collections.Generic;

namespace GameLibrary.Utility {

    public class DynamicEvents : TDictionary<string, List<Action<object[]>>> {

        public void Trigger(string evt, params object[] args) {
            if (ContainsKey(evt)) {
                this[evt].ForEach(a => a.Invoke(args));
            }
        }

        public void AddSingle(string evt, Action<object[]> callback) {
            if (!ContainsKey(evt)) {
                this[evt] = new List<Action<object[]>>();
            }
            if (this[evt].Contains(callback)) {
                return;
            }

            this[evt].Add(callback);
        }

        public void Add(string evt, Action<object[]> callback) {
            if (!ContainsKey(evt)) {
                this[evt] = new List<Action<object[]>>();
            }

            this[evt].Add(callback);
        }

        public void Remove(string evt, Action<object[]> callback) {
            if (!ContainsKey(evt)) {
                return;
            }

            this[evt].Remove(callback);
        }

        public void AddEventListener(string evt, Action<object[]> callback) {
            Add(evt, callback);
        }

        public void AddEventListener(Action<object[]> callback = null, params string[] evts) {
            foreach (string evt in evts) {
                Add(evt, callback);
            }
        }
    }

    public static class EventHandlerExtention {

        public static void TriggerEvent(this IEnumerable<DynamicEvents> handlers, string evt, params object[] args) {
            handlers.Foreach(i => i.Trigger(evt, args));
        }
    }
}