﻿using System;

namespace GameLibrary.Utility {

    public class RealTimeTimer {
        private DateTime start;
        private DateTime end;

        public float Interval_sec = 1f;
        public bool Loop = false;
        public bool Paused = true;
        private float progress = 0;

        public void Start(float interval_sec = 0, float time_passed_sec = 0) {
            if (interval_sec > 0) {
                Interval_sec = interval_sec;
            }
            start = DateTime.Now.AddSeconds(-time_passed_sec);
            end = start.AddSeconds(Interval_sec);
            Paused = false;
            progress = GetProgress();
        }

        public void Pause() {
            Paused = true;
            GetProgress();
        }

        public float GetProgress() {
            if (Paused) {
                return progress;
            }
            DateTime now = DateTime.Now;
            progress = (float)((now - start).TotalSeconds / Interval_sec);
            if (!Loop) {
                if (now > end) {
                    progress = 1;
                    Pause();
                } else {
                    progress = TMath.Range(progress, 0, 1);
                }
            }

            return progress;
        }
    }
}