﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameLibrary.Utility {

    public class CoroutineUtility : SingletonObject<CoroutineUtility> {

        public static Coroutine CallAction(Action action, float delay_sec = 0, float interval_sec = 0, int loopTime = 0, float duration_sec = 0, Func<bool> until = null, Action onComplete = null, GameObject refGameObject = null) {
            return Instance.StartCoroutine(coroutineAction(action, delay_sec, interval_sec, duration_sec, loopTime, until, onComplete, refGameObject));
        }

        public static Coroutine CallStartCoroutine(IEnumerator action, float delay_sec = 0) {
            return Instance.StartCoroutine(delayEnumerator(action, delay_sec));
        }

        public static Coroutine CallStartCoroutine(Action act, float delay_sec = 0) {
            return Instance.StartCoroutine(delayAction(act, delay_sec));
        }

        public static Coroutine CallStartCoroutine(IEnumerator action, Func<bool> check) {
            return Instance.StartCoroutine(untilAction(action, check));
        }

        public static Coroutine CallStartCoroutine(Action act, Func<bool> check) {
            return Instance.StartCoroutine(untilAction(act, check));
        }

        public static Coroutine CallStartIntervalAction(Action act, float interval = 1f, float period = 0f) {
            return Instance.StartCoroutine(intervalAction(act, interval, period));
        }

        public static void CallStopCoroutine(Coroutine coroutine) {
            Instance.StopCoroutine(coroutine);
        }

        public static void CallStopAllCoroutines() {
            Instance.StopAllCoroutines();
        }

        private static IEnumerator coroutineAction(Action action, float delay_sec = 0, float interval_sec = 0, float duration_sec = 0, int loopTime = 0, Func<bool> check = null, Action onComplete = null, GameObject refGameObject = null) {

            #region 初始化

            bool refGO = refGameObject != null;
            float passed = 0f;
            int looped = 0;

            List<Func<bool>> checks = new List<Func<bool>>();
            if (refGO) {
                checks.Add(() => !refGameObject);
            }
            if (loopTime != 0) {
                checks.Add(() => loopTime == -1 ? false : (looped >= loopTime));
            }
            if (duration_sec > 0) {
                checks.Add(() => passed >= duration_sec);
            }
            if (check != null) {
                checks.Add(check);
            }

            #endregion 初始化

            #region 執行

            if (delay_sec > 0) {
                yield return new WaitForSeconds(delay_sec);
            }

            if (interval_sec > 0 || loopTime != 0) {
                while (!checks.Exist(c => c.TryInvoke())) {
                    action.TryInvoke();
                    looped++;
                    if (interval_sec > 0) {
                        yield return new WaitForSeconds(interval_sec);
                        passed += interval_sec;
                    } else {
                        yield return null;
                        passed += Time.deltaTime;
                    }
                }
            } else if (checks.Count > 0) {
                yield return new WaitUntil(() => checks.Exist(c => c.TryInvoke()));
                action.TryInvoke();
            } else {
                action.TryInvoke();
            }

            #endregion 執行

            #region 完成後回呼

            onComplete.TryInvoke();

            #endregion 完成後回呼
        }

        private static IEnumerator untilAction(Action act, Func<bool> func) {
            yield return new WaitUntil(func);
            act.TryInvoke();
        }

        private static IEnumerator untilAction(IEnumerator act, Func<bool> func) {
            yield return new WaitUntil(func);
            yield return act;
        }

        private static IEnumerator delayAction(Action action, float delay_sec) {
            if (delay_sec > 0) {
                yield return new WaitForSeconds(delay_sec);
            }
            action();
        }

        private static IEnumerator intervalAction(Action action, float interval_sec, float period_sec) {
            float passed = 0;
            while (true) {
                action.TryInvoke();
                if (period_sec > 0 && passed >= period_sec) {
                    break;
                }

                yield return new WaitForSeconds(interval_sec);
            }
        }

        private static IEnumerator delayEnumerator(IEnumerator action, float delay_sec) {
            if (delay_sec > 0) {
                yield return new WaitForSeconds(delay_sec);
            }
            yield return action;
        }
    }
}