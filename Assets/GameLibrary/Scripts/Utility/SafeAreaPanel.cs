﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

/// <summary>
/// 设计安全区域面板（适配iPhone X）
/// Jeff 2017-12-1
/// 文件名 SafeAreaPanel.cs
/// </summary>
public class SafeAreaPanel : MonoBehaviour {
    private RectTransform target;

    [SerializeField]
    private Vector2 PreferRect;

#if UNITY_EDITOR

    [SerializeField]
    private bool Simulate_X = false;

#endif

    private void Awake() {
        target = GetComponent<RectTransform>();
        ApplySafeArea();
    }

    private void ApplySafeArea() {
        var area = SafeAreaUtils.Get(PreferRect.x / PreferRect.y);

#if UNITY_EDITOR

        /*
        iPhone X 横持手机方向:
        iPhone X 分辨率
        2436 x 1125 px

        safe area
        2172 x 1062 px

        左右边距分别
        132px

        底边距 (有Home条)
        63px

        顶边距
        0px
        */

        float Xwidth = 2436f;
        float Xheight = 1125f;
        float Margin = 132f;
        float InsetsBottom = 63f;

        if ((Screen.width == (int)Xwidth && Screen.height == (int)Xheight)
        || (Screen.height == (int)Xwidth && Screen.width == (int)Xheight)
        || (Screen.width / Screen.height == Xwidth / Xheight)
        || (Screen.width == 375 && Screen.height == 812)) {
            Simulate_X = true;
        }

        if (Simulate_X) {
            if (Screen.width > Screen.height) {
                var insets = area.width * Margin / Xwidth;
                var positionOffset = new Vector2(insets, 0);
                var sizeOffset = new Vector2(insets * 2, 0);
                area.position = area.position + positionOffset;
                area.size = area.size - sizeOffset;
            } else {
                var insets = area.height * Margin / Xheight;
                var positionOffset = new Vector2(0, insets);
                var sizeOffset = new Vector2(0, insets * 2);
                area.position = area.position + positionOffset;
                area.size = area.size - sizeOffset;
            }
        }
#endif

        var anchorMin = area.position;
        var anchorMax = area.position + area.size;
        anchorMin.x /= Screen.width;
        anchorMin.y /= Screen.height;
        anchorMax.x /= Screen.width;
        anchorMax.y /= Screen.height;
        target.anchorMin = anchorMin;
        target.anchorMax = anchorMax;
    }
}

/// <summary>
/// iPhone X适配工具类
/// Jeff 2017-12-1
/// 文件名 SafeAreaUtils.cs
/// </summary>
public class SafeAreaUtils {
    private const float Ratio_IphoneX = 2436f / 1125f;

#if UNITY_IOS
    [DllImport("__Internal")]
    private static extern void GetSafeAreaImpl(out float x, out float y, out float w, out float h);
#endif

    /// <summary>
    /// 获取iPhone X 等苹果未来的异性屏幕的安全区域Safe are
    /// </summary>
    /// <param name="showInsetsBottom"></param>
    /// <returns></returns>
    public static Rect Get(float ratio = 0) {
        float x, y, w, h;
#if UNITY_IOS && !UNITY_EDITOR
            GetSafeAreaImpl(out x, out y, out w, out h);
#else
        x = 0;
        y = 0;
        w = Screen.width;
        h = Screen.height;

        float _ratio = w / h;
        if (_ratio == Ratio_IphoneX || _ratio == 1 / Ratio_IphoneX) {
        } else if (ratio > 0) {
            if (_ratio > ratio) {
                float preW = w;
                w = h * ratio;
                x = (preW - w) / 2;
            } else if (_ratio < ratio) {
                float preH = h;
                h = w / ratio;
                y = (preH - h) / 2;
            }
        }

#endif
        return new Rect(x, y, w, h);
    }
}