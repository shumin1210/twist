﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameLibrary.Utility {

    [Serializable]
    public class RefData {
        public string DataKey;
    }

    [Serializable]
    public class RefData<T> : RefData {

        public T Value {
            get {
                return Get();
            }
            set {
                Set(value);
            }
        }

        public void Set(T val) {
            DataShared.Instance.Data.Set(DataKey, val);
        }

        public T Get() {
            return DataShared.Instance.Data.Get(DataKey, default(T));
        }

        public static implicit operator T(RefData<T> src) {
            return src.Get();
        }
    }

#if UNITY_EDITOR

    [UnityEditor.CustomPropertyDrawer(typeof(RefData), true)]
    public class RefDataPropertyDrawer : UnityEditor.PropertyDrawer {

        public override void OnGUI(Rect position, UnityEditor.SerializedProperty property, GUIContent label) {
            label = UnityEditor.EditorGUI.BeginProperty(position, label, property);
            var labelPosition = position;
            labelPosition.height = UnityEditor.EditorGUIUtility.singleLineHeight;

            UnityEditor.EditorGUI.PropertyField(position, property.FindPropertyRelative("DataKey"), label, false);

            UnityEditor.EditorGUI.EndProperty();
        }
    }

#endif
}