﻿using System;
using UnityEngine;

namespace GameLibrary.Utility {

    [Serializable]
    public class DataShared : SingletonComponent<DataShared> {

        [SerializeField]
        private string gid;

        public string GID {
            get {
                return gid;
            }
        }

        [Serializable]
        public class AudioRes : ResManager<AudioClip> {
        }

        public AudioRes StaticAudios = new AudioRes();

        public AudioRes Audios = new AudioRes();

        [Serializable]
        public class ScriptableRes : ResManager<ScriptableObject> {
        }

        public ScriptableRes ScriptableObjects = new ScriptableRes();

        [Serializable]
        public class GameobjectRes : ResManager<GameObject> {
        }

        public GameobjectRes Prefabs = new GameobjectRes();

        [Serializable]
        public class TextRes : ResManager<TextAsset> {
        }

        public TextRes StaticTextAssets = new TextRes();

        public TextRes TextAssets = new TextRes();

        public SpriteResManager StaticSprites = new SpriteResManager();

        public SpriteResManager Sprites = new SpriteResManager();

        public DataManager Data = new DataManager();

        public DataManager StaticData = new DataManager();

        public AssetMap Assets = new AssetMap();

        public DataShared() {
        }

        public void SetGame(string GID) {
            if (gid != GID) {
                gid = GID;
                Clear();
            }
        }

        public void Clear(bool clearAll = false) {
            Data.Clear();
            Audios.Clear();
            Sprites.Clear();
            TextAssets.Clear();
            Prefabs.Clear();

            if (clearAll) {
                StaticData.Clear();
                StaticAudios.Clear();
                StaticSprites.Clear();
                StaticTextAssets.Clear();
            }
        }

        #region DataRelated

        public void SetData(string keys, object value, bool isStatic = false) {
            (isStatic ? StaticData : Data).Set(keys, value);
        }

        public void SetData(string[] keys, object value, bool isStatic = false) {
            (isStatic ? StaticData : Data).Set(keys, value);
        }

        public bool ContainsKey(string key, bool isStatic = false) {
            return (isStatic ? StaticData : Data).ContainsKey(key);
        }

        public object GetData(string keys, bool isStatic = false, bool ignoreCatagory = false) {
            if (string.IsNullOrEmpty(keys)) {
                return null;
            } else {
                if (ignoreCatagory) {
                    object result = null;
                    if (StaticData.TryGetValue(keys, out result)) {
                        return result;
                    } else {
                        return Data.Get(keys);
                    }
                } else {
                    return (isStatic ? StaticData : Data)[keys];
                }
            }
        }

        public T GetData<T>(string keys, bool isStatic = false, bool ignoreCatagory = false) {
            if (string.IsNullOrEmpty(keys)) {
                return default(T);
            }
            if (ignoreCatagory) {
                T result;
                if (StaticData.TryGetValue<T>(keys, out result)) {
                    return result;
                } else {
                    return Data.Get<T>(keys);
                }
            } else {
                return (isStatic ? StaticData : Data).Get<T>(keys);
            }
        }

        public object GetData(string[] keys, bool isStatic = false, bool ignoreCatagory = false) {
            if (ignoreCatagory) {
                object result = null;
                if (StaticData.TryGetValue(keys, out result)) {
                    return result;
                } else {
                    return Data.Get(keys);
                }
            } else {
                return (isStatic ? StaticData : Data).Get(keys);
            }
        }

        #endregion DataRelated

        #region SpriteRelated

        [Obsolete("use Sprites.LoadAtlas(string root).", false)]
        public void LoadSpriteAtlas(string root, bool force = false) {
            Sprites.LoadAtlas(root);
        }

        [Obsolete("use Sprites.Set(string root,params atlas).", false)]
        public void SetSprite(string key, params Sprite[] atlas) {
            Sprites.SetAtlas(key, atlas);
        }

        [Obsolete("use Sprites.GetAtlas(string root).", false)]
        public ResManager<Sprite> GetAtlas(string root) {
            return Sprites.GetAtlas(root);
        }

        [Obsolete("use Sprites.GetAtlas(string root, string name)", false)]
        public Sprite GetSprite(string root, string name) {
            Sprites.GetAtlas(root).Get(name);
            return Sprites.GetAtlas(root)[name];
        }

        #endregion SpriteRelated

        #region AudioRelated

        [Obsolete("use Audio.Load(string res)", false)]
        public AudioClip LoadAudio(string res) {
            return Audios.Load(res);
        }

        [Obsolete("use Audio.Load(params string[] res)", false)]
        public AudioClip[] LoadAudios(params string[] res) {
            return Audios.Load(res);
        }

        [Obsolete("use Audio.Get(string res)", false)]
        public AudioClip GetAudio(string res) {
            return Audios[res];
        }

        #endregion AudioRelated
    }
}