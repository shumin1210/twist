﻿using System.Collections.Generic;
using GameLibrary.Utility;
using UnityEngine;

namespace GameLibrary.UI {

    public class TabGroup : MonoBehaviour {
        private HashSet<TabButton> tabs = new HashSet<TabButton>();
        private TabButton currentTab;

        [SerializeField]
        private GameObject content;

        public GameObject Content {
            get {
                return content ?? gameObject;
            }
        }

        public void ChangeTab(TabButton tab) {
            if (tabs.Contains(tab)) {
                currentTab = tab;
                tabs.Foreach(r => r.toggle(r == currentTab));
            }
        }

        internal void Regist(TabButton tab) {
            tabs.Add(tab);
        }

        internal void Remove(TabButton tab) {
            tabs.Remove(tab);
        }
    }
}