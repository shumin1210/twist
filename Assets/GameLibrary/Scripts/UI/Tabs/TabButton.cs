﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameLibrary.UI {

    public class TabButton : MonoBehaviour, IPointerClickHandler {

        [Serializable]
        public sealed class OnToggledEvent : UnityEvent<bool> {
        }

        [SerializeField]
        private string title;

        [SerializeField]
        private bool isOn;

        private int id;

        public Image Face;

        public Text Text;

        public Image ActivedFace;

        public Text ActivedText;

        [SerializeField]
        private TabGroup group;

        public TabGroup Group {
            get {
                return group;
            }
            set {
                if (group != value) {
                    if (group != null) {
                        group.Remove(this);
                    }
                    if (value != null) {
                        value.Regist(this);
                    }
                    group = value;
                }
            }
        }

        public OnToggledEvent OnToggled;
        public UnityEvent OnActived;
        public UnityEvent OnInactived;

        public bool IsOn {
            get {
                return isOn;
            }
            set {
                if (isOn != value) {
                    if (Group) {
                        Group.ChangeTab(this);
                    } else {
                        toggle(value);
                    }
                }
            }
        }

        public string Title {
            get {
                return title;
            }
            set {
                title = value;
                if (ActivedText) {
                    ActivedText.text = title;
                }
                if (Text) {
                    Text.text = title;
                }
            }
        }

        public void OnPointerClick(PointerEventData eventData) {
            IsOn = !IsOn;
        }

        internal void toggle(bool value) {
            isOn = value;
            if (Face) {
                Face.gameObject.SetActive(!isOn);
            }
            if (ActivedFace) {
                ActivedFace.gameObject.SetActive(isOn);
            }
            OnToggled.Invoke(value);
            if (isOn) {
                OnActived.Invoke();
            } else {
                OnInactived.Invoke();
            }
        }

        private void OnDestroy() {
            if (Group) {
                Group.Remove(this);
            }
        }

        private void Start() {
            Title = title;
            if (Group) {
                Group.Regist(this);
            }
            toggle(IsOn);
        }
    }
}