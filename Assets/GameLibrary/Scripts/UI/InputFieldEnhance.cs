﻿using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldEnhance : InputField {

    private void Update() {
        placeholder.gameObject.SetActive(!isFocused);
    }

    new public void Append(string input) {
        text += input;
        DataShared.Instance.Data.Get<string>("Data.GameID");
    }

    public void Preppend(string value) {
        text = value + text;
    }

    public void Clear() {
        text = string.Empty;
    }

    public void Backspace() {
        if (text.Length > 0) {
            text = text.Substring(0, text.Length - 1);
        }
    }
}