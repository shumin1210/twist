﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace GameLibrary.UI {

    public class PressingEvent : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler {

        public enum SpeedType {
            Linear,
            Increassing
        }

        public SpeedType type;
        public float Delay = 1f;
        public float PressingInterval = .5f;
        private int accelerateCount = 0;

        public UnityEvent OnPressing;
        public float accelerattion = .01f;
        public float startInterval = .5f;
        public float minInterval = .1f;

        private bool isPressed;
        private bool isPressing;
        private float passedTime;

        public void OnPointerDown(PointerEventData eventData) {
            isPressed = true;
            passedTime = 0;
            isPressing = false;
            accelerateCount = 0;
        }

        public void OnPointerExit(PointerEventData eventData) {
            isPressed = false;
            isPressing = false;
        }

        public void OnPointerUp(PointerEventData eventData) {
            isPressed = false;
            isPressing = false;
        }

        private float getAmount() {
            return Math.Max(minInterval, startInterval - accelerateCount * accelerattion);
        }

        public void Update() {
            if (isPressed) {
                passedTime += Time.deltaTime;
                if (!isPressing) {
                    if (passedTime >= Delay) {
                        passedTime -= Delay;
                        isPressing = true;
                    }
                }
                switch (type) {
                    case SpeedType.Linear:
                        while (PressingInterval > 0 && passedTime > PressingInterval) {
                            passedTime -= PressingInterval;
                            OnPressing.Invoke();
                        }
                        break;

                    case SpeedType.Increassing:
                        float interval = getAmount();
                        while (interval > 0 && passedTime > interval) {
                            passedTime -= interval;
                            accelerateCount++;
                            OnPressing.Invoke();
                        }
                        break;
                }
            }
        }
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(PressingEvent))]
    public class PressingEventEditor : Editor {

        public override void OnInspectorGUI() {
            var o = target as PressingEvent;

            o.type = (PressingEvent.SpeedType)EditorGUILayout.EnumPopup("SpeedType", o.type);
            serializedObject.FindProperty("type").enumValueIndex = (int)o.type;

            EditorGUILayout.PropertyField(serializedObject.FindProperty("Delay"));

            switch (o.type) {
                case PressingEvent.SpeedType.Linear:
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("PressingInterval"));
                    break;

                case PressingEvent.SpeedType.Increassing:
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("startInterval"));
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("minInterval"));
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("accelerattion"));
                    break;
            }
            EditorGUILayout.PropertyField(serializedObject.FindProperty("OnPressing"));

            serializedObject.ApplyModifiedProperties();
        }
    }

#endif
}