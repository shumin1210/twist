﻿using System.Collections;
using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GameLibrary.UI {

    [DisallowMultipleComponent]
    [RequireComponent(typeof(Button), typeof(AudioSource), typeof(ChanneledAudio))]
    public class ButtonSEPatcher : MonoBehaviour {
        public UnityEvent AfterSound;
        private AudioSource au;
        public bool TriggerOnClickBeforePlayEnd = false;

        private void Start() {
            Button btn = GetComponent<Button>();
            au = GetComponent<AudioSource>();

            AfterSound = btn.onClick;
            btn.onClick = new Button.ButtonClickedEvent();
            btn.onClick.AddListener(play);
        }

        private void play() {
            if (TriggerOnClickBeforePlayEnd) {
                au.Play();
                AfterSound.Invoke();
            } else {
                StartCoroutine(wairPlayAudio());
            }
        }

        private IEnumerator wairPlayAudio() {
            au.Play();
            yield return new WaitWhile(() => au.isPlaying);
            AfterSound.Invoke();
        }

        private void Reset() {
            AudioSource au = GetComponent<AudioSource>();
            ChanneledAudio channel = GetComponent<ChanneledAudio>();

            au.playOnAwake = false;
            channel.Channel = AudioChannel.SE;
            au.loop = false;
        }
    }
}