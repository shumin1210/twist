﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using GameLibrary.Utility;
using UnityEngine.UI;
using UnityEngine.Events;

namespace GameLibrary.UI {

    public class UISetManager : MonoBehaviour {
        public List<UISet> UISets = new List<UISet>();
        public string DisplayKey = "";

        private void refreshLayout() {
            if (GetComponent<LayoutGroup>()) {
                LayoutRebuilder.ForceRebuildLayoutImmediate(transform as RectTransform);
            }
        }

        public void Show(string key) {
            UISets.Toggle(key, true);
            refreshLayout();
        }

        public void Switch(string key) {
            DisplayKey = key;
            UISets.Toggle(key, true);
            refreshLayout();
        }

        public void Toggle(string key, bool on) {
            DisplayKey = key;
            UISets.Toggle(key, on);
            refreshLayout();
        }

        public void Toggle(string key) {
            bool on = DisplayKey != key;
            DisplayKey = on ? key : "";
            UISets.Toggle(key, on);
            refreshLayout();
        }

        public void ToggleAll(bool on) {
            UISets.ToggleAll(on);
            refreshLayout();
        }

        public UISet Get(string key) {
            return UISets.Get(key);
        }

        public UISet Get(int index) {
            return UISets.Get(index);
        }
    }

    [Serializable]
    public class ToggleEvent {

        [Serializable]
        public class OnToggled : UnityEvent<bool> {
        }

        public OnToggled OnToggle = new OnToggled();
        public UnityEvent OnOpen = new UnityEvent();
        public UnityEvent OnClose = new UnityEvent();

        public void Invoke(bool on) {
            OnToggle.Invoke(on);
            (on ? OnOpen : OnClose).Invoke();
        }
    }

    [Serializable]
    public class UISet {
        public string Key;
        public List<GameObject> Values;

        public bool isActived {
            get; set;
        }

        public ToggleEvent Events = new ToggleEvent();

        public UISet() {
            Key = "";
            Values = new List<GameObject>();
        }

        public void TriggerEvent(bool on) {
            isActived = on;
            Events.Invoke(on);
        }

        public void Toggle(bool on) {
            isActived = on;
            Values.ForEach(v => v.SetActive(on));
            Events.Invoke(on);
        }

        public static implicit operator bool(UISet s) {
            return s != null;
        }

        public static readonly UISet Default = new UISet();
    }

    public static class UISetManagerExtension {

        public static IEnumerable<GameObject> AllElements(this IEnumerable<UISet> src) {
            return src.SelectMany(s => s.Values).Distinct();
        }

        public static UISet Get(this IEnumerable<UISet> src, string key) {
            return src.FirstOrDefault(s => s.Key == key) ?? UISet.Default;
        }

        public static UISet Get(this IEnumerable<UISet> src, int index) {
            return index < src.Count() ? src.ElementAt(index) : UISet.Default;
        }

        public static void Toggle(this IEnumerable<UISet> src, int index, bool on = true, bool individual = false) {
            UISet target = src.Get(index);
            if (individual) {
                target.Toggle(on);
            } else {
                src.AllElements().Foreach(o => o.SetActive(on == target.Values.Contains(o)));
                target.TriggerEvent(on);

                src.Except(new UISet[] { target }).Foreach(a => a.TriggerEvent(!on));
            }
        }

        public static void Toggle(this IEnumerable<UISet> src, string key, bool on = true, bool individual = false) {
            UISet target = src.Get(key);
            if (individual) {
                target.Toggle(on);
            } else {
                src.AllElements().Foreach(o => o.SetActive(on == target.Values.Contains(o)));
                target.TriggerEvent(on);

                src.Except(new UISet[] { target }).Foreach(a => a.TriggerEvent(!on));
            }
        }

        public static void ToggleIndividual(this IEnumerable<UISet> src, string key, bool? on = null) {
            UISet target = src.Get(key);
            on = on ?? !target.isActived;
            Toggle(src, key: key, on: (bool)on, individual: true);
        }

        public static void ToggleAll(this IEnumerable<UISet> src, bool on) {
            //src.SelectMany(s => s.Values).Distinct().Foreach(a => a.SetActive(on));
            src.Foreach(s => s.Toggle(on));
        }
    }
}