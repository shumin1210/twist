﻿using System;
using System.Linq;
using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GameLibrary.UI {

    public class GroupManager : MonoBehaviour {

        [Serializable]
        public class GroupItemToggleEvent : UnityEvent<GameObject, bool> {
        }

        public GameObject[] Items;

        public GroupItemToggleEvent OnItemToggled = new GroupItemToggleEvent();
        public UnityEvent OnChanged = new UnityEvent();

        private void RefreshLayout() {
            if (GetComponent<LayoutGroup>()) {
                LayoutRebuilder.ForceRebuildLayoutImmediate(transform as RectTransform);
            }
            OnChanged.Invoke();
        }

        public void Trigger(GameObject o) {
            if (OnItemToggled.GetPersistentEventCount() > 0) {
                OnItemToggled.Invoke(o, !o.activeInHierarchy);
            } else {
                o.SetActive(!o.activeInHierarchy);
            }
            RefreshLayout();
        }

        public void Switch(GameObject o) {
            if (OnItemToggled.GetPersistentEventCount() > 0) {
                Items.Foreach(i => OnItemToggled.Invoke(i, i == o));
            } else {
                Items.Foreach(i => i.SetActive(i == o));
            }
            RefreshLayout();
        }

        public void ToggleAll(bool on) {
            if (OnItemToggled.GetPersistentEventCount() > 0) {
                Items.Foreach(i => OnItemToggled.Invoke(i, on));
            } else {
                Items.Foreach(i => i.SetActive(on));
            }
            RefreshLayout();
        }

        public void Toggle(params int[] indexes) {
            if (OnItemToggled.GetPersistentEventCount() > 0) {
                Items.Foreach((o, i) => OnItemToggled.Invoke(o, indexes.Contains(i)));
            } else {
                Items.Foreach((o, i) => o.SetActive(indexes.Contains(i)));
            }
            RefreshLayout();
        }
    }
}