﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitalRuby.Tween;
using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace GameLibrary.UI {

    [ExecuteInEditMode]
#if UNITU_EDITOR
    [CanEditMultipleObjects]
#endif
    public class EasingObj : MonoBehaviour {

        public enum Easing {
            Linear,
            Bouncing
        }

        public enum State {
            Start,
            In,
            Stay,
            Out
        }

        public Easing EaseInType;
        public Easing EaseOutType;

        public State state;
        public bool Acting;

        public Vector3 Start;
        public Vector3 Stay;
        public float Duration = 1f;
        public float passedTime = 0f;

        public float Progress {
            get {
                if (state == State.In) {
                    return easeIn(passedTime / Duration);
                } else if (state == State.Out) {
                    return easeOut(passedTime / Duration);
                } else {
                    return passedTime / Duration;
                }
            }
        }

        #region Ease function

        private Func<float, float> easeIn {
            get {
                switch (EaseInType) {
                    default:
                    case Easing.Linear:
                        return TweenScaleFunctions.Linear;

                    case Easing.Bouncing:
                        return bounce;
                }
            }
        }

        private Func<float, float> easeOut {
            get {
                switch (EaseOutType) {
                    default:
                    case Easing.Linear:
                        return TweenScaleFunctions.Linear;

                    case Easing.Bouncing:
                        return bouncein;
                }
            }
        }

        private float bouncein(float t) {
            return 1 - bounce(1 - t);
        }

        private float bounce(float t) {
            if (t < (1 / 2.75)) {
                return 7.5625f * t * t;
            } else if (t < (2 / 2.75)) {
                return 7.5625f * (t -= (1.5f / 2.75f)) * t + 0.75f;
            } else if (t < (2.5 / 2.75)) {
                return 7.5625f * (t -= (2.25f / 2.75f)) * t + 0.9375f;
            } else {
                return 7.5625f * (t -= (2.625f / 2.75f)) * t + 0.984375f;
            }
        }

        #endregion Ease function

        internal void setStartRect() {
            Start = transform.localPosition;
        }

        internal void setStayRect() {
            Stay = transform.localPosition;
        }

        public void ResetEase() {
            state = State.Start;
        }

        public void EndEase() {
            state = State.Start;
        }

        public void EaseIn() {
            state = State.In;
        }

        public void EaseOut() {
            state = State.Out;
        }

        private double lastAct = 0;

        private float getDeltaTime() {
            float t = Time.deltaTime;
#if UNITY_EDITOR
            if (!Application.isPlaying) {
                t = (float)(EditorApplication.timeSinceStartup - lastAct);
                lastAct = EditorApplication.timeSinceStartup;
            }
#endif
            return t;
        }

        private void updateProgress() {
            var t = getDeltaTime();

            switch (state) {
                case State.Start:
                    passedTime = 0;
                    break;

                case State.In:
                    passedTime += t;
                    if (passedTime >= Duration) {
                        passedTime = Duration;
                        state = State.Stay;
                    }
                    break;

                case State.Stay:
                    passedTime = Duration;
                    break;

                case State.Out:

                    passedTime -= t;
                    if (passedTime <= 0) {
                        passedTime = 0;
                        state = State.Start;
                    }
                    break;
            }
        }

        public void Update() {
#if UNITY_EDITOR
            if (!Application.isPlaying && !Acting) {
                return;
            }
#endif

            updateProgress();

            RectTransform rect = transform as RectTransform;
            if (Progress <= 0) {
                transform.localPosition = Start;
            } else if (Progress >= 1) {
                transform.localPosition = Stay;
            } else {
                //calc ease
                transform.localPosition = Vector3.Lerp(Start, Stay, Progress);
            }
        }
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(EasingObj))]
    [CanEditMultipleObjects]
    public class TweenEditor : Editor {

        private void OnEnable() {
            EditorApplication.update = () => (target as EasingObj).Update();
        }

        private void OnDisable() {
            EditorApplication.update = () => {
            };
        }

        public override void OnInspectorGUI() {
            var o = target as EasingObj;

            EditorGUILayout.PropertyField(serializedObject.FindProperty("EaseInType"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("EaseOutType"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("state"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Duration"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("passedTime"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Acting"));
            serializedObject.ApplyModifiedProperties();

            if (!o.Acting) {
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Set Start")) {
                    o.setStartRect();
                } else if (GUILayout.Button("Set Stay")) {
                    o.setStayRect();
                }
                GUILayout.EndHorizontal();
            } else {
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Ease In")) {
                    o.EaseIn();
                } else if (GUILayout.Button("Ease Out")) {
                    o.EaseOut();
                }
                GUILayout.EndHorizontal();
            }
        }
    }

#endif
}