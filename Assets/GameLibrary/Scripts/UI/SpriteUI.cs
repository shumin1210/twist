﻿using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GameLibrary.UI {

    /// <summary>動畫UI</summary>
    [ExecuteInEditMode]
    public class SpriteUI : MonoBehaviour {

        /// <summary>Sprite資源所在路徑</summary>
        public string SpriteRoot = "";

        /// <summary>要使用的Sprite</summary>
        public Sprite SpriteUsed;

        /// <summary>動畫總時間</summary>
        public float Duration_sec = .5f;

        /// <summary>是否啟用動畫</summary>
        [SerializeField]
        private bool IsActived = true;

        /// <summary>Sprite資源集合</summary>
        public Sprite[] Sprites;

        public UnityEvent OnEnd;

        public bool ButtonPressed {
            get {
                return frameIndex == 1;
            }
            set {
                SetFrame(value ? 1 : 0);
            }
        }

        private Image image {
            get {
                return GetComponent<Image>();
            }
        }

        /// <summary>現在的幀索引</summary>
        public int frameIndex = 0;

        /// <summary>幀經過時間</summary>
        private float frameDelta = 0f;

        /// <summary>每幀時間</summary>
        private float frameDuration_sec {
            get {
                return Duration_sec / frameCount;
            }
        }

        /// <summary>幀數</summary>
        private int frameCount {
            get {
                return Sprites.Length;
            }
        }

        private void Start() {
            if (image && Sprites == null || Sprites.Length == 0) {
                LoadSprite(image.sprite);
            }
        }

        public void SetSpritesFromDataShared(string sprite) {
            var atlas = DataShared.Instance.Sprites.GetAtlas(sprite);
            if (atlas != null) {
                SetSprites(atlas.Values);
            }
        }

        public void SetSprites(Sprite[] sprite) {
            Sprites = sprite;
            frameIndex = 0;
            frameDelta = 0;
            changeSprite();
        }

        /// <summary>讀取指定Sprite資源,或重新讀取現有Sprite資源</summary>
        /// <param name="newSprite">指定新Sprite資源,不指定則重新讀取</param>
        public void LoadSprite(string newSpriteName) {
            if (string.IsNullOrEmpty(newSpriteName)) {
                return;
            }
            ResManager<Sprite> atlas = DataShared.Instance.Sprites.GetAtlas(SpriteRoot);
            Sprite[] tmp = atlas != null ? atlas.Values : Resources.LoadAll<Sprite>(SpriteRoot + newSpriteName);
            if (tmp != null && tmp.Length > 0) {
                SpriteUsed = tmp[0];
                Sprites = tmp;
                frameIndex = 0;
                frameDelta = 0;
                changeSprite();
            }
        }

        /// <summary>讀取指定Sprite資源,或重新讀取現有Sprite資源</summary>
        /// <param name="newSprite">指定新Sprite資源,不指定則重新讀取</param>
        public void LoadSprite(Sprite newSprite = null) {
            if (newSprite != null) {
                SpriteUsed = newSprite;
            }
            ResManager<Sprite> atlas = DataShared.Instance.Sprites.GetAtlas(SpriteRoot);
            Sprites = atlas != null ? atlas.Values : Resources.LoadAll<Sprite>(SpriteRoot + SpriteUsed.texture.name);
            frameIndex = 0;
            frameDelta = 0;
            changeSprite();
        }

        /// <summary>停止動畫,並停在第一幀</summary>
        public void Stop() {
            IsActived = false;
            frameIndex = 0;
            changeSprite();
        }

        /// <summary>開始從頭撥放動畫</summary>
        public void Play() {
            IsActived = true;
            frameIndex = 0;
            frameDelta = 0;
            changeSprite();
        }

        private void Update() {
            if (IsActived) {
                updateSprite();
            }
        }

        /// <summary>更新幀</summary>
        private void updateSprite() {
            if (IsActived) {
                frameDelta += Time.deltaTime;
                if (frameDelta > frameDuration_sec) {
                    frameDelta = 0;
                    frameIndex = (frameIndex + 1);
                    if (frameIndex >= frameCount - 1) {
                        OnEnd.Invoke();
                    }
                    frameIndex %= frameCount;
                    changeSprite();
                }
            }
        }

        /// <summary>將圖片更換為Sprites[frameIndex]</summary>
        private void changeSprite() {
            if (!image) {
                return;
            }

            if (frameCount == 0) {
                return;
            }
            frameIndex = TMath.Range(frameIndex, 0, frameCount - 1);
            image.sprite = Sprites[frameIndex];
        }

        public void SetFrame(int index) {
            frameIndex = TMath.Range(index, 0, frameCount);
            changeSprite();
        }
    }
}