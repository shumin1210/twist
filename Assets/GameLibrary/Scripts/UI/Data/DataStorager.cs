﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameLibrary.Utility.SerializableDictionary;
using UnityEngine;

namespace GameLibrary.UI {

    public class DataStorager : ScriptableObject {

        [Serializable]
        public class Map : DataMap<string, StringMap> {
            public string Name;
        }

        public Map Value;
    }
}