﻿using System;
using GameLibrary.Utility;
using UnityEngine;
using System.Reflection;
using UnityEngine.UI;
using GameLibrary.Core;
using System.Collections.Generic;
using GameLibrary.Utility.SerializableDictionary;
using System.Security.Policy;
using UnityEngine.Events;

namespace GameLibrary.UI {

    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public sealed class FieldBindableAttribute : FieldReferencableAttribute {
        public string Tag;
    }

    [Serializable]
    public class FormatableText {
        public bool UseReplace = true;
        public string Format = string.Empty;

        public string Generate(object val) {
            if (val == null) {
                return string.Empty;
            }
            if (string.IsNullOrEmpty(Format)) {
                return val.ToString();
            }
            if (UseReplace) {
                return Format.format(val);
            } else if (val is DateTime) {
                return ((DateTime)val).ToString(Format);
            }
            return val.ToString();
        }
    }

    public class FieldBinder : MonoBehaviour {

        [Serializable]
        public class ToggleEvent : UnityEvent<bool> {
        }

        public enum FieldType : int {
            Text,
            Image,
            Active,
            Toggle,
            GameObjectName
        }

        private bool initialized;

        public FieldType Type;

        public string Key {
            get {
                return Field.FieldName;
            }
        }

        [Tooltip("${PlyerPrefID} , #{Player.Attr.Field} , #DataShared.Data.{Key}")]
        [SerializeField]
        public FieldReference Field;

        [SerializeField]
        public FormatableText Format;

        [SerializeField]
        public bool HideWhenNoValue = true;

        [SerializeField]
        public bool InverseActive = false;

        private bool Interactable = false;

        [SerializeField]
        private ToggleEvent onToggled;

        private Action<object> updateValue;

        public delegate void onValueChanged(object newVal);

        public onValueChanged OnValueChanged;

        public UnityEvent OnBinded;

        [SerializeField]
        private UnityEngine.Object defaultValue;

        private void Awake() {
            if (!initialized) {
                Bind(gameObject);
            }
        }

        public void Initialize(object val, Action<object> onValueChanged = null) {
            init(onValueChanged);

            SetValue(val);

            OnBinded.Invoke();
        }

        private void init(Action<object> onValueChanged = null) {
            initialized = true;
            if (onValueChanged != null) {
                OnValueChanged += v => onValueChanged(v);
            }

            switch (Type) {
                default:
                case FieldType.Text:
                    initText();
                    break;

                case FieldType.Image:
                    initImage();
                    break;

                case FieldType.Active:
                    initActive();
                    break;

                case FieldType.Toggle:
                    initToggle();
                    break;

                case FieldType.GameObjectName:
                    initGameObjectName();
                    break;
            }
        }

        private void initGameObjectName() {
            updateValue = val => {
                string name = Format.Generate(val);
                gameObject.name = name ?? gameObject.name;
                if (HideWhenNoValue) {
                    gameObject.SetActive(!string.IsNullOrEmpty(name));
                }
            };
        }

        private void initToggle() {
            if (GetComponent<Toggle>()) {
                Interactable = true;
                Toggle t = GetComponent<Toggle>();
                updateValue = val => {
                    bool b = TConvert.ToBoolean(val);
                    t.isOn = InverseActive ? !b : b;
                    onToggled.Invoke(InverseActive ? !b : b);
                };
                t.onValueChanged.AddListener(a => OnValueChanged.TryInvoke(InverseActive ? !a : a));
            } else if (GetComponent<ToggleAdv>()) {
                Interactable = true;
                ToggleAdv t = GetComponent<ToggleAdv>();
                updateValue = val => {
                    bool b = TConvert.ToBoolean(val);
                    t.isOn = InverseActive ? !b : b;
                    onToggled.Invoke(InverseActive ? !b : b);
                };
                t.onValueChanged.AddListener(a => OnValueChanged.TryInvoke(InverseActive ? !a : a));
            } else {
                updateValue = val => {
                    bool b = TConvert.ToBoolean(val);
                    onToggled.Invoke(InverseActive ? !b : b);
                };
            }
        }

        private void initActive() {
            Interactable = false;
            updateValue = val =>
                gameObject.SetActive(InverseActive ? !TConvert.ToBoolean(val) : TConvert.ToBoolean(val));
        }

        public void SetValue(object val) {
            if (val is IValueBind) {
                val = (val as IValueBind).GetValue;
            }
            updateValue.Invoke(val);
        }

        private void initText() {
            if (GetComponent<Text>()) {
                Interactable = false;
                updateValue = val => {
                    GetComponent<Text>().TrySetUtilValue(Format.Generate(val));
                    if (HideWhenNoValue) {
                        gameObject.SetActive(!string.IsNullOrEmpty(GetComponent<Text>().text));
                    }
                };
            } else if (GetComponent<InputField>()) {
                Interactable = true;
                GetComponent<InputField>().onValueChanged.AddListener(v => OnValueChanged.TryInvoke(v));
                updateValue = val => {
                    GetComponent<InputField>().text = Format.Generate(val);
                    if (HideWhenNoValue) {
                        gameObject.SetActive(!string.IsNullOrEmpty(GetComponent<InputField>().text));
                    }
                };
            }
        }

        private void initImage() {
            if (GetComponent<Image>()) {
                Interactable = false;
                Image img = GetComponent<Image>();
                updateValue = val => {
                    if (val is Sprite) {
                        Sprite s = img.sprite = val as Sprite;
                        if (HideWhenNoValue) {
                            gameObject.SetActive(s);
                        }
                    } else if (val is string) {
                        string str = val.ToString();
                        string[] keys = str.Split(".");
                        Uri result;
                        if (Uri.TryCreate(str, UriKind.Absolute, out result)) {
                            RemoteResLoader.Load<Sprite>(img.gameObject, val.ToString(), s => {
                                if (s != null) {
                                    img.sprite = s;
                                } else if (defaultValue != null && defaultValue is Sprite) {
                                    img.sprite = defaultValue as Sprite;
                                }
                                if (HideWhenNoValue) {
                                    gameObject.SetActive(img.sprite);
                                }
                            });
                            //img.LoadSpriteFromWWW(val.ToString(), s => {
                            //    img.sprite = s;
                            //    if (HideWhenNoValue) {
                            //        gameObject.SetActive(s);
                            //    }
                            //});
                        } else if (keys.Length > 2) {
                            Sprite sp = DataShared.Instance.Sprites.GetAtlas(keys[0])[str.Substring(keys[0].Length + 1)];
                        }
                    }
                };
            }
        }

        private void trySetValue<T>(Action<T> act) where T : MonoBehaviour {
            T c = GetComponent<T>();
            if (c) {
                act(c);
            }
        }

        public static void Bind(Component o, object data = null) {
            Bind(o.gameObject, data);
        }

        public static void Bind(GameObject o, object data = null) {
            if (!o) {
                return;
            }
            FieldBinder[] fields = o.GetComponentsInChildren<FieldBinder>(true);
            foreach (FieldBinder f in fields) {
                object val;
                string[] keys = f.Key.Split(".");
                if (string.IsNullOrEmpty(f.Key)) {
                    Debug.LogWarningFormat("key is empty on gameobject:{0}", o.name);
                    continue;
                } else if (f.Key[0] == '$') {
                    //use player pref;
                    string key = f.Key.Substring(1);
                    f.Initialize(PlayerPrefs.GetString(key), v => {
                        PlayerPrefs.SetString(key, v.ToString());
                        PlayerPrefs.Save();
                    });
                } else if (f.Key[0] == '#') {
                    if (keys.Length > 1) {
                        string key = keys[0].Substring(1);
                        string attr, field;
                        if (key == "Player") {
                            switch (keys.Length) {
                                //PlayerAttr
                                case 3:

                                    attr = keys[1];
                                    field = keys[2];
                                    Player p = DataShared.Instance.GetData<Player>(key, ignoreCatagory: true);
                                    Type t = ReflectionUtility.GetSubTypes(typeof(IAdditionalPlayerAttr<>), attr);
                                    object at = p.GetAdditionAttrByType(t);
                                    if (at != null && ReflectionUtility.TryGetValue(at, field, out val)) {
                                        f.Initialize(val);
                                    }
                                    break;

                                //DataShared.Data
                                default:

                                    field = keys[1];
                                    object d = DataShared.Instance.GetData(key, ignoreCatagory: true);
                                    if (d != null && ReflectionUtility.TryGetValue(d, field, out val)) {
                                        f.Initialize(val);
                                    }
                                    break;
                            }
                        } else {
                            string _k = f.Key.Substring(1);
                            object d = DataShared.Instance.GetData(_k, ignoreCatagory: true);
                            f.Initialize(d, v => DataShared.Instance.Data.Set(_k, v));
                        }
                    } else {
                        string key = keys[0].Substring(1);
                        object d = DataShared.Instance.GetData(key, ignoreCatagory: true);
                        f.Initialize(d, v => DataShared.Instance.Data.Set(key, v));
                    }
                } else if (data != null) {
                    if (ReflectionUtility.TryGetValue(data, f.Key, out val)) {
                        f.Initialize(val);
                    }
                }
            }
        }

        public override string ToString() {
            return "{FieldBinder} key=" + Key;
        }
    }
}