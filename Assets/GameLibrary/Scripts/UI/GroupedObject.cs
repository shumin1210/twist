﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GameLibrary.UI {

    public class GroupedObject : MonoBehaviour {
        private static Dictionary<string, HashSet<GroupedObject>> groups = new Dictionary<string, HashSet<GroupedObject>>();

        internal static void Registry(GroupedObject obj) {
            if (string.IsNullOrEmpty(obj.GroupName)) {
                return;
            }
            if (!groups.ContainsKey(obj.GroupName)) {
                groups.Add(obj.GroupName, new HashSet<GroupedObject>());
            }
            groups[obj.GroupName].Add(obj);
        }

        internal static void CancelRegistry(GroupedObject obj) {
            if (string.IsNullOrEmpty(obj.GroupName)) {
                return;
            }
            if (groups.ContainsKey(obj.GroupName)) {
                groups[obj.GroupName].Remove(obj);
            }
        }

        public static void Clear(string groupName) {
            if (groups.ContainsKey(groupName)) {
                groups.Remove(groupName);
            }
        }

        public string GroupName;
        public bool Actived;
        public UnityEvent OnActived = new UnityEvent();
        public UnityEvent OnDeactived = new UnityEvent();

        private void Start() {
            Registry(this);
        }

        public static void Switch(params GroupedObject[] obj) {
            var g = obj.GroupBy(s => s.GroupName);
            foreach (var item in g) {
                if (groups.ContainsKey(item.Key)) {
                    foreach (GroupedObject o in groups[item.Key]) {
                        o.Toggle(item.Contains(o));
                    }
                }
            }
        }

        public static void ToggleAll(string name, bool on) {
            foreach (GroupedObject o in groups[name]) {
                o.Toggle(on);
            }
        }

        public void AllOn() {
            ToggleAll(GroupName, true);
        }

        public void AllOff() {
            ToggleAll(GroupName, false);
        }

        public void Trigger() {
            if (Actived) {
                Toggle();
            } else {
                Switch();
            }
        }

        public void Switch() {
            Switch(this);
        }

        public void Toggle() {
            Toggle(!Actived);
        }

        public void Toggle(bool on) {
            Actived = (bool)on;
            if (GetComponent<SpriteUI>()) {
                GetComponent<SpriteUI>().SetFrame(Actived ? 1 : 0);
            } else if (GetComponent<Button>()) {
                if (Actived) {
                    GetComponent<Button>().Select();
                }
            } else {
                gameObject.SetActive(Actived);
            }
            (Actived ? OnActived : OnDeactived).Invoke();
        }
    }
}