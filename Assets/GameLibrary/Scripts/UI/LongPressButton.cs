﻿using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameLibrary.UI {

    public class LongPressButton : Button, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IPointerClickHandler {

        /// <summary>
        /// 長押事件發生需要的時間
        /// </summary>
        public float holdTime = 0.3f;

        /// <summary>
        /// 長押按鈕時發生的事件
        /// </summary>
        public UnityEvent onLongPress = new UnityEvent();
        /// <summary>
        /// 游標離開按鈕範圍時發生的事件
        /// </summary>
        public UnityEvent onPointerExit = new UnityEvent();
        /// <summary>
        /// 游標進入按鈕範圍內發生的事件
        /// </summary>
        public UnityEvent onPointerEnter = new UnityEvent();
        /// <summary>
        /// 點下此按鈕時發生的事件
        /// </summary>
        public UnityEvent onPointerDown = new UnityEvent();
        /// <summary>
        /// 已經點下此按鈕並放開，且游標仍在按鈕範圍內時發生的事件
        /// </summary>
        public UnityEvent onPointerUpInside = new UnityEvent();
        /// <summary>
        /// 已經點下此按鈕並放開，但游標已經離開按鈕範圍時發生的事件
        /// </summary>
        public UnityEvent onPointerUpOutside = new UnityEvent();

        /// <summary>
        /// 游標位置是否在此物件內
        /// </summary>
        bool pointerInside = false;

        public override void OnPointerEnter(PointerEventData eventData) {
            pointerInside = true;
        }

        public override void OnPointerDown(PointerEventData eventData) {
            Invoke("OnLongPress", holdTime);
            onPointerDown.Invoke();
        }

        public override void OnPointerExit(PointerEventData eventData) {
            pointerInside = false;
            CancelInvoke("OnLongPress");
            CancelInvoke("OnPointerUp");
            onPointerExit.Invoke();
        }

        public override void OnPointerUp(PointerEventData eventData) {
            CancelInvoke("OnLongPress");
            if (pointerInside) {
                onPointerUpInside.Invoke();
            } else {
                onPointerUpOutside.Invoke();
            }
        }

        private void OnLongPress() {
            onLongPress.Invoke();
        }

    }
}
