﻿using UnityEngine;

namespace GameLibrary.UI {

    [ExecuteInEditMode]
    [RequireComponent(typeof(RectTransform))]
    public class RotationFitter : MonoBehaviour {

        private RectTransform rect {
            get {
                return GetComponent<RectTransform>();
            }
        }

        [SerializeField]
        private RectTransform rectTransform;

        public RectTransform RefTransform {
            get {
                return rectTransform ?? transform.parent.GetComponent<RectTransform>();
            }
            set {
                rectTransform = value;
            }
        }

        private void Start() {
            ResetSize();
        }

        public void ResetSize() {
            var size = RefTransform.rect.size;
            if (Mathf.Abs(Mathf.Sin(transform.localEulerAngles.z * Mathf.Deg2Rad)) == 1) {
                rect.sizeDelta = new Vector2(size.y, size.x);
            } else {
                rect.sizeDelta = new Vector2(size.x, size.y);
            }
        }

        [ExecuteInEditMode]
        private void OnRectTransformDimensionsChange() {
            ResetSize();
        }
    }
}