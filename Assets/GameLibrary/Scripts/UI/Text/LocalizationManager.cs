﻿using System.Collections.Generic;
using System.Linq;
using GameLibrary.Utility;
using UnityEngine;

namespace GameLibrary.UI {

    /// <summary>多國語言核心</summary>
    public class LocalizationManager : Singleton<LocalizationManager> {

        private class LanguagePack {
            private Dictionary<string, int> id;
            private TDataTable data;

            public LanguagePack(TDataTable data) {
                Load(data);
            }

            public void Load(TDataTable data) {
                this.data = data;
                id = data.Rows.Select((r, i) => new {
                    key = r[0].ToString(),
                    id = i
                }).ToDictionary(s => s.key, s => s.id);
            }

            public string this[string lanuage, string key] {
                get {
                    if (id.ContainsKey(key)) {
                        return data.Rows[id[key]].Get<string>(lanuage) ?? "";
                    } else {
                        return "";
                    }
                }
            }
        }

        private const string KEY = "localization";

        //private static Dictionary<string, Dictionary<string, Dictionary<string, string>>> cacheDic = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();
        private static Dictionary<string, LanguagePack> cacheDic = new Dictionary<string, LanguagePack>();

        /// <summary>語言映射</summary>
        //private Dictionary<string, LanguagePackage> Map = new Dictionary<string, LanguagePackage>();

        /// <summary>使用中的語言ID</summary>
        public static string Language = "TW";

        public static void SetLanguageAsset(string bundle, TextAsset asset) {
            if (asset) {
                DataShared.Instance.TextAssets.Set(KEY + "." + bundle, asset);
                SetLanguageAsset(bundle, asset.text);
            }
        }

        public static void SetLanguageAsset(string bundle, string asset) {
            if (string.IsNullOrEmpty(asset)) {
                return;
            }
            Dictionary<string, Dictionary<string, string>> dictionary;
            TDataTable data = null;
            if (JS.TryDeserialize(asset, out dictionary)) {
                data = new TDataTable();
                data.Load(dictionary.Keys.ToArray(), dictionary.Values.Select(r => r.Values.Select(f => f as object).ToArray()).ToArray());
            } else {
                data = CSVReader.LoadCSV(asset, hasHeader: true);
            }

            if (cacheDic.ContainsKey(bundle)) {
                cacheDic[bundle].Load(data);
            } else {
                cacheDic.Add(bundle, new LanguagePack(data));
            }
        }

        public static void ReleaseLanguageAsset(string bundle) {
            if (cacheDic.ContainsKey(bundle)) {
                cacheDic.Remove(bundle);
            }
            if (DataShared.Instance && DataShared.Instance.TextAssets != null) {
                TextAsset txt = DataShared.Instance.TextAssets[KEY + "." + bundle];
                if (txt) {
                    DataShared.Instance.TextAssets.Unload(bundle);
                }
            }
        }

        /// <summary>讀取語言包</summary>
        /// <param name="bundle">指定語言包資源的來源</param>
        private void loadLocalization(string bundle) {
            if (!cacheDic.ContainsKey(bundle)) {
                TextAsset txt = DataShared.Instance.TextAssets[KEY] ?? DataShared.Instance.TextAssets[KEY + "." + bundle] ?? Resources.Load<TextAsset>(bundle + "/" + KEY);
                if (!txt) {
                    return;
                } else {
                    SetLanguageAsset(bundle, txt.text);
                }
            }
        }

        /// <summary>取得指定群組與key的語言字串</summary>
        /// <param name="bundle">群組ID</param>
        /// <param name="key">Key</param>
        /// <returns>指定語言的原始字串,若不包含此群組ID(bundle)或key值則回傳空字串</returns>
        public string GetLocalizedValue(string bundle, string key) {
            if (!cacheDic.ContainsKey(bundle)) {
                return null;
            }

            return cacheDic[bundle][Language, key];
        }
    }
}