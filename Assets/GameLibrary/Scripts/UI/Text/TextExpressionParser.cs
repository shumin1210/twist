﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameLibrary.Utility;
using GameLibrary.Utility.Parser;
using GameLibrary.Utility.SerializableDictionary;
using UnityEngine;

namespace GameLibrary.UI {

    public class TextExpressionParser : MonoBehaviour {
        private ExpressionParser parser = new ExpressionParser();

        [SerializeField]
        private string Expression;

        [SerializeField]
        private StringMap Parameters;

        public float Value {
            get {
                return (float)EvaluateExpression().Value;
            }
        }

        public Expression EvaluateExpression() {
            var val = parser.EvaluateExpression(Expression);
            foreach (KeyValuePair<string, string> item in Parameters) {
                double v;
                if (!double.TryParse(item.Value, out v)) {
                    v = DataShared.Instance.GetData<double>(item.Value);
                }
                val.Parameters[item.Key].Value = v;
            }
            return val;
        }
    }
}