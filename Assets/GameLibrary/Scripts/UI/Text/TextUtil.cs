﻿using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Events;
using System.Collections.Generic;
using GameLibrary.Utility.Parser;
using GameLibrary.Utility.SerializableDictionary;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace GameLibrary.Utility {

    public static class TextUtilExtension {

        public static void TrySetUtilValue(this Text src, string val) {
            var tu = src.GetComponent<TextUtil>();
            if (tu) {
                tu.SetValue(val);
            } else {
                src.text = val;
            }
        }
    }

    [RequireComponent(typeof(Text))]
    [ExecuteInEditMode]
    public class TextUtil : MonoBehaviour {

        [Serializable]
        public class OnValid : UnityEvent<string> {
        }

        [Serializable]
        public enum TextType {
            Custom,
            Currency,
            Integer,
            DateTime,
            Calculation
        }

        [Tooltip("Leave blank for default format('{0}')")]
        [SerializeField]
        private string format;

        public string Format {
            get {
                if (string.IsNullOrEmpty(format.Trim())) {
                    return getPresetFormat(Type);
                } else {
                    return format;
                }
            }
            set {
                format = value;
            }
        }

        public TextType Type;

        [Header("Valid Events")]
        [Tooltip("When text matches values in collection will trigger OnValueValid event.")]
        public List<string> ValidValues;

        private bool useValidEvent {
            get {
                return ValidValues.Count > 0;
            }
        }

        public OnValid OnValueValid;

        #region ExpressionPArser

        private ExpressionParser parser = new ExpressionParser();

        [Header("Expression Settings")]
        [SerializeField]
        private string Expression;

        [SerializeField]
        private StringMap Parameters;

        public double Value {
            get {
                return Evaluate();
            }
        }

        public double Evaluate() {
            var val = parser.EvaluateExpression(Expression);
            foreach (KeyValuePair<string, string> item in Parameters) {
                double v;
                if (!double.TryParse(item.Value, out v)) {
                    v = DataShared.Instance.GetData<double>(item.Value);
                }
                val.Parameters[item.Key].Value = v;
            }
            return val.Value;
        }

        #endregion ExpressionPArser

        [SerializeField]
        private string _val;

        private string Val {
            get {
                return _val;
            }
            set {
                _val = value;
                if (useValidEvent && ValidValues.Contains(_val)) {
                    OnValueValid.Invoke(_val);
                }
            }
        }

        private object _realVal;

        private void Start() {
            _val = GetComponent<Text>().text;
        }

        internal static string getPresetFormat(TextType type) {
            switch (type) {
                case TextType.Currency:
                    return "{0:N0}";

                case TextType.DateTime:
                    return "{0:yyyy/MM/dd HH:mm:ss}";

                default:
                    return "{0}";
            }
        }

        public void AddValue(int args) {
            switch (Type) {
                default:
                case TextType.Custom:
                    break;

                case TextType.Currency:
                    Curency_addValue(args);
                    break;

                case TextType.Integer:
                    Intege_addValue(args);
                    break;
            }
        }

        private void Intege_addValue(int args) {
            int val;
            if (int.TryParse(_val, out val)) {
                val += args;
                SetValue(val);
            }
        }

        private void Curency_addValue(int args) {
            decimal val;
            if (decimal.TryParse(_val, out val)) {
                val += args;
                SetValue(val);
            }
        }

        public void UpdateValue() {
            switch (Type) {
                case TextType.Calculation:
                    SetValue(Evaluate());
                    break;

                default:
                    GetComponent<Text>().text = string.Format(Format, _realVal);
                    break;
            }
        }

        public void SetValue(decimal args) {
            _realVal = args;
            Val = args.ToString();
            GetComponent<Text>().text = string.Format(Format, args);
        }

        public void SetValue(double args) {
            _realVal = args;
            Val = args.ToString();
            GetComponent<Text>().text = string.Format(Format, args);
        }

        public void SetValue(float args) {
            _realVal = args;
            Val = args.ToString();
            GetComponent<Text>().text = string.Format(Format, args);
        }

        public void SetValue(string args) {
            _realVal = args;
            Val = args.ToString();
            GetComponent<Text>().text = string.Format(Format, args);
        }

        public void SetValue(int args) {
            _realVal = args;
            Val = args.ToString();
            GetComponent<Text>().text = string.Format(Format, args);
        }

        public void SetValue(long args) {
            _realVal = args;
            Val = args.ToString();
            GetComponent<Text>().text = string.Format(Format, args);
        }

        public void SetArgs(params object[] args) {
            _realVal = args;
            GetComponent<Text>().text = string.Format(Format, args);
        }
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(TextUtil))]
    public class TextUtilEditor : Editor {

        public override void OnInspectorGUI() {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Type"));

            var a = (TextUtil.TextType)serializedObject.FindProperty("Type").enumValueIndex;
            GUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("format"));
            if (GUILayout.Button("Reset Format", GUILayout.Width(100))) {
                serializedObject.FindProperty("format").stringValue = TextUtil.getPresetFormat(a);
            }
            GUILayout.EndHorizontal();
            switch (a) {
                case TextUtil.TextType.Calculation:
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("Expression"));
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("Parameters"), true);
                    break;

                default:
                    break;
            }

            switch (a) {
                default:
                    break;

                case TextUtil.TextType.Integer:
                    serializedObject.FindProperty("_val").stringValue = EditorGUILayout.IntField("Value", TConvert.ToInt(serializedObject.FindProperty("_val").stringValue, 0)).ToString();
                    break;
            }

            EditorGUILayout.PropertyField(serializedObject.FindProperty("ValidValues"), includeChildren: true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("OnValueValid"), includeChildren: true);

            serializedObject.ApplyModifiedProperties();
        }
    }

#endif
}