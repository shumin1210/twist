﻿using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class TextResize : MonoBehaviour {
    private Text text;
    public int size;
    private float height;
    private bool needUpdate = false;

    private void Start() {
        text = GetComponent<Text>();
        if (size == 0) {
            size = text.fontSize;
        }
        height = (transform as RectTransform).rect.height;
    }

    public void RequestUpdate() {
        needUpdate = true;
    }

    private void OnRectTransformDimensionsChange() {
        needUpdate = true;
    }

    private void Update() {
#if UNITY_EDITOR
        text.fontSize = size;
#endif
        if (needUpdate) {
            float tmpHeight = (transform as RectTransform).rect.height;
            if (tmpHeight != height) {
                text.fontSize = (int)(tmpHeight / height * size);
            }
            needUpdate = false;
        }
    }
}