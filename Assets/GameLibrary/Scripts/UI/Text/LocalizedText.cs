﻿using UnityEngine;
using UnityEngine.UI;

namespace GameLibrary.UI {

    /// <summary>多國語言</summary>
    public class LocalizedText : Utility.InitializableObject<LocalizedText> {

        /// <summary>語言包資源路徑</summary>
        public string bundle;

        /// <summary>語言key</summary>
        public string key;

        /// <summary>語言原始字串</summary>
        private string text;

        /// <summary>用於替換原始字串的參數</summary>
        private object[] args;

        public override void OnResInitialized(params object[] args) {
            if (args.Length > 0) {
                if (args[0] as string != bundle) {
                    return;
                }
            }
            LoadLocalizationText();
            Refresh();
        }

        protected override void OnEnable() {
            base.OnEnable();
            Refresh();
        }

        /// <summary>刷新Text元件的文字</summary>
        private void Refresh() {
            if (string.IsNullOrEmpty(text)) {
                return;
            }
            if (args != null && args.Length > 0) {
                GetComponent<Text>().text = string.Format(text, args);
            } else {
                GetComponent<Text>().text = text;
            }
        }

        /// <summary>設定語言參數,並執行Refresh()</summary>
        /// <param name="pargs">字串參數</param>
        public void SetArgs(string[] pargs) {
            LoadLocalizationText();
            if (pargs.Length > 0) {
                args = pargs;
            }
            Refresh();
        }

        /// <summary>設定語言參數,並執行Refresh()</summary>
        /// <param name="pargs">任意型別的參數</param>
        public void SetArgs(params object[] pargs) {
            LoadLocalizationText();
            if (pargs.Length > 0) {
                args = pargs;
            }
            Refresh();
        }

        /// <summary>讀取語言字串</summary>
        private void LoadLocalizationText() {
            text = LocalizationManager.Instance.GetLocalizedValue(bundle, key) ?? text;
        }
    }
}