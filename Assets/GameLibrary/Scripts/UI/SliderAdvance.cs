﻿using UnityEngine.UI;

namespace GameLibrary.UI {

    public class SliderAdvance : Slider {

        public void IncreaseValue(float amount) {
            value += amount;
        }
    }
}