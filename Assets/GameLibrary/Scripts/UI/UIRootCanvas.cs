﻿using UnityEngine;
namespace GameLibrary.UI {

    /// <summary>指定此GameObject為UIManager的CanvasRoot</summary>
    public class UIRootCanvas : MonoBehaviour {
        private void Awake() {
            UIManager.Instance.CanvasRoot = gameObject;
        }
    }
}