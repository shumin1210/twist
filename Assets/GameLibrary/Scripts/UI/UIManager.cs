﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameLibrary.Core;
using GameLibrary.Utility;
using UnityEngine;

namespace GameLibrary.UI {

    /// <summary>浮動UI管理</summary>
    public class UIManager : Singleton<UIManager> {

        /// <summary>預存UI物件的路徑</summary>
        public string UI_GAMEPANEL_ROOT = "Prefabs/GamePanel/";

        private GameObject canvasRoot;

        /// <summary>基底Canvas物件</summary>
        public GameObject CanvasRoot {
            get {
                if (!canvasRoot) {
                    canvasRoot = GameObject.Find("CanvasRoot") ?? GameObject.FindObjectOfType<Canvas>().gameObject;
                }
                return canvasRoot;
            }
            set {
                if (value && value.GetComponent<Canvas>()) {
                    canvasRoot = value;
                }
            }
        }

        /// <summary>要附加UI到指定的目標,若無指定則預設以CanvasRoot為目標</summary>
        public GameObject TargetSite;

        /// <summary>現存UI集合</summary>
        public TDictionary<string, GameObject> PanelList = new TDictionary<string, GameObject>();

        /// <summary>檢查是否有基底Canvas可用,若無設定則嘗試尋找名為CanvasRoot的物件</summary>
        /// <returns>若不存在則回傳false</returns>
        private bool CheckCanvasRootIsNull() {
            //if (CanvasRoot == null) {
            //    CanvasRoot = GameObject.Find("CanvasRoot");
            //}
            if (CanvasRoot == null) {
                Debug.LogError("CanvasRoot is Null.");
                return true;
            } else {
                return false;
            }
        }

        /// <summary>檢查指定名稱的UI是否已建立</summary>
        /// <param name="name">UI名稱</param>
        /// <returns>若PanelList存在該名稱UI則回傳True</returns>
        private bool IsPanelLive(string name) {
            if (PanelList[name]) {
                return true;
            } else {
                var go = GameObject.Find(name);
                if (!go) {
                    var tmp = CanvasRoot.transform.FindDeepChild(name);
                    if (tmp) {
                        go = tmp.gameObject;
                    }
                }

                if (go) {
                    PanelList[name] = go;
                    return true;
                } else {
                    var tr = CanvasRoot.transform.Find(name);
                    if (tr) {
                        PanelList.Add(name, tr.gameObject);
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>顯示指定名稱的UI</summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public GameObject ShowPanel(string name, GameObject parent = null, bool noRoot = false) {
            if (CheckCanvasRootIsNull()) {
                return null;
            }

            if (IsPanelLive(name)) {
                if (PanelList[name].activeSelf) {
                    Debug.LogErrorFormat("[{0}] is showing.", name);
                    return null;
                } else {
                    PanelList[name].SetActive(true);
                    return PanelList[name];
                }
            }
            /*嘗試尋找預存UI*/
            GameObject loadGO = Utility.AssetRelate.ResourcesLoadCheckNull<GameObject>(noRoot ? name : UI_GAMEPANEL_ROOT + name);
            if (loadGO == null) {
                return null;
            }

            GameObject panel = Utility.GameObjectRelate.InstantiateGameObject(parent ?? TargetSite ?? CanvasRoot, loadGO);
            panel.name = name;

            PanelList.Add(name, panel);
            return panel;
        }

        /// <summary>開/關UI</summary>
        /// <param name="name">指定UI名稱</param>
        /// <param name="isOn">true則為開</param>
        public GameObject TogglePanel(string name, bool? isOn = null) {
            if (IsPanelLive(name)) {
                if (PanelList[name] != null) {
                    if (isOn == null) {
                        PanelList[name].SetActive(!PanelList[name].activeInHierarchy);
                    } else {
                        PanelList[name].SetActive((bool)isOn);
                    }
                    return PanelList[name];
                }
            } else {
                Debug.LogWarningFormat("TogglePanel [{0}] not found.", name);
            }
            return null;
        }

        /// <summary>開/關UI</summary>
        /// <param name="name">指定UI名稱</param>
        /// <param name="isOn">true則為開</param>
        public GameObject TogglePanel(Type panelType, bool? isOn = null) {
            string name = panelType.Name;

            var attrs = panelType.GetCustomAttributes(typeof(SharedViewAttribute), true);

            if (attrs.Length == 1) {
                name = (attrs[0] as SharedViewAttribute).NAME;
                if (IsPanelLive(name)) {
                    if (PanelList[name] != null) {
                        if (isOn == null) {
                            PanelList[name].SetActive(!PanelList[name].activeInHierarchy);
                        } else {
                            PanelList[name].SetActive((bool)isOn);
                        }
                        return PanelList[name];
                    }
                    MonoBehaviour panel = GameObject.FindObjectOfType(panelType) as MonoBehaviour;
                }
            }
            Debug.LogErrorFormat("TogglePanel [{0}] not found.", name);
            return null;
        }

        /// <summary>關閉UI,並銷毀</summary>
        /// <param name="name">指定UI名稱</param>
        public void ClosePanel(string name) {
            if (IsPanelLive(name)) {
                var a = PanelList[name];
                PanelList.Remove(name);
                if (a != null) {
                    try {
                        UnityEngine.Object.Destroy(a);
                    } catch (System.Exception e) {
                        Debug.LogWarningFormat("ClosePanel[{0}] exception:{1}" + e);
                    }
                }
            }
        }

        /// <summary>關閉並銷毀所有UI</summary>
        public void CloseAllPanel() {
            foreach (KeyValuePair<string, GameObject> item in PanelList) {
                if (item.Value != null) {
                    UnityEngine.Object.Destroy(item.Value);
                }
            }
            PanelList.Clear();
        }

        /// <summary>取得Canvas的大小</summary>
        /// <returns>sizeDelta</returns>
        public Vector2 GetCanvasSize() {
            if (CheckCanvasRootIsNull()) {
                return Vector2.one * -1;
            }
            RectTransform trans = CanvasRoot.transform as RectTransform;

            return trans.sizeDelta;
        }
    }
}