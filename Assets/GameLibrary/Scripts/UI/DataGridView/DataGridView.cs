﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameLibrary.Utility;
using UnityEngine;

namespace GameLibrary.UI {

    [ExecuteInEditMode]
    public class DataGridView : MonoBehaviour {

        [Serializable]
        public struct ColumnStyle {
            public string DisplayText;
            public string ColumnName;
            public Color BackGroundColor;
            public TextAnchor TextAnchor;
            public Color[] TextColor;
            public int FontSize;
            public string Format;
        }

        [SerializeField]
        private bool needUpdate = true;

        public GameObject Prefab_DataRow;
        public GameObject Prefab_DataCell;
        public GameObject Prefab_DataHeader;

        [SerializeField]
        private DataRow Header;

        public bool Outlined;
        public bool ShowHeader;

        public List<string> Columns {
            get {
                return ColumnStyles.Select(c => c.ColumnName).ToList();
            }
        }

        public Dictionary<string, Func<object, object>> Parsers = new Dictionary<string, Func<object, object>>();

        [SerializeField]
        public List<DataRow> Rows = new List<DataRow>();

        [SerializeField]
        internal GameObject Content;

        [SerializeField]
        public ColumnStyle[] ColumnStyles;

        public TDataTable DataSource {
            get; protected set;
        }

        [SerializeField]
        private bool isPaging = false;

        public bool IsPaging {
            get {
                return isPaging;
            }
            set {
                if (isPaging != value) {
                    RequestUpdate();
                    isPaging = value;
                }
            }
        }

        private int pageSize = 10;

        public int PageSize {
            get {
                return pageSize;
            }
            set {
                if (pageSize != value) {
                    RequestUpdate();
                    pageSize = value;
                }
            }
        }

        private int pageIndex = 0;

        public int PageIndex {
            get {
                return pageIndex;
            }
            set {
                if (pageIndex != value) {
                    RequestUpdate();
                    pageIndex = value;
                }
            }
        }

        public bool HasNextPage {
            get {
                return PageIndex < Mathf.FloorToInt((Rows.Count - 1) / pageSize);
            }
        }

        public bool HasPreviousPage {
            get {
                return PageIndex > 0;
            }
        }

        public void NextPage() {
            if (HasNextPage) {
                PageIndex++;
            }
        }

        public void PreviousPage() {
            if (HasPreviousPage) {
                PageIndex--;
            }
        }

        public DataGridView() {
            DataSource = new TDataTable();
            DataSource.DataChangedEvent += (s, e) => refreshTable();
            DataSource.RowChangedEvent += (s, e) => refreshTable();
        }

        /// <summary>取得單元大小</summary>
        /// <param name="columnIndex">欄位索引</param>
        /// <returns>返回單元大小</returns>
        public Vector2 GetCellSize(int columnIndex) {
            if (columnIndex < Header.Cells.Count) {
                return (Header.Cells[columnIndex].transform as RectTransform).sizeDelta;
            } else {
                return Vector2.zero;
            }
        }

        /// <summary>取得欄位的設定值</summary>
        /// <param name="colName">欄位ID</param>
        /// <returns>欄位設定資料</returns>
        public ColumnStyle GetColumnStyle(string colName) {
            IEnumerable<ColumnStyle> find = ColumnStyles.Where(s => s.ColumnName == colName);
            if (find.Count() > 0) {
                return find.ElementAt(0);
            } else {
                return default(ColumnStyle);
            }
        }

        /// <summary>讀取資料</summary>
        /// <param name="data">資料</param>
        public void LoadData(TDataTable data) {
            this.DataSource = data;
            PageIndex = TMath.Range(PageIndex, 0, Mathf.FloorToInt((DataSource.Rows.Count - 1) / PageSize));
            refresh();
        }

        private void Update() {
            if (Header.isActiveAndEnabled != ShowHeader) {
                Header.gameObject.SetActive(ShowHeader);
                refreshHeader();
            } else if (ShowHeader && Header.Cells.Count != ColumnStyles.Length) {
                refreshHeader();
            }
            if (needUpdate) {
                refresh();
                needUpdate = false;
            }
        }

        public void RequestUpdate() {
            needUpdate = true;
        }

        private void refresh() {
            refreshHeader();
            refreshTable();
            refreshPage();
        }

        private void refreshHeader() {
            Header.Outlined = Outlined;
            Header.SetValues(ColumnStyles.Select(s => s.DisplayText).ToArray());
        }

        private void refreshTable() {
            for (int i = 0; i < DataSource.Rows.Count; i++) {
                TDataTable.DataRow dr = DataSource.Rows[i];
                DataRow row;
                if (i < Rows.Count) {
                    row = Rows[i];
                } else {
                    GameObject go = GameObjectRelate.InstantiateGameObject(Content, Prefab_DataRow);
                    row = go.GetComponent<DataRow>();
                    Rows.Add(row);
                    row.parent = this;
                }
                row.Outlined = Outlined;
                row.SetIndex(i);
                row.gameObject.SetActive(true);
            }
            for (int i = DataSource.Rows.Count; i < Rows.Count; i++) {
                Rows[i].gameObject.SetActive(false);
            }
        }

        private void refreshPage() {
            if (IsPaging) {
                int startIndex = PageIndex * PageSize;
                int endIndex = TMath.Range(startIndex + PageSize, 0, Rows.Count);
                for (int i = 0; i < Rows.Count; i++) {
                    Rows[i].gameObject.SetActive(i >= startIndex && i < endIndex);
                }
            } else {
                for (int i = 0; i < Rows.Count; i++) {
                    Rows[i].gameObject.SetActive(true);
                }
            }
        }
    }

#if UNITY_EDITOR

    [UnityEditor.CustomEditor(typeof(DataGridView))]
    public class DataGridViewEditor : UnityEditor.Editor {
        private float offsetX = .405f;

        public override void OnInspectorGUI() {
            offsetX = UnityEditor.EditorGUIUtility.labelWidth - 4;
            DrawDefaultInspector();
            DataGridView dgv = target as DataGridView;
            GUILayout.ExpandWidth(true);
            GUILayout.BeginHorizontal();

            GUILayout.Label("Use Page", GUILayout.Width(offsetX));
            dgv.IsPaging = UnityEditor.EditorGUILayout.Toggle(dgv.IsPaging);
            GUILayout.EndHorizontal();
            if (dgv.IsPaging) {
                DrawPagingEditor();
            }
        }

        private void DrawPagingEditor() {
            DataGridView dgv = target as DataGridView;
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Page Index", GUILayout.Width(offsetX));
            dgv.PageIndex = UnityEditor.EditorGUILayout.IntField(dgv.PageIndex);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Page Size", GUILayout.Width(offsetX));
            dgv.PageSize = UnityEditor.EditorGUILayout.IntField(dgv.PageSize);
            GUILayout.EndHorizontal();
        }

        [UnityEditor.MenuItem("GameObject/UI/DataTable/DataGridView", false, priority = 29)]
        private static void InstantiateDataGridView() {
            GameObject go = UnityEditor.Selection.activeGameObject;
            GameObject obj = UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Plugins/GameLibrary/UI/DataGridView/DataGridView.prefab");
            GameObject instance = GameObjectRelate.InstantiateGameObject(go, obj);
            instance.name = "DataGridView";
        }

        [UnityEditor.MenuItem("GameObject/UI/DataTable/DataRow", false)]
        private static void AddRow() {
            GameObject go = UnityEditor.Selection.activeGameObject;
            GameObject obj = UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Plugins/GameLibrary/UI/DataGridView/DataRow.prefab");
            DataGridView dgv = go.GetComponent<DataGridView>();

            GameObject instance = GameObjectRelate.InstantiateGameObject(dgv.Content, obj);
            instance.name = "DataRow";
        }

        [UnityEditor.MenuItem("GameObject/UI/DataTable/DataRow", validate = true)]
        private static bool canAddRow() {
            GameObject go = UnityEditor.Selection.activeGameObject;
            return go && go.GetComponent<DataGridView>();
        }

        [UnityEditor.MenuItem("GameObject/UI/DataTable/DataCell", validate = false)]
        private static void AddCell() {
            GameObject go = UnityEditor.Selection.activeGameObject;
            GameObject obj = UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Plugins/GameLibrary/UI/DataGridView/DataCell.prefab");
            if (go.GetComponent<DataRow>()) {
                GameObject instance = GameObjectRelate.InstantiateGameObject(go, obj);
                instance.name = "DataCell";
            }
        }

        [UnityEditor.MenuItem("GameObject/UI/DataTable/DataCell", validate = true)]
        private static bool canAddCell() {
            GameObject go = UnityEditor.Selection.activeGameObject;
            return go && go.GetComponent<DataRow>();
        }
    }

#endif
}