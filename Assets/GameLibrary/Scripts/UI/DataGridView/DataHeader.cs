﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameLibrary.UI {

    [ExecuteInEditMode]
    public class DataHeader : MonoBehaviour {

        [SerializeField]
        private Text Text;

        [SerializeField]
        private Image Background;

        [SerializeField]
        private Outline Outline;

        public bool ShowBorder = true;
        public int ColumnIndex;

        private void Update() {
            if (Outline.isActiveAndEnabled != ShowBorder) {
                Outline.gameObject.SetActive(ShowBorder);
            }
        }
    }
}