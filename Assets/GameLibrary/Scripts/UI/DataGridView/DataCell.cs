﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.UI;

namespace GameLibrary.UI {

    [ExecuteInEditMode]
    public class DataCell : MonoBehaviour {

        [SerializeField]
        private Text Text;

        [SerializeField]
        private Image Background;

        [SerializeField]
        private Outline Outline;

        public bool Outlined;
        public bool IsHeader;
        public int RowIndex;
        public int ColumnIndex;
        private DataGridView.ColumnStyle Style;

        private void Start() {
            if (Text && Text.font == null) {
                Text.font = DefaultResManager.DefaultFont;
            }
        }

        private void Update() {
            if (Outline.isActiveAndEnabled != Outlined) {
                Outline.enabled = Outlined;
            }
        }

        public void SetValue(object val, DataGridView.ColumnStyle style) {
            SetStyle(style);
            SetValue(val);
        }

        public void SetValue(object val) {
            if (!IsHeader && !string.IsNullOrEmpty(Style.Format)) {
                string text;
                text = TString.Format(Style.Format, val);

                if (Style.TextColor != null && Style.TextColor.Length > 0) {
                    for (int i = 0; i < Style.TextColor.Length; i++) {
                        text = text.Replace("##" + i, "#" + ColorUtility.ToHtmlStringRGB(Style.TextColor[i]));
                    }
                }
                text = text.Replace("\\n", "\n");
                Text.text = text;
            } else {
                Text.text = val.ToString();
            }
        }

        public void SetStyle(DataGridView.ColumnStyle style) {
            Style = style;
            Background.color = style.BackGroundColor;
            if (IsHeader) {
                return;
            }
            Text.alignment = style.TextAnchor;
            if (style.FontSize != 0) {
                Text.fontSize = style.FontSize;
            }
        }
    }
}