﻿using System.Collections.Generic;
using GameLibrary.Utility;
using UnityEngine;

namespace GameLibrary.UI {

    [ExecuteInEditMode]
    public class DataRow : MonoBehaviour {
        public int Index;
        public DataGridView parent;
        public bool IsHeader = false;

        [SerializeField]
        public List<DataCell> Cells = new List<DataCell>();

        public bool Outlined;
        private bool dataBinded = false;
        private bool needUpdate = true;

        private void Update() {
            if (needUpdate && dataBinded) {
                refresh();
                needUpdate = false;
            }
        }

        public void SetIndex(int index) {
            Index = index;
            dataBinded = true;
            refresh();
        }

        protected void refresh() {
            List<string> col = parent.Columns;
            int columnShown = parent.ColumnStyles.Length;

            for (int i = 0; i < columnShown; i++) {
                DataCell cell;
                if (i < Cells.Count) {
                    //use instance
                    cell = Cells[i];
                } else {
                    //create instance
                    GameObject go = GameObjectRelate.InstantiateGameObject(gameObject, parent.Prefab_DataCell);
                    cell = go.GetComponent<DataCell>();
                    Cells.Add(cell);
                }

                cell.Outlined = Outlined;
                cell.RowIndex = Index;
                cell.ColumnIndex = i;

                object value = "";
                string colName = "";
                if (i < col.Count) {
                    colName = col[i];
                    value = parent.DataSource.GetData(Index, colName, false);
                    if (value == null) {
                        value = "";
                    }
                }

                if (parent.Parsers.ContainsKey(colName)) {
                    cell.SetValue(parent.Parsers[colName](value), parent.GetColumnStyle(colName));
                } else {
                    cell.SetValue(value, parent.GetColumnStyle(colName));
                }

                (cell.transform as RectTransform).sizeDelta = parent.GetCellSize(i);
                Cells[i].gameObject.SetActive(true);
            }
            for (int i = columnShown; i < Cells.Count; i++) {
                Cells[i].gameObject.SetActive(false);
            }
        }

        public void SetValues(params object[] values) {
            List<string> col = parent.Columns;
            int columnShown = parent.ColumnStyles.Length;
            for (int i = 0; i < values.Length; i++) {
                object value;
                if (i < values.Length) {
                    value = values[i];
                } else {
                    value = "";
                }
                DataCell cell;
                if (i < Cells.Count) {
                    //use instance
                    cell = Cells[i];
                } else {
                    //create instance
                    GameObject go = GameObjectRelate.InstantiateGameObject(gameObject, parent.Prefab_DataCell);
                    cell = go.GetComponent<DataCell>();
                    Cells.Add(cell);
                }

                cell.IsHeader = IsHeader;
                cell.Outlined = Outlined;
                cell.RowIndex = Index;
                cell.ColumnIndex = i;

                if (i < parent.ColumnStyles.Length) {
                    cell.SetValue(value, parent.ColumnStyles[i]);
                }

                (cell.transform as RectTransform).sizeDelta = parent.GetCellSize(i);
                Cells[i].gameObject.SetActive(true);
            }
            for (int i = columnShown; i < Cells.Count; i++) {
                Cells[i].gameObject.SetActive(false);
            }
        }
    }
}