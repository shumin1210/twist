﻿using System;
using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.Events;

namespace GameLibrary.UI {

    public class CountdownTimer : MonoBehaviour {

        [Serializable]
        public class ProgressEvent : UnityEvent<float> {
        }

        private CoroutineHandle thread;
        private bool enabled;

        private bool Enabled {
            get {
                return enabled;
            }
            set {
                if (thread != null) {
                    if (value) {
                        Timing.ResumeCoroutines(thread);
                    } else {
                        Timing.PauseCoroutines(thread);
                    }
                }
                enabled = value;
            }
        }

        [SerializeField]
        private bool Auto = true;

        [SerializeField]
        private float countdown = 1f;

        public float Countdown {
            get {
                return countdown;
            }
            set {
                countdown = value;
            }
        }

        private DateTime start;

        [SerializeField]
        public ProgressEvent onProgress = new ProgressEvent();

        [SerializeField]
        public UnityEvent OnComplete = new UnityEvent();

        public void Start() {
            thread = Timing.RunCoroutine(update().CancelWith(gameObject));
        }

        public void Run() {
            Enabled = true;
        }

        public void OnEnable() {
            if (Auto) {
                start = DateTime.Now;
                Enabled = true;
            }
        }

        public void OnDisable() {
            Enabled = false;
        }

        private IEnumerator<float> update() {
            while (true) {
                if (enabled) {
                    if (countdown == 0) {
                        complete();
                    } else {
                        countdown -= Timing.DeltaTime;
                        if (countdown <= 0) {
                            complete();
                        } else {
                            onProgress.Invoke(countdown);
                        }
                    }
                }
                yield return Timing.WaitForOneFrame;
            }
        }

        private void complete() {
            countdown = 0;
            Enabled = false;
            OnComplete.Invoke();
        }
    }
}