﻿using System;
using DigitalRuby.Tween;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameLibrary.UI {

    [ExecuteInEditMode]
    public class Slidable : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {

        public enum Easing {
            Linear,
            Bouncing
        }

        public enum State {
            None,
            Drag,
            AnimBack,
            AnimOut,
            Out
        }

        public enum Direction {
            ToRight,
            ToLeft,
            ToTop,
            ToBottom
        }

        private RectTransform origin {
            get {
                return FrameObject.GetComponent<RectTransform>();
            }
        }

        public GameObject FrameObject;

        [SerializeField]
        private State state;

        public State SlideState {
            get {
                return state;
            }
            set {
                state = value;
                gameObject.SetActive(state != State.Out);
            }
        }

        public Easing EasingType;
        public Direction direction;

        public float passedTime = 0f;
        private Vector2 startPos;
        public bool isDrag = false;
        public bool hideAfterOut = false;
        public UnityEvent OnSlideOut;

        public float Duration = 1f;
        public float MaxSlide = .5f;

        public UnityEvent<PointerEventData> clickEvent;

        [Range(0.01f, 1)]
        public float DragSensitive;

        public float Progress {
            get {
                if (state == State.AnimBack) {
                    return easeIn(passedTime / Duration);
                } else if (state == State.AnimOut) {
                    return easeOut(passedTime / Duration);
                } else {
                    return passedTime / Duration;
                }
            }
        }

        private Func<float, float> easeIn {
            get {
                switch (EasingType) {
                    default:
                    case Easing.Linear:
                        return TweenScaleFunctions.Linear;

                    case Easing.Bouncing:
                        return bouncein;
                }
            }
        }

        private Func<float, float> easeOut {
            get {
                switch (EasingType) {
                    default:
                    case Easing.Linear:
                        return TweenScaleFunctions.Linear;

                    case Easing.Bouncing:
                        return bounce;
                }
            }
        }

        private float bouncein(float t) {
            return 1 - bounce(Duration - t);
        }

        private float bounce(float t) {
            t *= 1000;
            float c = 1, d = Duration * 1000;
            if ((t /= d) < (1 / 2.75)) {
                return c * (7.5625f * t * t);
            } else if (t < (2 / 2.75)) {
                return c * (7.5625f * (t -= (1.5f / 2.75f)) * t + .75f);
            } else if (t < (2.5f / 2.75f)) {
                return c * (7.5625f * (t -= (2.25f / 2.75f)) * t + .9375f);
            } else {
                return c * (7.5625f * (t -= (2.625f / 2.75f)) * t + .984375f);
            }
        }

        public void SlideIn() {
            if (SlideState != State.Drag) {
                SlideState = State.AnimBack;
            }
        }

        public void SlideOut() {
            if (SlideState != State.Drag) {
                SlideState = State.AnimOut;
            }
        }

        public void OnPointerDown(PointerEventData eventData) {
            if (!isDrag && SlideState < State.Drag) {
                startPos = Input.mousePosition;
                isDrag = true;
                SlideState = State.Drag;
            }
        }

        public void OnDrag(PointerEventData eventData) {
            SlideState = State.Drag;
            //(startPos - (Vector2)Input.mousePosition).sqrMagnitude;
            switch (direction) {
                case Direction.ToRight:
                case Direction.ToLeft:
                    passedTime = (Input.mousePosition.x - startPos.x) / origin.rect.width * Duration;
                    break;

                case Direction.ToTop:
                case Direction.ToBottom:
                    passedTime = (Input.mousePosition.y - startPos.y) / origin.rect.height * Duration;
                    break;
            }
        }

        public void OnPointerUp(PointerEventData eventData) {
            startPos = Input.mousePosition;
            isDrag = false;
            startSlideAnim();
        }

        private void startSlideAnim() {
            if (Progress > MaxSlide) {
                OnSlideOut.Invoke();
                SlideState = State.AnimOut;
            } else {
                SlideState = State.AnimBack;
            }
        }

        private void Update() {
            //if (Input.GetKeyDown(KeyCode.Alpha1)) {
            //    SlideIn();
            //} else if (Input.GetKeyDown(KeyCode.Alpha2)) {
            //    SlideOut();
            //}

            updateProgress();
            updatePosition();
        }

        private void updateProgress() {
            switch (state) {
                case State.None:
                    passedTime = 0;
                    break;

                case State.Out:
                    passedTime = Duration;
                    gameObject.SetActive(!hideAfterOut);
                    break;

                default:
                    if (isDrag) {
                        state = State.Drag;
                        if (Progress > 1) {
                            passedTime = Duration;
                        } else if (Progress < 0) {
                            passedTime = 0;
                        }
                    } else {
                        if (state == State.AnimOut) {
                            passedTime += Time.deltaTime;
                            if (Progress > 1) {
                                passedTime = Duration;
                                state = State.Out;
                            }
                        } else {
                            passedTime -= Time.deltaTime;
                            if (Progress < 0) {
                                passedTime = 0;
                                state = State.None;
                            }
                        }
                    }
                    break;
            }
        }

        private void updatePosition() {
            if (!FrameObject) {
                return;
            }
            RectTransform rect = transform as RectTransform;
            rect.anchorMax = new Vector2(.5f, .5f);
            rect.anchorMin = new Vector2(.5f, .5f);
            rect.sizeDelta = origin.rect.size;

            if (Progress == 0) {
                rect.pivot = new Vector2(.5f, .5f);
            } else {
                switch (direction) {
                    case Direction.ToRight:
                        rect.pivot = new Vector2(.5f - Progress, .5f);
                        break;

                    case Direction.ToLeft:
                        rect.pivot = new Vector2(.5f + Progress, .5f);
                        break;

                    case Direction.ToTop:
                        rect.pivot = new Vector2(.5f, .5f - Progress);
                        break;

                    case Direction.ToBottom:
                        rect.pivot = new Vector2(.5f, .5f + Progress);
                        break;
                }
            }
        }
    }
}