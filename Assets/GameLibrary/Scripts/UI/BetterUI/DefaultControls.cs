﻿using GameLibrary.Utility;

#if UNITY_EDITOR

using UnityEditor;

#endif

using UnityEngine;
using UnityEngine.UI;

namespace GameLibrary.UI {

    public static class DefaultControls {

        public struct Resources {
            public Sprite standard;
            public Sprite background;
            public Sprite inputField;
            public Sprite knob;
            public Sprite checkmark;
            public Sprite dropdown;
            public Sprite mask;
        }

        private const string kStandardSpritePath = "UI/Skin/UISprite.psd";
        private const string kBackgroundSpritePath = "UI/Skin/Background.psd";
        private const string kInputFieldBackgroundPath = "UI/Skin/InputFieldBackground.psd";
        private const string kKnobPath = "UI/Skin/Knob.psd";
        private const string kCheckmarkPath = "UI/Skin/Checkmark.psd";
        private const string kDropdownArrowPath = "UI/Skin/DropdownArrow.psd";
        private const string kMaskPath = "UI/Skin/UIMask.psd";

        public const float WIDTH = 160f;
        public const float THICK_HEIGHT = 30f;
        public const float THIN_HEIGHT = 20f;
        public static Vector2 ThickElementSize = new Vector2(WIDTH, THICK_HEIGHT);
        public static Vector2 ThinElementSize = new Vector2(WIDTH, THIN_HEIGHT);
        public static Vector2 ImageElementSize = new Vector2(100f, 100f);
        public static Color DefaultSelectableColor = new Color(1f, 1f, 1f, 1f);
        public static Color PanelColor = new Color(1f, 1f, 1f, 0.392f);
        public static Color TextColor = new Color(50f / 255f, 50f / 255f, 50f / 255f, 1f);

        public static Resources Resource = GetStandardResources();

        private static Resources GetStandardResources() {
#if UNITY_EDITOR
            return new Resources() {
                standard = AssetDatabase.GetBuiltinExtraResource<Sprite>(kStandardSpritePath),
                background = AssetDatabase.GetBuiltinExtraResource<Sprite>(kBackgroundSpritePath),
                inputField = AssetDatabase.GetBuiltinExtraResource<Sprite>(kInputFieldBackgroundPath),
                knob = AssetDatabase.GetBuiltinExtraResource<Sprite>(kKnobPath),
                checkmark = AssetDatabase.GetBuiltinExtraResource<Sprite>(kCheckmarkPath),
                dropdown = AssetDatabase.GetBuiltinExtraResource<Sprite>(kDropdownArrowPath),
                mask = AssetDatabase.GetBuiltinExtraResource<Sprite>(kMaskPath)
            };
#else
            return new Resources();
#endif
        }

        public static GameObject CreateUIElementRoot(string name, Vector2 size) {
            GameObject child = new GameObject(name);
            RectTransform rectTransform = child.AddComponent<RectTransform>();
            rectTransform.sizeDelta = size;
            return child;
        }

        public static GameObject CreateUIObject(string name, GameObject parent) {
            GameObject go = new GameObject(name);
            go.AddComponent<RectTransform>();
            SetParentAndAlign(go, parent);
            return go;
        }

        public static void SetParentAndAlign(GameObject child, GameObject parent) {
            if (parent == null)
                return;

            child.transform.SetParent(parent.transform, false);
            SetLayerRecursively(child, parent.layer);
        }

        public static void SetLayerRecursively(GameObject go, int layer) {
            go.layer = layer;
            Transform t = go.transform;
            for (int i = 0; i < t.childCount; i++)
                SetLayerRecursively(t.GetChild(i).gameObject, layer);
        }

        public static void SetDefaultColorTransitionValues(Selectable slider) {
            ColorBlock colors = slider.colors;
            colors.highlightedColor = new Color(0.882f, 0.882f, 0.882f);
            colors.pressedColor = new Color(0.698f, 0.698f, 0.698f);
            colors.disabledColor = new Color(0.521f, 0.521f, 0.521f);
        }

        public static void SetDefaultTextValues(Text lbl) {
            // Set text values we want across UI elements in default controls.
            // Don't set values which are the same as the default values for the Text component,
            // since there's no point in that, and it's good to keep them as consistent as possible.
            lbl.color = TextColor;

            // Reset() is not called when playing. We still want the default font to be assigned

            //lbl.AssignDefaultFont();
            ReflectionUtility.GetMember(typeof(Text), "AssignDefaultFont").TryInvoke(lbl);
        }

        public static GameObject CreateScrollbar(Resources resources) {
            // Create GOs Hierarchy
            GameObject scrollbarRoot = CreateUIElementRoot("Scrollbar", ThinElementSize);

            GameObject sliderArea = CreateUIObject("Sliding Area", scrollbarRoot);
            GameObject handle = CreateUIObject("Handle", sliderArea);

            Image bgImage = scrollbarRoot.AddComponent<Image>();
            bgImage.sprite = resources.background;
            bgImage.type = Image.Type.Sliced;
            bgImage.color = DefaultSelectableColor;

            Image handleImage = handle.AddComponent<Image>();
            handleImage.sprite = resources.standard;
            handleImage.type = Image.Type.Sliced;
            handleImage.color = DefaultSelectableColor;

            RectTransform sliderAreaRect = sliderArea.GetComponent<RectTransform>();
            sliderAreaRect.sizeDelta = new Vector2(-20, -20);
            sliderAreaRect.anchorMin = Vector2.zero;
            sliderAreaRect.anchorMax = Vector2.one;

            RectTransform handleRect = handle.GetComponent<RectTransform>();
            handleRect.sizeDelta = new Vector2(20, 20);

            Scrollbar scrollbar = scrollbarRoot.AddComponent<Scrollbar>();
            scrollbar.handleRect = handleRect;
            scrollbar.targetGraphic = handleImage;
            SetDefaultColorTransitionValues(scrollbar);

            return scrollbarRoot;
        }
    }
}