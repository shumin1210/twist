﻿using System;
using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace GameLibrary.UI {

    /// <summary>
    /// Simple toggle -- something that has an 'on' and 'off' states: checkbox, toggle button, radio button, etc.
    /// </summary>
    [AddComponentMenu("UI/UIAdv/Toggle Adv", 32)]
    [RequireComponent(typeof(RectTransform))]
    [ExecuteInEditMode]
    public class ToggleAdv : Selectable, IPointerClickHandler, ISubmitHandler, ICanvasElement {

        public enum ToggleTransition {
            None,
            Fade,
            Active
        }

        [Serializable]
        public class ToggleEvent : UnityEvent<bool> {
        }

        [SerializeField]
        private string m_LabelText;

        public string LabelText {
            get {
                return m_LabelText;
            }
            set {
                m_LabelText = value;
                if (m_Label) {
                    m_Label.text = value;
                }
            }
        }

        [Tooltip("Label text")]
        [SerializeField]
        private Text m_Label;

        // Whether the toggle is on
        [FormerlySerializedAs("m_IsActive")]
        [Tooltip("Is the toggle currently on or off?")]
        [SerializeField]
        private bool m_IsOn = false;

        /// <summary>
        /// Transition type.
        /// </summary>
        public ToggleTransition toggleTransition = ToggleTransition.Fade;

        /// <summary>
        /// Graphic the toggle should be working with.
        /// </summary>
        public Graphic graphic;

        // group that this toggle can belong to
        [SerializeField]
        private ToggleGroupAdv m_Group;

        public ToggleGroupAdv group {
            get {
                return m_Group;
            }
            set {
                m_Group = value;
#if UNITY_EDITOR
                if (Application.isPlaying)
#endif
                {
                    SetToggleGroupAdv(m_Group, true);
                    PlayEffect();
                }
            }
        }

        /// <summary>
        /// Allow for delegate-based subscriptions for faster events than 'eventReceiver', and allowing for multiple receivers.
        /// </summary>
        public ToggleEvent onValueChanged = new ToggleEvent();

        public UnityEvent onActived = new UnityEvent();
        public UnityEvent onDeactived = new UnityEvent();

        protected ToggleAdv() {
        }

#if UNITY_EDITOR

        protected override void Reset() {
            base.Reset();
            targetGraphic = gameObject.GetComponentOfChild<Graphic>("Background");
            graphic = gameObject.GetComponentOfChild<Graphic>("Checkmark");
            m_Label = gameObject.GetComponentOfChild<Text>("Label");
            LabelText = m_Label ? m_Label.text : "";
            group = gameObject.GetComponentInParent<ToggleGroupAdv>();
        }

        protected override void OnValidate() {
            base.OnValidate();

            var prefabType = UnityEditor.PrefabUtility.GetPrefabType(this);
            if (prefabType != UnityEditor.PrefabType.Prefab && !Application.isPlaying)
                CanvasUpdateRegistry.RegisterCanvasElementForLayoutRebuild(this);
        }

#endif // if UNITY_EDITOR

        public virtual void Rebuild(CanvasUpdate executing) {
#if UNITY_EDITOR
            if (executing == CanvasUpdate.Prelayout) {
                onValueChanged.Invoke(m_IsOn);
                if (isOn) {
                    onActived.Invoke();
                } else {
                    onDeactived.Invoke();
                }
            }
#endif
        }

        public virtual void LayoutComplete() {
        }

        public virtual void GraphicUpdateComplete() {
        }

        protected override void OnEnable() {
            base.OnEnable();
            SetToggleGroupAdv(m_Group, false);
            PlayEffect();
        }

        protected override void OnDisable() {
            SetToggleGroupAdv(null, false);
            base.OnDisable();
        }

        protected override void OnDestroy() {
            if (m_Group) {
                m_Group.Disreference(this);
            }
        }

        protected override void OnDidApplyAnimationProperties() {
            // Check if isOn has been changed by the animation.
            // Unfortunately there is no way to check if we don�t have a graphic.
            if (graphic != null) {
                bool oldValue = !Mathf.Approximately(graphic.canvasRenderer.GetColor().a, 0);
                if (m_IsOn != oldValue) {
                    m_IsOn = oldValue;
                    Set(!oldValue);
                }
            }

            base.OnDidApplyAnimationProperties();
        }

        private void SetToggleGroupAdv(ToggleGroupAdv newGroup, bool setMemberValue) {
            ToggleGroupAdv oldGroup = m_Group;

            // Sometimes IsActive returns false in OnDisable so don't check for it.
            // Rather remove the toggle too often than too little.
            if (m_Group != null)
                m_Group.UnregisterToggle(this);

            // At runtime the group variable should be set but not when calling this method from OnEnable or OnDisable.
            // That's why we use the setMemberValue parameter.
            if (setMemberValue)
                m_Group = newGroup;

            // Only register to the new group if this Toggle is active.
            if (newGroup != null && IsActive())
                newGroup.RegisterToggle(this);

            // If we are in a new group, and this toggle is on, notify group.
            // Note: Don't refer to m_Group here as it's not guaranteed to have been set.
            if (newGroup != null && newGroup != oldGroup && isOn && IsActive())
                newGroup.NotifyToggleOn(this);
        }

        /// <summary>
        /// Whether the toggle is currently active.
        /// </summary>
        public bool isOn {
            get {
                return m_IsOn;
            }
            set {
                Set(value);
            }
        }

        public void SilentSet(bool value) {
            m_IsOn = value;
            PlayEffect();
        }

        internal void Set(bool value) {
            Set(value, true);
        }

        internal void Set(bool value, bool sendCallback) {
            if (m_IsOn == value) {
                return;
            }

            // if we are in a group and set to true, do group logic
            m_IsOn = value;
            if (m_Group != null && IsActive()) {
                if (m_IsOn || (!m_Group.AnyTogglesOn() && !m_Group.allowSwitchOff)) {
                    m_IsOn = true;
                    m_Group.NotifyToggleOn(this);
                }
            }

            // Always send event when toggle is clicked, even if value didn't change
            // due to already active toggle in a toggle group being clicked.
            // Controls like Dropdown rely on this.
            // It's up to the user to ignore a selection being set to the same value it already was, if desired.
            PlayEffect(toggleTransition);
            if (sendCallback) {
                onValueChanged.Invoke(m_IsOn);
                if (isOn) {
                    onActived.Invoke();
                } else {
                    onDeactived.Invoke();
                }
            }
        }

        public void PlayEffect() {
            PlayEffect(toggleTransition);
        }

        /// <summary>
        /// Play the appropriate effect.
        /// </summary>
        public void PlayEffect(ToggleTransition mode) {
            if (graphic == null) {
                return;
            }

#if UNITY_EDITOR
            if (!Application.isPlaying) {
                switch (mode) {
                    case ToggleTransition.Fade:
                    case ToggleTransition.None:
                        graphic.canvasRenderer.SetAlpha(m_IsOn ? 1f : 0f);
                        break;

                    case ToggleTransition.Active:
                        graphic.SetActive(m_IsOn);
                        break;
                }
            } else
#endif
                switch (mode) {
                    case ToggleTransition.None:
                        graphic.CrossFadeAlpha(m_IsOn ? 1f : 0f, 0f, true);
                        break;

                    case ToggleTransition.Fade:
                        graphic.CrossFadeAlpha(m_IsOn ? 1f : 0f, .1f, true);
                        break;

                    case ToggleTransition.Active:
                        graphic.SetActive(m_IsOn);
                        break;
                }
        }

        /// <summary>
        /// Assume the correct visual state.
        /// </summary>
        protected override void Start() {
            base.Start();
            PlayEffect();
        }

        private void InternalToggle() {
            if (!IsActive() || !IsInteractable())
                return;

            isOn = !isOn;
        }

        /// <summary>
        /// React to clicks.
        /// </summary>
        public virtual void OnPointerClick(PointerEventData eventData) {
            if (eventData.button != PointerEventData.InputButton.Left)
                return;

            InternalToggle();
        }

        public virtual void OnSubmit(BaseEventData eventData) {
            InternalToggle();
        }

#if UNITY_EDITOR

#endif
    }
}