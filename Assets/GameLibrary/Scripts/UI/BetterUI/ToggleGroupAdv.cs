﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace GameLibrary.UI {

    [AddComponentMenu("UI/UIAdv/Toggle Group Adv", 32)]
    [DisallowMultipleComponent]
    [ExecuteInEditMode]
    public class ToggleGroupAdv : UIBehaviour {
        [SerializeField] private bool m_AllowSwitchOff = false;

        public bool allowSwitchOff {
            get {
                return m_AllowSwitchOff;
            }
            set {
                m_AllowSwitchOff = value;
            }
        }

        private List<ToggleAdv> m_Toggles;

        public ReadOnlyCollection<ToggleAdv> Toggles {
            get {
                return m_Toggles.AsReadOnly();
            }
        }

        private HashSet<ToggleAdv> refToggles = new HashSet<ToggleAdv>();

        public ReadOnlyCollection<ToggleAdv> ReferecnedToggles {
            get {
                return refToggles.ToList().AsReadOnly();
            }
        }

        [Serializable]
        public class ToggleChangeEvent : UnityEvent<ToggleAdv> {
        }

        [Tooltip("Triggered when any toggle is turned ON.")]
        public ToggleChangeEvent OnValueChanged;

        protected ToggleGroupAdv() {
            m_Toggles = new List<ToggleAdv>();
        }

        private void ValidateToggleIsInGroup(ToggleAdv toggle) {
            if (toggle == null || !m_Toggles.Contains(toggle)) {
                throw new ArgumentException(string.Format("Toggle {0} is not part of ToggleGroup {1}", new object[] { toggle, this }));
            }
        }

        public void NotifyToggleOn(ToggleAdv toggle) {
            ValidateToggleIsInGroup(toggle);
            bool hasChange = false;
            // disable all toggles in the group
            for (int i = 0; i < m_Toggles.Count; i++) {
                if (m_Toggles[i] == toggle) {
                    m_Toggles[i].SilentSet(true);
                    hasChange = true;
                    continue;
                }
                m_Toggles[i].isOn = false;
            }

            if (hasChange) {
                OnValueChanged.Invoke(m_Toggles.FirstOrDefault(f => f.isOn));
            }
        }

        public void UnregisterToggle(ToggleAdv toggle) {
            if (m_Toggles.Contains(toggle)) {
                m_Toggles.Remove(toggle);
            }
        }

        public void RegisterToggle(ToggleAdv toggle) {
            if (!m_Toggles.Contains(toggle)) {
                m_Toggles.Add(toggle);
            }
            refToggles.Add(toggle);
            refreshToggles();
        }

        private void refreshToggles() {
            //if (m_Toggles.Count > 0 && !m_Toggles.Exists(a => a.isOn)) {
            //    m_Toggles.Foreach((m, i) => m.isOn = i == 0);
            //}
        }

        public void Disreference(ToggleAdv toggle) {
            refToggles.Remove(toggle);
        }

        public bool AnyTogglesOn() {
            return m_Toggles.Find(x => x.isOn) != null;
        }

        public IEnumerable<ToggleAdv> ActiveToggles() {
            return m_Toggles.Where(x => x.isOn);
        }

        public void SetAllTogglesOff() {
            bool oldAllowSwitchOff = m_AllowSwitchOff;
            m_AllowSwitchOff = true;

            for (int i = 0; i < m_Toggles.Count; i++) {
                m_Toggles[i].isOn = false;
            }

            m_AllowSwitchOff = oldAllowSwitchOff;
        }
    }
}